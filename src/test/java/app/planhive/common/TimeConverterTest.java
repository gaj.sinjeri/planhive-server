package app.planhive.common;

import static org.assertj.core.api.Assertions.assertThat;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

public class TimeConverterTest {

  private static final Long MILLIS = 705492000000L;
  private static final Long MILLIS_2 = 946681200000L;
  private static final Instant TS = new Instant(MILLIS);
  private static final Instant TS_2 = new Instant(MILLIS_2);

  @Test
  public void instantToSecondsConvertsCorrectly() {
    var res = TimeConverter.instantToSeconds(TS);

    assertThat(res).isEqualTo(MILLIS / 1000);
  }

  @Test
  public void secondsToInstantConvertsCorrectly() {
    var res = TimeConverter.secondsToInstant(MILLIS / 1000);

    assertThat(res).isEqualTo(TS);
  }

  @Test
  public void millisToInstantConvertsCorrectly() {
    var res = TimeConverter.millisToInstant(MILLIS);

    assertThat(res).isEqualTo(TS);
  }

  @Test
  public void instantListToMillisListConvertsCorrectly() {
    var res = TimeConverter.instantListToMillisList(List.of(TS, TS_2));

    assertThat(res).containsOnly(MILLIS, MILLIS_2);
  }

  @Test
  public void bigDecimalListToInstantListConvertsCorrectly() {
    var res = TimeConverter
        .bigDecimalListToInstantList(List.of(new BigDecimal(MILLIS), new BigDecimal(MILLIS_2)));

    assertThat(res).containsOnly(TS, TS_2);
  }
}
