package app.planhive.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

@TestInstance(Lifecycle.PER_CLASS)
public class RootControllerTest {

  private RootController rootController;

  @BeforeEach
  public void setUp() {
    rootController = new RootController();
  }

  @Test
  public void root() {
    var response = rootController.root();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("Hollo World");
  }
}
