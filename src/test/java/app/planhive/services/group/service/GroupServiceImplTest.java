package app.planhive.services.group.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.UnauthorisedException;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node.Builder;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@TestInstance(Lifecycle.PER_CLASS)
public class GroupServiceImplTest {

  private static final String NODE_ID = "5555";
  private static final String CHILD_NODE_ID = "7777";
  private static final String ID_1 = "2345";
  private static final String ID_2 = "3456";
  private static final String ID_3 = "4567";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String PERMISSION_GROUP_ID_2 = "adf34f42-ffasdas";
  private static final String USER_ID = "123";
  private static final String USER_ID_2 = "234";
  private static final User USER = new User.Builder(USER_ID).build();
  private static final User USER_2 = new User.Builder(USER_ID_2).build();
  private static final AccessControl ADMIN_ACC_CTRL = AccessControl
      .withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private UserService userService;
  private GroupService groupService;

  @BeforeEach
  public void setUp() {
    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    userService = mock(UserService.class);

    groupService = new GroupServiceImpl(nodeService, feedService, permissionService, userService);
  }

  @Test
  public void addUsersToGroup() {
    var permission2 = new Permission.Builder(ID_2, PERMISSION_GROUP_ID).build();
    var permission3 = new Permission.Builder(ID_3, PERMISSION_GROUP_ID).build();
    when(permissionService.getPermissions(PERMISSION_GROUP_ID, TS_2))
        .thenReturn(List.of(permission2, permission3));

    when(userService.getUsers(List.of(ID_1))).thenReturn(List.of(USER));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .setPrivateNode(false)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNode = new Builder(new NodeId(NODE_ID, NodeType.CHAT_GROUP))
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.addUsersToGroup(groupNode, List.of(ID_1, ID_2), TS_1, TS_2);

    assertThat(result).isEqualTo(List.of(USER));

    verify(permissionService).addUsersToPermissionGroup(
        Map.of(USER_ID, AccessControl.withRights(AccessRight.READ)), PERMISSION_GROUP_ID, TS_2);
    verify(permissionService).addUsersToPermissionGroup(
        Map.of(USER_ID, AccessControl.withRights(AccessRight.READ)), PERMISSION_GROUP_ID_2, TS_2);

    verify(feedService).createFeeds(groupNode.getNodeId().toFeedItemKey(), List.of(USER_ID),
        groupNode.getNodeId().getType().name(), TS_2);
    verify(feedService)
        .createFeeds(childNodeId.toFeedItemKey(), List.of(USER_ID), childNodeId.getType().name(),
            TS_1, TS_2);
  }

  @Test
  public void addUsersToGroupFiltersOutPrivateNodes() {
    var permission2 = new Permission.Builder(ID_2, PERMISSION_GROUP_ID).build();
    var permission3 = new Permission.Builder(ID_3, PERMISSION_GROUP_ID).build();
    when(permissionService.getPermissions(PERMISSION_GROUP_ID, TS_2))
        .thenReturn(List.of(permission2, permission3));

    when(userService.getUsers(List.of(ID_1))).thenReturn(List.of(USER));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNode = new Builder(new NodeId(NODE_ID, NodeType.CHAT_GROUP))
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    groupService.addUsersToGroup(groupNode, List.of(ID_1), TS_1, TS_2);

    verify(permissionService).addUsersToPermissionGroup(
        Map.of(USER_ID, AccessControl.withRights(AccessRight.READ)), PERMISSION_GROUP_ID, TS_2);
    verify(permissionService, never()).addUsersToPermissionGroup(
        Map.of(USER_ID, AccessControl.withRights(AccessRight.READ)), PERMISSION_GROUP_ID_2, TS_2);

    verify(feedService).createFeeds(groupNode.getNodeId().toFeedItemKey(), List.of(USER_ID),
        groupNode.getNodeId().getType().name(), TS_2);
    verify(feedService, never())
        .createFeeds(childNodeId.toFeedItemKey(), List.of(USER_ID), childNodeId.getType().name(),
            TS_1, TS_2);
  }

  @Test
  public void addUsersToGroupReturnsEmptyListIfRetrievedIdsEmpty() {
    var permission2 = new Permission.Builder(ID_2, PERMISSION_GROUP_ID).build();
    var permission3 = new Permission.Builder(ID_3, PERMISSION_GROUP_ID).build();
    when(permissionService.getPermissions(PERMISSION_GROUP_ID, TS_2))
        .thenReturn(List.of(permission2, permission3));

    when(userService.getUsers(List.of())).thenReturn(List.of());

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .setPrivateNode(false)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNode = new Builder(new NodeId(NODE_ID, NodeType.CHAT_GROUP))
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.addUsersToGroup(groupNode, List.of(ID_2), TS_1, TS_2);

    assertThat(result).isEmpty();

    verify(permissionService, never()).addUsersToPermissionGroup(any(), any(), any());
    verify(feedService, never()).createFeeds(any(), any(),any(), any());
  }

  @Test
  public void setMemberAdminStatusMakeAdmin() throws Exception {
    var nodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var node = new Builder(nodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    groupService.setMemberAdminStatus(node, USER, "someone", true, TS_1);

    verify(permissionService).updateAccessControl("someone", PERMISSION_GROUP_ID,
        AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE), TS_1);
  }

  @Test
  public void setMemberAdminStatusRemoveAdmin() throws Exception {
    var nodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var node = new Builder(nodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    groupService.setMemberAdminStatus(node, USER, "someone", false, TS_1);

    verify(permissionService).updateAccessControl("someone", PERMISSION_GROUP_ID,
        AccessControl.withRights(AccessRight.READ), TS_1);
  }

  @Test
  public void setMemberAdminStatusThrowsIfCallerWantsToUpdateOwnStatus() throws Exception {
    var nodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var node = new Builder(nodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.setMemberAdminStatus(node, USER, USER_ID, false, TS_1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("User cannot change their own admin status");

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
  }

  @Test
  public void setMemberAdminStatusThrowsIfCallerNotCreator() throws Exception {
    var nodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var node = new Builder(nodeId)
        .withCreatorId("azzz")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.setMemberAdminStatus(node, USER, "azzz", false, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Only the group creator can change admins");

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
  }

  @Test
  public void removeUserFromGroupMakesOldestMemberAdmin() throws Exception {
    var permission1 = new Permission.Builder("USER_2", PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder("USER_3", PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_2)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER, TS_1);

    assertThat(result).isEqualTo(Optional.of("USER_2"));

    verify(permissionService)
        .updateAccessControl("USER_2", PERMISSION_GROUP_ID, ADMIN_ACC_CTRL, TS_1);
    verify(feedService).batchHardDeleteFeed(USER_ID,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupAnotherMemberAlreadyAdmin() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder("USER_2", PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupFiltersOutPermissionOfCaller() throws Exception {
    var permission = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void deleteGroup() throws Exception {
    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var groupNodeDeleted = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .setDeleted(true)
        .build();
    var childNodeDeleted = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .setDeleted(true)
        .build();
    when(nodeService.batchSoftDeleteNode(List.of(childNodeId, groupNodeId), TS_1))
        .thenReturn(List.of(childNodeDeleted, groupNodeDeleted));

    var result = groupService.deleteGroup(groupNode, USER, TS_1);

    Assertions.assertThat(result).containsOnly(childNodeDeleted, groupNodeDeleted);

    verify(feedService).batchRefreshFeedsForResource(
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()), TS_1);

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);

    verify(nodeService).softDeleteChildNodesAsync(CHILD_NODE_ID, TS_1);
  }

  @Test
  public void deleteGroupThrowsIfRequesterNotCreator() {
    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId("Ha")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.deleteGroup(groupNode, USER, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Only the group creator can delete the group");

    verify(nodeService, never()).batchSoftDeleteNode(any(), any());
    verify(feedService, never()).batchRefreshFeedsForResource(any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
  }

  @Test
  public void removeUserFromGroupRequesterIsCreatorAndTargetIsCreator() throws Exception {
    var permission = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupRequesterIsCreatorAndTargetIsAdmin() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID_2,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID_2), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupRequesterIsCreatorAndTargetIsRegular() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID_2,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID_2), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupRequesterIsAdminAndTargetIsCreator() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID_2)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Non-creator cannot remove creator from group");

    verify(nodeService, never()).getChildNodes(any());
    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
    verify(feedService, never()).batchHardDeleteFeed(any(), any());
  }

  @Test
  public void removeUserFromGroupRequesterIsAdminAndTargetIsAdmin() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId("Mystery")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("An admin cannot remove another admin from group");

    verify(nodeService, never()).getChildNodes(any());
    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
    verify(feedService, never()).batchHardDeleteFeed(any(), any());
  }

  @Test
  public void removeUserFromGroupRequesterIsAdminAndTargetIsRegular() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var childNodeId = new NodeId(CHILD_NODE_ID, NodeType.THREAD);
    var childNode = new Builder(childNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID_2)
        .build();
    when(nodeService.getChildNodes(NODE_ID)).thenReturn(List.of(childNode));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId("Mystery")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    var result = groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1);

    assertThat(result).isEqualTo(Optional.empty());

    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(feedService).batchHardDeleteFeed(USER_ID_2,
        List.of(childNodeId.toFeedItemKey(), groupNodeId.toFeedItemKey()));

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionService).batchSoftDeletePermissions(eq(USER_ID_2), captor.capture(), eq(TS_1));
    assertThat(captor.getValue()).containsOnly(PERMISSION_GROUP_ID_2, PERMISSION_GROUP_ID);
  }

  @Test
  public void removeUserFromGroupRequesterIsRegularAndTargetIsCreator() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId(USER_ID_2)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Non-creator cannot remove creator from group");

    verify(nodeService, never()).getChildNodes(any());
    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
    verify(feedService, never()).batchHardDeleteFeed(any(), any());
  }

  @Test
  public void removeUserFromGroupRequesterIsRegularAndTargetIsAdmin() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(ADMIN_ACC_CTRL)
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId("Mystery")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("A non-admin cannot remove members from group");

    verify(nodeService, never()).getChildNodes(any());
    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
    verify(feedService, never()).batchHardDeleteFeed(any(), any());
  }

  @Test
  public void removeUserFromGroupRequesterIsRegularAndTargetIsRegular() throws Exception {
    var permission1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    var permission2 = new Permission.Builder(USER_ID_2, PERMISSION_GROUP_ID)
        .withAccessControl(AccessControl.withRights(AccessRight.READ))
        .withCreationTs(TS_1)
        .build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, new Instant(0)))
        .thenReturn(List.of(permission1, permission2));

    var groupNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var groupNode = new Builder(groupNodeId)
        .withCreatorId("Mystery")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();

    assertThatThrownBy(() -> groupService.removeUserFromGroup(groupNode, USER, USER_2, TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("A non-admin cannot remove members from group");

    verify(nodeService, never()).getChildNodes(any());
    verify(permissionService, never()).updateAccessControl(any(), any(), any(), any());
    verify(permissionService, never()).batchSoftDeletePermissions(any(), any(), any());
    verify(feedService, never()).batchHardDeleteFeed(any(), any());
  }
}
