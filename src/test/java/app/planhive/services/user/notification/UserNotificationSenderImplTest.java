package app.planhive.services.user.notification;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class UserNotificationSenderImplTest {

  private static final String NAME = "Roger";
  private static final String ID = "123";
  private static final User USER = new User.Builder(ID)
      .withName(NAME)
      .build();
  private static final List<String> IDS = List.of("234", "435");

  private NotificationService notificationService;
  private UserNotificationSenderImpl userNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    userNotificationSender = new UserNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendContactRequestNotification() {
    userNotificationSender.sendContactRequestNotification(USER, IDS.get(0));

    var expectedNotification = new Notification.Builder(
        "New contact request", String.format("%s sent you a contact request.", NAME),
        NotificationCategory.CONTACTS)
        .withResourceType(NotificationKey.USER.getValue())
        .withAdditionalData(Map.of(NotificationKey.USER_ID.getValue(), ID))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of(IDS.get(0)), true);
  }

  @Test
  public void sendContactRequestAcceptedNotification() {
    userNotificationSender.sendContactRequestAcceptedNotification(USER, IDS.get(0));

    var expectedNotification = new Notification.Builder(
        "New contact", String.format("%s accepted your contact request!", NAME),
        NotificationCategory.CONTACTS)
        .withResourceType(NotificationKey.USER.getValue())
        .withAdditionalData(Map.of(NotificationKey.USER_ID.getValue(), ID))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of(IDS.get(0)), true);
  }
}
