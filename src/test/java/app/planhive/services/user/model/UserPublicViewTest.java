package app.planhive.services.user.model;

import static org.assertj.core.api.Assertions.assertThat;

import app.planhive.model.EmailAddress;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class UserPublicViewTest {

  private static final String USER_ID = "asd-32rfe-2f-faf3";
  private static final String USER_NAME = "aldo.1";
  private static final String NAME = "Aldo";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final User USER = new User.Builder(USER_ID)
      .withSessionId("123")
      .withNotificationToken("234")
      .withUsername(USER_NAME)
      .withName(NAME)
      .withEmailAddress(new EmailAddress("user@email.com"))
      .withCreationTs(Instant.now())
      .withLastUpdateTs(TS_1)
      .withLastAvatarUpdateTs(TS_2)
      .setDeleted(true)
      .build();

  @Test
  public void constructor() {
    var obj = UserPublicView.fromUser(USER);

    assertThat(obj.getUser().getId()).isEqualTo(USER_ID);
    assertThat(obj.getUser().getSessionId()).isNull();
    assertThat(obj.getUser().getNotificationToken()).isNull();
    assertThat(obj.getUser().getUsername()).isEqualTo(USER_NAME);
    assertThat(obj.getUser().getName()).isEqualTo(NAME);
    assertThat(obj.getUser().getEmailAddress()).isNull();
    assertThat(obj.getUser().getCreationTs()).isNull();
    assertThat(obj.getUser().getLastUpdateTs()).isEqualTo(TS_1);
    assertThat(obj.getUser().getLastAvatarUpdateTs()).isEqualTo(TS_2);
    assertThat(obj.getUser().isDeleted()).isEqualTo(true);
  }

  @Test
  public void equality() {
    var obj1 = UserPublicView.fromUser(USER);
    var obj2 = UserPublicView.fromUser(USER);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = UserPublicView.fromUser(USER);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = UserPublicView.fromUser(USER);
    var obj2 = UserPublicView.fromUser(new User.Builder("").build());

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = UserPublicView.fromUser(USER);

    var mapper = new ObjectMapper();
    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format("{\"id\":\"%s\",\"username\":\"%s\""
            + ",\"name\":\"%s\",\"lastUpdateTs\":%s,\"lastAvatarUpdateTs\":%s,\"deleted\":%s}",
        USER_ID, USER_NAME, NAME, TS_1.getMillis(), TS_2.getMillis(), true));
  }
}
