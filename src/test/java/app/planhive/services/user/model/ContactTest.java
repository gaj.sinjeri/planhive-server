package app.planhive.services.user.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class ContactTest {

  private static final String USER_ID = "123";
  private static final String CONTACT_ID = "456";
  private static final Instant TS = Instant.now();

  private final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);

    assertThat(obj.getContactState()).isEqualTo(ContactState.CONNECTED);
    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getContactId()).isEqualTo(CONTACT_ID);
    assertThat(obj.getLastUpdateTs()).isEqualTo(TS);
  }

  @Test
  public void constructorThrowsIfContactStateNull() {
    assertThatThrownBy(() -> new Contact(null, USER_ID, CONTACT_ID, TS))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfUserIdNull() {
    assertThatThrownBy(() -> new Contact(ContactState.CONNECTED, null, CONTACT_ID, TS))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfContactIdNull() {
    assertThatThrownBy(() -> new Contact(ContactState.CONNECTED, USER_ID, null, TS))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfLastUpdatedTsNull() {
    assertThatThrownBy(() -> new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);
    var obj2 = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);
    var obj2 = new Contact(ContactState.REQUESTED, USER_ID, CONTACT_ID, TS);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = new Contact(ContactState.CONNECTED, USER_ID, CONTACT_ID, TS);

    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format("{\"contactState\":\"%s\",\"userId\":\"%s\","
            + "\"contactId\":\"%s\",\"lastUpdateTs\":%s}", ContactState.CONNECTED, USER_ID,
        CONTACT_ID, TS.getMillis()));
  }
}
