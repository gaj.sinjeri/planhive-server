package app.planhive.services.user.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class ContactWithUserViewTest {

  private static final Contact CONTACT = new Contact(ContactState.CONNECTED, "1", "2",
      Instant.now());
  private static final UserView USER_VIEW = UserPrivateView.fromUser(new User.Builder("1").build());

  @Test
  public void constructor() {
    var obj = new ContactWithUserView(CONTACT, USER_VIEW);

    assertThat(obj.getContact()).isEqualTo(CONTACT);
    assertThat(obj.getUserView()).isEqualTo(USER_VIEW);
  }

  @Test
  public void equality() {
    var obj1 = new ContactWithUserView(CONTACT, USER_VIEW);
    var obj2 = new ContactWithUserView(CONTACT, USER_VIEW);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ContactWithUserView(CONTACT, USER_VIEW);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ContactWithUserView(CONTACT, USER_VIEW);
    var obj2 = new ContactWithUserView(CONTACT,
        UserPublicView.fromUser(new User.Builder("1").build()));

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
