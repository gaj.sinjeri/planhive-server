package app.planhive.services.user.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.model.EmailAddress;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class UserTest {

  private static final String USER_ID = "asd-32rfe-2f-faf3";
  private static final String SESSION_ID = "fe09420f32krm";
  private static final String NOTIFICATION_TOKEN = "9284hg9824hg982";
  private static final String USER_NAME = "aldo.1";
  private static final String NAME = "Aldo";
  private static final EmailAddress EMAIL = new EmailAddress("hello@hello.abc");
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final Instant TS_3 = new Instant(3);

  private final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void builder() {
    var obj = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();

    assertThat(obj.getId()).isEqualTo(USER_ID);
    assertThat(obj.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(obj.getNotificationToken()).isEqualTo(NOTIFICATION_TOKEN);
    assertThat(obj.getNotificationCount()).isEqualTo(1);
    assertThat(obj.getUsername()).isEqualTo(USER_NAME);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getEmailAddress()).isEqualTo(EMAIL);
    assertThat(obj.getCreationTs()).isEqualTo(TS_1);
    assertThat(obj.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(obj.getLastAvatarUpdateTs()).isEqualTo(TS_3);
    assertThat(obj.isDeleted()).isEqualTo(true);
  }

  @Test
  public void builderThrowsIfIdNull() {
    assertThatThrownBy(() -> new User.Builder(null).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfUsernameDoesNotMatchPattern() {
    assertThatThrownBy(() -> new User.Builder(USER_ID)
        .withUsername("_thisWon'TWOR-k")
        .build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Username _thisWon'TWOR-k does not match pattern");
  }

  @Test
  public void builderThrowsIfNameDoesNotMatchPattern() {
    assertThatThrownBy(() -> new User.Builder(USER_ID)
        .withName("X AE A-12")
        .build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Name X AE A-12 does not match pattern");
  }

  @Test
  public void equality() {
    var obj1 = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();
    var obj2 = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();
    var obj2 = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationToken(NOTIFICATION_TOKEN)
        .withNotificationCount(1)
        .withUsername(USER_NAME)
        .withName("Karren")
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = new User.Builder(USER_ID)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(EMAIL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .setDeleted(true)
        .build();

    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format("{\"id\":\"%s\",\"lastUpdateTs\":%s,\"deleted\":%s,"
            + "\"username\":\"%s\",\"name\":\"%s\",\"creationTs\":%s,\"lastAvatarUpdateTs\":%s,"
            + "\"email\":\"%s\"}", USER_ID, TS_2.getMillis(), true, USER_NAME, NAME,
        TS_1.getMillis(), TS_3.getMillis(), EMAIL.getValue()));
  }

  @Test
  public void serializeRequiredFieldsOnly() throws Exception {
    var obj = new User.Builder(USER_ID)
        .withLastUpdateTs(TS_2)
        .build();

    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String
        .format("{\"id\":\"%s\",\"lastUpdateTs\":%s,\"deleted\":%s}", USER_ID, TS_2.getMillis(),
            false));
  }
}
