package app.planhive.services.user.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.notification.NotificationCategory;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class NotificationTest {

  private static final String TITLE = "Meg's 16th";
  private static final String BODY = "Peter is not attending.";
  private static final String RESOURCE_TYPE = "PARTICIPATION";
  private static final Map<String, String> ADDITIONAL_DATA = Map.of("Shut up", "Meg");
  private static final NotificationCategory CATEGORY = NotificationCategory.EVENT_UPDATES;

  @Test
  public void constructor() {
    var obj = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    assertThat(obj.getTitle()).isEqualTo(TITLE);
    assertThat(obj.getBody()).isEqualTo(BODY);
    assertThat(obj.getResourceType()).isEqualTo(RESOURCE_TYPE);
    assertThat(obj.getAdditionalData()).isEqualTo(ADDITIONAL_DATA);
    assertThat(obj.getCategory()).isEqualTo(CATEGORY);
  }

  @Test
  public void builderThrowsIfTitleNull() {
    assertThatThrownBy(() -> new Notification.Builder(null, BODY, CATEGORY).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfBodyNull() {
    assertThatThrownBy(() -> new Notification.Builder(TITLE, null, CATEGORY).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfCategoryNull() {
    assertThatThrownBy(() -> new Notification.Builder(TITLE, BODY, null).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();
    var obj2 = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();
    var obj2 = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(Map.of())
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
