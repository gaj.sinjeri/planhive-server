package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class UpdateUserRequestTest {

  private static final String USERNAME = "zorg";
  private static final String NAME = "Zorg";
  private static final Long TS = 1L;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateUserRequest(USERNAME, NAME, TS);

    assertThat(obj.getUsername()).isEqualTo(USERNAME);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getLastAvatarUpdateTs()).isEqualTo(TS);
  }

  @Test
  public void deserializationFailsIfLastAvatarUpdateTsMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", UpdateUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'lastAvatarUpdateTs'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateUserRequest(NAME, USERNAME, TS);
    var obj2 = new UpdateUserRequest(NAME, USERNAME, TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateUserRequest(NAME, USERNAME, TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateUserRequest(NAME, USERNAME, TS);
    var obj2 = new UpdateUserRequest(NAME, USERNAME, 2L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
