package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class UpdateNotificationCountRequestTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateNotificationCountRequest(5);

    assertThat(obj.getNotificationCount()).isEqualTo(5);
  }

  @Test
  public void deserializationFailsIfNotificationCountMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", UpdateNotificationCountRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'notificationCount'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateNotificationCountRequest(5);
    var obj2 = new UpdateNotificationCountRequest(5);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateNotificationCountRequest(5);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateNotificationCountRequest(5);
    var obj2 = new UpdateNotificationCountRequest(10);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
