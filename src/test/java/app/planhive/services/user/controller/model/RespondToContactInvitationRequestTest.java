package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class RespondToContactInvitationRequestTest {

  protected static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new RespondToContactInvitationRequest(true);

    assertThat(obj.isAccepted()).isEqualTo(true);
  }

  @Test
  public void deserializationFailsIfContactStateMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", RespondToContactInvitationRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'accepted'");
  }

  @Test
  public void equality() {
    var obj1 = new RespondToContactInvitationRequest(true);
    var obj2 = new RespondToContactInvitationRequest(true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new RespondToContactInvitationRequest(true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new RespondToContactInvitationRequest(true);
    var obj2 = new RespondToContactInvitationRequest(false);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
