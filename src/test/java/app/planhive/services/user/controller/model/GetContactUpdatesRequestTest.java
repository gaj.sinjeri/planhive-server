package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class GetContactUpdatesRequestTest {

  private static final Instant TS = Instant.now();

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new GetContactUpdatesRequest(TS.getMillis());

    assertThat(obj.getLastUpdateTs()).isEqualTo(TS.getMillis());
  }

  @Test
  public void deserializationFailsIfLastUpdateTsMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", GetContactUpdatesRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'lastUpdateTs'");
  }

  @Test
  public void equality() {
    var obj1 = new GetContactUpdatesRequest(TS.getMillis());
    var obj2 = new GetContactUpdatesRequest(TS.getMillis());

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new GetContactUpdatesRequest(TS.getMillis());

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new GetContactUpdatesRequest(TS.getMillis());
    var obj2 = new GetContactUpdatesRequest(123L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
