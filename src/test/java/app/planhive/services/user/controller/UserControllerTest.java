package app.planhive.services.user.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.ResourceType;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.user.controller.model.GetContactUpdatesRequest;
import app.planhive.services.user.controller.model.GetPublicUserDetailsRequest;
import app.planhive.services.user.controller.model.RespondToContactInvitationRequest;
import app.planhive.services.user.controller.model.UpdateNotificationCountRequest;
import app.planhive.services.user.controller.model.UpdateNotificationTokenRequest;
import app.planhive.services.user.controller.model.UpdateUserRequest;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;
import app.planhive.services.user.model.ContactWithUserView;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.UserPrivateView;
import app.planhive.services.user.model.UserPublicView;
import app.planhive.services.user.service.ContactService;
import app.planhive.services.user.service.NotificationService;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class UserControllerTest {

  private static final String USER_ID = "1234";
  private static final String USERNAME = "a.0";
  private static final String NAME = "baobab";
  private static final User USER = new User.Builder("1234").build();
  private static final User USER_2 = new User.Builder("2345").build();
  private static final Instant TS = Instant.now();
  private static final byte[] BYTES = new byte[]{1};
  private static final MultipartFile FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg",
      BYTES);

  private UserService userService;
  private ContactService contactService;
  private NotificationService notificationService;
  private UserController userController;

  @BeforeEach
  public void setUp() {
    userService = mock(UserService.class);
    contactService = mock(ContactService.class);
    notificationService = mock(NotificationService.class);
    userController = new UserController(userService, contactService, notificationService);
  }

  @Test
  public void updateUserDetailsSuccessfulCall() throws Exception {
    when(userService.updateUserDetails(eq(USER), eq(NAME), eq(USERNAME), eq(TS), eq(FILE), any()))
        .thenReturn(USER);

    var request = new UpdateUserRequest(USERNAME, NAME, TS.getMillis());
    var response = userController.updateUserDetails(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(USER));
  }

  @Test
  public void updateUserDetailsReturnsErrorIfInvalidImage() throws Exception {
    Mockito.doThrow(new InvalidImageException("")).when(userService)
        .updateUserDetails(eq(USER), eq(NAME), eq(USERNAME), eq(TS), eq(FILE), any());

    var request = new UpdateUserRequest(USERNAME, NAME, TS.getMillis());
    var response = userController.updateUserDetails(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateUserDetailsReturnsErrorIfResourceNotFound() throws Exception {
    Mockito.doThrow(new ResourceNotFoundException("")).when(userService)
        .updateUserDetails(eq(USER), eq(NAME), eq(USERNAME), eq(TS), eq(FILE), any());

    var request = new UpdateUserRequest(USERNAME, NAME, TS.getMillis());
    var response = userController.updateUserDetails(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateUserDetailsReturnsErrorIfUsernameAlreadyTaken() throws Exception {
    Mockito.doThrow(new ResourceAlreadyExistsException("", ResourceType.DB_USERNAME_ITEM))
        .when(userService)
        .updateUserDetails(eq(USER), eq(NAME), eq(USERNAME), eq(TS), eq(FILE), any());

    var request = new UpdateUserRequest(USERNAME, NAME, TS.getMillis());
    var response = userController.updateUserDetails(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.errorResponse(ErrorCode.USERNAME_TAKEN));
  }

  @Test
  public void updateUserDetailsReturnsErrorIfOtherResourceAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("", null)).when(userService)
        .updateUserDetails(eq(USER), eq(NAME), eq(USERNAME), eq(TS), eq(FILE), any());

    var request = new UpdateUserRequest(USERNAME, NAME, TS.getMillis());
    var response = userController.updateUserDetails(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getUserDetailsSuccessfulCall() {
    var response = userController.getUserDetails(USER);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(USER));
  }

  @Test
  public void getUserUpdatesForContactsSuccessfulCall() {
    var contact = new Contact(ContactState.CONNECTED, USER_ID, USER_2.getId(), TS);
    var contactWithUserView = new ContactWithUserView(contact, UserPrivateView.fromUser(USER_2));
    when(contactService.getContactUpdates(USER, TS)).thenReturn(List.of(contactWithUserView));

    var request = new GetContactUpdatesRequest(TS.getMillis());
    var response = userController.getContactUpdates(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(List.of(contactWithUserView)));
  }

  @Test
  public void createContactRequestSuccessfulCall() throws Exception {
    var contact = new Contact(ContactState.REQUESTED, USER_ID, USER_2.getId(), TS);
    when(contactService.createContactRequest(eq(USER), eq(USER_2.getId()), any()))
        .thenReturn(contact);

    var response = userController.createContactRequest(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(contact));
  }

  @Test
  public void createContactRequestReturnsErrorIfResourceAlreadyExists() throws Exception {
    when(contactService.createContactRequest(eq(USER), eq(USER_2.getId()), any()))
        .thenThrow(new ResourceNotFoundException(""));

    var response = userController.createContactRequest(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void createContactRequestReturnsErrorIfInvalidOperation() throws Exception {
    when(contactService.createContactRequest(eq(USER), eq(USER_2.getId()), any()))
        .thenThrow(new InvalidOperationException(""));

    var response = userController.createContactRequest(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void respondToContactRequestSuccessfulCall() throws Exception {
    var contact = new Contact(ContactState.CONNECTED, USER_ID, USER_2.getId(), TS);
    when(contactService.respondToContactRequest(eq(USER_2.getId()), eq(USER), eq(true), any()))
        .thenReturn(contact);

    var request = new RespondToContactInvitationRequest(true);
    var response = userController.respondToContactRequest(USER, USER_2.getId(), request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(contact));
  }

  @Test
  public void respondToContactRequestReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("")).when(contactService)
        .respondToContactRequest(eq(USER_2.getId()), eq(USER), eq(true), any());

    var request = new RespondToContactInvitationRequest(true);
    var response = userController.respondToContactRequest(USER, USER_2.getId(), request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void cancelContactRequestSuccessfulCall() throws Exception {
    var contact = new Contact(ContactState.REJECTED, USER_ID, USER_2.getId(), TS);
    when(contactService.cancelContactRequest(eq(USER), eq(USER_2.getId()), any()))
        .thenReturn(contact);

    var response = userController.cancelContactRequest(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(contact));
  }

  @Test
  public void cancelContactRequestReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("")).when(contactService)
        .cancelContactRequest(eq(USER), eq(USER_2.getId()), any());

    var response = userController.cancelContactRequest(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteContactSuccessfulCall() throws Exception {
    var contact = new Contact(ContactState.DELETED, USER_ID, USER_2.getId(), TS);
    when(contactService.deleteContact(eq(USER), eq(USER_2.getId()), any())).thenReturn(contact);

    var response = userController.deleteContact(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(contact));
  }

  @Test
  public void deleteContactReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("")).when(contactService)
        .deleteContact(eq(USER), eq(USER_2.getId()), any());

    var response = userController.deleteContact(USER, USER_2.getId());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void batchGetPublicUserDetailsSuccessfulCall() {
    when(userService.getUsers(List.of(USER_ID))).thenReturn(List.of(USER_2));

    var request = new GetPublicUserDetailsRequest(List.of(USER_ID));
    var response = userController.batchGetPublicUserDetails(request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(List.of(UserPublicView.fromUser(USER_2))));
  }

  @Test
  public void getAvatarSuccessfulCall() throws Exception {
    when(userService.getAvatar(USER_ID)).thenReturn(BYTES);

    var response = userController.getAvatar(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(BYTES);
  }

  @Test
  public void getAvatarReturnsErrorIfFileDoesNotExist() throws Exception {
    when(userService.getAvatar(USER_ID)).thenThrow(new ResourceNotFoundException(""));

    var response = userController.getAvatar(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getAvatarThumbnailSuccessfulCall() throws Exception {
    when(userService.getAvatarThumbnail(USER_ID)).thenReturn(BYTES);

    var response = userController.getAvatarThumbnail(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(BYTES);
  }

  @Test
  public void getAvatarThumbnailReturnsErrorIfFileDoesNotExist() throws Exception {
    when(userService.getAvatarThumbnail(USER_ID)).thenThrow(new ResourceNotFoundException(""));

    var response = userController.getAvatarThumbnail(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getAvatarMicroSuccessfulCall() throws Exception {
    when(userService.getAvatarMicro(USER_ID)).thenReturn(BYTES);

    var response = userController.getAvatarMicro(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(BYTES);
  }

  @Test
  public void getAvatarMicroReturnsErrorIfFileDoesNotExist() throws Exception {
    when(userService.getAvatarMicro(USER_ID)).thenThrow(new ResourceNotFoundException(""));

    var response = userController.getAvatarMicro(USER_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void updateNotificationTokenSuccessfulCall() throws Exception {
    var request = new UpdateNotificationTokenRequest("123");
    var response = userController.updateNotificationToken(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());

    verify(notificationService).updateNotificationToken(eq(USER), eq("123"), any());
  }

  @Test
  public void updateNotificationTokenReturnsErrorIfResourceNotFound() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(notificationService)
        .updateNotificationToken(eq(USER), eq("123"), any());

    var request = new UpdateNotificationTokenRequest("123");
    var response = userController.updateNotificationToken(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateNotificationCountSuccessfulCall() throws Exception {
    var request = new UpdateNotificationCountRequest(5);
    var response = userController.updateNotificationCount(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());

    verify(notificationService).updateNotificationCount(USER_ID, 5);
  }

  @Test
  public void updateNotificationCountReturnsErrorIfResourceNotFound() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(notificationService)
        .updateNotificationCount(USER_ID, 5);

    var request = new UpdateNotificationCountRequest(5);
    var response = userController.updateNotificationCount(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
