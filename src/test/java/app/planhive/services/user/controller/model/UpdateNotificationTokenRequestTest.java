package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class UpdateNotificationTokenRequestTest {

  private static final String NOTIFICATION_TOKEN = "42857682138573201";

  @Test
  public void constructor() {
    var obj = new UpdateNotificationTokenRequest(NOTIFICATION_TOKEN);

    assertThat(obj.getNotificationToken()).isEqualTo(NOTIFICATION_TOKEN);
  }

  @Test
  public void constructorWithNullArgument() {
    var obj = new UpdateNotificationTokenRequest(null);

    assertThat(obj.getNotificationToken()).isEqualTo(null);
  }

  @Test
  public void constructorThrowsIfNotificationTokenEmpty() {
    assertThatThrownBy(() -> new UpdateNotificationTokenRequest(""))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void equality() {
    var obj1 = new UpdateNotificationTokenRequest(NOTIFICATION_TOKEN);
    var obj2 = new UpdateNotificationTokenRequest(NOTIFICATION_TOKEN);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateNotificationTokenRequest(NOTIFICATION_TOKEN);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateNotificationTokenRequest(NOTIFICATION_TOKEN);
    var obj2 = new UpdateNotificationTokenRequest("something something something");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
