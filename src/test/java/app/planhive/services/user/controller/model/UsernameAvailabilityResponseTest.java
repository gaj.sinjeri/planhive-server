package app.planhive.services.user.controller.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class UsernameAvailabilityResponseTest {

  @Test
  public void constructor() {
    var obj = new UsernameAvailabilityResponse(true);

    assertThat(obj.isAvailable()).isTrue();
  }

  @Test
  public void equality() {
    var obj1 = new UsernameAvailabilityResponse(true);
    var obj2 = new UsernameAvailabilityResponse(true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UsernameAvailabilityResponse(true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UsernameAvailabilityResponse(true);
    var obj2 = new UsernameAvailabilityResponse(false);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
