package app.planhive.services.user.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.model.RestResponse;
import app.planhive.services.user.controller.model.UsernameAvailabilityResponse;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.UserPublicView;
import app.planhive.services.user.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class UsernameControllerTest {

  private static final String USER_ID = "1234";
  private static final String USERNAME = "a.0";
  private static final User USER = new User.Builder("1234").build();

  private UserService userService;
  private UsernameController usernameController;

  @BeforeEach
  public void setUp() {
    userService = mock(UserService.class);
    usernameController = new UsernameController(userService);
  }

  @Test
  public void checkUsernameAvailabilitySuccessfulCall() {
    when(userService.getUserId(USERNAME)).thenReturn(USER_ID);

    var response = usernameController.checkUsernameAvailability(USERNAME);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(new UsernameAvailabilityResponse(false)));
  }

  @Test
  public void checkUsernameAvailabilityReturnsErrorIfUserDoesNotExist() {
    when(userService.getUserId(USERNAME)).thenReturn(null);

    var response = usernameController.checkUsernameAvailability(USERNAME);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(new UsernameAvailabilityResponse(true)));
  }

  @Test
  public void getUsernameAutocompleteSuggestionSuccessfulCall() {
    when(userService.queryUsersByUsername("abc")).thenReturn(List.of(USER));

    var response = usernameController.getUsernameAutocompleteSuggestion("abc");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(List.of(UserPublicView.fromUser(USER))));
  }
}
