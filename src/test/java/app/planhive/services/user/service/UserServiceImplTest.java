package app.planhive.services.user.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.threading.TaskRunner;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;

@TestInstance(Lifecycle.PER_CLASS)
public class UserServiceImplTest {

  private static final String USER_ID = "1234";
  private static final String USER_ID_2 = "2345";
  private static final String USER_ID_3 = "3456";
  private static final String USERNAME = "tim.tom";
  private static final String USERNAME_2 = "tom.tim";
  private static final String NAME = "Aldo";
  private static final String NAME_2 = "Susan";
  private static final EmailAddress EMAIL_1 = new EmailAddress("user@planhive.app");
  private static final EmailAddress EMAIL_2 = new EmailAddress("admin@planhive.app");
  private static final String SESSION_ID = "fe09420f32krm";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final byte[] BYTES = new byte[]{1};
  private static final User USER_1 = new User.Builder(USER_ID)
      .withUsername(USERNAME)
      .withEmailAddress(EMAIL_1)
      .withLastAvatarUpdateTs(TS_2)
      .build();

  private UserDAO userDAO;
  private ContactService contactService;
  private CloudStore cloudStore;
  private SecureGenerator secureGenerator;
  private TaskRunner taskRunner;
  private UserService userService;

  private InputStream imageInputStream;
  private MultipartFile FILE;

  @BeforeEach
  public void setUp() throws Exception {
    userDAO = mock(UserDAO.class);
    contactService = mock(ContactService.class);
    cloudStore = mock(CloudStore.class);
    secureGenerator = mock(SecureGenerator.class);
    taskRunner = mock(TaskRunner.class);

    userService = new UserServiceImpl(userDAO, contactService, cloudStore, secureGenerator,
        taskRunner, 2048, 256, 64);

    when(secureGenerator.generate256BitKey()).thenReturn(SESSION_ID);

    imageInputStream = generateImageInputStream(2, 1);
    FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg", imageInputStream);
  }

  @Test
  public void createUserCallsDAO() throws Exception {
    var user = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationCount(0)
        .withUsername(USERNAME)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    when(userDAO.putItem(user)).thenReturn(user);

    var result = userService.createUser(USER_ID, USERNAME, NAME, EMAIL_1, FILE, TS_1);

    Assertions.assertThat(result).isEqualTo(user);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("users/%s/avatar.jpeg", USER_ID)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("users/%s/t-avatar.jpeg", USER_ID)), any());
    verify(cloudStore).storeImage(eq(String.format("users/%s/m-avatar.jpeg", USER_ID)), any());
  }

  @Test
  public void createUserDoesNotCallCloudStoreIfImageNull() throws Exception {
    var user = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withNotificationCount(0)
        .withUsername(USERNAME)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(new Instant(0))
        .build();
    when(userDAO.putItem(user)).thenReturn(user);

    var result = userService.createUser(USER_ID, USERNAME, NAME, EMAIL_1, null, TS_1);

    Assertions.assertThat(result).isEqualTo(user);

    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void createUserRethrowsIfAvatarInaccessible() throws Exception {
    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(
        () -> userService.createUser(USER_ID, USERNAME, NAME, EMAIL_1, fileMock, TS_1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error handling image. This could be either due to not being "
            + "able to access the input stream of the original image or create thumbnail");

    verify(cloudStore, never()).storeImage(any(), any());
    verify(secureGenerator, never()).generate256BitKey();
    verify(userDAO, never()).putItem(any());
  }

  @Test
  public void createUserThrowsIfAvatarFormatIncorrect() throws Exception {
    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(
        () -> userService.createUser(USER_ID, USERNAME, NAME, EMAIL_1, fileMock, TS_1))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");

    verify(cloudStore, never()).storeImage(any(), any());
    verify(secureGenerator, never()).generate256BitKey();
    verify(userDAO, never()).putItem(any());
  }

  @Test
  public void createUserThrowsIfAvatarResolutionTooHigh() throws Exception {
    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(() -> userService
        .createUser(USER_ID, USERNAME, NAME, EMAIL_1, file, TS_1))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");

    verify(cloudStore, never()).storeImage(any(), any());
    verify(secureGenerator, never()).generate256BitKey();
    verify(userDAO, never()).putItem(any());
  }

  @Test
  public void updateUser() throws Exception {
    var userUpdate = new User.Builder(USER_ID)
        .withName(NAME)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    var user = new User.Builder(USER_ID)
        .withUsername(USERNAME)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();

    when(userDAO.updateUser(userUpdate, TS_1)).thenReturn(user);
    when(userDAO.updateUsername(USER_ID, USERNAME, USERNAME_2, TS_1))
        .thenReturn(USERNAME_2);

    var result = userService.updateUserDetails(USER_1, NAME, USERNAME_2, TS_2, FILE, TS_1);

    var expectedUser = new User.Builder(USER_ID)
        .withName(NAME)
        .withUsername(USERNAME_2)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedUser);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("users/%s/avatar.jpeg", USER_ID)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("users/%s/t-avatar.jpeg", USER_ID)), any());
    verify(cloudStore).storeImage(eq(String.format("users/%s/m-avatar.jpeg", USER_ID)), any());

    executeTaskRunnerFunction();
    verify(contactService).refreshContacts(USER_ID, TS_1);
  }

  @Test
  public void updateUserDetailsWithoutAvatar() throws Exception {
    var userUpdate = new User.Builder(USER_ID)
        .withName(NAME)
        .withLastAvatarUpdateTs(TS_2)
        .build();
    var user = new User.Builder(USER_ID)
        .withUsername(USERNAME)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_2)
        .build();

    when(userDAO.updateUser(userUpdate, TS_1)).thenReturn(user);
    when(userDAO.updateUsername(USER_ID, USERNAME, USERNAME_2, TS_1))
        .thenReturn(USERNAME_2);

    var result = userService.updateUserDetails(USER_1, NAME, USERNAME_2, TS_2, null, TS_1);

    var expectedUser = new User.Builder(USER_ID)
        .withName(NAME)
        .withUsername(USERNAME_2)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_2)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedUser);

    verify(cloudStore, never()).storeImage(any(), any());

    executeTaskRunnerFunction();
    verify(contactService).refreshContacts(USER_ID, TS_1);
  }

  @Test
  public void updateUserDetailsWithoutNameAndAvatar() throws Exception {
    when(userDAO.updateUsername(USER_ID, USERNAME, USERNAME_2, TS_1))
        .thenReturn(USERNAME_2);

    var result = userService.updateUserDetails(USER_1, null, USERNAME_2, TS_2, null, TS_1);

    var expectedUser = new User.Builder(USER_ID)
        .withUsername(USERNAME_2)
        .withLastUpdateTs(TS_1)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedUser);

    verify(userDAO, never()).updateUser(any(), any());
    verify(cloudStore, never()).storeImage(any(), any());

    executeTaskRunnerFunction();
    verify(contactService).refreshContacts(USER_ID, TS_1);
  }

  @Test
  public void updateUserDetailsWithoutUsername() throws Exception {
    var userUpdate = new User.Builder(USER_ID)
        .withName(NAME)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    var user = new User.Builder(USER_ID)
        .withUsername(USERNAME)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();

    when(userDAO.updateUser(userUpdate, TS_1)).thenReturn(user);

    var result = userService.updateUserDetails(USER_1, NAME, null, TS_2, FILE, TS_1);

    var expectedUser = new User.Builder(USER_ID)
        .withName(NAME)
        .withUsername(USERNAME)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedUser);

    verify(userDAO, never()).updateUsername(any(), any(), any(), any());

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("users/%s/avatar.jpeg", USER_ID)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("users/%s/t-avatar.jpeg", USER_ID)), any());
    verify(cloudStore).storeImage(eq(String.format("users/%s/m-avatar.jpeg", USER_ID)), any());

    executeTaskRunnerFunction();
    verify(contactService).refreshContacts(USER_ID, TS_1);
  }

  @Test
  public void updateUserDetailsThrowsIfUsernameInvalid() {

    assertThatThrownBy(
        () -> userService.updateUserDetails(USER_1, NAME, "_thisWon'TWOR-k", TS_2, FILE, TS_1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Username _thisWon'TWOR-k does not match pattern");
  }

  @Test
  public void updateUserDetailsThrowsIfNameInvalid() {

    assertThatThrownBy(
        () -> userService.updateUserDetails(USER_1, "X AE A-12", null, TS_2, FILE, TS_1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Name X AE A-12 does not match pattern");
  }

  @Test
  public void updateSessionId() throws Exception {
    var userUpdate = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();
    var user = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();

    when(userDAO.updateUser(userUpdate, TS_2)).thenReturn(user);

    var result = userService.updateSessionId(USER_ID, SESSION_ID, TS_2);

    Assertions.assertThat(result).isEqualTo(user);
  }

  @Test
  public void getUserIdByUsername() {
    when(userDAO.getUserId(USERNAME)).thenReturn(USER_ID);

    var result = userService.getUserId(USERNAME);

    assertThat(result).isEqualTo(USER_ID);
  }

  @Test
  public void getUserIdByUsernameConvertsToLowerCase() {
    when(userDAO.getUserId("aaa")).thenReturn(USER_ID);

    var result = userService.getUserId("AAA");

    assertThat(result).isEqualTo(USER_ID);
  }


  @Test
  public void getUserIdByEmail() {
    when(userDAO.getUserId(EMAIL_1)).thenReturn(USER_ID);

    var result = userService.getUserId(EMAIL_1);

    assertThat(result).isEqualTo(USER_ID);
  }

  @Test
  public void getUser() {
    var user = new User.Builder(USER_ID)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .build();
    when(userDAO.getUser(USER_ID)).thenReturn(user);

    var result = userService.getUser(USER_ID);

    Assertions.assertThat(result).isEqualTo(user);
  }

  @Test
  public void getUsers() {
    var user1 = new User.Builder(USER_ID)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .build();
    var user2 = new User.Builder(USER_ID_2)
        .withName(NAME_2)
        .withEmailAddress(EMAIL_2)
        .build();
    var users = List.of(user1, user2);
    when(userDAO.batchGetUser(List.of(USER_ID, USER_ID_2))).thenReturn(users);

    var result = userService.getUsers(List.of(USER_ID, USER_ID_2));

    Assertions.assertThat(result).containsExactlyInAnyOrder(user1, user2);
  }

  @Test
  public void getAvatar() throws Exception {
    when(cloudStore.getFile(String.format("users/%s/avatar.jpeg", USER_ID))).thenReturn(BYTES);

    var res = userService.getAvatar(USER_ID);

    assertThat(res).isEqualTo(BYTES);
  }

  @Test
  public void getAvatarThumbnail() throws Exception {
    when(cloudStore.getFile(String.format("users/%s/t-avatar.jpeg", USER_ID))).thenReturn(BYTES);

    var res = userService.getAvatarThumbnail(USER_ID);

    assertThat(res).isEqualTo(BYTES);
  }

  @Test
  public void getAvatarMicro() throws Exception {
    when(cloudStore.getFile(String.format("users/%s/m-avatar.jpeg", USER_ID))).thenReturn(BYTES);

    var res = userService.getAvatarMicro(USER_ID);

    assertThat(res).isEqualTo(BYTES);
  }

  @Test
  public void queryUserIdsByUsername() {
    var userIds = List.of(USER_ID, USER_ID_2);
    when(userDAO.queryUserIdsByUsername("abc", 10)).thenReturn(userIds);

    var user1 = new User.Builder(USER_ID).build();
    var user2 = new User.Builder(USER_ID_2).build();
    var users = List.of(user1, user2);
    when(userDAO.batchGetUser(userIds)).thenReturn(users);

    var result = userService.queryUsersByUsername("abc");

    Assertions.assertThat(result).isEqualTo(users);
  }

  @Test
  public void queryUserIdsByUsernameConvertsToLowerCase() {
    var userIds = List.of(USER_ID, USER_ID_2);
    when(userDAO.queryUserIdsByUsername("abc", 10)).thenReturn(userIds);

    var user1 = new User.Builder(USER_ID).build();
    var user2 = new User.Builder(USER_ID_2).build();
    var users = List.of(user1, user2);
    when(userDAO.batchGetUser(userIds)).thenReturn(users);

    var result = userService.queryUsersByUsername("ABC");

    Assertions.assertThat(result).isEqualTo(users);
  }

  private InputStream generateImageInputStream(int width, int height) throws Exception {
    var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    var outputStream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpeg", outputStream);

    return new ByteArrayInputStream(outputStream.toByteArray());
  }

  private void executeTaskRunnerFunction() {
    var captor = ArgumentCaptor.forClass(Runnable.class);
    verify(taskRunner).run(captor.capture());

    captor.getValue().run();
  }
}
