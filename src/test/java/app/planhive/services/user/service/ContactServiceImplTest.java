package app.planhive.services.user.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.EmailAddress;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;
import app.planhive.services.user.model.ContactWithUserView;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.UserPrivateView;
import app.planhive.services.user.notification.UserNotificationSender;
import app.planhive.services.user.persistence.ContactDAO;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.services.user.persistence.model.ContactUpdate;
import app.planhive.services.user.persistence.model.ContactUpdateComparisonOperator;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class ContactServiceImplTest {

  private static final String USER_ID = "1234";
  private static final String USER_ID_2 = "2345";
  private static final String USER_ID_3 = "3456";
  private static final String NAME = "Aldo";
  private static final EmailAddress EMAIL_1 = new EmailAddress("abc@planhive.app");
  private static final EmailAddress EMAIL_2 = new EmailAddress("def@planhive.app");
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final Instant TS_3 = new Instant(3);
  private static final User USER_1 = new User.Builder(USER_ID).build();
  private static final User USER_2 = new User.Builder(USER_ID_2).build();

  private UserDAO userDAO;
  private ContactDAO contactDAO;
  private UserNotificationSender userNotificationSender;
  private ContactService contactService;

  @BeforeEach
  public void setUp() {
    userDAO = mock(UserDAO.class);
    contactDAO = mock(ContactDAO.class);
    userNotificationSender = mock(UserNotificationSender.class);

    contactService = new ContactServiceImpl(userDAO, contactDAO, userNotificationSender);
  }

  @Test
  public void batchGetContact() {
    var contact = new Contact(ContactState.CONNECTED, USER_ID, USER_ID_2, TS_1);

    when(contactDAO.batchGetItem(USER_ID, List.of(USER_ID_2))).thenReturn(List.of(contact));

    var result = contactService.batchGetContact(USER_ID, List.of(USER_ID_2));

    Assertions.assertThat(result).containsOnly(contact);
  }

  @Test
  public void getContactUpdates() {
    var contact1 = new Contact(ContactState.CONNECTED, USER_ID, USER_ID_2, TS_2);
    var contact2 = new Contact(ContactState.CONNECTED, USER_ID, USER_ID_3, TS_3);

    when(contactDAO.queryByUserIdWithTsFilter(USER_ID, TS_1))
        .thenReturn(List.of(contact1, contact2));

    var user2 = new User.Builder(USER_ID_2)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withLastUpdateTs(TS_1)
        .withLastAvatarUpdateTs(TS_1)
        .build();
    var user3 = new User.Builder(USER_ID_3)
        .withName(NAME)
        .withEmailAddress(EMAIL_2)
        .withLastUpdateTs(TS_3)
        .withLastAvatarUpdateTs(TS_3)
        .build();
    when(userDAO.getUser(USER_ID_2)).thenReturn(user2);
    when(userDAO.getUser(USER_ID_3)).thenReturn(user3);

    var result = contactService.getContactUpdates(USER_1, TS_1);

    Assertions.assertThat(result).hasSize(2);
    Assertions.assertThat(result).containsOnly(
        new ContactWithUserView(contact1, UserPrivateView.fromUser(user2)),
        new ContactWithUserView(contact2, UserPrivateView.fromUser(user3)));
  }

  @Test
  public void getContactUpdatesReturnsEmptyList() {
    when(contactDAO.queryByUserIdWithTsFilter(USER_ID, TS_3)).thenReturn(List.of());

    var result = contactService.getContactUpdates(USER_1, TS_3);

    Assertions.assertThat(result).isEmpty();
    verify(userDAO, never()).getUser(any());
  }

  @Test
  public void refreshContacts() {
    var contact = new Contact(ContactState.CONNECTED, USER_ID, USER_ID_2, TS_1);
    when(contactDAO.queryByUserId(USER_ID)).thenReturn(List.of(contact));

    contactService.refreshContacts(USER_ID, TS_2);

    verify(contactDAO).refreshLastUpdateTs(USER_ID_2, USER_ID, TS_2);
  }

  @Test
  public void createContactRequest() throws Exception {
    when(userDAO.getUser(USER_ID_2)).thenReturn(new User.Builder(USER_ID_2).build());

    var result = contactService.createContactRequest(USER_1, USER_ID_2, TS_1);

    var requesterContact = new Contact(ContactState.REQUESTED, USER_ID, USER_ID_2, TS_1);

    Assertions.assertThat(result).isEqualTo(requesterContact);

    verify(userNotificationSender).sendContactRequestNotification(USER_1, USER_ID_2);

    var inviteeContact = new Contact(ContactState.INVITED, USER_ID_2, USER_ID, TS_1);
    verify(contactDAO).updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.NOT_EQUALS),
        new ContactUpdate(inviteeContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.NOT_EQUALS)
    ));
  }

  @Test
  public void createContactRequestThrowsIfInviteeDoesNotExist() {
    assertThatThrownBy(() -> contactService.createContactRequest(USER_1, USER_ID_2, TS_1))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String.format("User with id %s does not exist", USER_ID_2));
  }

  @Test
  public void respondToContactRequestAccept() throws Exception {
    var result = contactService.respondToContactRequest(USER_ID, USER_2, true, TS_1);

    var inviteeContact = new Contact(ContactState.CONNECTED, USER_ID_2, USER_ID, TS_1);
    Assertions.assertThat(result).isEqualTo(inviteeContact);

    var requesterContact = new Contact(ContactState.CONNECTED, USER_ID, USER_ID_2, TS_1);
    verify(contactDAO).updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.REQUESTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.INVITED,
            ContactUpdateComparisonOperator.EQUALS)
    ));
    verify(userNotificationSender).sendContactRequestAcceptedNotification(USER_2, USER_ID);
  }

  @Test
  public void respondToContactRequestReject() throws Exception {
    var result = contactService.respondToContactRequest(USER_ID, USER_2, false, TS_1);

    var inviteeContact = new Contact(ContactState.REJECTED, USER_ID_2, USER_ID, TS_1);
    Assertions.assertThat(result).isEqualTo(inviteeContact);

    var requesterContact = new Contact(ContactState.REJECTED, USER_ID, USER_ID_2, TS_1);
    verify(contactDAO).updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.REQUESTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.INVITED,
            ContactUpdateComparisonOperator.EQUALS)
    ));
    verify(userNotificationSender, never()).sendContactRequestAcceptedNotification(any(), any());
  }

  @Test
  public void cancelContactRequest() throws Exception {
    var result = contactService.cancelContactRequest(USER_1, USER_ID_2, TS_1);

    var requesterContact = new Contact(ContactState.REJECTED, USER_ID, USER_ID_2, TS_1);
    Assertions.assertThat(result).isEqualTo(requesterContact);

    var inviteeContact = new Contact(ContactState.REJECTED, USER_ID_2, USER_ID, TS_1);
    verify(contactDAO).updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.REQUESTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.INVITED,
            ContactUpdateComparisonOperator.EQUALS)
    ));
  }

  @Test
  public void deleteContact() throws Exception {
    var result = contactService.deleteContact(USER_1, USER_ID_2, TS_1);

    var requesterContact = new Contact(ContactState.DELETED, USER_ID, USER_ID_2, TS_1);
    Assertions.assertThat(result).isEqualTo(requesterContact);

    var inviteeContact = new Contact(ContactState.DELETED, USER_ID_2, USER_ID, TS_1);
    verify(contactDAO).updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.EQUALS)
    ));
  }
}
