package app.planhive.services.user.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.notification.NotificationCategory;
import app.planhive.notification.NotificationClient;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.threading.TaskRunnerSync;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class NotificationServiceImplTest {

  private static final String TITLE = "Meg's 16th";
  private static final String BODY = "Peter is not attending.";
  private static final NotificationCategory CATEGORY = NotificationCategory.INVITES;
  private static final String RESOURCE_TYPE = "PARTICIPATION";
  private static final Map<String, String> ADDITIONAL_DATA = Map.of("Shut up", "Meg");
  private static final String USER_ID_1 = "1";
  private static final String USER_ID_2 = "2";
  private static final User USER_1 = new User.Builder(USER_ID_1).build();
  private static final Instant TS = Instant.now();

  private UserDAO userDAO;
  private FeedService feedService;
  private NotificationClient notificationClient;
  private NotificationService notificationService;


  @BeforeEach
  public void setUp() {
    userDAO = mock(UserDAO.class);
    feedService = mock(FeedService.class);
    notificationClient = mock(NotificationClient.class);
    var taskRunner = new TaskRunnerSync();

    notificationService = new NotificationServiceImpl(userDAO, feedService, notificationClient,
        taskRunner);
  }

  @Test
  public void updateNotificationToken() throws Exception {
    notificationService.updateNotificationToken(USER_1, "123", TS);

    var expectedUserUpdate = new User.Builder(USER_ID_1)
        .withNotificationToken("123")
        .build();
    verify(userDAO).updateUser(expectedUserUpdate, TS);
    verify(userDAO, never()).removeNotificationToken(any());
  }

  @Test
  public void updateNotificationTokenWithNullToken() throws Exception {
    notificationService.updateNotificationToken(USER_1, null, TS);

    verify(userDAO).removeNotificationToken(USER_ID_1);
    verify(userDAO, never()).updateUser(any(), any());
  }

  @Test
  public void updateNotificationCount() throws Exception {
    notificationService.updateNotificationCount(USER_ID_1, 5);

    verify(userDAO).updateNotificationCount(USER_ID_1, 5);
  }

  @Test
  public void sendNotificationByUserIdsWithCounterIncrement() {
    var user1 = new User.Builder(USER_ID_1).build();
    var user2 = new User.Builder(USER_ID_2)
        .withNotificationToken("tok")
        .withNotificationCount(5)
        .build();

    when(userDAO.batchGetUserWithNotificationCountIncrement(List.of(USER_ID_1, USER_ID_2)))
        .thenReturn(List.of(user1, user2));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService
        .sendNotificationByUserIds(notification, List.of(USER_ID_1, USER_ID_2), true);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 5), TITLE, BODY, expectedData, CATEGORY);
    verify(userDAO, never()).batchGetUser(any());
  }

  @Test
  public void sendNotificationByUserIdsWithoutCounterIncrement() {
    var user1 = new User.Builder(USER_ID_1).build();
    var user2 = new User.Builder(USER_ID_2)
        .withNotificationToken("tok")
        .withNotificationCount(5)
        .build();

    when(userDAO.batchGetUser(List.of(USER_ID_1, USER_ID_2))).thenReturn(List.of(user1, user2));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService
        .sendNotificationByUserIds(notification, List.of(USER_ID_1, USER_ID_2), false);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 5), TITLE, BODY, expectedData, CATEGORY);
    verify(userDAO, never()).batchGetUserWithNotificationCountIncrement(any());
  }

  @Test
  public void sendNotificationByNodeIdWithCounterIncrement() {
    var nodeId = new NodeId("1", NodeType.EVENT);

    var feed1 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_1)
        .setMute(false)
        .build();
    var feed2 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_2)
        .setMute(false)
        .build();

    when(feedService.getFeedsByResourceId(nodeId.toFeedItemKey()))
        .thenReturn(List.of(feed1, feed2));

    var user = new User.Builder(USER_ID_1)
        .withNotificationToken("tok")
        .withNotificationCount(8)
        .build();
    when(userDAO.batchGetUserWithNotificationCountIncrement(List.of(USER_ID_1)))
        .thenReturn(List.of(user));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService.sendNotificationByNodeId(notification, nodeId, List.of(USER_ID_2), true);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 8), TITLE, BODY, expectedData, CATEGORY);
    verify(userDAO, never()).batchGetUser(any());
  }

  @Test
  public void sendNotificationByNodeIdWithoutCounterIncrement() {
    var nodeId = new NodeId("1", NodeType.EVENT);

    var feed1 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_1)
        .setMute(false)
        .build();
    var feed2 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_2)
        .setMute(false)
        .build();

    when(feedService.getFeedsByResourceId(nodeId.toFeedItemKey()))
        .thenReturn(List.of(feed1, feed2));

    var user = new User.Builder(USER_ID_1)
        .withNotificationToken("tok")
        .withNotificationCount(8)
        .build();
    when(userDAO.batchGetUser(List.of(USER_ID_1))).thenReturn(List.of(user));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService.sendNotificationByNodeId(notification, nodeId, List.of(USER_ID_2), false);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 8), TITLE, BODY, expectedData, CATEGORY);
    verify(userDAO, never()).batchGetUserWithNotificationCountIncrement(any());
  }

  @Test
  public void sendNotificationByNodeIdFiltersDuplicateTokens() {
    var nodeId = new NodeId("1", NodeType.EVENT);

    var feed1 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_1)
        .setMute(false)
        .build();
    var feed2 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_2)
        .setMute(false)
        .build();

    when(feedService.getFeedsByResourceId(nodeId.toFeedItemKey()))
        .thenReturn(List.of(feed1, feed2));

    var user1 = new User.Builder(USER_ID_1)
        .withNotificationToken("tok")
        .withNotificationCount(8)
        .build();
    var user2 = new User.Builder(USER_ID_2)
        .withNotificationToken("tok")
        .withNotificationCount(8)
        .build();
    when(userDAO.batchGetUser(List.of(USER_ID_1, USER_ID_2))).thenReturn(List.of(user1, user2));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService.sendNotificationByNodeId(notification, nodeId, List.of("incognito"), false);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 8), TITLE, BODY, expectedData, CATEGORY);
    verify(userDAO, never()).batchGetUserWithNotificationCountIncrement(any());
  }

  @Test
  public void sendNotificationByNodeIdFiltersIfOnMute() {
    var nodeId = new NodeId("1", NodeType.EVENT);

    var feed1 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_1)
        .setMute(false)
        .build();
    var feed2 = new Feed.Builder(nodeId.toFeedItemKey(), USER_ID_2)
        .setMute(true)
        .build();

    when(feedService.getFeedsByResourceId(nodeId.toFeedItemKey()))
        .thenReturn(List.of(feed1, feed2));

    var user = new User.Builder(USER_ID_1)
        .withNotificationToken("tok")
        .withNotificationCount(5)
        .build();
    when(userDAO.batchGetUserWithNotificationCountIncrement(List.of(USER_ID_1)))
        .thenReturn(List.of(user));

    var notification = new Notification.Builder(TITLE, BODY, CATEGORY)
        .withResourceType(RESOURCE_TYPE)
        .withAdditionalData(ADDITIONAL_DATA)
        .build();

    notificationService.sendNotificationByNodeId(notification, nodeId, List.of(), true);

    var expectedData = Map.of("typ", RESOURCE_TYPE, "Shut up", "Meg");
    verify(notificationClient)
        .sendNotifications(Map.of("tok", 5), TITLE, BODY, expectedData, CATEGORY);
  }
}
