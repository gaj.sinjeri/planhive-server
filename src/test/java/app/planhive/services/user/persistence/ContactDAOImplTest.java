package app.planhive.services.user.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.exception.InvalidOperationException;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;
import app.planhive.services.user.persistence.model.ContactUpdate;
import app.planhive.services.user.persistence.model.ContactUpdateComparisonOperator;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
public class ContactDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final Instant TS_1 = new Instant(5);
  private static final Instant TS_2 = Instant.now();

  private DynamoDbTestUtils dynamoDbTestUtils;
  private ContactDAOImpl contactDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getAmazonDynamoDB()).thenReturn(dynamoDbTestUtils.getAmazonDynamoDB());
    when(dynamoDBClientMock.getDynamoDB()).thenReturn(dynamoDbTestUtils.getDynamoDB());
    when(dynamoDBClientMock.getTable()).thenReturn(dynamoDbTestUtils.getTable());

    contactDAO = new ContactDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void updateContactsWhenItemsDoNotExist() throws Exception {
    var userIdA = UUID.randomUUID().toString();
    var userIdB = UUID.randomUUID().toString();

    var contactA = new Contact(ContactState.CONNECTED, userIdA, userIdB, TS_2);
    var contactB = new Contact(ContactState.CONNECTED, userIdB, userIdA, TS_2);

    var contactUpdateA = new ContactUpdate(contactA, ContactState.REQUESTED,
        ContactUpdateComparisonOperator.NOT_EQUALS);
    var contactUpdateB = new ContactUpdate(contactB, ContactState.CONNECTED,
        ContactUpdateComparisonOperator.NOT_EQUALS);

    contactDAO.updateContacts(List.of(contactUpdateA, contactUpdateB));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userIdA,
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", userIdB)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userIdB,
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", userIdA))
    ));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(userIdA, userIdB);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("ct|%s", userIdB), String.format("ct|%s", userIdA));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(storedItems).extracting(it -> it.getString("ConState"))
        .containsOnly(ContactState.CONNECTED.name());
  }

  @Test
  public void updateContactsWhenItemsExist() throws Exception {
    var contactA = new Contact(ContactState.CONNECTED, "3", "friend-7", TS_2);
    var contactB = new Contact(ContactState.CONNECTED, "4", "friend-8", TS_2);

    var contactUpdateA = new ContactUpdate(contactA, ContactState.REQUESTED,
        ContactUpdateComparisonOperator.EQUALS);
    var contactUpdateB = new ContactUpdate(contactB, ContactState.CONNECTED,
        ContactUpdateComparisonOperator.NOT_EQUALS);

    contactDAO.updateContacts(List.of(contactUpdateA, contactUpdateB));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", "friend-7")),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", "friend-8"))
    ));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("3", "4");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("ct|%s", "friend-7"), String.format("ct|%s", "friend-8"));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(storedItems).extracting(it -> it.getString("ConState"))
        .containsOnly(ContactState.CONNECTED.name());
  }

  @Test
  public void updateContactsNoCondition() throws Exception {
    var userId = UUID.randomUUID().toString();
    var contact = new Contact(ContactState.CONNECTED, userId, "x", TS_2);

    var contactUpdate = new ContactUpdate(contact);

    contactDAO.updateContacts(List.of(contactUpdate));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", "x"))
    ));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(userId);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("ct|%s", "x"));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(storedItems).extracting(it -> it.getString("ConState"))
        .containsOnly(ContactState.CONNECTED.name());
  }

  @Test
  public void updateContactsThrowsIfConditionFails() {
    var contact = new Contact(ContactState.CONNECTED, "123", "friend-1", TS_2);

    var contactUpdate = new ContactUpdate(contact, ContactState.CONNECTED,
        ContactUpdateComparisonOperator.NOT_EQUALS);

    assertThatThrownBy(() -> contactDAO.updateContacts(List.of(contactUpdate)))
        .isInstanceOf(InvalidOperationException.class)
        .hasMessageContaining(String.format("%s condition(s) failed. Could not update contacts "
            + "from update items: %s", 1, List.of(contactUpdate)));
  }

  @Test
  public void refreshLastUpdateTs() {
    contactDAO.refreshLastUpdateTs("6", "friend-11", TS_2);

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "6",
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", "friend-11"))
    ));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("6");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("ct|%s", "friend-11"));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(storedItems).extracting(it -> it.getString("ConState"))
        .containsOnly(ContactState.REQUESTED.name());
  }

  @Test
  public void refreshLastUpdateTsDoesNotUpdateIfTsBefore() {
    contactDAO.refreshLastUpdateTs("123", "friend-2", TS_1);

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "123",
            DynamoDBClient.PRIMARY_KEY_R, String.format("ct|%s", "friend-2"))
    ));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("123");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("ct|%s", "friend-2"));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(storedItems).extracting(it -> it.getString("ConState"))
        .containsOnly(ContactState.CONNECTED.name());
  }

  @Test
  public void queryByUserIdReturnsItems() {
    var res = contactDAO.queryByUserId("123");

    Assertions.assertThat(res).extracting(Contact::getContactState).containsOnly(ContactState.CONNECTED);
    Assertions.assertThat(res).extracting(Contact::getUserId).containsOnly("123");
    Assertions.assertThat(res).extracting(Contact::getContactId).containsOnly("friend-1", "friend-2");
    Assertions.assertThat(res).extracting(Contact::getLastUpdateTs).containsOnly(TS_1, TS_2);
  }

  @Test
  public void queryByUserIdReturnsEmpty() {
    var res = contactDAO.queryByUserId("incognito");

    Assertions.assertThat(res).isEmpty();
  }

  @Test
  public void queryByUserIdThrowsIfIdEmpty() {
    assertThatThrownBy(() -> contactDAO.queryByUserId(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void queryByUserIdWithTsFilter() {
    var res = contactDAO.queryByUserIdWithTsFilter("123", TS_1);

    Assertions.assertThat(res).extracting(Contact::getContactState).containsOnly(ContactState.CONNECTED);
    Assertions.assertThat(res).extracting(Contact::getUserId).containsOnly("123");
    Assertions.assertThat(res).extracting(Contact::getContactId).containsOnly("friend-2");
    Assertions.assertThat(res).extracting(Contact::getLastUpdateTs).containsOnly(TS_2);
  }

  @Test
  public void batchGetItem() {
    var result = contactDAO.batchGetItem("123", List.of("friend-1", "friend-2"));

    Assertions.assertThat(result).hasSize(2);
    Assertions.assertThat(result).extracting(Contact::getContactState).containsOnly(ContactState.CONNECTED);
    Assertions.assertThat(result).extracting(Contact::getUserId).containsOnly("123");
    Assertions
        .assertThat(result).extracting(Contact::getContactId).containsOnly("friend-1", "friend-2");
    Assertions.assertThat(result).extracting(Contact::getLastUpdateTs).containsOnly(TS_1, TS_2);
  }

  @Test
  public void batchGetItemIfContactIdsEmpty() {
    var result = contactDAO.batchGetItem("123", List.of());

    Assertions.assertThat(result).isEmpty();
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "123",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-1")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_1.getMillis(), 36)))
        .withString("ConState", ContactState.CONNECTED.name()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "123",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-2")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("ConState", ContactState.CONNECTED.name()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-7")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("ConState", ContactState.REQUESTED.name()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-8")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("ConState", ContactState.REQUESTED.name()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "5",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-10")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("ConState", ContactState.REQUESTED.name()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "6",
            DynamoDBClient.PRIMARY_KEY_R, "ct|friend-11")
        .withString(DynamoDBClient.LSI1_R,
            String.format("ct|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("ConState", ContactState.REQUESTED.name()));

    return items;
  }
}
