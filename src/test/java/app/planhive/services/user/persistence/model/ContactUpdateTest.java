package app.planhive.services.user.persistence.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class ContactUpdateTest {

  private static final Contact CONTACT = new Contact(ContactState.CONNECTED, "A", "B",
      Instant.now());
  private static final ContactState CONDITION = ContactState.REQUESTED;
  private static final ContactUpdateComparisonOperator OPERATOR =
      ContactUpdateComparisonOperator.EQUALS;

  @Test
  public void constructor() {
    var obj = new ContactUpdate(CONTACT);

    Assertions.assertThat(obj.getContact()).isEqualTo(CONTACT);
    Assertions.assertThat(obj.getCondition()).isNull();
    assertThat(obj.getOperator()).isNull();
  }

  @Test
  public void constructorWithCondition() {
    var obj = new ContactUpdate(CONTACT, CONDITION, OPERATOR);

    Assertions.assertThat(obj.getContact()).isEqualTo(CONTACT);
    Assertions.assertThat(obj.getCondition()).isEqualTo(CONDITION);
    assertThat(obj.getOperator()).isEqualTo(OPERATOR);
  }

  @Test
  public void constructorThrowsIfContactNull() {
    assertThatThrownBy(() -> new ContactUpdate(null)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfConditionNull() {
    assertThatThrownBy(() -> new ContactUpdate(CONTACT, null, OPERATOR))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfOperatorNull() {
    assertThatThrownBy(() -> new ContactUpdate(CONTACT, CONDITION, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new ContactUpdate(CONTACT, CONDITION, OPERATOR);
    var obj2 = new ContactUpdate(CONTACT, CONDITION, OPERATOR);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ContactUpdate(CONTACT, CONDITION, OPERATOR);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ContactUpdate(CONTACT, CONDITION, OPERATOR);
    var obj2 = new ContactUpdate(CONTACT);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
