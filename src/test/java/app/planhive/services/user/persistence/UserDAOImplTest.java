package app.planhive.services.user.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.User.Builder;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
public class UserDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final String SESSION_ID = "fe09420f32krm";
  private static final String NOTIFICATION_TOKEN = "24u9gh289h";
  private static final String NOTIFICATION_TOKEN_2 = "24u9gh289haha";
  private static final String USER_NAME = "abcbbb";
  private static final String USER_NAME_2 = "dodo";
  private static final String USER_NAME_3 = "abcaaa";
  private static final String NAME = "Aldo";
  private static final EmailAddress EMAIL_1 = new EmailAddress("user1@planhive.app");
  private static final EmailAddress EMAIL_2 = new EmailAddress("user2@planhive.app");
  private static final EmailAddress EMAIL_3 = new EmailAddress("user3@planhive.app");
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final Instant TS_3 = new Instant(3);

  private DynamoDbTestUtils dynamoDbTestUtils;
  private UserDAOImpl userDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getAmazonDynamoDB()).thenReturn(dynamoDbTestUtils.getAmazonDynamoDB());
    when(dynamoDBClientMock.getDynamoDB()).thenReturn(dynamoDbTestUtils.getDynamoDB());
    when(dynamoDBClientMock.getTable()).thenReturn(dynamoDbTestUtils.getTable());

    userDAO = new UserDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void putItemReturnsUserItem() throws ResourceAlreadyExistsException {
    var userId = UUID.randomUUID().toString();
    var username = "aaa.aaa";
    var email = new EmailAddress("superuser@planhive.app");

    var user = new Builder(userId)
        .withSessionId(SESSION_ID)
        .withNotificationCount(0)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    var result = userDAO.putItem(user);

    assertThat(result.getId()).isEqualTo(userId);
    assertThat(result.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(result.getNotificationToken()).isNull();
    assertThat(result.getNotificationCount()).isEqualTo(0);
    assertThat(result.getUsername()).isEqualTo(username);
    assertThat(result.getName()).isEqualTo(NAME);
    assertThat(result.getEmailAddress()).isEqualTo(email);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(result.getLastAvatarUpdateTs()).isEqualTo(TS_3);
    assertThat(result.isDeleted()).isFalse();
  }

  @Test
  public void putItemStoresUserItem() throws ResourceAlreadyExistsException {
    var userId = UUID.randomUUID().toString();
    var username = "aaa.aab";
    var email = new EmailAddress("random@email.com");

    var user = new Builder(userId)
        .withSessionId(SESSION_ID)
        .withNotificationCount(0)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    userDAO.putItem(user);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userId,
            DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(userId);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.getString("Username")).containsOnly(username);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(email.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(0);
    assertThat(storedItems).extracting(it -> it.hasAttribute("NotTok")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.hasAttribute("Del")).containsOnly(false);
  }

  @Test
  public void putItemStoresUsernameItem() throws ResourceAlreadyExistsException {
    var userId = UUID.randomUUID().toString();
    var username = "aaa.aac";
    var email = new EmailAddress("random2@email.com");

    var user = new Builder(userId)
        .withNotificationCount(0)
        .withSessionId(SESSION_ID)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    userDAO.putItem(user);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, username.substring(0, 3),
            DynamoDBClient.PRIMARY_KEY_R, String.format("username|%s", username.substring(3)))));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(username.substring(0, 3));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("username|%s", username.substring(3)));
    assertThat(storedItems).extracting(it -> it.getString("UserId")).containsOnly(userId);
  }

  @Test
  public void putItemStoresUserEmailItem() throws ResourceAlreadyExistsException {
    var userId = UUID.randomUUID().toString();
    var username = "aaa.aad";
    var email = new EmailAddress("random3@email.com");

    var user = new Builder(userId)
        .withNotificationCount(0)
        .withSessionId(SESSION_ID)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    userDAO.putItem(user);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, email.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, String.format("email|%s", userId))));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(email.getValue());
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("email|%s", userId));
  }

  @Test
  public void putItemThrowsUserExceptionIfUserItemAlreadyExistsInDB() {
    var username = "aaa.aae";
    var email = new EmailAddress("random4@email.com");

    var item = new Builder("1")
        .withNotificationCount(0)
        .withSessionId(SESSION_ID)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    assertThatThrownBy(() -> userDAO.putItem(item))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining("User item already exists. User id 1");
  }

  @Test
  public void putItemThrowsUserExceptionIfUsernameItemAlreadyExistsInDB() {
    var userId = UUID.randomUUID().toString();
    var email = new EmailAddress("random5@email.com");

    var item = new Builder(userId)
        .withNotificationCount(0)
        .withSessionId(SESSION_ID)
        .withUsername(USER_NAME)
        .withName(NAME)
        .withEmailAddress(email)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    assertThatThrownBy(() -> userDAO.putItem(item))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining(
            String.format("Username item already exists. Username %s", USER_NAME));
  }

  @Test
  public void putItemThrowsUserExceptionIfUserEmailItemAlreadyExistsInDB() {
    var username = "aaa.aaf";

    var item = new Builder("111")
        .withNotificationCount(0)
        .withSessionId(SESSION_ID)
        .withUsername(username)
        .withName(NAME)
        .withEmailAddress(EMAIL_1)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withLastAvatarUpdateTs(TS_3)
        .build();

    assertThatThrownBy(() -> userDAO.putItem(item))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining(
            String.format("User email item already exists. Email %s", EMAIL_1.getValue()));
  }

  @Test
  public void updateUsername() throws Exception {
    var username = "baa.aaa";

    var result = userDAO.updateUsername("2", USER_NAME_2, username, TS_3);

    assertThat(result).isEqualTo(username);

    var storedUserItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "2",
            DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedUserItems.size()).isEqualTo(1);
    assertThat(storedUserItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("2");
    assertThat(storedUserItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedUserItems).extracting(it -> it.getString("Username")).containsOnly(username);
    assertThat(storedUserItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedUserItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedUserItems).extracting(it -> it.getString("NotTok"))
        .containsOnly(NOTIFICATION_TOKEN);
    assertThat(storedUserItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedUserItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedUserItems).extracting(it -> it.getLong("Ts-LU"))
        .containsOnly(TS_3.getMillis());
    assertThat(storedUserItems).extracting(it -> it.getLong("Ts-LAU"))
        .containsOnly(TS_3.getMillis());
    assertThat(storedUserItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);

    var storedUsernameItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
                DynamoDBClient.PRIMARY_KEY_H, username.substring(0, 3),
                DynamoDBClient.PRIMARY_KEY_R, String.format("username|%s", username.substring(3))),
            new PrimaryKey(
                DynamoDBClient.PRIMARY_KEY_H, USER_NAME_2.substring(0, 3),
                DynamoDBClient.PRIMARY_KEY_R,
                String.format("username|%s", USER_NAME_2.substring(3)))));

    assertThat(storedUsernameItems.size()).isEqualTo(1);
    assertThat(storedUsernameItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(username.substring(0, 3));
    assertThat(storedUsernameItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("username|%s", username.substring(3)));
    assertThat(storedUsernameItems).extracting(it -> it.getString("UserId")).containsOnly("2");
  }

  @Test
  public void updateUsernameThrowsIfUserItemDoesNotExist() {
    assertThatThrownBy(() -> userDAO.updateUsername("missing", USER_NAME, "baa.aab", TS_3))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("No user with id %s exists", "missing"));
  }

  @Test
  public void updateUsernameThrowsIfOldUsernameItemDoesNotExist() {
    assertThatThrownBy(() -> userDAO.updateUsername("6", "not_here", "baa.aab", TS_3))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("Username item not found. Username %s", "not_here"));
  }

  @Test
  public void updateUsernameThrowsIfNewUsernameItemExists() {
    assertThatThrownBy(() -> userDAO.updateUsername("2", USER_NAME_2, USER_NAME, TS_3))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining(
            String.format("Username item already exists. Username %s", USER_NAME));
  }

  @Test
  public void updateUser() throws Exception {
    var newSessionId = "a!b@c#";
    var newNotificationToken = "19283756";
    var newName = "Percival";
    var newTs = Instant.now();

    var user = new Builder("3")
        .withSessionId(newSessionId)
        .withNotificationToken(newNotificationToken)
        .withName(newName)
        .withLastAvatarUpdateTs(newTs)
        .build();
    var result = userDAO.updateUser(user, newTs);

    assertThat(result.getId()).isEqualTo("3");
    assertThat(result.getSessionId()).isEqualTo(newSessionId);
    assertThat(result.getNotificationToken()).isEqualTo(newNotificationToken);
    assertThat(result.getNotificationCount()).isEqualTo(0);
    assertThat(result.getUsername()).isEqualTo(USER_NAME_2);
    assertThat(result.getName()).isEqualTo(newName);
    assertThat(result.getEmailAddress()).isEqualTo(EMAIL_1);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(newTs);
    assertThat(result.getLastAvatarUpdateTs()).isEqualTo(newTs);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("3");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.getString("Username")).containsOnly(USER_NAME_2);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(newSessionId);
    assertThat(storedItems).extracting(it -> it.getString("NotTok"))
        .containsOnly(newNotificationToken);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(0);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(newName);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(newTs.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(newTs.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  @Test
  public void updateUserRequiredFieldsOnly() throws Exception {
    var user = new Builder("4").build();
    var result = userDAO.updateUser(user, TS_3);

    assertThat(result.getId()).isEqualTo("4");
    assertThat(result.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(result.getNotificationToken()).isEqualTo(NOTIFICATION_TOKEN);
    assertThat(result.getNotificationCount()).isEqualTo(7);
    assertThat(result.getUsername()).isEqualTo(USER_NAME_2);
    assertThat(result.getName()).isEqualTo(NAME);
    assertThat(result.getEmailAddress()).isEqualTo(EMAIL_1);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_3);
    assertThat(result.getLastAvatarUpdateTs()).isEqualTo(TS_3);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("4");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.getString("Username")).containsOnly(USER_NAME_2);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedItems).extracting(it -> it.getString("NotTok"))
        .containsOnly(NOTIFICATION_TOKEN);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(7);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  @Test
  public void updateUserThrowsIfItemDoesNotExist() {
    assertThatThrownBy(() -> userDAO.updateUser(new Builder("missing").build(), TS_3))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(
            "Attempted to update user with id missing, but the item does not exist");
  }

  @Test
  public void getUser() {
    var result = userDAO.getUser("1");

    assertThat(result.getId()).isEqualTo("1");
    assertThat(result.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(result.getUsername()).isEqualTo(USER_NAME);
    assertThat(result.getNotificationToken()).isEqualTo(NOTIFICATION_TOKEN);
    assertThat(result.getName()).isEqualTo(NAME);
    assertThat(result.getEmailAddress()).isEqualTo(EMAIL_1);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(result.getLastAvatarUpdateTs()).isEqualTo(TS_3);
    assertThat(result.isDeleted()).isFalse();
  }

  @Test
  public void getUserRequiredAttributesOnly() {
    var result = userDAO.getUser("5");

    assertThat(result.getId()).isEqualTo("5");
    assertThat(result.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(result.getUsername()).isEqualTo(USER_NAME_2);
    assertThat(result.getNotificationToken()).isNull();
    assertThat(result.getNotificationCount()).isEqualTo(0);
    assertThat(result.getName()).isEqualTo(NAME);
    assertThat(result.getEmailAddress()).isEqualTo(EMAIL_1);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(result.getLastAvatarUpdateTs()).isEqualTo(TS_3);
    assertThat(result.isDeleted()).isFalse();
  }

  @Test
  public void getUserReturnsNullIfItemDoesNotExist() {
    var result = userDAO.getUser("missing");

    assertThat(result).isNull();
  }

  @Test
  public void batchGetUser() {
    var result = userDAO.batchGetUser(List.of("1", "5"));

    assertThat(result.size()).isEqualTo(2);
    assertThat(result).extracting(User::getId).containsOnly("1", "5");
    assertThat(result).extracting(User::getSessionId).containsOnly(SESSION_ID);
    assertThat(result).extracting(User::getNotificationToken)
        .containsOnly(NOTIFICATION_TOKEN, null);
    assertThat(result).extracting(User::getUsername).containsOnly(USER_NAME, USER_NAME_2);
    assertThat(result).extracting(User::getName).containsOnly(NAME);
    assertThat(result).extracting(User::getEmailAddress).containsOnly(EMAIL_1);
    assertThat(result).extracting(User::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(User::getLastUpdateTs).containsOnly(TS_2);
    assertThat(result).extracting(User::getLastAvatarUpdateTs).containsOnly(TS_3);
  }

  @Test
  public void batchGetUserReturnsEmptyIfNoUsersFound() {
    var result = userDAO.batchGetUser(List.of("missing"));

    assertThat(result).isEmpty();
  }

  @Test
  public void batchGetUserEmptyList() {
    var result = userDAO.batchGetUser(List.of());

    assertThat(result).isEmpty();
  }

  @Test
  public void getUserIdFromUsername() {
    var result = userDAO.getUserId(USER_NAME);

    assertThat(result).isEqualTo("1");
  }

  @Test
  public void getUserIdFromUsernameReturnsNullIfUsernameDoesNotExist() {
    var result = userDAO.getUserId("missing");

    assertThat(result).isNull();
  }

  @Test
  public void getUserIdFromEmail() {
    var result = userDAO.getUserId(EMAIL_1);

    assertThat(result).isEqualTo("111");
  }

  @Test
  public void getUserIdFromEmailReturnsNullIfEmailDoesNotExist() {
    var result = userDAO.getUserId(new EmailAddress("ghost@planhive.app"));

    assertThat(result).isNull();
  }

  @Test
  public void getUserIdFromEmailThrowsIfMultipleUserIdsWithSameEmail() {
    assertThatThrownBy(() -> userDAO.getUserId(EMAIL_2))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("Query returned more than 1 user email item "
            + "with email %s", EMAIL_2.getValue()));
  }

  @Test
  public void queryUserIdsByUsername() {
    var result = userDAO.queryUserIdsByUsername("abc", 10);

    assertThat(result).hasSize(2);
    assertThat(result.get(0)).isEqualTo("u1");
    assertThat(result.get(1)).isEqualTo("1");
  }

  @Test
  public void queryUserIdsByUsernameLimitsResultSize() {
    var result = userDAO.queryUserIdsByUsername("abc", 1);

    assertThat(result).hasSize(1);
    assertThat(result).containsOnly("u1");
  }

  @Test
  public void queryUserIdsByUsernameThrowsIfBeginsWithTooShort() {
    assertThatThrownBy(() -> userDAO.queryUserIdsByUsername("ab", 1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Username query length must be at least 3 characters long");
  }

  @Test
  public void updateNotificationCount() throws Exception {
    userDAO.updateNotificationCount("9", 205);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "9",
            DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("9");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.getString("Username")).containsOnly(USER_NAME_2);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedItems).extracting(it -> it.getString("NotTok"))
        .containsOnly(NOTIFICATION_TOKEN);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(205);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  @Test
  public void updateNotificationCountThrowsIfUserDoesNotExist() {
    assertThatThrownBy(() -> userDAO.updateNotificationCount("invisible", 11))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining("Attempted to update notification count for user with id invisible, "
            + "but the item does not exist");
  }

  @Test
  public void batchGetUserWithNotificationCountIncrement() {
    var result = userDAO.batchGetUserWithNotificationCountIncrement(List.of("7", "8"));

    assertThat(result).hasSize(2);
    assertThat(result).extracting(User::getId).containsOnly("7", "8");
    assertThat(result).extracting(User::getSessionId).containsOnly(SESSION_ID);
    assertThat(result).extracting(User::getNotificationToken)
        .containsOnly(NOTIFICATION_TOKEN, NOTIFICATION_TOKEN_2);
    assertThat(result).extracting(User::getNotificationCount).containsOnly(1, 2);
    assertThat(result).extracting(User::getUsername).containsOnly(USER_NAME, USER_NAME_2);
    assertThat(result).extracting(User::getName).containsOnly(NAME);
    assertThat(result).extracting(User::getEmailAddress).containsOnly(EMAIL_1);
    assertThat(result).extracting(User::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(User::getLastUpdateTs).containsOnly(TS_2);
    assertThat(result).extracting(User::getLastAvatarUpdateTs).containsOnly(TS_3);

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(
            new PrimaryKey(
                DynamoDBClient.PRIMARY_KEY_H, "7",
                DynamoDBClient.PRIMARY_KEY_R, "user"),
            new PrimaryKey(
                DynamoDBClient.PRIMARY_KEY_H, "8",
                DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("7", "8");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.getString("NotTok"))
        .containsOnly(NOTIFICATION_TOKEN, NOTIFICATION_TOKEN_2);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(1, 2);
    assertThat(storedItems).extracting(it -> it.getString("Username"))
        .containsOnly(USER_NAME, USER_NAME_2);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  @Test
  public void batchGetUserWithNotificationCountIncrementDoesNotThrowIfItemDoesNotExist() {
    var result = userDAO.batchGetUserWithNotificationCountIncrement(List.of("haha"));

    assertThat(result).isEmpty();
  }

  @Test
  public void removeNotificationToken() {
    userDAO.removeNotificationToken("10");

    var storedItems = dynamoDbTestUtils.batchGetItem(
        List.of(
            new PrimaryKey(
                DynamoDBClient.PRIMARY_KEY_H, "10",
                DynamoDBClient.PRIMARY_KEY_R, "user")));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("10");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("user");
    assertThat(storedItems).extracting(it -> it.hasAttribute("NotTok"))
        .containsOnly(false);
    assertThat(storedItems).extracting(it -> it.getInt("NotC")).containsOnly(0);
    assertThat(storedItems).extracting(it -> it.getString("Username"))
        .containsOnly(USER_NAME_2);
    assertThat(storedItems).extracting(it -> it.getString("Email"))
        .containsOnly(EMAIL_1.getValue());
    assertThat(storedItems).extracting(it -> it.getString("Session")).containsOnly(SESSION_ID);
    assertThat(storedItems).extracting(it -> it.getString("Name")).containsOnly(NAME);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LU")).containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Ts-LAU")).containsOnly(TS_3.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, USER_NAME.substring(0, 3),
            DynamoDBClient.PRIMARY_KEY_R, String.format("username|%s", USER_NAME.substring(3)))
        .withString("UserId", "1"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, EMAIL_1.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "email|111"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "2",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, USER_NAME_2.substring(0, 3),
            DynamoDBClient.PRIMARY_KEY_R, String.format("username|%s", USER_NAME_2.substring(3)))
        .withString("UserId", "2"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 7)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "5",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withInt("NotC", 0)
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, EMAIL_2.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "email|222"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, EMAIL_2.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "email|333"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, EMAIL_3.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "email|444"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "6",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withInt("NotC", 0)
        .withString("Username", "not_here")
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, USER_NAME_3.substring(0, 3),
            DynamoDBClient.PRIMARY_KEY_R, String.format("username|%s", USER_NAME_3.substring(3)))
        .withString("UserId", "u1"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "7",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Username", USER_NAME)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "8",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("NotTok", NOTIFICATION_TOKEN_2)
        .withInt("NotC", 1)
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "9",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "10",
            DynamoDBClient.PRIMARY_KEY_R, "user")
        .withString("Username", USER_NAME_2)
        .withString("Email", EMAIL_1.getValue())
        .withString("Session", SESSION_ID)
        .withString("NotTok", NOTIFICATION_TOKEN)
        .withInt("NotC", 0)
        .withString("Name", NAME)
        .withLong("Ts-C", TS_1.getMillis())
        .withLong("Ts-LU", TS_2.getMillis())
        .withLong("Ts-LAU", TS_3.getMillis())
        .withBoolean("Del", false));

    return items;
  }
}
