package app.planhive.services.node.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node.Builder;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.persistence.NodeDAO;
import app.planhive.services.user.model.User;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class NodeServiceImplTest {

  private static final String UUID = "123";
  private static final String USER_ID_1 = "A1";
  private static final User USER_1 = new User.Builder(USER_ID_1).build();
  private static final String PERMISSION_GROUP_ID = "P1";
  private static final Instant TS_0 = new Instant(0);
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private NodeDAO nodeDao;
  private FeedService feedService;
  private NodeService nodeService;

  @BeforeEach
  public void setUp() {
    nodeDao = mock(NodeDAO.class);
    feedService = mock(FeedService.class);

    nodeService = new NodeServiceImpl(nodeDao, feedService);
  }

  @Test
  public void insertNodeStoresNode() throws Exception {
    var node = new Builder(new NodeId("1", NodeType.EVENT)).build();
    nodeService.insertNode(node);

    verify(nodeDao).putItem(node);
  }

  @Test
  public void insertNodeThrowsIfNodeNull() {
    assertThatThrownBy(() -> nodeService.insertNode(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void batchInsertNode() {
    var node1 = new Builder(new NodeId("1", NodeType.EVENT)).build();
    var node2 = new Builder(new NodeId("2", NodeType.EVENT)).build();
    var nodes = List.of(node1, node2);

    nodeService.batchInsertNode(nodes);

    verify(nodeDao).batchPutItem(nodes);
  }

  @Test
  public void insertNodeWithDataOnly() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId).build();

    when(nodeDao.insertNodeWithDataOnly(nodeId, USER_ID_1, "{}", TS_1)).thenReturn(node);

    var result = nodeService.insertNodeWithDataOnly(nodeId, USER_ID_1, "{}", TS_1);

    Assertions.assertThat(result).isEqualTo(node);
  }

  @Test
  public void updateNodeData() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId).build();
    var data = "{}";

    when(nodeDao.updateNodeData(nodeId, data, TS_1)).thenReturn(node);

    var result = nodeService.updateNodeData(nodeId, data, TS_1);

    Assertions.assertThat(result).isEqualTo(node);
  }

  @Test
  public void refreshLastUpdateTs() throws Exception {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId).build();

    when(nodeDao.refreshLastUpdateTs(nodeId, TS_1)).thenReturn(node);

    var result = nodeService.refreshLastUpdateTs(nodeId, TS_1);

    Assertions.assertThat(result).isEqualTo(node);
  }

  @Test
  public void getNodeOrThrowReturnsNode()
      throws ResourceNotFoundException, ResourceDeletedException {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData("{}")
        .setDeleted(false)
        .build();
    when(nodeDao.readItem(nodeId)).thenReturn(node);

    var result = nodeService.getNodeOrThrow(nodeId);

    Assertions.assertThat(result).isEqualTo(node);
  }

  @Test
  public void getNodeOrThrowThrowsIfNodeDoesNotExist() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    when(nodeDao.readItem(nodeId)).thenReturn(null);

    assertThatThrownBy(() -> nodeService
        .getNodeOrThrow(nodeId))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String
            .format("No %s node with id %s, parent id %s exists", nodeId.getType(), nodeId.getId(),
                nodeId.getParentId()));
  }

  @Test
  public void getNodeOrThrowThrowsIfNodeDeleted() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId)
        .setDeleted(true)
        .build();
    when(nodeDao.readItem(nodeId)).thenReturn(node);

    assertThatThrownBy(() -> nodeService
        .getNodeOrThrow(nodeId))
        .isInstanceOf(ResourceDeletedException.class)
        .hasMessageContaining(String
            .format("%s node with id %s, parent id %s has been deleted", nodeId.getType().name(),
                nodeId.getId(), nodeId.getParentId()))
        .extracting(ex -> ((ResourceDeletedException) ex).getResource()).isEqualTo(node);
  }

  @Test
  public void getChildNodesReturnsListOfNodes() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData("{}")
        .setDeleted(false)
        .build();
    when(nodeDao.queryByParentNodeId("1")).thenReturn(List.of(node));

    var result = nodeService.getChildNodes("1");

    Assertions.assertThat(result).containsOnly(node);
  }

  @Test
  public void getChildNodesWithTsRange() throws Exception {
    var parentNodeId = new NodeId("2", NodeType.EVENT);

    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var node = new Builder(nodeId)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .withData("{}")
        .setDeleted(false)
        .build();
    when(nodeDao.queryByParentNodeIdWithTsRange("2", TS_1, TS_2)).thenReturn(List.of(node));

    var feed = new Feed.Builder(parentNodeId.toFeedItemKey(), USER_ID_1)
        .withBeginTs(TS_1)
        .build();
    when(feedService.getFeedOrThrow(parentNodeId.toFeedItemKey(), USER_ID_1)).thenReturn(feed);

    var result = nodeService.getChildNodesWithTsRange(USER_1, parentNodeId, TS_2);

    Assertions.assertThat(result).containsOnly(node);
  }

  @Test
  public void getChildNodesWithTsRangeThrowsIfUnsupportedNodeType() {
    assertThatThrownBy(() -> nodeService
        .getChildNodesWithTsRange(USER_1, new NodeId("1", "2", NodeType.MESSAGE), TS_2))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("This operation does not support nodes of type %s",
            NodeType.MESSAGE.name()));
  }

  @Test
  public void getChildNodesWithTsRangeReturnsEmptyListIfBeginTsAfterToTs() throws Exception {
    var parentNodeId = new NodeId("2", NodeType.EVENT);

    var feed = new Feed.Builder(parentNodeId.toFeedItemKey(), USER_ID_1)
        .withBeginTs(TS_2)
        .build();
    when(feedService.getFeedOrThrow(parentNodeId.toFeedItemKey(), USER_ID_1)).thenReturn(feed);

    var result = nodeService.getChildNodesWithTsRange(USER_1, parentNodeId, TS_1);

    Assertions.assertThat(result).isEmpty();
  }

  @Test
  public void getUpdatesReturnsListOfParentAndChildNodes() {
    var feed = new Feed.Builder("EVENT|qwe321", "2")
        .withBeginTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    when(feedService.getFeedsForUser(USER_ID_1, NodeType.EVENT.name(), TS_1, 100))
        .thenReturn(List.of(feed));

    var nodeId = new NodeId("qwe321", NodeType.EVENT);
    var parentNode = new Builder(nodeId).build();
    when(nodeDao.batchGetItem(List.of(nodeId))).thenReturn(List.of(parentNode));

    var childNode = new Builder(new NodeId("Lelele", UUID, NodeType.PARTICIPATION)).build();
    when(nodeDao.queryByParentNodeIdWithTsFilter("qwe321", (TS_1))).thenReturn(List.of(childNode));

    var result = nodeService.getUpdates(USER_1, NodeType.EVENT, TS_1);

    Assertions.assertThat(result).containsOnly(parentNode, childNode);
  }

  @Test
  public void getUpdatesFiltersByLastUpdateTs() {
    var feed = new Feed.Builder("EVENT|qwe321", "2")
        .withBeginTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    when(feedService.getFeedsForUser(USER_ID_1, NodeType.EVENT.name(), TS_0, 100))
        .thenReturn(List.of(feed));

    var nodeId = new NodeId("qwe321", NodeType.EVENT);
    var parentNode = new Builder(nodeId).build();
    when(nodeDao.batchGetItem(List.of(nodeId))).thenReturn(List.of(parentNode));

    var childNode = new Builder(new NodeId("Lelele", UUID, NodeType.PARTICIPATION)).build();
    when(nodeDao.queryByParentNodeIdWithTsFilter("qwe321", TS_1)).thenReturn(List.of(childNode));

    var result = nodeService.getUpdates(USER_1, NodeType.EVENT, TS_0);

    Assertions.assertThat(result).containsOnly(parentNode, childNode);
  }

  @Test
  public void getUpdatesFiltersByBeginTs() {
    var feed = new Feed.Builder("EVENT|qwe321", "2")
        .withBeginTs(TS_2)
        .withLastUpdateTs(TS_1)
        .build();
    when(feedService.getFeedsForUser(USER_ID_1, NodeType.EVENT.name(), TS_0, 100))
        .thenReturn(List.of(feed));

    when(nodeDao.batchGetItem(List.of())).thenReturn(List.of());

    var result = nodeService.getUpdates(USER_1, NodeType.EVENT, TS_0);

    Assertions.assertThat(result).isEmpty();

    verify(nodeDao, never()).queryByParentNodeIdWithTsFilter(any(), any());
  }

  @Test
  public void getUpdatesThrowsIfUnsupportedNodeType() {
    assertThatThrownBy(() -> nodeService.getUpdates(USER_1, NodeType.PARTICIPANT, TS_1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("This operation does not support nodes of type %s",
            NodeType.PARTICIPANT.name()));
  }

  @Test
  public void getHistoricalData() {
    var feed = new Feed.Builder("EVENT|qwe321", "2")
        .withBeginTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    when(feedService.getHistoricalFeedsForUser(USER_ID_1, NodeType.EVENT.name(), TS_1))
        .thenReturn(List.of(feed));

    var nodeId = new NodeId("qwe321", NodeType.EVENT);
    var node = new Builder(nodeId).build();
    when(nodeDao.batchGetItem(List.of(nodeId))).thenReturn(List.of(node));

    var result = nodeService.getHistoricalData(USER_1, NodeType.EVENT, TS_1);

    Assertions.assertThat(result).containsOnly(node);
  }

  @Test
  public void getHistoricalDataFiltersByBeginTs() {
    var feed = new Feed.Builder("EVENT|qwe321", "2")
        .withBeginTs(TS_2)
        .withLastUpdateTs(TS_1)
        .build();
    when(feedService.getHistoricalFeedsForUser(USER_ID_1, NodeType.EVENT.name(), TS_0))
        .thenReturn(List.of(feed));

    when(nodeDao.batchGetItem(List.of())).thenReturn(List.of());

    var result = nodeService.getUpdates(USER_1, NodeType.EVENT, TS_0);

    Assertions.assertThat(result).isEmpty();
  }

  @Test
  public void getHistoricalDataThrowsIfUnsupportedNodeType() {
    assertThatThrownBy(() -> nodeService.getHistoricalData(USER_1, NodeType.PARTICIPANT, TS_1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("This operation does not support nodes of type %s",
            NodeType.PARTICIPANT.name()));
  }

  @Test
  public void queryNode() {
    var node = new Builder(new NodeId("1", "2", NodeType.ACTIVITY_REACTION)).build();
    when(nodeDao.queryNode("1", NodeType.ACTIVITY_REACTION, "USER")).thenReturn(List.of(node));

    var result = nodeService.queryNode("1", NodeType.ACTIVITY_REACTION, "USER");

    Assertions.assertThat(result).containsOnly(node);
  }

  @Test
  public void softDeleteNode() throws ResourceNotFoundException {
    var nodeId = new NodeId("qwe321", NodeType.EVENT);
    var node = new Builder(nodeId).build();

    when(nodeDao.softDeleteNode(nodeId, TS_1)).thenReturn(node);

    var result = nodeService.softDeleteNode(nodeId, TS_1);

    Assertions.assertThat(result).isEqualTo(node);
  }

  @Test
  public void batchSoftDeleteNode() throws Exception {
    var nodeId1 = new NodeId("1", NodeType.EVENT);
    var nodeId2 = new NodeId("2", NodeType.EVENT);
    var node1 = new Builder(nodeId1).build();
    var node2 = new Builder(nodeId2).build();
    when(nodeDao.softDeleteNode(nodeId1, TS_1)).thenReturn(node1);
    when(nodeDao.softDeleteNode(nodeId2, TS_1)).thenReturn(node2);

    var result = nodeService
        .batchSoftDeleteNode(List.of(nodeId1, nodeId2), TS_1);

    Assertions.assertThat(result).containsOnly(node1, node2);
  }

  @Test
  public void batchSoftDeleteNodesIgnoresExceptionIfItemDoesNotExist() throws Exception {
    var nodeId1 = new NodeId("1", NodeType.EVENT);
    var nodeId2 = new NodeId("2", NodeType.EVENT);
    var node1 = new Builder(nodeId1).build();
    when(nodeDao.softDeleteNode(nodeId1, TS_1)).thenReturn(node1);

    doThrow(new ResourceNotFoundException("")).when(nodeDao).softDeleteNode(nodeId2, TS_1);

    var result = nodeService
        .batchSoftDeleteNode(List.of(nodeId1, nodeId2), TS_1);

    Assertions.assertThat(result).containsOnly(node1);
  }

  @Test
  public void softDeleteChildNodesAsync() throws Exception {
    var nodeId1 = new NodeId("2", "1", NodeType.MESSAGE);
    var nodeId2 = new NodeId("3", "1", NodeType.MESSAGE);
    var node1 = new Builder(nodeId1)
        .withLastUpdateTs(TS_1)
        .build();
    var node2 = new Builder(nodeId2)
        .withLastUpdateTs(TS_2)
        .build();

    when(nodeDao.queryByParentNodeIdWithTsFilter("1", TS_0)).thenReturn(List.of(node1));
    when(nodeDao.queryByParentNodeIdWithTsFilter("1", TS_2)).thenReturn(List.of(node2));
    when(nodeDao.queryByParentNodeIdWithTsFilter("1", new Instant(3))).thenReturn(List.of());

    nodeService.softDeleteChildNodesAsync("1", TS_1);

    verify(nodeDao).softDeleteNode(nodeId1, TS_1);
    verify(nodeDao).softDeleteNode(nodeId2, TS_1);
  }

  @Test
  public void softDeleteChildNodesAsyncRethrowsAsInternalError() throws Exception {
    var nodeId = new NodeId("2", "1", NodeType.MESSAGE);
    var node = new Builder(nodeId).build();
    when(nodeDao.queryByParentNodeIdWithTsFilter("1", TS_0)).thenReturn(List.of(node));

    when(nodeDao.softDeleteNode(nodeId, TS_1)).thenThrow(new ResourceNotFoundException("A ghost"));

    assertThatThrownBy(() -> nodeService.softDeleteChildNodesAsync("1", TS_1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("A ghost");
  }
}
