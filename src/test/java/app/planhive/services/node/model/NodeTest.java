package app.planhive.services.node.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class NodeTest {

  private static final String ID = "123";
  private static final String PARENT_ID = "234";
  private static final NodeType TYPE = NodeType.TIMELINE;
  private static final String CREATOR_ID = "777";
  private static final String PERMISSION_GROUP_ID = "345";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final String DATA = "{\"Message\":\"Hello\"}";

  @Test
  public void constructor() {
    var nodeId = new NodeId(ID, PARENT_ID, TYPE);
    var obj = new Node.Builder(nodeId)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();

    assertThat(obj.getNodeId()).isEqualTo(nodeId);
    assertThat(obj.getCreatorId()).isEqualTo(CREATOR_ID);
    assertThat(obj.getPrivateNode()).isEqualTo(true);
    assertThat(obj.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(obj.getCreationTs()).isEqualTo(TS_1);
    assertThat(obj.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(obj.getData()).isEqualTo(DATA);
    assertThat(obj.getDeleted()).isEqualTo(true);
  }

  @Test
  public void builderThrowsIfNodeIdNull() {
    assertThatThrownBy(() -> new Node.Builder(null).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();
    var obj2 = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();
    var obj2 = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_1)
        .withData(DATA)
        .setDeleted(true)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = new Node.Builder(new NodeId(ID, PARENT_ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(true)
        .build();

    var mapper = new ObjectMapper();
    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format(
        "{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\",\"creatorId\":\"%s\","
            + "\"lastUpdateTs\":%s,\"privateNode\":%s,\"permissionGroupId\":\"%s\""
            + ",\"creationTs\":%s,\"data\":\"{\\\"Message\\\":\\\"Hello\\\"}\",\"deleted\":%s}", ID,
        PARENT_ID, TYPE.name(), CREATOR_ID, TS_2.getMillis(), true, PERMISSION_GROUP_ID,
        TS_1.getMillis(), true));
  }

  @Test
  public void serializeRequiredFieldsOnly() throws Exception {
    var obj = new Node.Builder(new NodeId(ID, TYPE))
        .withCreatorId(CREATOR_ID)
        .withLastUpdateTs(TS_2)
        .build();

    var mapper = new ObjectMapper();
    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format(
        "{\"id\":\"%s\",\"parentId\":%s,\"type\":\"%s\",\"creatorId\":\"%s\","
            + "\"lastUpdateTs\":%s}", ID, null, TYPE.name(), CREATOR_ID, TS_2.getMillis()));
  }
}
