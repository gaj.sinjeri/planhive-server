package app.planhive.services.node.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class NodeIdTest {

  private static final String ID = "123";
  private static final String PARENT_ID = "234";
  private static final NodeType TYPE = NodeType.TIMELINE;
  private static final String FEED_ITEM_KEY = String.format("%s|%s", TYPE.name(), ID);
  private static final String FEED_ITEM_KEY_WITH_PARENT = String
      .format("%s|%s|%s", TYPE.name(), PARENT_ID, ID);

  @Test
  public void constructor() {
    var obj = new NodeId(ID, TYPE);

    assertThat(obj.getId()).isEqualTo(ID);
    assertThat(obj.getParentId()).isNull();
    assertThat(obj.getType()).isEqualTo(TYPE);
  }

  @Test
  public void constructorWithParentId() {
    var obj = new NodeId(ID, PARENT_ID, TYPE);

    assertThat(obj.getId()).isEqualTo(ID);
    assertThat(obj.getParentId()).isEqualTo(PARENT_ID);
    assertThat(obj.getType()).isEqualTo(TYPE);
  }

  @Test
  public void constructorThrowsIfIdNull() {
    assertThatThrownBy(() -> new NodeId(null, PARENT_ID, TYPE))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfNodeTypeNull() {
    assertThatThrownBy(() -> new NodeId(ID, PARENT_ID, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfNodeIdIsEqualToParentId() {
    assertThatThrownBy(() -> new NodeId(ID, ID, TYPE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Node id cannot be the same as parent id");
  }

  @Test
  public void equality() {
    var obj1 = new NodeId(ID, PARENT_ID, TYPE);
    var obj2 = new NodeId(ID, PARENT_ID, TYPE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new NodeId(ID, PARENT_ID, TYPE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new NodeId(ID, PARENT_ID, TYPE);
    var obj2 = new NodeId("what where?", PARENT_ID, TYPE);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toFeedItemKey() {
    var nodeId = new NodeId(ID, TYPE);

    assertThat(nodeId.toFeedItemKey()).isEqualTo(FEED_ITEM_KEY);
  }

  @Test
  public void toFeedItemKeyWithParentId() {
    var nodeId = new NodeId(ID, PARENT_ID, TYPE);

    assertThat(nodeId.toFeedItemKey()).isEqualTo(FEED_ITEM_KEY_WITH_PARENT);
  }

  @Test
  public void fromFeedItemKey() {
    var node = NodeId.fromFeedItemKey(FEED_ITEM_KEY);

    assertThat(node).isEqualTo(new NodeId(ID, TYPE));
  }

  @Test
  public void fromFeedItemKeyWithParentId() {
    var node = NodeId.fromFeedItemKey(FEED_ITEM_KEY_WITH_PARENT);

    assertThat(node).isEqualTo(new NodeId(ID, PARENT_ID, TYPE));
  }
}
