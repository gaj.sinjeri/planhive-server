package app.planhive.services.node.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class NodeDataTest {

  private static final String NAME = "Birthday Party!";
  private static final String DESCRIPTION = "18!";
  private static final String JSON = String
      .format("{\"name\":\"%s\",\"description\":\"%s\"}", NAME, DESCRIPTION);

  private static class FakeNodeData extends NodeData {

    private String name;
    private String description;

    private FakeNodeData() {
    }

    public FakeNodeData(String name, String description) {
      this.name = name;
      this.description = description;
    }

    public String getName() {
      return name;
    }

    public String getDescription() {
      return description;
    }
  }


  @Test
  public void fromJson() {
    var obj = NodeData.fromJson(JSON, FakeNodeData.class);

    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
  }

  @Test
  public void toJson() {
    var obj = new FakeNodeData(NAME, DESCRIPTION);

    assertThat(obj.toJson()).isEqualTo(JSON);
  }
}
