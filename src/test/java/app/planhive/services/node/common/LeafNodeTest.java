package app.planhive.services.node.common;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class LeafNodeTest {

  @Test
  public void generateLeafNodeId() {
    var result = LeafNode.generateLeafNodeId("123", "abc");

    assertThat(result).isEqualTo("123|abc");
  }
}
