package app.planhive.services.node.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.controller.model.GetChildNodesRequest;
import app.planhive.services.node.controller.model.GetHistoricalDataRequest;
import app.planhive.services.node.controller.model.GetUpdatesRequest;
import app.planhive.services.node.model.Node.Builder;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.user.model.User;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class NodeControllerTest {

  private static final User USER = new User.Builder("123").build();
  private static final NodeType NODE_TYPE = NodeType.PARTICIPATION;
  private static final Instant TS = Instant.now();

  private NodeService nodeService;
  private NodeController nodeController;

  @BeforeEach
  public void setUp() {
    nodeService = mock(NodeService.class);
    nodeController = new NodeController(nodeService);
  }

  @Test
  public void getNodeSuccessfulCall() throws Exception {
    var nodeId = new NodeId("2", "1", NODE_TYPE);
    var node = new Builder(nodeId).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);

    var response = nodeController.getNode(USER, "1", NODE_TYPE, "2");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions.assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(node));
  }

  @Test
  public void getNodeSuccessfulCallWithoutParentId() throws Exception {
    var nodeId = new NodeId("2", NODE_TYPE);
    var node = new Builder(nodeId).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);

    var response = nodeController.getNode(USER, null, NODE_TYPE, "2");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions.assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(node));
  }

  @Test
  public void getNodeReturnsErrorIfResourceNotFound() throws Exception {
    var nodeId = new NodeId("2", NODE_TYPE);
    Mockito.doThrow(new ResourceNotFoundException("")).when(nodeService).getNodeOrThrow(nodeId);

    var response = nodeController.getNode(USER, null, NODE_TYPE, "2");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    Assertions.assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getNodeReturnsErrorIfResourceDeleted() throws Exception {
    var nodeId = new NodeId("2", NODE_TYPE);
    Mockito.doThrow(new ResourceDeletedException("", new Object()))
        .when(nodeService).getNodeOrThrow(nodeId);

    var response = nodeController.getNode(USER, null, NODE_TYPE, "2");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    Assertions.assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getUpdatesSuccessfulCall() {
    var node1 = new Builder(new NodeId("2", "1", NODE_TYPE)).build();
    var node2 = new Builder(new NodeId("3", "1", NODE_TYPE)).build();
    when(nodeService.getUpdates(USER, NODE_TYPE, TS)).thenReturn(List.of(node1, node2));

    var request = new GetUpdatesRequest(TS.getMillis());
    var response = nodeController.getUpdates(USER, NODE_TYPE, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions
        .assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node1, node2)));
  }

  @Test
  public void getHistoricalDataSuccessfulCall() {
    var node1 = new Builder(new NodeId("2", "1", NODE_TYPE)).build();
    var node2 = new Builder(new NodeId("3", "1", NODE_TYPE)).build();
    when(nodeService.getHistoricalData(USER, NODE_TYPE, TS)).thenReturn(List.of(node1, node2));

    var request = new GetHistoricalDataRequest(TS.getMillis());
    var response = nodeController.getHistoricalData(USER, NODE_TYPE, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions
        .assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node1, node2)));
  }

  @Test
  public void getChildNodesHistoricalSuccessfulCall() throws Exception {
    var parentNodeId = new NodeId("1", NODE_TYPE);

    var node1 = new Builder(new NodeId("2", "1", NODE_TYPE)).build();
    var node2 = new Builder(new NodeId("3", "1", NODE_TYPE)).build();
    when(nodeService.getChildNodesWithTsRange(USER, parentNodeId, TS))
        .thenReturn(List.of(node1, node2));

    var request = new GetChildNodesRequest(TS.getMillis());
    var response = nodeController.getChildNodesHistorical(USER, null, NODE_TYPE, "1", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions
        .assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node1, node2)));
  }

  @Test
  public void getChildNodesHistoricalSuccessfulCallWithParentId() throws Exception{
    var parentNodeId = new NodeId("1", "0", NODE_TYPE);

    var node1 = new Builder(new NodeId("2", "1", NODE_TYPE)).build();
    var node2 = new Builder(new NodeId("3", "1", NODE_TYPE)).build();
    when(nodeService.getChildNodesWithTsRange(USER, parentNodeId, TS))
        .thenReturn(List.of(node1, node2));

    var request = new GetChildNodesRequest(TS.getMillis());
    var response = nodeController.getChildNodesHistorical(USER, "0", NODE_TYPE, "1", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    Assertions
        .assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node1, node2)));
  }

  @Test
  public void getChildNodesHistoricalReturnsErrorIfResourceNotFound() throws Exception {
    var parentNodeId = new NodeId("1", NODE_TYPE);

    doThrow(new ResourceNotFoundException("")).when(nodeService).getChildNodesWithTsRange(USER, parentNodeId, TS);

    var request = new GetChildNodesRequest(TS.getMillis());
    var response = nodeController.getChildNodesHistorical(USER, null, NODE_TYPE, "1", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    Assertions.assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
