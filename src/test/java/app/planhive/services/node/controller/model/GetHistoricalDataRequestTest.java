package app.planhive.services.node.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class GetHistoricalDataRequestTest {

  private static final Long TS = Instant.now().getMillis();

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new GetHistoricalDataRequest(TS);

    assertThat(obj.getLastUpdateTs()).isEqualTo(TS);
  }

  @Test
  public void deserializationFailsIfLastUpdateTsMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", GetHistoricalDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'lastUpdateTs'");
  }

  @Test
  public void equality() {
    var obj1 = new GetHistoricalDataRequest(TS);
    var obj2 = new GetHistoricalDataRequest(TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new GetHistoricalDataRequest(TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new GetHistoricalDataRequest(TS);
    var obj2 = new GetHistoricalDataRequest(0L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
