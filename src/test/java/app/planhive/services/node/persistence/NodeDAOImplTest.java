package app.planhive.services.node.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.Node.Builder;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
public class NodeDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final String CREATOR_ID = "Tweety";
  private static final NodeType TYPE = NodeType.TIMELINE;
  private static final String PERMISSION_GROUP_ID = "sfgs-3414b45-53g-gefsd";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final Instant TS_3 = Instant.now();
  private static final String DATA = "{\"Name\":\"Birthday party!\"}";

  private DynamoDbTestUtils dynamoDbTestUtils;
  private NodeDAO nodeDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());
    var dynamoDB = dynamoDbTestUtils.getDynamoDB();
    var table = dynamoDbTestUtils.getTable();

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getDynamoDB()).thenReturn(dynamoDB);
    when(dynamoDBClientMock.getTable()).thenReturn(table);

    nodeDAO = new NodeDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void batchPut() {
    var parentId = UUID.randomUUID().toString();

    var id1 = UUID.randomUUID().toString();
    var nodeId1 = new NodeId(id1, parentId, TYPE);
    var node1 = new Builder(nodeId1)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(false)
        .build();

    var id2 = UUID.randomUUID().toString();
    var nodeId2 = new NodeId(id2, parentId, TYPE);
    var node2 = new Builder(nodeId2)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(false)
        .build();

    nodeDAO.batchPutItem(List.of(node1, node2));

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s|%s", "TIMELINE", id1)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s|%s", "TIMELINE", id2))));

    assertThat(retrievedItems.size()).isEqualTo(2);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(parentId);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s|%s", "TIMELINE", id1),
            String.format("node|%s|%s", "TIMELINE", id2));
    assertThat(retrievedItems).extracting(it -> it.getString("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.getBoolean("Priv")).containsOnly(true);
    assertThat(retrievedItems).extracting(it -> it.getString("Perm"))
        .containsOnly(PERMISSION_GROUP_ID);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Del")).containsOnly(false);
  }

  @Test
  public void batchPutRequiredAttributesOnly() {
    var parentId = UUID.randomUUID().toString();

    var id1 = UUID.randomUUID().toString();
    var nodeId1 = new NodeId(id1, parentId, TYPE);
    var node1 = new Builder(nodeId1)
        .withLastUpdateTs(TS_2)
        .build();

    var id2 = UUID.randomUUID().toString();
    var nodeId2 = new NodeId(id2, parentId, TYPE);
    var node2 = new Builder(nodeId2)
        .withLastUpdateTs(TS_2)
        .build();

    nodeDAO.batchPutItem(List.of(node1, node2));

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s|%s", "TIMELINE", id1)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s|%s", "TIMELINE", id2))));

    assertThat(retrievedItems.size()).isEqualTo(2);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(parentId);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s|%s", "TIMELINE", id1),
            String.format("node|%s|%s", "TIMELINE", id2));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("CreatorId")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Data")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void batchPutEmptyList() {
    // This test is to ensure that an empty list input does not cause an error.
    nodeDAO.batchPutItem(List.of());
  }

  @Test
  public void putItemWritesItemCorrectly() throws Exception {
    var parentId = UUID.randomUUID().toString();
    var nodeId = new NodeId("121", parentId, TYPE);
    var node = new Builder(nodeId)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(false)
        .build();

    nodeDAO.putItem(node);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s", "TIMELINE|121"))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(parentId);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s", "TIMELINE|121"));
    assertThat(retrievedItems).extracting(it -> it.getString("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.getBoolean("Priv")).containsOnly(true);
    assertThat(retrievedItems).extracting(it -> it.getString("Perm"))
        .containsOnly(PERMISSION_GROUP_ID);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Del")).containsOnly(false);
  }

  @Test
  public void putItemWritesItemCorrectlyForNodeIdWithoutParent() throws Exception {
    var id = UUID.randomUUID().toString();
    var nodeId = new NodeId(id, TYPE);
    var node = new Builder(nodeId)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .withData(DATA)
        .setDeleted(false)
        .build();

    nodeDAO.putItem(node);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, id,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|TIMELINE|%s", id))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(id);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|TIMELINE|%s", id));
    assertThat(retrievedItems).extracting(it -> it.getString("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.getBoolean("Priv")).containsOnly(true);
    assertThat(retrievedItems).extracting(it -> it.getString("Perm"))
        .containsOnly(PERMISSION_GROUP_ID);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-C")).containsOnly(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Del")).containsOnly(false);
  }

  @Test
  public void putItemWritesItemWithOnlyRequiredAttributes() throws Exception {
    var parentId = UUID.randomUUID().toString();
    var nodeId = new NodeId("121", parentId, TYPE);
    var node = new Builder(nodeId)
        .withLastUpdateTs(TS_2)
        .build();

    nodeDAO.putItem(node);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|%s", "TIMELINE|121"))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(parentId);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s", "TIMELINE|121"));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("CreatorId")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Data")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void putItemThrowsUserExceptionIfItemAlreadyExistsInDB() {
    var nodeId = new NodeId("3", "1", TYPE);
    var node = new Builder(nodeId)
        .withCreatorId(CREATOR_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .setDeleted(false)
        .build();

    assertThatThrownBy(() -> nodeDAO.putItem(node))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining(String
            .format("%s node with id %s already exists in DB", TYPE, node.getNodeId().getId()));
  }

  @Test
  public void insertNodeWithDataOnlyCreatesNewItem() {
    var childId = UUID.randomUUID().toString();
    var parentId = UUID.randomUUID().toString();
    var nodeId = new NodeId(childId, parentId, NodeType.TIMELINE);

    var result = nodeDAO.insertNodeWithDataOnly(nodeId, CREATOR_ID, DATA, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualByComparingTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(DATA);
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, parentId,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|TIMELINE|%s", childId))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(parentId);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|TIMELINE|%s", childId));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void insertNodeWithDataOnlyCreatesNewItemWhereNodeHasNoParentId() {
    var id = UUID.randomUUID().toString();
    var nodeId = new NodeId(id, NodeType.TIMELINE);

    var result = nodeDAO.insertNodeWithDataOnly(nodeId, CREATOR_ID, DATA, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualByComparingTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(DATA);
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, id,
            DynamoDBClient.PRIMARY_KEY_R, String.format("node|TIMELINE|%s", id))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(id);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|TIMELINE|%s", id));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void insertNodeWithDataOnlyOverwritesExistingData() {
    var nodeId = new NodeId("11", "22", NodeType.PARTICIPATION);
    var result = nodeDAO.insertNodeWithDataOnly(nodeId, CREATOR_ID, DATA, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualByComparingTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(DATA);
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "22",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPATION|11")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("22");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("node|PARTICIPATION|11");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnly(CREATOR_ID);
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnly(DATA);
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void updateNodeData() {
    var nodeId = new NodeId("8", "7", NodeType.ACTIVITY);
    var data = "{}";

    var result = nodeDAO.updateNodeData(nodeId, data, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isNull();
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(data);
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "7",
            DynamoDBClient.PRIMARY_KEY_R, "node|ACTIVITY|8")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("7");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("node|ACTIVITY|8");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Data")).containsOnly(data);
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void updateNodeDataWhereNodeHasNoParentId() {
    var nodeId = new NodeId("55", NodeType.ACTIVITY);
    var data = "{}";

    var result = nodeDAO.updateNodeData(nodeId, data, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isNull();
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(data);
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "55",
            DynamoDBClient.PRIMARY_KEY_R, "node|ACTIVITY|55")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("55");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("node|ACTIVITY|55");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Data")).containsOnly(data);
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void updateNodeDataThrowsIfTsBefore() {
    assertThatThrownBy(
        () -> nodeDAO.updateNodeData(new NodeId("8", "7", NodeType.ACTIVITY), "{}", TS_1))
        .isInstanceOf(ConditionalCheckFailedException.class);
  }

  @Test
  public void updateNodeDataThrowsIfNodeDoesNotExist() {
    assertThatThrownBy(
        () -> nodeDAO
            .updateNodeData(new NodeId("missing", "missing-p", NodeType.ACTIVITY), "{}", TS_1))
        .isInstanceOf(ConditionalCheckFailedException.class);
  }

  @Test
  public void refreshLastUpdateTs() throws Exception {
    var nodeId = new NodeId("77", NodeType.ACTIVITY);

    var result = nodeDAO.refreshLastUpdateTs(nodeId, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isNull();
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getData()).isNull();
    Assertions.assertThat(result.getDeleted()).isFalse();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, nodeId.getId(),
            DynamoDBClient.PRIMARY_KEY_R,
            String.format("node|%s|%s", nodeId.getType(), nodeId.getId()))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(nodeId.getId());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s|%s", nodeId.getType(), nodeId.getId()));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Del")).contains(false);
  }

  @Test
  public void refreshLastUpdateTsThrowsIfItemDoesNotExist() {
    assertThatThrownBy(
        () -> nodeDAO.refreshLastUpdateTs(new NodeId("missing", NodeType.ACTIVITY), TS_2))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String.format(
            "Could not refresh lastUpdateTs for %s node with id %s because either the item does "
                + "not exist or an item with a newer timestamp already exists",
            NodeType.ACTIVITY.name(), "missing"));
  }

  @Test
  public void readItemReturnsRequestedItem() {
    var nodeId = new NodeId("3", "1", NodeType.TIMELINE);

    var result = nodeDAO.readItem(nodeId);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isTrue();
    Assertions.assertThat(result.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.getCreationTs()).isEqualTo(TS_1);
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(DATA);
    Assertions.assertThat(result.getDeleted()).isFalse();
  }

  @Test
  public void readItemReturnsRequestedItemWhereNodeHasNoParentId() {
    var nodeId = new NodeId("101", NodeType.EVENT);
    var result = nodeDAO.readItem(nodeId);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isTrue();
    Assertions.assertThat(result.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.getCreationTs()).isEqualTo(TS_1);
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getData()).isEqualTo(DATA);
    Assertions.assertThat(result.getDeleted()).isFalse();
  }

  @Test
  public void readItemReturnsRequestedItemOnlyRequiredAttributes() {
    var nodeId = new NodeId("1", "2", NodeType.PARTICIPATION);
    var result = nodeDAO.readItem(new NodeId("1", "2", NodeType.PARTICIPATION));

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isEqualTo(CREATOR_ID);
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_1);
    Assertions.assertThat(result.getData()).isNull();
    Assertions.assertThat(result.getDeleted()).isFalse();
  }

  @Test
  public void readItemReturnsNullIfItemDoesNotExist() {
    var result = nodeDAO.readItem(new NodeId("missing", NodeType.EVENT));

    Assertions.assertThat(result).isNull();
  }

  @Test
  public void batchGetItemReturnsItems() {
    var nodeId1 = new NodeId("3", "1", NodeType.TIMELINE);
    var nodeId2 = new NodeId("PARENT|UID", "1", NodeType.PARTICIPANT);

    var result = nodeDAO.batchGetItem(List.of(nodeId1, nodeId2));

    assertThat(result.size()).isEqualTo(2);
    Assertions.assertThat(result).extracting(Node::getNodeId).containsOnly(nodeId1, nodeId2);
    Assertions.assertThat(result).extracting(Node::getCreatorId).containsOnly(CREATOR_ID);
    Assertions.assertThat(result).extracting(Node::getPrivateNode).containsOnly(true, false);
    Assertions.assertThat(result).extracting(Node::getPermissionGroupId)
        .containsOnly(PERMISSION_GROUP_ID);
    Assertions.assertThat(result).extracting(Node::getCreationTs).containsOnly(TS_1, TS_2);
    Assertions.assertThat(result).extracting(Node::getLastUpdateTs).containsOnly(TS_1, TS_2);
    Assertions.assertThat(result).extracting(Node::getData).containsOnly(DATA);
    Assertions.assertThat(result).extracting(Node::getDeleted).containsOnly(true, false);
  }

  @Test
  public void queryNode() {
    var result = nodeDAO.queryNode("1", NodeType.PARTICIPANT, "PARENT");

    var expectedNodeId = new NodeId("PARENT|UID", "1", NodeType.PARTICIPANT);

    assertThat(result.size()).isEqualTo(1);
    Assertions.assertThat(result).extracting(Node::getNodeId).containsOnly(expectedNodeId);
    Assertions.assertThat(result).extracting(Node::getCreatorId).containsOnly(CREATOR_ID);
    Assertions.assertThat(result).extracting(Node::getPrivateNode).containsOnly(false);
    Assertions.assertThat(result).extracting(Node::getPermissionGroupId)
        .containsOnly(PERMISSION_GROUP_ID);
    Assertions.assertThat(result).extracting(Node::getCreationTs).containsOnly(TS_2);
    Assertions.assertThat(result).extracting(Node::getLastUpdateTs).containsOnly(TS_1);
    Assertions.assertThat(result).extracting(Node::getData).containsOnly(DATA);
    Assertions.assertThat(result).extracting(Node::getDeleted).containsOnly(true);
  }

  @Test
  public void queryByParentNodeIdReturnsItems() {
    var result = nodeDAO.queryByParentNodeId("1");

    var expectedNodeId1 = new NodeId("3", "1", NodeType.TIMELINE);
    var expectedNodeId2 = new NodeId("4", "1", NodeType.TIMELINE);
    var expectedNodeId3 = new NodeId("PARENT|UID", "1", NodeType.PARTICIPANT);

    assertThat(result.size()).isEqualTo(3);
    Assertions.assertThat(result).extracting(Node::getNodeId)
        .containsOnly(expectedNodeId1, expectedNodeId2, expectedNodeId3);
    Assertions.assertThat(result).extracting(Node::getCreatorId).containsOnly(CREATOR_ID);
    Assertions.assertThat(result).extracting(Node::getPrivateNode).containsOnly(true, false);
    Assertions.assertThat(result).extracting(Node::getPermissionGroupId)
        .containsOnly(PERMISSION_GROUP_ID);
    Assertions.assertThat(result).extracting(Node::getCreationTs).containsOnly(TS_1, TS_2);
    Assertions.assertThat(result).extracting(Node::getLastUpdateTs).containsOnly(TS_1, TS_2, TS_3);
    Assertions.assertThat(result).extracting(Node::getData).containsOnly(DATA);
    Assertions.assertThat(result).extracting(Node::getDeleted).containsOnly(true, false);
  }

  @Test
  public void queryByParentNodeIdFiltersParentNode() {
    var result = nodeDAO.queryByParentNodeId("101");

    Assertions.assertThat(result).isEmpty();
  }

  @Test
  public void queryByParentNodeIdWithTsFilterReturnsItemsFilteredByTs() {
    var result = nodeDAO.queryByParentNodeIdWithTsFilter("1", TS_3);

    var expectedNodeId = new NodeId("4", "1", NodeType.TIMELINE);

    assertThat(result.size()).isEqualTo(1);
    Assertions.assertThat(result).extracting(Node::getNodeId).containsOnly(expectedNodeId);
    Assertions.assertThat(result).extracting(Node::getCreatorId).containsOnly(CREATOR_ID);
    Assertions.assertThat(result).extracting(Node::getPrivateNode).containsOnly(true);
    Assertions.assertThat(result).extracting(Node::getPermissionGroupId)
        .containsOnly(PERMISSION_GROUP_ID);
    Assertions.assertThat(result).extracting(Node::getCreationTs).containsOnly(TS_1);
    Assertions.assertThat(result).extracting(Node::getLastUpdateTs).containsOnly(TS_3);
    Assertions.assertThat(result).extracting(Node::getData).containsOnly(DATA);
    Assertions.assertThat(result).extracting(Node::getDeleted).containsOnly(false);
  }

  @Test
  public void queryByParentNodeIdWithTsFilterFiltersParentNode() {
    var result = nodeDAO.queryByParentNodeIdWithTsFilter("101", TS_1);

    Assertions.assertThat(result).isEmpty();
  }

  @Test
  public void queryByParentNodeIdWithTsRange() {
    var result = nodeDAO.queryByParentNodeIdWithTsRange("1", TS_1, TS_2);

    var expectedNodeId1 = new NodeId("PARENT|UID", "1", NodeType.PARTICIPANT);
    var expectedNodeId2 = new NodeId("3", "1", NodeType.TIMELINE);

    assertThat(result.size()).isEqualTo(2);
    Assertions.assertThat(result).extracting(Node::getNodeId)
        .containsOnly(expectedNodeId1, expectedNodeId2);
    Assertions.assertThat(result).extracting(Node::getCreatorId).containsOnly(CREATOR_ID);
    Assertions.assertThat(result).extracting(Node::getPrivateNode).containsOnly(false, true);
    Assertions.assertThat(result).extracting(Node::getPermissionGroupId)
        .containsOnly(PERMISSION_GROUP_ID);
    Assertions.assertThat(result).extracting(Node::getCreationTs).containsOnly(TS_1, TS_2);
    Assertions.assertThat(result).extracting(Node::getLastUpdateTs).containsOnly(TS_1, TS_2);
    Assertions.assertThat(result).extracting(Node::getData).containsOnly(DATA);
    Assertions.assertThat(result).extracting(Node::getDeleted).containsOnly(true, false);
  }

  @Test
  public void queryByParentNodeIdWithTsRangeFiltersParentNode() {
    var result = nodeDAO.queryByParentNodeIdWithTsRange("101", TS_1, TS_2);

    Assertions.assertThat(result).isEmpty();
  }

  @Test
  public void softDeleteNode() throws ResourceNotFoundException {
    var nodeId = new NodeId("111", "222", NodeType.PARTICIPATION);

    var result = nodeDAO.softDeleteNode(nodeId, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isNull();
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getDeleted()).isTrue();
    Assertions.assertThat(result.getData()).isNull();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, nodeId.getParentId(),
            DynamoDBClient.PRIMARY_KEY_R,
            String.format("node|%s|%s", nodeId.getType(), nodeId.getId()))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(nodeId.getParentId());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s|%s", nodeId.getType(), nodeId.getId()));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Del")).contains(true);
  }

  @Test
  public void softDeleteNodeWhereNodeHasNoParentId() throws ResourceNotFoundException {
    var nodeId = new NodeId("1111", NodeType.PARTICIPATION);

    var result = nodeDAO.softDeleteNode(nodeId, TS_2);

    Assertions.assertThat(result.getNodeId()).isEqualTo(nodeId);
    Assertions.assertThat(result.getCreatorId()).isNull();
    Assertions.assertThat(result.getPrivateNode()).isFalse();
    Assertions.assertThat(result.getPermissionGroupId()).isNull();
    Assertions.assertThat(result.getCreationTs()).isNull();
    Assertions.assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    Assertions.assertThat(result.getDeleted()).isTrue();
    Assertions.assertThat(result.getData()).isNull();

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, nodeId.getId(),
            DynamoDBClient.PRIMARY_KEY_R,
            String.format("node|%s|%s", nodeId.getType(), nodeId.getId()))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(nodeId.getId());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("node|%s|%s", nodeId.getType(), nodeId.getId()));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.LSI1_R))
        .containsOnly(String.format("node|%s", Long.toString(TS_2.getMillis(), 36)));
    assertThat(retrievedItems).extracting(it -> it.get("Creator")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Priv")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Perm")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Ts-C")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.getString("Data")).containsOnlyNulls();
    assertThat(retrievedItems).extracting(it -> it.get("Del")).contains(true);
  }

  @Test
  public void softDeleteNodeThrowsIfNodeDoesNotExist() {
    var nodeId = new NodeId("missing", "missing-parent", NodeType.PARTICIPATION);

    assertThatThrownBy(() -> nodeDAO.softDeleteNode(nodeId, TS_2))
        .isInstanceOf(ResourceNotFoundException.class).hasMessageContaining(String.format(
        "Attempted to set %s node with id %s as deleted but the node does not exist",
        nodeId.getType().name(), nodeId.getId()));
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "101",
            DynamoDBClient.PRIMARY_KEY_R, "node|EVENT|101")
        .withString("Creator", CREATOR_ID)
        .withBoolean("Priv", true)
        .withString("Perm", PERMISSION_GROUP_ID)
        .withLong("Ts-C", TS_1.getMillis())
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("Data", DATA)
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1",
            DynamoDBClient.PRIMARY_KEY_R, "node|TIMELINE|4")
        .withString("Creator", CREATOR_ID)
        .withBoolean("Priv", true)
        .withString("Perm", PERMISSION_GROUP_ID)
        .withLong("Ts-C", TS_1.getMillis())
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_3.getMillis(), 36)))
        .withString("Data", DATA)
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1",
            DynamoDBClient.PRIMARY_KEY_R, "node|TIMELINE|3")
        .withString("Creator", CREATOR_ID)
        .withBoolean("Priv", true)
        .withString("Perm", PERMISSION_GROUP_ID)
        .withLong("Ts-C", TS_1.getMillis())
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_2.getMillis(), 36)))
        .withString("Data", DATA)
        .withBoolean("Del", false));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPANT|PARENT|UID")
        .withString("Creator", CREATOR_ID)
        .withBoolean("Priv", false)
        .withString("Perm", PERMISSION_GROUP_ID)
        .withLong("Ts-C", TS_2.getMillis())
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36)))
        .withString("Data", DATA)
        .withBoolean("Del", true));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "2",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPATION|1")
        .withString("Creator", CREATOR_ID)
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36))));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "22",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPATION|11")
        .withString("Perm", PERMISSION_GROUP_ID)
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36)))
        .withString("Data", "{}"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "222",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPATION|111")
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36))));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1111",
            DynamoDBClient.PRIMARY_KEY_R, "node|PARTICIPATION|1111")
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36))));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "7",
            DynamoDBClient.PRIMARY_KEY_R, "node|ACTIVITY|8")
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36)))
        .withString("Data", "{\"name\":\"Kayaking\"}"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "55",
            DynamoDBClient.PRIMARY_KEY_R, "node|ACTIVITY|55")
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36)))
        .withString("Data", "{\"name\":\"Kayaking\"}"));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "77",
            DynamoDBClient.PRIMARY_KEY_R, "node|ACTIVITY|77")
        .withString(DynamoDBClient.LSI1_R,
            String.format("node|%s", Long.toString(TS_1.getMillis(), 36))));

    return items;
  }
}
