package app.planhive.services.participation.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class ParticipantDataTest {

  private static final ParticipationStatus STATUS = ParticipationStatus.ACCEPTED;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new ParticipantData(STATUS);

    assertThat(obj.getStatus()).isEqualTo(STATUS);
  }

  @Test
  public void constructorThrowsIfParticipationStatusNull() {
    assertThatThrownBy(() -> new ParticipantData(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void deserializationFailsIfStatusMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", ParticipantData.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'status'");
  }

  @Test
  public void equality() {
    var obj1 = new ParticipantData(STATUS);
    var obj2 = new ParticipantData(STATUS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ParticipantData(STATUS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ParticipantData(STATUS);
    var obj2 = new ParticipantData(ParticipationStatus.DECLINED);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new ParticipantData(STATUS);

    assertThat(obj.toJson()).isEqualTo(String.format("{\"status\":\"%s\"}", STATUS));
  }

  @Test
  public void fromJson() {
    var obj = NodeData
        .fromJson(String.format("{\"status\":\"%s\"}", STATUS), ParticipantData.class);

    assertThat(obj).isEqualTo(new ParticipantData(STATUS));
  }
}
