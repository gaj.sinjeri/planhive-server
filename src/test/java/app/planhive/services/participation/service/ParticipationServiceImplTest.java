package app.planhive.services.participation.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.participation.model.ParticipantData;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.participation.notification.ParticipationNotificationSender;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class ParticipationServiceImplTest {

  private static final String UUID = "1234";
  private static final String PARENT_ID = "v34v-43-gj03g";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "123";
  private static final User USER = new User.Builder(USER_ID).withName("Antoine").build();
  private static final Instant TS_1 = new Instant(1);

  private NodeService nodeService;
  private PermissionService permissionService;
  private FeedService feedService;
  private ParticipationNotificationSender participationNotificationSender;
  private ParticipationService participationService;

  @BeforeEach
  public void setUp() {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    nodeService = mock(NodeService.class);
    permissionService = mock(PermissionService.class);
    feedService = mock(FeedService.class);
    participationNotificationSender = mock(ParticipationNotificationSender.class);

    participationService = new ParticipationServiceImpl(secureGenerator, nodeService,
        permissionService, feedService, participationNotificationSender);
  }

  @Test
  public void addParticipationWidget() throws Exception {
    var expectedNodeId = new NodeId(String.format("%s|%s", UUID, USER_ID), UUID,
        NodeType.PARTICIPANT);
    var participantNode = new Node.Builder(expectedNodeId).build();
    when(nodeService.insertNodeWithDataOnly(expectedNodeId, USER_ID,
        new ParticipantData(ParticipationStatus.ACCEPTED).toJson(), TS_1))
        .thenReturn(participantNode);

    var result = participationService
        .addParticipationWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(), TS_1);

    var expectedNode = new Node.Builder(new NodeId(UUID, PARENT_ID, NodeType.PARTICIPATION))
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .build();
    assertThat(result).isEqualTo(List.of(expectedNode, participantNode));

    verify(nodeService).insertNode(expectedNode);
    verify(feedService).createFeeds(expectedNode.getNodeId().toFeedItemKey(), List.of(USER_ID),
        NodeType.PARTICIPATION.name(), TS_1);
  }

  @Test
  public void addParticipationWidgetRethrowsIfWidgetAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(() -> participationService
        .addParticipationWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(USER_ID), TS_1))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void updateParticipationStatus() throws Exception {
    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS_1)
        .withData("{\"eventState\":\"ACTIVE\",\"name\":\"Surfing\", \"coverTs\":0}")
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var participationNodeId = new NodeId(UUID, PARENT_ID, NodeType.PARTICIPATION);
    var participationNode = new Node.Builder(participationNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS_1)
        .setDeleted(false)
        .build();
    when(nodeService.getNodeOrThrow(participationNodeId)).thenReturn(participationNode);

    var expectedNodeId = new NodeId(String.format("%s|%s", UUID, USER_ID), UUID,
        NodeType.PARTICIPANT);
    var participantNode = new Node.Builder(expectedNodeId).build();
    when(nodeService.insertNodeWithDataOnly(expectedNodeId, USER_ID,
        new ParticipantData(ParticipationStatus.ACCEPTED).toJson(), TS_1))
        .thenReturn(participantNode);

    var result = participationService
        .updateParticipationStatus(USER, participationNodeId, ParticipationStatus.ACCEPTED, TS_1);

    assertThat(result).isEqualTo(participantNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(feedService)
        .refreshFeedsForResource(participationNode.getNodeId().toFeedItemKey(), TS_1);
    verify(participationNotificationSender)
        .sendParticipationStatusUpdatedNotification(USER, eventNodeId, "Surfing",
            participationNodeId, ParticipationStatus.ACCEPTED);
  }
}
