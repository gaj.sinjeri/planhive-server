package app.planhive.services.participation.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.participation.model.ParticipationStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class UpdateParticipationStatusRequestTest {

  private static final ParticipationStatus STATUS = ParticipationStatus.ACCEPTED;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateParticipationStatusRequest(STATUS);

    assertThat(obj.getStatus()).isEqualTo(STATUS);
  }

  @Test
  public void deserializationFailsIfParticipationStatusMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{}", UpdateParticipationStatusRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'participationStatus'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateParticipationStatusRequest(STATUS);
    var obj2 = new UpdateParticipationStatusRequest(STATUS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateParticipationStatusRequest(STATUS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateParticipationStatusRequest(STATUS);
    var obj2 = new UpdateParticipationStatusRequest(ParticipationStatus.DECLINED);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
