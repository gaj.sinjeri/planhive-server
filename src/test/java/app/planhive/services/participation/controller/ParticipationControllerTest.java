package app.planhive.services.participation.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.participation.controller.model.UpdateParticipationStatusRequest;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.participation.service.ParticipationService;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

@TestInstance(Lifecycle.PER_CLASS)
public class ParticipationControllerTest {

  private static final User USER = new User.Builder("v34v-43-gj03g").build();
  private static final String EVENT_NODE_ID = "123";
  private static final String PARTICIPATION_NODE_ID = "234";
  private static final ParticipationStatus STATUS = ParticipationStatus.ACCEPTED;

  private ParticipationService participationService;
  private ParticipationController participationController;

  @BeforeEach
  public void setUp() {
    participationService = mock(ParticipationService.class);

    participationController = new ParticipationController(participationService);
  }

  @Test
  public void updateParticipationStatusSuccessfulCall() throws Exception {
    var request = new UpdateParticipationStatusRequest(STATUS);

    var participationNodeId = new NodeId(PARTICIPATION_NODE_ID, EVENT_NODE_ID,
        NodeType.PARTICIPATION);
    var participantNode = new Node.Builder(new NodeId("1", "2", NodeType.PARTICIPANT)).build();
    when(participationService
        .updateParticipationStatus(eq(USER), eq(participationNodeId), eq(STATUS), any()))
        .thenReturn(participantNode);

    var response = participationController
        .updateParticipationStatus(USER, EVENT_NODE_ID, PARTICIPATION_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(participantNode));
  }

  @Test
  public void updateParticipationStatusReturnsErrorIfNoSuchParticipationNodeExists()
      throws Exception {
    var participationNodeId = new NodeId(PARTICIPATION_NODE_ID, EVENT_NODE_ID,
        NodeType.PARTICIPATION);
    doThrow(new ResourceNotFoundException("Bam!")).when(participationService)
        .updateParticipationStatus(eq(USER), eq(participationNodeId), eq(STATUS), any());

    var request = new UpdateParticipationStatusRequest(STATUS);
    var response = participationController
        .updateParticipationStatus(USER, EVENT_NODE_ID, PARTICIPATION_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateParticipationStatusReturnsErrorIfResourceDeleted() throws Exception {
    var participationNodeId = new NodeId(PARTICIPATION_NODE_ID, EVENT_NODE_ID,
        NodeType.PARTICIPATION);
    var node = new Node.Builder(new NodeId("1", NodeType.PARTICIPATION)).build();
    doThrow(new ResourceDeletedException("", node))
        .when(participationService)
        .updateParticipationStatus(eq(USER), eq(participationNodeId), eq(STATUS), any());

    var request = new UpdateParticipationStatusRequest(STATUS);
    var response = participationController
        .updateParticipationStatus(USER, EVENT_NODE_ID, PARTICIPATION_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateParticipationStatusReturnsErrorIfUserNotAuthorised() throws Exception {
    var participationNodeId = new NodeId(PARTICIPATION_NODE_ID, EVENT_NODE_ID,
        NodeType.PARTICIPATION);
    doThrow(new UnauthorisedException("Bam!")).when(participationService)
        .updateParticipationStatus(eq(USER), eq(participationNodeId), eq(STATUS), any());

    var request = new UpdateParticipationStatusRequest(STATUS);
    var response = participationController
        .updateParticipationStatus(USER, EVENT_NODE_ID, PARTICIPATION_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
