package app.planhive.services.participation.notification;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class ParticipationNotificationSenderImplTest {

  private static final User USER = new User.Builder("123").withName("Roger").build();
  private static final NodeId EVENT_NODE_ID = new NodeId("123", NodeType.EVENT);
  private static final String EVENT_NAME = "Las Vegas";
  private static final NodeId PARTICIPATION_NODE_ID = new NodeId("234", "123",
      NodeType.PARTICIPATION);

  private NotificationService notificationService;
  private ParticipationNotificationSender participationNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    participationNotificationSender = new ParticipationNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendParticipationStatusUpdatedNotificationAccepted() {
    participationNotificationSender
        .sendParticipationStatusUpdatedNotification(USER, EVENT_NODE_ID, EVENT_NAME,
            PARTICIPATION_NODE_ID, ParticipationStatus.ACCEPTED);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name(),
        "234", "123", NodeType.PARTICIPATION.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s accepted the invitation for %s", USER.getName(), EVENT_NAME),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, PARTICIPATION_NODE_ID,
            List.of(USER.getId()), true);
  }

  @Test
  public void sendParticipationStatusUpdatedNotificationMaybe() {
    participationNotificationSender
        .sendParticipationStatusUpdatedNotification(USER, EVENT_NODE_ID, EVENT_NAME,
            PARTICIPATION_NODE_ID, ParticipationStatus.MAYBE);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name(),
        "234", "123", NodeType.PARTICIPATION.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s might be attending %s", USER.getName(), EVENT_NAME),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, PARTICIPATION_NODE_ID,
            List.of(USER.getId()), true);
  }

  @Test
  public void sendParticipationStatusUpdatedNotificationDeclined() {
    participationNotificationSender
        .sendParticipationStatusUpdatedNotification(USER, EVENT_NODE_ID, EVENT_NAME,
            PARTICIPATION_NODE_ID, ParticipationStatus.DECLINED);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name(),
        "234", "123", NodeType.PARTICIPATION.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s declined the invitation for %s", USER.getName(), EVENT_NAME),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, PARTICIPATION_NODE_ID,
            List.of(USER.getId()), true);
  }
}
