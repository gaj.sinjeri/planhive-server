package app.planhive.services.thread.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class ClearThreadForUserRequestTest {

  private static final String PARENT_NODE_ID = "123";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new ClearThreadForUserRequest(PARENT_NODE_ID);

    assertThat(obj.getParentNodeId()).isEqualTo(PARENT_NODE_ID);
  }


  @Test
  public void deserializationFailsIfParentNodeIdMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{}", ClearThreadForUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'parentNodeId'");
  }

  @Test
  public void equality() {
    var obj1 = new ClearThreadForUserRequest(PARENT_NODE_ID);
    var obj2 = new ClearThreadForUserRequest(PARENT_NODE_ID);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ClearThreadForUserRequest(PARENT_NODE_ID);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ClearThreadForUserRequest(PARENT_NODE_ID);
    var obj2 = new ClearThreadForUserRequest("Blabla");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
