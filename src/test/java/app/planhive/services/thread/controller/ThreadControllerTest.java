package app.planhive.services.thread.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.thread.controller.model.AddMessageRequest;
import app.planhive.services.thread.controller.model.ClearThreadForUserRequest;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class ThreadControllerTest {

  private static final User USER = new User.Builder("123").build();
  private static final String PARENT_NODE_ID = "123";
  private static final String THREAD_NODE_ID = "234";
  private static final String MESSAGE_NODE_ID = "345";
  private static final String TEXT = "What when?";
  private static final Node NODE = new Node.Builder(new NodeId("1", NodeType.THREAD)).build();
  private static final MultipartFile FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg",
      new byte[1]);

  private ThreadService threadService;
  private ThreadController threadController;

  @BeforeEach
  public void setUp() {
    threadService = mock(ThreadService.class);
    threadController = new ThreadController(threadService);
  }

  @Test
  public void addMessageSuccessfulCallDirectChatThread() throws Exception {
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    var messageNode = new Node.Builder(new NodeId("1", "2", NodeType.MESSAGE)).build();
    when(threadService
        .addMessageToDirectThread(eq(USER), eq(threadNodeId), eq(TEXT), eq(FILE), any()))
        .thenReturn(List.of(messageNode));

    var request = new AddMessageRequest(NodeType.CHAT_DIRECT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(messageNode)));
  }

  @Test
  public void addMessageSuccessfulCallEventThread() throws Exception {
    var groupNodeId = new NodeId(PARENT_NODE_ID, NodeType.EVENT);
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    var messageNode = new Node.Builder(new NodeId("1", "2", NodeType.MESSAGE)).build();
    when(threadService
        .addMessageToGroupThread(eq(USER), eq(groupNodeId), eq(threadNodeId), eq(TEXT), eq(FILE),
            any()))
        .thenReturn(List.of(messageNode));

    var request = new AddMessageRequest(NodeType.EVENT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(messageNode)));
  }

  @Test
  public void addMessageSuccessfulCallGroupChatThread() throws Exception {
    var groupNodeId = new NodeId(PARENT_NODE_ID, NodeType.CHAT_GROUP);
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    var messageNode = new Node.Builder(new NodeId("1", "2", NodeType.MESSAGE)).build();
    when(threadService
        .addMessageToGroupThread(eq(USER), eq(groupNodeId), eq(threadNodeId), eq(TEXT), eq(FILE),
            any()))
        .thenReturn(List.of(messageNode));

    var request = new AddMessageRequest(NodeType.CHAT_GROUP, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(messageNode)));
  }

  @Test
  public void addMessageReturnsErrorIfNodeNotFound() throws Exception {
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    Mockito.doThrow(new ResourceNotFoundException("")).when(threadService)
        .addMessageToDirectThread(eq(USER), eq(threadNodeId), eq(TEXT), eq(FILE), any());

    var request = new AddMessageRequest(NodeType.CHAT_DIRECT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addMessageReturnsErrorIfResourceDeleted() throws Exception {
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    Mockito.doThrow(new ResourceDeletedException("", NODE)).when(threadService)
        .addMessageToDirectThread(eq(USER), eq(threadNodeId), eq(TEXT), eq(FILE), any());

    var request = new AddMessageRequest(NodeType.CHAT_DIRECT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addMessageReturnsErrorIfUnauthorized() throws Exception {
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    Mockito.doThrow(new UnauthorisedException("")).when(threadService)
        .addMessageToDirectThread(eq(USER), eq(threadNodeId), eq(TEXT), eq(FILE), any());

    var request = new AddMessageRequest(NodeType.CHAT_DIRECT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addMessageReturnsErrorIfInvalidImage() throws Exception {
    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    Mockito.doThrow(new InvalidImageException("")).when(threadService)
        .addMessageToDirectThread(eq(USER), eq(threadNodeId), eq(TEXT), eq(FILE), any());

    var request = new AddMessageRequest(NodeType.CHAT_DIRECT, PARENT_NODE_ID, TEXT);
    var response = threadController.addMessage(USER, THREAD_NODE_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getImageSuccessfulCall() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImage(USER, messageId)).thenReturn(FILE.getBytes());

    var response = threadController.getImage(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getImageReturnsErrorIfResourceNotFound() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImage(USER, messageId)).thenThrow(new ResourceNotFoundException(""));

    var response = threadController.getImage(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getImageReturnsErrorIfResourceDeleted() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImage(USER, messageId))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = threadController.getImage(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getImageReturnsErrorIfUserNotAuthorised() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImage(USER, messageId)).thenThrow(new UnauthorisedException(""));

    var response = threadController.getImage(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getImageThumbnailSuccessfulCall() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImageThumbnail(USER, messageId)).thenReturn(FILE.getBytes());

    var response = threadController.getImageThumbnail(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getImageThumbnailReturnsErrorIfResourceNotFound() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImageThumbnail(USER, messageId))
        .thenThrow(new ResourceNotFoundException(""));

    var response = threadController.getImageThumbnail(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getImageThumbnailReturnsErrorIfResourceDeleted() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImageThumbnail(USER, messageId))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = threadController.getImageThumbnail(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getImageThumbnailReturnsErrorIfUserNotAuthorised() throws Exception {
    var messageId = new NodeId(MESSAGE_NODE_ID, THREAD_NODE_ID, NodeType.MESSAGE);
    when(threadService.getImageThumbnail(USER, messageId)).thenThrow(new UnauthorisedException(""));

    var response = threadController.getImageThumbnail(USER, THREAD_NODE_ID, MESSAGE_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void clearThreadForUserSuccessfulCall() throws Exception {
    var request = new ClearThreadForUserRequest(PARENT_NODE_ID);
    var response = threadController.clearThreadForUser(USER, THREAD_NODE_ID, request);

    var threadNodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    verify(threadService).clearThreadForUser(eq(USER), eq(threadNodeId), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void clearThreadForUserReturnsErrorIfResourceDoesNotExist() throws Exception {
    var nodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    doThrow(new ResourceNotFoundException("")).when(threadService)
        .clearThreadForUser(eq(USER), eq(nodeId), any());

    var request = new ClearThreadForUserRequest(PARENT_NODE_ID);
    var response = threadController.clearThreadForUser(USER, THREAD_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void clearThreadForUserReturnsErrorIfResourceDeleted() throws Exception {
    var nodeId = new NodeId(THREAD_NODE_ID, PARENT_NODE_ID, NodeType.THREAD);
    doThrow(new ResourceDeletedException("", NODE)).when(threadService)
        .clearThreadForUser(eq(USER), eq(nodeId), any());

    var request = new ClearThreadForUserRequest(PARENT_NODE_ID);
    var response = threadController.clearThreadForUser(USER, THREAD_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
