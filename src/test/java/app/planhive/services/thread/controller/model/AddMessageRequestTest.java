package app.planhive.services.thread.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class AddMessageRequestTest {

  private static final NodeType PARENT_TYPE = NodeType.CHAT_DIRECT;
  private static final String PARENT_NODE_ID = "123";
  private static final String TEXT = "Let's go!";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, TEXT);

    assertThat(obj.getParentNodeType()).isEqualTo(PARENT_TYPE);
    assertThat(obj.getParentNodeId()).isEqualTo(PARENT_NODE_ID);
    assertThat(obj.getTextContent()).isEqualTo(TEXT);
  }

  @Test
  public void constructorThrowsIfParentNodeTypeNotAllowed() {
    assertThatThrownBy(() -> new AddMessageRequest(NodeType.ACTIVITY, PARENT_NODE_ID, "Hello?"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Parent node type not allowed");
  }

  @Test
  public void deserializationFailsIfParentNodeTypeMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"parentNodeId\":\"1\",\"textContent\":\"OMG\"}", AddMessageRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'parentNodeType'");
  }

  @Test
  public void deserializationFailsIfParentNodeIdMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"parentNodeType\":\"EVENT\",\"textContent\":\"OMG\"}",
            AddMessageRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'parentNodeId'");
  }

  @Test
  public void equality() {
    var obj1 = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, TEXT);
    var obj2 = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, TEXT);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, TEXT);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, TEXT);
    var obj2 = new AddMessageRequest(PARENT_TYPE, PARENT_NODE_ID, "LOL");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
