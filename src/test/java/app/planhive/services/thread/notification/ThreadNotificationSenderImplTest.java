package app.planhive.services.thread.notification;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.chat.model.ChatData;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class ThreadNotificationSenderImplTest {

  private static final User USER = new User.Builder("123").withName("Roger").build();
  private static final NodeId CHAT_GROUP_NODE_ID = new NodeId("246", NodeType.CHAT_GROUP);
  private static final NodeId EVENT_NODE_ID = new NodeId("246", NodeType.EVENT);
  private static final NodeId THREAD_NODE_ID = new NodeId("123", "246", NodeType.THREAD);
  private static final NodeId MESSAGE_NODE_ID = new NodeId("345", "123", NodeType.MESSAGE);
  private static final String GROUP_NAME = "Weekend";
  private static final String TEXT = "Vegas?";

  private NotificationService notificationService;
  private ThreadNotificationSender threadNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    threadNotificationSender = new ThreadNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendNewDirectMessageNotification() {
    threadNotificationSender
        .sendNewDirectMessageNotification(USER, CHAT_GROUP_NODE_ID, THREAD_NODE_ID, MESSAGE_NODE_ID,
            TEXT);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.CHAT_GROUP.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(USER.getName(), TEXT,
        NotificationCategory.DIRECT_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewDirectMessageNotificationWithoutTextContent() {
    threadNotificationSender
        .sendNewDirectMessageNotification(USER, CHAT_GROUP_NODE_ID, THREAD_NODE_ID, MESSAGE_NODE_ID,
            null);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.CHAT_GROUP.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(USER.getName(), "Shared a photo.",
        NotificationCategory.DIRECT_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewGroupMessageNotificationEventThread() {
    var eventNode = new Node.Builder(EVENT_NODE_ID)
        .withData(new EventData(EventState.ACTIVE, GROUP_NAME, "", 0L, null).toJson())
        .build();

    threadNotificationSender
        .sendNewGroupMessageNotification(USER, eventNode, THREAD_NODE_ID, MESSAGE_NODE_ID, TEXT);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.EVENT.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(GROUP_NAME,
        String.format("%s: %s", USER.getName(), TEXT), NotificationCategory.EVENT_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewGroupMessageNotificationEventThreadNoTextContent() {
    var eventNode = new Node.Builder(EVENT_NODE_ID)
        .withData(new EventData(EventState.ACTIVE, GROUP_NAME, "", 0L, null).toJson())
        .build();

    threadNotificationSender
        .sendNewGroupMessageNotification(USER, eventNode, THREAD_NODE_ID, MESSAGE_NODE_ID, null);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.EVENT.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(GROUP_NAME,
        String.format("%s: %s", USER.getName(), "Shared a photo."),
        NotificationCategory.EVENT_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewGroupMessageNotificationGroupChatThread() {
    var groupChatData = new ChatData(GROUP_NAME, 0L);
    var groupChatNode = new Node.Builder(CHAT_GROUP_NODE_ID)
        .withData(groupChatData.toJson())
        .build();

    threadNotificationSender
        .sendNewGroupMessageNotification(USER, groupChatNode, THREAD_NODE_ID, MESSAGE_NODE_ID,
            TEXT);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.CHAT_GROUP.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(GROUP_NAME,
        String.format("%s: %s", USER.getName(), TEXT), NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewGroupMessageNotificationGroupChatThreadNoTextContent() {
    var groupChatData = new ChatData(GROUP_NAME, 0L);
    var groupChatNode = new Node.Builder(CHAT_GROUP_NODE_ID)
        .withData(groupChatData.toJson())
        .build();

    threadNotificationSender
        .sendNewGroupMessageNotification(USER, groupChatNode, THREAD_NODE_ID, MESSAGE_NODE_ID,
            null);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "246",
        NodeType.CHAT_GROUP.name(), "123", "246", NodeType.THREAD.name(), "345", "123",
        NodeType.MESSAGE);
    var expectedNotification = new Notification.Builder(GROUP_NAME,
        String.format("%s: %s", USER.getName(), "Shared a photo."),
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, THREAD_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewGroupMessageNotificationThrowsIfNodeTypeNotSupported() {
    var groupChatNodeId = new NodeId("246", NodeType.TIMELINE);
    var groupChatNode = new Node.Builder(groupChatNodeId).build();

    assertThatThrownBy(() -> threadNotificationSender
        .sendNewGroupMessageNotification(USER, groupChatNode, THREAD_NODE_ID, MESSAGE_NODE_ID,
            TEXT))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining(String.format("Unsupported node type: %s", NodeType.TIMELINE.name()));

    verify(notificationService, never())
        .sendNotificationByNodeId(any(), any(), any(), anyBoolean());
  }
}
