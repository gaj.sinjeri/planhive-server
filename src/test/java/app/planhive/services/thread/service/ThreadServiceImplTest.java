package app.planhive.services.thread.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node.Builder;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.model.MessageData;
import app.planhive.services.thread.notification.ThreadNotificationSender;
import app.planhive.services.user.model.User;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;

@TestInstance(Lifecycle.PER_CLASS)
public class ThreadServiceImplTest {

  private static final String UUID = "1234";
  private static final String NODE_ID = "747";
  private static final String PARENT_ID = "v34v-43-gj03g";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "123";
  private static final String NAME = "Fawlty";
  private static final User USER = new User.Builder(USER_ID).withName(NAME).build();
  private static final String TEXT = "I will not buy this record, it is scratched.";
  private static final Instant TS = Instant.now();

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private ThreadNotificationSender threadNotificationSender;
  private CloudStore cloudStore;
  private ThreadService threadService;

  private InputStream imageInputStream;
  private MultipartFile FILE;

  @BeforeEach
  public void setUp() throws Exception {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    threadNotificationSender = mock(ThreadNotificationSender.class);
    cloudStore = mock(CloudStore.class);

    threadService = new ThreadServiceImpl(secureGenerator, nodeService, feedService,
        permissionService, threadNotificationSender, cloudStore, 2048, 512);

    imageInputStream = generateImageInputStream(2, 1);
    FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg", imageInputStream);
  }

  @Test
  public void createThreadOpen() throws Exception {
    var parentNodeId = new NodeId(PARENT_ID, NodeType.EVENT);

    var result = threadService
        .createThread(parentNodeId, PERMISSION_GROUP_ID, false, USER_ID, List.of("USER_2"), TS);

    var expectedNodeId = new NodeId(UUID, PARENT_ID, NodeType.THREAD);
    var expectedNode = new Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(ArgumentMatchers.eq(result.getNodeId().toFeedItemKey()), captor.capture(),
        eq(NodeType.THREAD.name()), eq(TS));
    assertThat(captor.getValue()).containsOnly(USER_ID, "USER_2");
  }

  @Test
  public void createThreadPrivate() throws Exception {
    var parentNodeId = new NodeId(PARENT_ID, NodeType.EVENT);

    var result = threadService
        .createThread(parentNodeId, PERMISSION_GROUP_ID, true, USER_ID, List.of("USER_2"), TS);

    var expectedNodeId = new NodeId(UUID, PARENT_ID, NodeType.THREAD);
    var expectedNode = new Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .setPrivateNode(true)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .build();
    Assertions.assertThat(result).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);

    ArgumentCaptor<List<String>> captor = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(ArgumentMatchers.eq(result.getNodeId().toFeedItemKey()), captor.capture(),
        eq(NodeType.THREAD.name()), eq(TS));
    assertThat(captor.getValue()).containsOnly(USER_ID, "USER_2");
  }

  @Test
  public void createThreadRethrowsIfThreadAlreadyExists() throws Exception {
    Mockito.doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    var parentNodeId = new NodeId(PARENT_ID, NodeType.EVENT);

    assertThatThrownBy(
        () -> threadService
            .createThread(parentNodeId, PERMISSION_GROUP_ID, true, USER_ID, List.of(), TS))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void addMessageToDirectThread() throws Exception {
    var directChatNodeId = new NodeId(PARENT_ID, NodeType.CHAT_DIRECT);
    var directChatNode = new Builder(directChatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(directChatNodeId)).thenReturn(directChatNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var updatedThreadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS)
        .build();
    when(nodeService.refreshLastUpdateTs(threadNodeId, TS)).thenReturn(updatedThreadNode);

    var result = threadService.addMessageToDirectThread(USER, threadNodeId, TEXT, null, TS);

    Assertions.assertThat(result.get(0)).isEqualTo(updatedThreadNode);

    var expectedMessageNodeId = new NodeId(UUID, NODE_ID, NodeType.MESSAGE);
    Assertions.assertThat(result.get(1).getNodeId()).isEqualTo(expectedMessageNodeId);
    Assertions.assertThat(result.get(1).getCreatorId()).isEqualTo(USER_ID);
    Assertions.assertThat(result.get(1).getPrivateNode()).isNull();
    Assertions.assertThat(result.get(1).getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.get(1).getCreationTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getLastUpdateTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getData()).isEqualTo(new MessageData(TEXT, false).toJson());
    Assertions.assertThat(result.get(1).getDeleted()).isNull();

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(nodeService).insertNode(result.get(1));
    verify(nodeService).refreshLastUpdateTs(threadNodeId, TS);
    verify(feedService).refreshFeedsForResource(threadNodeId.toFeedItemKey(), TS);
    verify(threadNotificationSender)
        .sendNewDirectMessageNotification(USER, directChatNodeId, threadNodeId,
            expectedMessageNodeId, TEXT);
    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void addMessageToDirectThreadWithImage() throws Exception {
    var directChatNodeId = new NodeId(PARENT_ID, NodeType.CHAT_DIRECT);
    var directChatNode = new Builder(directChatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(directChatNodeId)).thenReturn(directChatNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var updatedThreadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS)
        .build();
    when(nodeService.refreshLastUpdateTs(threadNodeId, TS)).thenReturn(updatedThreadNode);

    var result = threadService.addMessageToDirectThread(USER, threadNodeId, null, FILE, TS);

    Assertions.assertThat(result.get(0)).isEqualTo(updatedThreadNode);

    var expectedMessageNodeId = new NodeId(UUID, NODE_ID, NodeType.MESSAGE);
    Assertions.assertThat(result.get(1).getNodeId()).isEqualTo(expectedMessageNodeId);
    Assertions.assertThat(result.get(1).getCreatorId()).isEqualTo(USER_ID);
    Assertions.assertThat(result.get(1).getPrivateNode()).isNull();
    Assertions.assertThat(result.get(1).getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.get(1).getCreationTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getLastUpdateTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getData()).isEqualTo(new MessageData(null, true).toJson());
    Assertions.assertThat(result.get(1).getDeleted()).isNull();

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(nodeService).insertNode(result.get(1));
    verify(feedService).refreshFeedsForResource(threadNodeId.toFeedItemKey(), TS);
    verify(threadNotificationSender)
        .sendNewDirectMessageNotification(USER, directChatNodeId, threadNodeId,
            expectedMessageNodeId, null);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("threads/%s/messages/%s/image.jpeg", NODE_ID, UUID)),
            captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore)
        .storeImage(eq(String.format("threads/%s/messages/%s/t-image.jpeg", NODE_ID, UUID)), any());
  }

  @Test
  public void addMessageToDirectThreadRethrowsIfThreadAlreadyExists() throws Exception {
    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(
        () -> threadService.addMessageToDirectThread(USER, threadNodeId, TEXT, null, TS))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void addMessageToDirectThreadRethrowsIfImageInaccessible() throws Exception {
    var directChatNodeId = new NodeId(PARENT_ID, NodeType.CHAT_DIRECT);
    var directChatNode = new Builder(directChatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(directChatNodeId)).thenReturn(directChatNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(
        () -> threadService.addMessageToDirectThread(USER, threadNodeId, TEXT, fileMock, TS))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error accessing image input stream");

    verify(nodeService, never()).refreshLastUpdateTs(any(), any());
    verify(permissionService, never()).createPermissionGroup(any(), any());
    verify(nodeService, never()).insertNode(any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void addMessageToDirectThreadThrowsIfImageFileFormatIncorrect() throws Exception {
    var directChatNodeId = new NodeId(PARENT_ID, NodeType.CHAT_DIRECT);
    var directChatNode = new Builder(directChatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(directChatNodeId)).thenReturn(directChatNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(
        () -> threadService.addMessageToDirectThread(USER, threadNodeId, TEXT, fileMock, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");
  }

  @Test
  public void addMessageToDirectThreadThrowsIfImageResolutionTooHigh() throws Exception {
    var directChatNodeId = new NodeId(PARENT_ID, NodeType.CHAT_DIRECT);
    var directChatNode = new Builder(directChatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(directChatNodeId)).thenReturn(directChatNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(
        () -> threadService.addMessageToDirectThread(USER, threadNodeId, TEXT, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");
  }

  @Test
  public void addMessageToGroupThread() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var updatedThreadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS)
        .build();
    when(nodeService.refreshLastUpdateTs(threadNodeId, TS)).thenReturn(updatedThreadNode);

    var result = threadService
        .addMessageToGroupThread(USER, groupNodeId, threadNodeId, TEXT, null, TS);

    Assertions.assertThat(result.get(0)).isEqualTo(updatedThreadNode);

    var expectedMessageNodeId = new NodeId(UUID, NODE_ID, NodeType.MESSAGE);
    Assertions.assertThat(result.get(1).getNodeId()).isEqualTo(expectedMessageNodeId);
    Assertions.assertThat(result.get(1).getCreatorId()).isEqualTo(USER_ID);
    Assertions.assertThat(result.get(1).getPrivateNode()).isNull();
    Assertions.assertThat(result.get(1).getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.get(1).getCreationTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getLastUpdateTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getData()).isEqualTo(new MessageData(TEXT, false).toJson());
    Assertions.assertThat(result.get(1).getDeleted()).isNull();

    verify(permissionService, times(2))
        .verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(nodeService).insertNode(result.get(1));
    verify(feedService).refreshFeedsForResource(threadNodeId.toFeedItemKey(), TS);
    verify(threadNotificationSender)
        .sendNewGroupMessageNotification(USER, groupNode, threadNodeId, expectedMessageNodeId,
            TEXT);
    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void addMessageToGroupThreadWithImage() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var updatedThreadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS)
        .build();
    when(nodeService.refreshLastUpdateTs(threadNodeId, TS)).thenReturn(updatedThreadNode);

    var result = threadService
        .addMessageToGroupThread(USER, groupNodeId, threadNodeId, null, FILE, TS);

    Assertions.assertThat(result.get(0)).isEqualTo(updatedThreadNode);

    var expectedMessageNodeId = new NodeId(UUID, NODE_ID, NodeType.MESSAGE);
    Assertions.assertThat(result.get(1).getNodeId()).isEqualTo(expectedMessageNodeId);
    Assertions.assertThat(result.get(1).getCreatorId()).isEqualTo(USER_ID);
    Assertions.assertThat(result.get(1).getPrivateNode()).isNull();
    Assertions.assertThat(result.get(1).getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    Assertions.assertThat(result.get(1).getCreationTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getLastUpdateTs()).isEqualTo(TS);
    Assertions.assertThat(result.get(1).getData()).isEqualTo(new MessageData(null, true).toJson());
    Assertions.assertThat(result.get(1).getDeleted()).isNull();

    verify(permissionService, times(2))
        .verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(nodeService).insertNode(result.get(1));
    verify(feedService).refreshFeedsForResource(threadNodeId.toFeedItemKey(), TS);
    verify(threadNotificationSender)
        .sendNewGroupMessageNotification(USER, groupNode, threadNodeId, expectedMessageNodeId,
            null);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("threads/%s/messages/%s/image.jpeg", NODE_ID, UUID)),
            captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore)
        .storeImage(eq(String.format("threads/%s/messages/%s/t-image.jpeg", NODE_ID, UUID)), any());
  }

  @Test
  public void addMessageToGroupThreadRethrowsIfThreadAlreadyExists() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(
        () -> threadService
            .addMessageToGroupThread(USER, groupNodeId, threadNodeId, TEXT, null, TS))
        .isInstanceOf(InternalError.class);

    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(threadNotificationSender, never())
        .sendNewGroupMessageNotification(any(), any(), any(), any(), any());
  }

  @Test
  public void addMessageToGroupThreadRethrowsIfImageInaccessible() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(
        () -> threadService
            .addMessageToGroupThread(USER, groupNodeId, threadNodeId, TEXT, fileMock, TS))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error accessing image input stream");

    verify(nodeService, never()).refreshLastUpdateTs(any(), any());
    verify(permissionService, never()).createPermissionGroup(any(), any());
    verify(nodeService, never()).insertNode(any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void addMessageToGroupThreadThrowsIfImageFileFormatIncorrect() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(
        () -> threadService
            .addMessageToGroupThread(USER, groupNodeId, threadNodeId, TEXT, fileMock, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");
  }

  @Test
  public void addMessageToGroupThreadThrowsIfImageResolutionTooHigh() throws Exception {
    var groupNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var groupNode = new Builder(groupNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(groupNodeId)).thenReturn(groupNode);

    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(
        () -> threadService
            .addMessageToGroupThread(USER, groupNodeId, threadNodeId, TEXT, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");
  }

  @Test
  public void getImage() throws Exception {
    var nodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.MESSAGE);
    var node = new Builder(nodeId).withPermissionGroupId(PERMISSION_GROUP_ID).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);
    when(cloudStore.getFile(String.format("threads/%s/messages/%s/image.jpeg", PARENT_ID, NODE_ID)))
        .thenReturn(FILE.getBytes());

    var result = threadService.getImage(USER, nodeId);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getImageThumbnail() throws Exception {
    var nodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.MESSAGE);
    var node = new Builder(nodeId).withPermissionGroupId(PERMISSION_GROUP_ID).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);
    when(cloudStore
        .getFile(String.format("threads/%s/messages/%s/t-image.jpeg", PARENT_ID, NODE_ID)))
        .thenReturn(FILE.getBytes());

    var result = threadService.getImageThumbnail(USER, nodeId);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void clearThreadForUser() throws Exception {
    var threadNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.THREAD);
    var threadNode = new Builder(threadNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(threadNodeId)).thenReturn(threadNode);

    threadService.clearThreadForUser(USER, threadNodeId, TS);

    verify(feedService).updateBeginTs(threadNodeId.toFeedItemKey(), USER_ID, TS, TS);
  }

  private InputStream generateImageInputStream(int width, int height) throws Exception {
    var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    var outputStream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpeg", outputStream);

    return new ByteArrayInputStream(outputStream.toByteArray());
  }
}
