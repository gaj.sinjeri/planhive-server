package app.planhive.services.thread.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import org.junit.jupiter.api.Test;

public class MessageDataTest {

  private static final String CONTENT = "This is our first conversation.";

  @Test
  public void constructor() {
    var obj = new MessageData(CONTENT, true);

    assertThat(obj.getTextContent()).isEqualTo(CONTENT);
    assertThat(obj.isImagePresent()).isEqualTo(true);
  }

  @Test
  public void constructorWithoutContent() {
    var obj = new MessageData(null, true);

    assertThat(obj.getTextContent()).isEqualTo(null);
    assertThat(obj.isImagePresent()).isEqualTo(true);
  }

  @Test
  public void constructorWithoutImage() {
    var obj = new MessageData(CONTENT, false);

    assertThat(obj.getTextContent()).isEqualTo(CONTENT);
    assertThat(obj.isImagePresent()).isEqualTo(false);
  }

  @Test
  public void constructorThrowsIfTextContentNullAndImageNotPresent() {
    assertThatThrownBy(() -> new MessageData(null, false))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Either textContent has to be present or imagePresent true");
  }

  @Test
  public void constructorThrowsIfTextContentEmpty() {
    assertThatThrownBy(() -> new MessageData("", false))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Either textContent has to be present or imagePresent true");
  }

  @Test
  public void equality() {
    var obj1 = new MessageData(CONTENT, true);
    var obj2 = new MessageData(CONTENT, true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new MessageData(CONTENT, true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new MessageData(CONTENT, true);
    var obj2 = new MessageData(CONTENT, false);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new MessageData(CONTENT, true);

    assertThat(obj.toJson())
        .isEqualTo(String.format("{\"textContent\":\"%s\",\"imagePresent\":%s}", CONTENT, true));
  }

  @Test
  public void toJsonRequiredFieldsOnly() {
    var obj = new MessageData(null, true);

    assertThat(obj.toJson()).isEqualTo(String.format("{\"imagePresent\":%s}", true));
  }

  @Test
  public void fromJson() {
    var obj = NodeData
        .fromJson(String.format("{\"textContent\":\"%s\",\"imagePresent\":%s}", CONTENT, true),
            MessageData.class);

    var expectedObj = new MessageData(CONTENT, true);
    assertThat(obj).isEqualTo(expectedObj);
  }

  @Test
  public void fromJsonRequiredFieldsOnly() {
    var obj = NodeData
        .fromJson(String.format("{\"imagePresent\":\"%s\"}", true), MessageData.class);

    var expectedObj = new MessageData(null, true);
    assertThat(obj).isEqualTo(expectedObj);
  }
}
