package app.planhive.services.authentication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.AuthenticationFailedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class AuthenticatorImplTest {

  private static final String USER_ID = "asd-32rfe-2f-faf3";
  private static final String SESSION_ID = "fe09420f32krm";

  private UserDAO userDAO;
  private AuthenticatorImpl authenticatorImpl;

  @BeforeEach
  public void setUp() {
    userDAO = mock(UserDAO.class);
    authenticatorImpl = new AuthenticatorImpl(userDAO);
  }

  @Test
  public void authenticateUserSuccessfulAuthentication()
      throws ResourceNotFoundException, AuthenticationFailedException {
    var user = new User.Builder(USER_ID).withSessionId(SESSION_ID).build();
    when(userDAO.getUser(USER_ID)).thenReturn(user);

    var result = authenticatorImpl.authenticateUser(USER_ID, SESSION_ID);

    assertThat(result).isEqualTo(user);
  }

  @Test
  public void authenticateUserThrowsIfUnauthenticated() {
    var user = new User.Builder(USER_ID).withSessionId(SESSION_ID).build();
    when(userDAO.getUser(USER_ID)).thenReturn(user);

    assertThatThrownBy(
        () -> authenticatorImpl.authenticateUser(USER_ID, "dr35f-12evil2vc32rf"))
        .isInstanceOf(AuthenticationFailedException.class);
  }

  @Test
  public void authenticateUserThrowsIfItemDoesNotExist() {
    when(userDAO.getUser(USER_ID)).thenReturn(null);

    assertThatThrownBy(
        () -> authenticatorImpl.authenticateUser(USER_ID, "dr35f-12evil2vc32rf"))
        .isInstanceOf(ResourceNotFoundException.class);
  }
}
