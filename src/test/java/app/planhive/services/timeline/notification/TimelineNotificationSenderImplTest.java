package app.planhive.services.timeline.notification;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import app.planhive.exception.UnauthorisedException;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class TimelineNotificationSenderImplTest {

  private static final User USER = new User.Builder("123").withName("Roger").build();
  private static final String USER_ID_2 = "234";
  private static final String EVENT_NAME = "Vegas!";
  private static final String ACTIVITY_NAME = "Yes!";
  private static final NodeId EVENT_NODE_ID = new NodeId("012", NodeType.EVENT);
  private static final String PERMISSION_GROUP_ID = "P123";
  private static final Node EVENT_NODE = new Node.Builder(EVENT_NODE_ID)
      .withPermissionGroupId(PERMISSION_GROUP_ID)
      .withCreatorId(USER_ID_2)
      .withData(new EventData(EventState.ACTIVE, EVENT_NAME, "Whatever", 0L, null).toJson())
      .build();
  private static final NodeId TIMELINE_NODE_ID = new NodeId("123", "012", NodeType.TIMELINE);
  private static final NodeId ACTIVITY_NODE_ID = new NodeId("234", "123", NodeType.ACTIVITY);

  private NotificationService notificationService;
  private PermissionService permissionService;
  private TimelineNotificationSender timelineNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);
    permissionService = mock(PermissionService.class);

    timelineNotificationSender = new TimelineNotificationSenderImpl(notificationService,
        permissionService);
  }

  @Test
  public void sendNewActivityNotificationApproved() {
    timelineNotificationSender
        .sendNewActivityNotification(USER, EVENT_NODE_ID, EVENT_NAME, TIMELINE_NODE_ID,
            ACTIVITY_NODE_ID, ActivityState.APPROVED);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s added a new activity.", USER.getName()),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendNewActivityNotificationSuggestion() {
    timelineNotificationSender
        .sendNewActivityNotification(USER, EVENT_NODE_ID, EVENT_NAME, TIMELINE_NODE_ID,
            ACTIVITY_NODE_ID, ActivityState.SUGGESTION);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s suggested a new activity.", USER.getName()),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityCanceledNotification() {
    timelineNotificationSender
        .sendActivityCanceledNotification(USER, EVENT_NODE_ID, EVENT_NAME, TIMELINE_NODE_ID,
            ACTIVITY_NODE_ID, ACTIVITY_NAME);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s has been cancelled.", ACTIVITY_NAME), NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityApprovedNotification() {
    timelineNotificationSender
        .sendActivityApprovedNotification(USER, EVENT_NODE_ID, EVENT_NAME, TIMELINE_NODE_ID,
            ACTIVITY_NODE_ID, ACTIVITY_NAME);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s has been approved.", ACTIVITY_NAME), NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityDataChangedNotificationForSuggestion() {
    timelineNotificationSender
        .sendActivityDataChangedNotification(USER.getId(), EVENT_NODE_ID, EVENT_NAME,
            TIMELINE_NODE_ID, ACTIVITY_NODE_ID, null, true);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "A suggestion has been updated!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityDataChangedNotificationForApproved() {
    timelineNotificationSender
        .sendActivityDataChangedNotification(USER.getId(), EVENT_NODE_ID, EVENT_NAME,
            TIMELINE_NODE_ID, ACTIVITY_NODE_ID, null, false);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "An activity has been updated!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityDataChangedNotificationForSuggestionWithTitle() {
    timelineNotificationSender
        .sendActivityDataChangedNotification(USER.getId(), EVENT_NODE_ID, EVENT_NAME,
            TIMELINE_NODE_ID, ACTIVITY_NODE_ID, "Coffee", true);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "A suggestion (Coffee) has been updated!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityDataChangedNotificationForApprovedWithTitle() {
    timelineNotificationSender
        .sendActivityDataChangedNotification(USER.getId(), EVENT_NODE_ID, EVENT_NAME,
            TIMELINE_NODE_ID, ACTIVITY_NODE_ID, "Coffee", false);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "An activity (Coffee) has been updated!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityReactionNotification() throws Exception {
    timelineNotificationSender
        .sendActivityReactionNotification(USER, EVENT_NODE, TIMELINE_NODE_ID, ACTIVITY_NODE_ID,
            null, ActivityReaction.VOTE);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "Roger liked a suggestion!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of(USER_ID_2), false);

    verify(permissionService).verifyAccessRight(USER_ID_2, PERMISSION_GROUP_ID, AccessRight.WRITE);
  }

  @Test
  public void sendActivityReactionNotificationWithTitle() throws Exception {
    timelineNotificationSender
        .sendActivityReactionNotification(USER, EVENT_NODE, TIMELINE_NODE_ID, ACTIVITY_NODE_ID,
            "Coffee", ActivityReaction.REJECT);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "Roger rejected a suggestion (Coffee)!", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of(USER_ID_2), false);

    verify(permissionService).verifyAccessRight(USER_ID_2, PERMISSION_GROUP_ID, AccessRight.WRITE);
  }

  @Test
  public void sendActivityReactionNotificationReturnsIfReactionByCreator() throws Exception {
    timelineNotificationSender
        .sendActivityReactionNotification(new User.Builder(USER_ID_2).build(), EVENT_NODE,
            TIMELINE_NODE_ID, ACTIVITY_NODE_ID, null, ActivityReaction.VOTE);

    verify(notificationService, never()).sendNotificationByUserIds(any(), any(), anyBoolean());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
  }

  @Test
  public void sendActivityReactionNotificationReturnsIfCreatorNoLongerPartOfEvent()
      throws Exception {
    doThrow(new UnauthorisedException("")).when(permissionService)
        .verifyAccessRight(USER_ID_2, PERMISSION_GROUP_ID, AccessRight.WRITE);

    timelineNotificationSender
        .sendActivityReactionNotification(USER, EVENT_NODE, TIMELINE_NODE_ID, ACTIVITY_NODE_ID,
            null, ActivityReaction.VOTE);

    verify(notificationService, never()).sendNotificationByUserIds(any(), any(), anyBoolean());
  }

  @Test
  public void sendActivityDeletedNotification() {
    timelineNotificationSender
        .sendActivityDeletedNotification(USER, EVENT_NODE, TIMELINE_NODE_ID, ACTIVITY_NODE_ID,
            null);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME, "An activity has been deleted.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }

  @Test
  public void sendActivityDeletedNotificationWithTitle() {
    timelineNotificationSender
        .sendActivityDeletedNotification(USER, EVENT_NODE, TIMELINE_NODE_ID, ACTIVITY_NODE_ID,
            "Coffee");

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "012", NodeType.EVENT.name(),
        "123", "012", NodeType.TIMELINE.name(), "234", "123", NodeType.ACTIVITY.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "An activity has been deleted (Coffee).", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, TIMELINE_NODE_ID, List.of(USER.getId()),
            true);
  }
}
