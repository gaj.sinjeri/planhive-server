package app.planhive.services.timeline.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

public class ActivityTimeTest {

  private static final Long TS_1 = 1L;
  private static final Long TS_2 = 2L;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void builder() {
    var obj = new ActivityTime(TS_1, TS_2);

    assertThat(obj.getStartTs()).isEqualTo(TS_1);
    assertThat(obj.getEndTs()).isEqualTo(TS_2);
  }

  @Test
  public void builderWithEndTsZero() {
    var obj = new ActivityTime(TS_1, 0L);

    assertThat(obj.getStartTs()).isEqualTo(TS_1);
    assertThat(obj.getEndTs()).isEqualTo(0L);
  }

  @Test
  public void builderThrowsIfStartTsNull() {
    assertThatThrownBy(() -> new ActivityTime(null, TS_2))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfEndTsNull() {
    assertThatThrownBy(() -> new ActivityTime(TS_1, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfStartTsAfterEndTs() {
    assertThatThrownBy(() -> new ActivityTime(TS_2, TS_1))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void equality() {
    var obj1 = new ActivityTime(TS_1, TS_2);
    var obj2 = new ActivityTime(TS_1, TS_2);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ActivityTime(TS_1, TS_2);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ActivityTime(TS_1, TS_2);
    var obj2 = new ActivityTime(TS_1, 0L);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = new ActivityTime(TS_1, TS_2);

    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr)
        .isEqualTo(String.format("{\"startTs\":%s,\"endTs\":%s}", TS_1, TS_2));
  }

  @Test
  public void deserialize() throws Exception {
    var input = String.format("{\"startTs\":%s,\"endTs\":%s}", TS_1, TS_2);

    var obj = mapper.readValue(input, ActivityTime.class);

    assertThat(obj).isEqualTo(new ActivityTime(TS_1, TS_2));
  }
}
