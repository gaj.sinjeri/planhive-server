package app.planhive.services.timeline.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import org.junit.jupiter.api.Test;

public class ActivityReactionDataTest {

  private static final ActivityReaction ACTIVITY_REACTION = ActivityReaction.VOTE;

  @Test
  public void constructor() {
    var obj = new ActivityReactionData(ACTIVITY_REACTION);

    assertThat(obj.getActivityReaction()).isEqualTo(ACTIVITY_REACTION);
  }

  @Test
  public void constructorThrowsIfActivityReactionNull() {
    assertThatThrownBy(() -> new ActivityReactionData(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new ActivityReactionData(ACTIVITY_REACTION);
    var obj2 = new ActivityReactionData(ACTIVITY_REACTION);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ActivityReactionData(ACTIVITY_REACTION);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ActivityReactionData(ACTIVITY_REACTION);
    var obj2 = new ActivityReactionData(ActivityReaction.REJECT);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new ActivityReactionData(ACTIVITY_REACTION);

    assertThat(obj.toJson()).isEqualTo(String.format("{\"activityReaction\":\"%s\"}", ACTIVITY_REACTION));
  }

  @Test
  public void fromJson() {
    var obj = NodeData
        .fromJson(String.format("{\"activityReaction\":\"%s\"}", ACTIVITY_REACTION),
            ActivityReactionData.class);

    var expectedObj = new ActivityReactionData(ACTIVITY_REACTION);
    assertThat(obj).isEqualTo(expectedObj);
  }

  @Test
  public void fromJsonFailsIfActivityReactionMissing() {
    assertThatThrownBy(() -> NodeData.fromJson("{}", ActivityReactionData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'activityReaction'");
  }
}