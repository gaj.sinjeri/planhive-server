package app.planhive.services.timeline.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import org.junit.jupiter.api.Test;

public class ActivityDataTest {

  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);
  private static final String ACTIVITY_TITLE = "Water Skiing";
  private static final String DESCRIPTION = "Meet at the water sports shack";
  private static final String NOTE = "Bring a camera!";

  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";
  private static final Location LOCATION = new Location(LOCATION_TITLE, ADDRESS, LATITUDE,
      LONGITUDE);

  @Test
  public void builder() {
    var obj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThat(obj.getActivityState()).isEqualTo(ActivityState.APPROVED);
    assertThat(obj.getTitle()).isEqualTo(ACTIVITY_TITLE);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getNote()).isEqualTo(NOTE);
    assertThat(obj.getTime()).isEqualTo(ACTIVITY_TIME);
    assertThat(obj.getLocation()).isEqualTo(LOCATION);
  }

  @Test
  public void builderThrowsIfActivityStateNull() {
    var builder = new ActivityData.Builder()
        .withActivityState(null)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build)
        .isInstanceOf(NullPointerException.class)
        .hasMessageContaining("Activity State cannot be null");
  }

  @Test
  public void builderThrowsIfActivityTitleNull() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(null)
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void builderThrowsIfActivityTitleEmpty() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle("")
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void builderThrowsIfActivityTitleTooLong() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(new String(new char[26]).replace('\0', ' '))
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void builderThrowsIfDescriptionTooLong() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(new String(new char[513]).replace('\0', ' '))
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void builderThrowsIfNoteTooLong() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withNote(new String(new char[129]).replace('\0', ' '))
        .withTime(ACTIVITY_TIME);

    assertThatThrownBy(builder::build).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void builderThrowsIfTimeNull() {
    var builder = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(null);

    assertThatThrownBy(builder::build)
        .isInstanceOf(NullPointerException.class)
        .hasMessageContaining("Time cannot be null");
  }

  @Test
  public void equality() {
    var obj1 = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    var obj2 = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    var obj2 = new ActivityData.Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThat(obj.toJson()).isEqualTo(String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"description\":\"%s\",\"note\":\"%s\","
            + "\"time\":{\"startTs\":%s,\"endTs\":%s},"
            + "\"location\":{\"title\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
            + "\"longitude\":\"%s\"}}", ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE));
  }

  @Test
  public void toJsonRequiredFieldsOnly() {
    var obj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();

    assertThat(obj.toJson()).isEqualTo(String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"time\":{\"startTs\":%s,\"endTs\":%s}}",
        ActivityState.APPROVED, ACTIVITY_TITLE, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs()));
  }

  @Test
  public void fromJson() {
    var obj = NodeData.fromJson(String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"description\":\"%s\",\"note\":\"%s\","
            + "\"time\":{\"startTs\":%s,\"endTs\":%s},"
            + "\"location\":{\"title\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
            + "\"longitude\":\"%s\"}}", ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE), ActivityData.class);

    var expectedObj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    assertThat(obj).isEqualTo(expectedObj);
  }

  @Test
  public void fromJsonRequiredFieldsOnly() {
    var obj = NodeData.fromJson(String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"time\":{\"startTs\":%s,\"endTs\":%s}}",
        ActivityState.APPROVED, ACTIVITY_TITLE, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs()), ActivityData.class);

    var expectedObj = new ActivityData.Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    assertThat(obj).isEqualTo(expectedObj);
  }
}
