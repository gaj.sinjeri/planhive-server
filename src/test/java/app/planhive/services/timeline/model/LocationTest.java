package app.planhive.services.timeline.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

public class LocationTest {

  private static final String TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj.getTitle()).isEqualTo(TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void equality() {
    var obj1 = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);
    var obj2 = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);
    var obj2 = new Location(TITLE, "No no...", LATITUDE, LONGITUDE);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void constructorThrowsIfLatitudeNotPresent() {
    assertThatThrownBy(() -> new Location(null, null, null, LONGITUDE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Latitude cannot be null or empty");
  }

  @Test
  public void constructorThrowsIfLongitudeNotPresent() {
    assertThatThrownBy(() -> new Location(null, null, LATITUDE, null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Longitude cannot be null or empty");
  }

  @Test
  public void serialize() throws Exception {
    var obj = new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE);

    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format("{\"title\":\"%s\",\"address\":\"%s\","
        + "\"latitude\":\"%s\",\"longitude\":\"%s\"}", TITLE, ADDRESS, LATITUDE, LONGITUDE));
  }

  @Test
  public void deserialize() throws Exception {
    var input = String.format("{\"title\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
        + "\"longitude\":\"%s\"}", TITLE, ADDRESS, LATITUDE, LONGITUDE);

    var obj = mapper.readValue(input, Location.class);

    assertThat(obj).isEqualTo(new Location(TITLE, ADDRESS, LATITUDE, LONGITUDE));
  }
}
