package app.planhive.services.timeline.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.timeline.model.ActivityData.Builder;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityReactionData;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.timeline.notification.TimelineNotificationSender;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Optional;

@TestInstance(Lifecycle.PER_CLASS)
public class TimelineServiceImplTest {

  private static final String UUID = "1234";
  private static final String UUID_2 = "2345";
  private static final String NODE_ID = "321321";
  private static final String PARENT_ID = "v34v-43-gj03g";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "123";
  private static final User USER = new User.Builder(USER_ID).build();
  private static final Instant TS = Instant.now();

  private static final String ACTIVITY_TITLE = "Something Something";
  private static final String DESCRIPTION = "Something Something Something";
  private static final String NOTE = "Something.";
  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);
  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";
  private static final Location LOCATION = new Location(LOCATION_TITLE, ADDRESS, LATITUDE,
      LONGITUDE);

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private TimelineNotificationSender timelineNotificationSender;
  private TimelineService timelineService;

  @BeforeEach
  public void setUp() {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID).thenReturn(UUID_2);

    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    timelineNotificationSender = mock(TimelineNotificationSender.class);

    timelineService = new TimelineServiceImpl(secureGenerator, nodeService, feedService,
        permissionService, timelineNotificationSender);
  }

  @Test
  public void addTimelineWidgetStoresWidget() {
    var result = timelineService
        .addTimelineWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(), Optional.empty(),
            TS);

    ArgumentCaptor<List<Node>> captor = ArgumentCaptor.forClass(List.class);
    verify(nodeService).batchInsertNode(captor.capture());

    var arg = captor.getValue();

    assertThat(arg).hasSize(1);
    assertThat(arg).extracting(Node::getNodeId)
        .containsOnly(new NodeId(UUID, PARENT_ID, NodeType.TIMELINE));
    assertThat(arg).extracting(Node::getCreatorId).containsOnly(USER_ID);
    assertThat(arg).extracting(Node::getPrivateNode).containsOnlyNulls();
    assertThat(arg).extracting(Node::getPermissionGroupId).containsOnly(PERMISSION_GROUP_ID);
    assertThat(arg).extracting(Node::getCreationTs).containsOnly(TS);
    assertThat(arg).extracting(Node::getLastUpdateTs).containsOnly(TS);
    assertThat(arg).extracting(Node::getData).containsOnlyNulls();
    assertThat(arg).extracting(Node::getDeleted).containsOnlyNulls();

    assertThat(result).isEqualTo(arg);

    verify(feedService).createFeeds(arg.get(0).getNodeId().toFeedItemKey(), List.of(USER_ID),
        NodeType.TIMELINE.name(), TS);
  }

  @Test
  public void addTimelineWidgetWithActivityData() {
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    var result = timelineService
        .addTimelineWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(),
            Optional.of(activityData), TS);

    ArgumentCaptor<List<Node>> captor = ArgumentCaptor.forClass(List.class);
    verify(nodeService).batchInsertNode(captor.capture());

    var arg = captor.getValue();

    assertThat(arg).hasSize(2);
    assertThat(arg).extracting(Node::getNodeId)
        .containsOnly(new NodeId(UUID, PARENT_ID, NodeType.TIMELINE),
            new NodeId(UUID_2, UUID, NodeType.ACTIVITY));
    assertThat(arg).extracting(Node::getCreatorId).containsOnly(USER_ID);
    assertThat(arg).extracting(Node::getPrivateNode).containsOnlyNulls();
    assertThat(arg).extracting(Node::getPermissionGroupId).containsOnly(PERMISSION_GROUP_ID);
    assertThat(arg).extracting(Node::getCreationTs).containsOnly(TS);
    assertThat(arg).extracting(Node::getLastUpdateTs).containsOnly(TS);
    assertThat(arg).extracting(Node::getData).containsExactly(null, activityData.toJson());
    assertThat(arg).extracting(Node::getDeleted).containsOnlyNulls();

    assertThat(result).isEqualTo(arg);

    verify(feedService).createFeeds(arg.get(0).getNodeId().toFeedItemKey(), List.of(USER_ID),
        NodeType.TIMELINE.name(), TS);
  }

  @Test
  public void addActivitySuggestion() throws Exception {
    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withData("{\"eventState\":\"ACTIVE\",\"name\":\"Kayaking\",\"coverTs\":0}")
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(timelineNodeId)).thenReturn(timelineNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    var result = timelineService.addActivity(USER, timelineNodeId, activityData, TS);

    var expectedNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .withData(activityData.toJson())
        .build();
    assertThat(result).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendNewActivityNotification(USER, eventNodeId, "Kayaking", timelineNodeId, expectedNodeId,
            ActivityState.SUGGESTION);
  }

  @Test
  public void addActivityApproved() throws Exception {
    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withData("{\"eventState\":\"ACTIVE\",\"name\":\"Kayaking\",\"coverTs\":0}")
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(timelineNodeId)).thenReturn(timelineNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();
    var result = timelineService.addActivity(USER, timelineNodeId, activityData, TS);

    var expectedNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .withData(activityData.toJson())
        .build();
    assertThat(result).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendNewActivityNotification(USER, eventNodeId, "Kayaking", timelineNodeId, expectedNodeId,
            ActivityState.APPROVED);
  }

  @Test
  public void addActivityThrowsIfActivityStateCancelled() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(timelineNodeId)).thenReturn(timelineNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.CANCELED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    assertThatThrownBy(() -> timelineService.addActivity(USER, timelineNodeId, activityData, TS))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining(String.format("Activity state {%s} not supported for this operation",
            ActivityState.CANCELED));

    verify(nodeService, never()).insertNode(any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendNewActivityNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void addActivityRethrowsIfNodeAlreadyExists() throws Exception {
    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(timelineNodeId)).thenReturn(timelineNode);

    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();

    assertThatThrownBy(() -> timelineService.addActivity(USER, timelineNodeId, activityData, TS))
        .isInstanceOf(InternalError.class);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendNewActivityNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void updateActivityStateForApproval() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, "Yes", "No", TS.getMillis(), null);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var updatedActivityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var updatedActivityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(updatedActivityData.toJson())
        .build();
    when(nodeService.updateNodeData(activityNodeId, updatedActivityData.toJson(), TS))
        .thenReturn(updatedActivityNode);

    var result = timelineService
        .updateActivityState(USER, timelineNodeId, activityNodeId, ActivityState.APPROVED, TS);

    assertThat(result).isEqualTo(updatedActivityNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityApprovedNotification(USER, eventNodeId, "Yes", timelineNodeId,
            activityNodeId, ACTIVITY_TITLE);
  }

  @Test
  public void updateActivityStateForCancellation() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, "Yes", "No", TS.getMillis(), null);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var updatedActivityData = new Builder()
        .withActivityState(ActivityState.CANCELED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var updatedActivityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(updatedActivityData.toJson())
        .build();
    when(nodeService.updateNodeData(activityNodeId, updatedActivityData.toJson(), TS))
        .thenReturn(updatedActivityNode);

    var result = timelineService
        .updateActivityState(USER, timelineNodeId, activityNodeId, ActivityState.CANCELED, TS);

    assertThat(result).isEqualTo(updatedActivityNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityCanceledNotification(USER, eventNodeId, "Yes", timelineNodeId,
            activityNodeId, ACTIVITY_TITLE);
  }

  @Test
  public void updateActivityStateThrowsIfUpdateIsApproveButStateIsNotSuggestion() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateActivityState(USER, timelineNodeId, activityNodeId, ActivityState.APPROVED, TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            String.format("Cannot approve activity unless state is %s", ActivityState.SUGGESTION));

    verify(nodeService, never()).updateNodeData(any(), any(), any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityCanceledNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void updateActivityStateThrowsIfUpdateIsCancelButStateIsNotApproved() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateActivityState(USER, timelineNodeId, activityNodeId, ActivityState.CANCELED, TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(
            String.format("Cannot cancel activity unless state is %s", ActivityState.APPROVED));

    verify(nodeService, never()).updateNodeData(any(), any(), any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityCanceledNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void updateActivityStateThrowsIfActivityStateUnsupported() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateActivityState(USER, timelineNodeId, activityNodeId, ActivityState.SUGGESTION, TS))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining(
            String.format("Unsupported activity state {%s}", ActivityState.SUGGESTION));

    verify(nodeService, never()).updateNodeData(any(), any(), any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityCanceledNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void updateActivityDataForSuggestion() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, "Yes", "No", TS.getMillis(), null);
    var eventNode = new Node.Builder(eventNodeId)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var data = String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"description\":\"%s\",\"note\":\"%s\","
            + "\"time\":{\"startTs\":%s,\"endTs\":%s},"
            + "\"location\":{\"title\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
            + "\"longitude\":\"%s\"}}", ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var updatedActivityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData("{}")
        .build();
    when(nodeService.updateNodeData(activityNodeId, data, TS)).thenReturn(updatedActivityNode);

    var result = timelineService
        .updateActivityData(USER, timelineNodeId, activityNodeId, ACTIVITY_TITLE, DESCRIPTION, NOTE,
            ACTIVITY_TIME, LOCATION, TS);

    assertThat(result).isEqualTo(updatedActivityNode);
    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityDataChangedNotification(USER_ID, eventNodeId, "Yes", timelineNodeId,
            activityNodeId, ACTIVITY_TITLE, true);
  }

  @Test
  public void updateActivityDataForFinal() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, "Yes", "No", TS.getMillis(), null);
    var eventNode = new Node.Builder(eventNodeId)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var data = String.format(
        "{\"activityState\":\"%s\",\"title\":\"%s\",\"description\":\"%s\",\"note\":\"%s\","
            + "\"time\":{\"startTs\":%s,\"endTs\":%s},"
            + "\"location\":{\"title\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
            + "\"longitude\":\"%s\"}}", ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var updatedActivityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData("{}")
        .build();
    when(nodeService.updateNodeData(activityNodeId, data, TS)).thenReturn(updatedActivityNode);

    var result = timelineService
        .updateActivityData(USER, timelineNodeId, activityNodeId, ACTIVITY_TITLE, DESCRIPTION, NOTE,
            ACTIVITY_TIME, LOCATION, TS);

    assertThat(result).isEqualTo(updatedActivityNode);
    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityDataChangedNotification(USER_ID, eventNodeId, "Yes", timelineNodeId,
            activityNodeId, ACTIVITY_TITLE, false);
  }

  @Test
  public void updateActivityDataThrowsIfActivityStateCanceled() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.CANCELED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateActivityData(USER, timelineNodeId, activityNodeId, ACTIVITY_TITLE, DESCRIPTION, NOTE,
            ACTIVITY_TIME, LOCATION, TS))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining(String
            .format("Activity state {%s} not supported for this operation",
                ActivityState.CANCELED));

    verify(nodeService, never()).updateNodeData(any(), any(), any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityDataChangedNotification(any(), any(), any(), any(), any(), any(),
            anyBoolean());
  }

  @Test
  public void updateActivityDataForSuggestionThrowsSinceNotCreator() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateActivityData(USER, timelineNodeId, activityNodeId, null, null, null, null, null, TS))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining(
            String.format("Cannot update data for activity %s since user is not creator", UUID));

    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(nodeService, never()).updateNodeData(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityDataChangedNotification(any(), any(), any(), any(), any(), any(),
            anyBoolean());
  }

  @Test
  public void updateReaction() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTime(ACTIVITY_TIME)
        .withTitle(ACTIVITY_TITLE)
        .build();
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId("Someone else")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var expectedNodeId = new NodeId(String.format("%s|%s", UUID, USER_ID), NODE_ID,
        NodeType.ACTIVITY_REACTION);
    var expectedData = new ActivityReactionData(ActivityReaction.VOTE).toJson();
    var activityReactionNode = new Node.Builder(new NodeId("2", "1", NodeType.ACTIVITY_REACTION))
        .build();
    when(nodeService
        .insertNodeWithDataOnly(expectedNodeId, USER_ID, expectedData, TS))
        .thenReturn(activityReactionNode);

    var result = timelineService
        .updateReaction(USER, timelineNodeId, activityNodeId, ActivityReaction.VOTE, TS);

    assertThat(result).isEqualTo(activityReactionNode);
    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityReactionNotification(USER, eventNode, timelineNodeId, activityNodeId,
            ACTIVITY_TITLE, ActivityReaction.VOTE);
  }

  @Test
  public void updateReactionThrowsIfUserCreatorOfActivity() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    assertThatThrownBy(() -> timelineService
        .updateReaction(USER, timelineNodeId, activityNodeId, ActivityReaction.VOTE, TS))
        .isInstanceOf(InvalidOperationException.class).hasMessageContaining(String
        .format("User cannot react to their own activity. Activity id %s, user id %s",
            activityNodeId.getId(), USER_ID));

    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(nodeService, never()).insertNodeWithDataOnly(any(), any(), any(), any());
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(feedService, never()).refreshFeedsForResource(any(), any());
    verify(timelineNotificationSender, never())
        .sendActivityReactionNotification(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void deleteReaction() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    timelineService.deleteReaction(USER, timelineNodeId, activityNodeId, TS);

    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);

    var expectedNodeId = new NodeId(String.format("%s|%s", UUID, USER_ID), NODE_ID,
        NodeType.ACTIVITY_REACTION);
    verify(nodeService).softDeleteNode(expectedNodeId, TS);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
  }

  @Test
  public void deleteActivitySuggestionCreator() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var activityReactionNodeId = new NodeId("123", UUID, NodeType.ACTIVITY_REACTION);
    var activityReactionNode = new Node.Builder(activityReactionNodeId).build();
    when(nodeService.queryNode(NODE_ID, NodeType.ACTIVITY_REACTION, UUID))
        .thenReturn(List.of(activityReactionNode));

    var activityNodeUpdated = new Node.Builder(activityNodeId)
        .withCreatorId("Tim")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .setDeleted(true)
        .build();
    when(nodeService.softDeleteNode(activityNodeId, TS)).thenReturn(activityNodeUpdated);

    var result = timelineService.deleteActivity(USER, timelineNodeId, activityNodeId, TS);

    assertThat(result).isEqualTo(activityNodeUpdated);

    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(nodeService).batchSoftDeleteNode(List.of(activityReactionNodeId), TS);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityDeletedNotification(USER, eventNode, timelineNodeId, activityNodeId,
            ACTIVITY_TITLE);
  }

  @Test
  public void deleteActivityApprovedCreator() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId).build();
    when(nodeService.getNodeOrThrow(timelineNodeId)).thenReturn(timelineNode);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.APPROVED)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    verify(permissionService, never()).verifyAccessRight(any(), any(), any());

    var activityReactionNodeId = new NodeId("123", UUID, NodeType.ACTIVITY_REACTION);
    var activityReactionNode = new Node.Builder(activityReactionNodeId).build();
    when(nodeService.queryNode(NODE_ID, NodeType.ACTIVITY_REACTION, UUID))
        .thenReturn(List.of(activityReactionNode));

    var activityNodeUpdated = new Node.Builder(activityNodeId)
        .withCreatorId("Tim")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .setDeleted(true)
        .build();
    when(nodeService.softDeleteNode(activityNodeId, TS)).thenReturn(activityNodeUpdated);

    var result = timelineService.deleteActivity(USER, timelineNodeId, activityNodeId, TS);

    assertThat(result).isEqualTo(activityNodeUpdated);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.DELETE);
    verify(nodeService).batchSoftDeleteNode(List.of(activityReactionNodeId), TS);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityDeletedNotification(USER, eventNode, timelineNodeId, activityNodeId,
            ACTIVITY_TITLE);
  }

  @Test
  public void deleteActivityAdmin() throws Exception {
    var timelineNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.TIMELINE);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withTime(ACTIVITY_TIME)
        .build();
    var activityNodeId = new NodeId(UUID, NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId("Tim")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(activityData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(activityNodeId)).thenReturn(activityNode);

    var activityReactionNodeId = new NodeId("123", UUID, NodeType.ACTIVITY_REACTION);
    var activityReactionNode = new Node.Builder(activityReactionNodeId).build();
    when(nodeService.queryNode(NODE_ID, NodeType.ACTIVITY_REACTION, UUID))
        .thenReturn(List.of(activityReactionNode));

    var activityNodeUpdated = new Node.Builder(activityNodeId)
        .withCreatorId("Tim")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .setDeleted(true)
        .build();
    when(nodeService.softDeleteNode(activityNodeId, TS)).thenReturn(activityNodeUpdated);

    var result = timelineService.deleteActivity(USER, timelineNodeId, activityNodeId, TS);

    assertThat(result).isEqualTo(activityNodeUpdated);

    verify(nodeService).getNodeOrThrow(timelineNodeId);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.DELETE);
    verify(nodeService).batchSoftDeleteNode(List.of(activityReactionNodeId), TS);
    verify(feedService).refreshFeedsForResource(timelineNodeId.toFeedItemKey(), TS);
    verify(timelineNotificationSender)
        .sendActivityDeletedNotification(USER, eventNode, timelineNodeId, activityNodeId,
            ACTIVITY_TITLE);
  }
}
