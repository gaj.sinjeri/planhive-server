package app.planhive.services.timeline.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityState;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UpdateActivityStateRequestTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateActivityStateRequest(ActivityState.APPROVED);

    Assertions.assertThat(obj.getActivityState()).isEqualTo(ActivityState.APPROVED);
  }

  @Test
  public void deserializationFailsIfActivityStateMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", UpdateActivityStateRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'activityState'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var obj2 = new UpdateActivityStateRequest(ActivityState.APPROVED);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateActivityStateRequest(ActivityState.APPROVED);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var obj2 = new UpdateActivityStateRequest(ActivityState.SUGGESTION);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
