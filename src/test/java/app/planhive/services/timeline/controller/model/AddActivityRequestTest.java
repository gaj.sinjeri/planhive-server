package app.planhive.services.timeline.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class AddActivityRequestTest {

  private static final String ACTIVITY_TITLE = "Water Skiing";
  private static final String DESCRIPTION = "Meet at the water sports shack";
  private static final String NOTE = "Bring a camera!";

  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);

  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    Assertions.assertThat(obj.getActivityState()).isEqualTo(ActivityState.APPROVED);
    assertThat(obj.getActivityTitle()).isEqualTo(ACTIVITY_TITLE);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getNote()).isEqualTo(NOTE);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(ACTIVITY_TIME.getEndTs());
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void constructorWithEndTsZero() {
    var obj = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), 0L, LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    Assertions.assertThat(obj.getActivityState()).isEqualTo(ActivityState.APPROVED);
    assertThat(obj.getActivityTitle()).isEqualTo(ACTIVITY_TITLE);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getNote()).isEqualTo(NOTE);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(0L);
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void constructorThrowsIfStartTsAfterEndTs() {
    assertThatThrownBy(
        () -> new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
            ACTIVITY_TIME.getEndTs(), ACTIVITY_TIME.getStartTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
            LONGITUDE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Invalid start and/or end timestamp");
  }

  @Test
  public void deserializationFailsIfActivityStateMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"activityTitle\":\"Hello\",\"startTs\":1,\"endTs\":2}",
            AddActivityRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'activityState'");
  }

  @Test
  public void deserializationFailsIfActivityTitleMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"activityState\":\"APPROVED\",\"startTs\":1,\"endTs\":2}",
            AddActivityRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'activityTitle'");
  }

  @Test
  public void deserializationFailsIfStartTsMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"activityState\":\"APPROVED\",\"activityTitle\":\"Hello\",\"endTs\":2}",
            AddActivityRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'startTs'");
  }

  @Test
  public void deserializationFailsIfEndTsMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"activityState\":\"APPROVED\",\"activityTitle\":\"Hello\",\"startTs\":1}",
            AddActivityRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'endTs'");
  }

  @Test
  public void equality() {
    var obj1 = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var obj2 = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var obj2 = new AddActivityRequest(ActivityState.APPROVED, ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), 0L, LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
