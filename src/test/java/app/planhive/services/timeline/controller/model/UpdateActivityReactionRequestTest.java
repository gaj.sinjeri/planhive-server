package app.planhive.services.timeline.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityReaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UpdateActivityReactionRequestTest {

  private static final ActivityReaction ACTIVITY_REACTION = ActivityReaction.VOTE;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateActivityReactionRequest(true, ACTIVITY_REACTION);

    assertThat(obj.isReset()).isEqualTo(true);
    Assertions.assertThat(obj.getActivityReaction()).isEqualTo(ACTIVITY_REACTION);
  }

  @Test
  public void deserializationFailsIfResetMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{}", UpdateActivityReactionRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'reset'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateActivityReactionRequest(true, ACTIVITY_REACTION);
    var obj2 = new UpdateActivityReactionRequest(true, ACTIVITY_REACTION);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateActivityReactionRequest(true, ACTIVITY_REACTION);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateActivityReactionRequest(true, ACTIVITY_REACTION);
    var obj2 = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
