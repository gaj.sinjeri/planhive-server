package app.planhive.services.timeline.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityState;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class BatchUpdateActivityStateRequestTest {

  private static final Map<String, ActivityState> UPDATES = Map.of("123", ActivityState.APPROVED);

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new BatchUpdateActivityStateRequest(UPDATES);

    Assertions.assertThat(obj.getActivityStateUpdates()).isEqualTo(UPDATES);
  }

  @Test
  public void deserializationFailsIfActivityStateUpdatesMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{}", BatchUpdateActivityStateRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'activityStateUpdates'");
  }

  @Test
  public void equality() {
    var obj1 = new BatchUpdateActivityStateRequest(UPDATES);
    var obj2 = new BatchUpdateActivityStateRequest(UPDATES);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new BatchUpdateActivityStateRequest(UPDATES);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new BatchUpdateActivityStateRequest(UPDATES);
    var obj2 = new BatchUpdateActivityStateRequest(Map.of());

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
