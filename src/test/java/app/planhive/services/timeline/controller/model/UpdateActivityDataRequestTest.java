package app.planhive.services.timeline.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

public class UpdateActivityDataRequestTest {

  private static final String ACTIVITY_TITLE = "Water Skiing";
  private static final String DESCRIPTION = "Meet at the water sports shack";
  private static final String NOTE = "Bring a camera!";

  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);

  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    assertThat(obj.getActivityTitle()).isEqualTo(ACTIVITY_TITLE);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getNote()).isEqualTo(NOTE);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(ACTIVITY_TIME.getEndTs());
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void constructorWithEndTsZero() {
    var obj = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), 0L, LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj.getActivityTitle()).isEqualTo(ACTIVITY_TITLE);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getNote()).isEqualTo(NOTE);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(0L);
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void constructorThrowsIfStartTsPresentButEndTsNot() {
    assertThatThrownBy(
        () -> new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE, 1L, null,
            LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfEndTsPresentButStartTsNot() {
    assertThatThrownBy(
        () -> new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE, null, 1L,
            LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfStartTsAfterEndTs() {
    assertThatThrownBy(
        () -> new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE, 2L, 1L,
            LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Invalid start and/or end timestamp");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var obj2 = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var obj2 = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), 15L, LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
