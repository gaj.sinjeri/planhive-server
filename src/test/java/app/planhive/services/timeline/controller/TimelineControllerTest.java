package app.planhive.services.timeline.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.timeline.controller.model.AddActivityRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityDataRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityReactionRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityStateRequest;
import app.planhive.services.timeline.model.ActivityData.Builder;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.timeline.service.TimelineService;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

@TestInstance(Lifecycle.PER_CLASS)
public class TimelineControllerTest {

  private static final User USER = new User.Builder("U1").build();
  private static final String EVENT_NODE_ID = "123";
  private static final String TIMELINE_NODE_ID = "234";
  private static final String ACTIVITY_NODE_ID = "345";

  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);
  private static final String ACTIVITY_TITLE = "Water Skiing";
  private static final String DESCRIPTION = "Meet at the water sports shack";
  private static final String NOTE = "Bring a camera!";

  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";
  private static final Location LOCATION = new Location(LOCATION_TITLE, ADDRESS, LATITUDE,
      LONGITUDE);

  private static final ActivityReaction ACTIVITY_REACTION = ActivityReaction.VOTE;
  private static final Node NODE = new Node.Builder(new NodeId("1", NodeType.TIMELINE)).build();

  private TimelineService timelineService;
  private TimelineController timelineController;

  @BeforeEach
  public void setUp() {
    timelineService = mock(TimelineService.class);

    timelineController = new TimelineController(timelineService);
  }

  @Test
  public void addActivitySuccessfulCall() throws Exception {
    var nodeId = new NodeId("1", "2", NodeType.ACTIVITY);
    var node = new Node.Builder(nodeId).build();

    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .withLocation(LOCATION)
        .build();

    when(timelineService.addActivity(eq(USER), eq(timelineNodeId), eq(activityData), any()))
        .thenReturn(node);

    var request = new AddActivityRequest(ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION,
        NOTE, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS,
        LATITUDE, LONGITUDE);
    var response = timelineController.addActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(node));
  }

  @Test
  public void addActivityWithoutLocation() throws Exception {
    var nodeId = new NodeId("1", "2", NodeType.ACTIVITY);
    var node = new Node.Builder(nodeId).build();

    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityData = new Builder()
        .withActivityState(ActivityState.SUGGESTION)
        .withTitle(ACTIVITY_TITLE)
        .withDescription(DESCRIPTION)
        .withNote(NOTE)
        .withTime(ACTIVITY_TIME)
        .build();

    when(timelineService.addActivity(eq(USER), eq(timelineNodeId), eq(activityData), any()))
        .thenReturn(node);

    var request = new AddActivityRequest(ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION,
        NOTE, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS,
        null, null);
    var response = timelineController.addActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(node));
  }

  @Test
  public void addActivityThrowsIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("Bam!")).when(timelineService)
        .addActivity(any(), any(), any(), any());

    var request = new AddActivityRequest(ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION,
        NOTE, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS,
        LATITUDE, LONGITUDE);
    var response = timelineController.addActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addActivityThrowsIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("Bam!", NODE)).when(timelineService)
        .addActivity(any(), any(), any(), any());

    var request = new AddActivityRequest(ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION,
        NOTE, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS,
        LATITUDE, LONGITUDE);
    var response = timelineController.addActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }


  @Test
  public void addActivityThrowsIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("Bam!")).when(timelineService)
        .addActivity(any(), any(), any(), any());

    var request = new AddActivityRequest(ActivityState.SUGGESTION, ACTIVITY_TITLE, DESCRIPTION,
        NOTE, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS,
        LATITUDE, LONGITUDE);
    var response = timelineController.addActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityState() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId).build();

    when(timelineService.updateActivityState(eq(USER), eq(timelineNodeId), eq(activityNodeId),
        eq(ActivityState.APPROVED), any()))
        .thenReturn(activityNode);

    var request = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var response = timelineController
        .updateActivityState(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(activityNode));
  }

  @Test
  public void updateActivityStateReturnsErrorIfResourceDoesNotExist() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceNotFoundException("Bam!")).when(timelineService)
        .updateActivityState(eq(USER), eq(timelineNodeId), eq(activityNodeId),
            eq(ActivityState.APPROVED), any());

    var request = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var response = timelineController
        .updateActivityState(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityStateReturnsErrorIfResourceDeleted() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceDeletedException("Bam!", NODE)).when(timelineService)
        .updateActivityState(eq(USER), eq(timelineNodeId), eq(activityNodeId),
            eq(ActivityState.APPROVED), any());

    var request = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var response = timelineController
        .updateActivityState(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityStateReturnsErrorIfUnauthorized() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new UnauthorisedException("Bam!")).when(timelineService)
        .updateActivityState(eq(USER), eq(timelineNodeId), eq(activityNodeId),
            eq(ActivityState.APPROVED), any());

    var request = new UpdateActivityStateRequest(ActivityState.APPROVED);
    var response = timelineController
        .updateActivityState(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityData() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId).build();

    when(timelineService.updateActivityData(eq(USER), eq(timelineNodeId), eq(activityNodeId),
        eq(ACTIVITY_TITLE), eq(DESCRIPTION), eq(NOTE), eq(ACTIVITY_TIME), eq(LOCATION), any()))
        .thenReturn(activityNode);

    var request = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var response = timelineController
        .updateActivityData(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(activityNode));
  }

  @Test
  public void updateActivityDataWithoutLocation() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId).build();

    when(timelineService.updateActivityData(eq(USER), eq(timelineNodeId), eq(activityNodeId),
        eq(ACTIVITY_TITLE), eq(DESCRIPTION), eq(NOTE), eq(ACTIVITY_TIME), eq(null), any()))
        .thenReturn(activityNode);

    var request = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, null,
        null);
    var response = timelineController
        .updateActivityData(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(activityNode));
  }

  @Test
  public void updateActivityDataReturnsErrorIfResourceDoesNotExist() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceNotFoundException("Bam!"))
        .when(timelineService)
        .updateActivityData(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_TITLE),
            eq(DESCRIPTION), eq(NOTE), eq(ACTIVITY_TIME), eq(LOCATION), any());

    var request = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var response = timelineController
        .updateActivityData(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityDataReturnsErrorIfResourceDeleted() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceDeletedException("Bam!", NODE))
        .when(timelineService)
        .updateActivityData(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_TITLE),
            eq(DESCRIPTION), eq(NOTE), eq(ACTIVITY_TIME), eq(LOCATION), any());

    var request = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var response = timelineController
        .updateActivityData(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityDataReturnsErrorIfUnauthorized() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new UnauthorisedException("Bam!"))
        .when(timelineService)
        .updateActivityData(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_TITLE),
            eq(DESCRIPTION), eq(NOTE), eq(ACTIVITY_TIME), eq(LOCATION), any());

    var request = new UpdateActivityDataRequest(ACTIVITY_TITLE, DESCRIPTION, NOTE,
        ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE,
        LONGITUDE);
    var response = timelineController
        .updateActivityData(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityReaction() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    var responseNode = new Node.Builder(new NodeId("1", "2", NodeType.ACTIVITY_REACTION)).build();
    when(timelineService
        .updateReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_REACTION),
            any()))
        .thenReturn(responseNode);

    var request = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(responseNode));
  }

  @Test
  public void updateActivityReactionReset() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    var responseNode = new Node.Builder(new NodeId("1", "2", NodeType.ACTIVITY_REACTION)).build();
    when(timelineService.deleteReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), any()))
        .thenReturn(responseNode);

    var request = new UpdateActivityReactionRequest(true, null);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).updateReaction(any(), any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(responseNode));
  }

  @Test
  public void updateActivityReactionReturnsErrorIfNotResetAndNoActivityReaction() throws Exception {
    var request = new UpdateActivityReactionRequest(false, null);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());
    verify(timelineService, never()).updateReaction(any(), any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityReactionReturnsErrorIfResourceDoesNotExist() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceNotFoundException("Bam!")).when(timelineService)
        .updateReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_REACTION),
            any());

    var request = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityReactionReturnsErrorIfResourceDeleted() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceDeletedException("Bam!", NODE)).when(timelineService)
        .updateReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_REACTION),
            any());

    var request = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityReactionReturnsErrorIfInvalidOperation() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new InvalidOperationException("Bam!")).when(timelineService)
        .updateReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_REACTION),
            any());

    var request = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateActivityReactionReturnsErrorIfUnauthorized() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new UnauthorisedException("Bam!")).when(timelineService)
        .updateReaction(eq(USER), eq(timelineNodeId), eq(activityNodeId), eq(ACTIVITY_REACTION),
            any());

    var request = new UpdateActivityReactionRequest(false, ACTIVITY_REACTION);
    var response = timelineController
        .updateActivityReaction(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID, request);

    verify(timelineService, never()).deleteReaction(any(), any(), any(), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteActivity() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    var node = new Node.Builder(new NodeId("1", "2", NodeType.ACTIVITY)).build();
    when(timelineService.deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any()))
        .thenReturn(node);

    var response = timelineController
        .deleteActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(node));
  }

  @Test
  public void deleteActivityReturnsErrorIfResourceDoesNotExist() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceNotFoundException("Bam!")).when(timelineService)
        .deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    var response = timelineController
        .deleteActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID);

    verify(timelineService).deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteActivityReturnsErrorIfResourceDeleted() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new ResourceDeletedException("Bam!", NODE)).when(timelineService)
        .deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    var response = timelineController
        .deleteActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID);

    verify(timelineService).deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteActivityReturnsErrorIfUnauthorized() throws Exception {
    var timelineNodeId = new NodeId(TIMELINE_NODE_ID, EVENT_NODE_ID, NodeType.TIMELINE);
    var activityNodeId = new NodeId(ACTIVITY_NODE_ID, TIMELINE_NODE_ID, NodeType.ACTIVITY);

    doThrow(new UnauthorisedException("Bam!")).when(timelineService)
        .deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    var response = timelineController
        .deleteActivity(USER, EVENT_NODE_ID, TIMELINE_NODE_ID, ACTIVITY_NODE_ID);

    verify(timelineService).deleteActivity(eq(USER), eq(timelineNodeId), eq(activityNodeId), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
