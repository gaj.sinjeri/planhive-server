package app.planhive.services.feed.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.feed.model.Feed;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
public class FeedDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final String RESOURCE_ID = "123";
  private static final String RESOURCE_ID_2 = "223";
  private static final String RESOURCE_ID_3 = "346";
  private static final String USER_ID_1 = "234";
  private static final String USER_ID_2 = "345";
  private static final String USER_ID_3 = "A6g3";
  private static final String RESOURCE_TYPE = "USER";
  private static final Instant TS_0 = new Instant(0);
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private DynamoDbTestUtils dynamoDbTestUtils;
  private FeedDAO feedDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());
    var dynamoDB = dynamoDbTestUtils.getDynamoDB();
    var table = dynamoDbTestUtils.getTable();

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getDynamoDB()).thenReturn(dynamoDB);
    when(dynamoDBClientMock.getTable()).thenReturn(table);

    feedDAO = new FeedDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void batchPut() {
    var userId = UUID.randomUUID().toString();
    var feed1 = new Feed.Builder("111", userId)
        .withResourceType(RESOURCE_TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    var feed2 = new Feed.Builder("222", userId)
        .withResourceType(RESOURCE_TYPE)
        .withBeginTs(TS_2)
        .setMute(false)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    feedDAO.batchPutItem(List.of(feed1, feed2));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "111",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", userId)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "222",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", userId))
    ));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("111", "222");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("feed|%s", userId));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("feed|%s|USER", userId));
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Begin"))
        .containsOnly(TS_1.getMillis(), TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getBoolean("Mute"))
        .containsOnly(true, false);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
  }

  @Test
  public void batchPutRequiredAttributesOnly() {
    var userId = UUID.randomUUID().toString();
    var feed1 = new Feed.Builder("111", userId)
        .withResourceType(RESOURCE_TYPE)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    var feed2 = new Feed.Builder("222", userId)
        .withResourceType(RESOURCE_TYPE)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_2)
        .build();
    feedDAO.batchPutItem(List.of(feed1, feed2));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "111",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", userId)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "222",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", userId))
    ));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("111", "222");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("feed|%s", userId));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("feed|%s|USER", userId));
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.get("Begin")).containsOnlyNulls();
    assertThat(storedItems).extracting(it -> it.get("End")).containsOnlyNulls();
    assertThat(storedItems).extracting(it -> it.get("Mute")).containsOnlyNulls();
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
  }

  @Test
  public void batchPutEmptyList() {
    // This test is to ensure that an empty list input does not cause an error.
    feedDAO.batchPutItem(List.of());
  }

  @Test
  public void refreshLastUpdateTsUpdatesTimestampOnly() {
    var feed1 = new Feed.Builder(RESOURCE_ID_2, USER_ID_1)
        .withLastUpdateTs(TS_2)
        .build();
    var feed2 = new Feed.Builder(RESOURCE_ID_2, USER_ID_2)
        .withLastUpdateTs(TS_2)
        .build();

    feedDAO.refreshLastUpdateTs(List.of(feed1, feed2));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_2,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_2,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_2))
    ));

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(RESOURCE_ID_2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("feed|%s", USER_ID_1), String.format("feed|%s", USER_ID_2));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE),
            String.format("feed|%s|%s", USER_ID_2, RESOURCE_TYPE));
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.hasAttribute("Begin")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.hasAttribute("Mute")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
  }

  @Test
  public void refreshLastUpdateTsDoesNotUpdateIfItemDoesNotExist() {
    var feed = new Feed.Builder("some-non-existent-resource", "some-hypothetical-user")
        .withLastUpdateTs(TS_0)
        .build();
    feedDAO.refreshLastUpdateTs(List.of(feed));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "some-non-existent-resource",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", "some-hypothetical-user"))
    ));

    assertThat(storedItems.size()).isEqualTo(0);
  }

  @Test
  public void refreshLastUpdateTsDoesNotUpdateIfTsOlderThanDbItem() {
    var feed = new Feed.Builder(RESOURCE_ID_2, USER_ID_1)
        .withLastUpdateTs(TS_0)
        .build();
    feedDAO.refreshLastUpdateTs(List.of(feed));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_3,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
    ));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(RESOURCE_ID_3);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("feed|%s", USER_ID_1));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE));
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.hasAttribute("Begin")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.hasAttribute("Mute")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
  }

  @Test
  public void refreshLastUpdateTsEmptyList() {
    // This test is to ensure that an empty list input does not cause an error.
    feedDAO.refreshLastUpdateTs(List.of());
  }

  @Test
  public void getFeed() {
    var result = feedDAO.getFeed(RESOURCE_ID, USER_ID_1);

    assertThat(result.getResourceId()).isEqualTo(RESOURCE_ID);
    assertThat(result.getUserId()).isEqualTo(USER_ID_1);
    assertThat(result.getResourceType()).isEqualTo(RESOURCE_TYPE);
    assertThat(result.getBeginTs()).isEqualTo(TS_1);
    assertThat(result.getMute()).isEqualTo(true);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
  }

  @Test
  public void getFeedReturnsNull() {
    var result = feedDAO.getFeed("Missing", "Not here");

    assertThat(result).isNull();
  }

  @Test
  public void queryByResourceIdReturnsItems() {
    var result = feedDAO.queryByResourceId(RESOURCE_ID);

    assertThat(result.size()).isEqualTo(2);
    assertThat(result).extracting(Feed::getResourceId).containsOnly(RESOURCE_ID);
    assertThat(result).extracting(Feed::getUserId).containsOnly(USER_ID_1, USER_ID_2);
    assertThat(result).extracting(Feed::getResourceType).containsOnly(RESOURCE_TYPE);
    assertThat(result).extracting(Feed::getBeginTs).containsOnly(TS_1);
    assertThat(result).extracting(Feed::getMute).containsOnly(true, false);
    assertThat(result).extracting(Feed::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(Feed::getLastUpdateTs).containsOnly(TS_2);
  }

  @Test
  public void queryByUserIdAndResourceTypeReturnsItems() {
    var result = feedDAO.queryByUserIdAndResourceType(USER_ID_3, RESOURCE_TYPE, TS_1, 10);

    assertThat(result.size()).isEqualTo(1);
    assertThat(result).extracting(Feed::getResourceId).containsOnly(RESOURCE_ID_3);
    assertThat(result).extracting(Feed::getUserId).containsOnly(USER_ID_3);
    assertThat(result).extracting(Feed::getResourceType).containsOnly(RESOURCE_TYPE);
    assertThat(result).extracting(Feed::getBeginTs).containsOnlyNulls();
    assertThat(result).extracting(Feed::getMute).containsOnly(false);
    assertThat(result).extracting(Feed::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(Feed::getLastUpdateTs).containsOnly(TS_2);
  }

  @Test
  public void queryByUserIdAndResourceTypeFiltersOnTs() {
    var result = feedDAO.queryByUserIdAndResourceType(USER_ID_3, RESOURCE_TYPE, TS_2, 10);

    assertThat(result.size()).isEqualTo(0);
  }

  @Test
  public void queryByUserIdAndResourceTypeLimitsResultSize() {
    var result = feedDAO.queryByUserIdAndResourceType(USER_ID_1, RESOURCE_TYPE, TS_0, 1);

    assertThat(result.size()).isEqualTo(1);
  }

  @Test
  public void queryByUserIdAndResourceTypeHistorical() {
    var result = feedDAO
        .queryByUserIdAndResourceTypeHistorical(USER_ID_3, RESOURCE_TYPE, Instant.now());

    assertThat(result.size()).isEqualTo(1);
    assertThat(result).extracting(Feed::getResourceId).containsOnly(RESOURCE_ID_3);
    assertThat(result).extracting(Feed::getUserId).containsOnly(USER_ID_3);
    assertThat(result).extracting(Feed::getResourceType).containsOnly(RESOURCE_TYPE);
    assertThat(result).extracting(Feed::getBeginTs).containsOnlyNulls();
    assertThat(result).extracting(Feed::getMute).containsOnly(false);
    assertThat(result).extracting(Feed::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(Feed::getLastUpdateTs).containsOnly(TS_2);
  }

  @Test
  public void queryByUserIdAndResourceTypeHistoricalFiltersOnTs() {
    var result = feedDAO.queryByUserIdAndResourceTypeHistorical(USER_ID_3, RESOURCE_TYPE, TS_0);

    assertThat(result.size()).isEqualTo(0);
  }

  @Test
  public void updateBeginTs() {
    feedDAO.updateBeginTs("thread-node-1", USER_ID_1, TS_2, TS_2);

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "thread-node-1",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
    ));

    assertThat(storedItems.size()).isEqualTo(1);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("thread-node-1");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("feed|%s", USER_ID_1));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE));
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong("Begin")).containsOnly(TS_2.getMillis());
    assertThat(storedItems).extracting(it -> it.hasAttribute("Mute")).containsOnly(false);
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
  }

  @Test
  public void updateBeginTsThrowsIfFeedDoesNotExist() {
    assertThatThrownBy(() -> feedDAO.updateBeginTs("missing", "not-here", TS_1, TS_1))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Could not update beginTs for resource id missing, "
            + "user id not-here because the feed item does not exist");
  }

  @Test
  public void batchHardDeleteFeed() {
    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-me",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-yourselves",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
    ));

    assertThat(retrievedItems).hasSize(2);

    feedDAO.batchHardDeleteFeed(USER_ID_1, List.of("save-me", "save-yourselves"));

    retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-me",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-yourselves",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
    ));

    assertThat(retrievedItems).hasSize(0);
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withLong("Begin", TS_1.getMillis())
        .withLong("End", TS_2.getMillis())
        .withBoolean("Mute", true)
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_2))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_2, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withLong("Begin", TS_1.getMillis())
        .withLong("End", TS_2.getMillis())
        .withBoolean("Mute", false)
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_2,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_2,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_2))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_2, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_3,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, RESOURCE_ID_3,
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_3))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_3, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "thread-node-1",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-yourselves",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "save-me",
            DynamoDBClient.PRIMARY_KEY_R, String.format("feed|%s", USER_ID_1))
        .withString(DynamoDBClient.GSI1_H, String.format("feed|%s|%s", USER_ID_1, RESOURCE_TYPE))
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withLong("Ts-C", TS_1.getMillis()));

    return items;
  }
}
