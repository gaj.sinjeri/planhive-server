package app.planhive.services.feed.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class FeedTest {

  private static final String RESOURCE_ID = "123";
  private static final String USER_ID = "234";
  private static final String TYPE = "USER";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final Instant TS_3 = new Instant(3);

  @Test
  public void constructor() {
    var obj = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_3)
        .build();

    assertThat(obj.getResourceId()).isEqualTo(RESOURCE_ID);
    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getResourceType()).isEqualTo(TYPE);
    assertThat(obj.getBeginTs()).isEqualTo(TS_1);
    assertThat(obj.getMute()).isEqualTo(true);
    assertThat(obj.getCreationTs()).isEqualTo(TS_2);
    assertThat(obj.getLastUpdateTs()).isEqualTo(TS_3);
  }

  @Test
  public void builderThrowsIfResourceIdNull() {
    assertThatThrownBy(() -> new Feed.Builder(null, USER_ID).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void builderThrowsIfUserIdNull() {
    assertThatThrownBy(() -> new Feed.Builder(RESOURCE_ID, null).build())
        .isInstanceOf(NullPointerException.class);
  }


  @Test
  public void equality() {
    var obj1 = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_3)
        .build();
    var obj2 = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_3)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_3)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_3)
        .build();
    var obj2 = new Feed.Builder(RESOURCE_ID, USER_ID)
        .withResourceType(TYPE)
        .withBeginTs(TS_1)
        .setMute(true)
        .withCreationTs(TS_3)
        .withLastUpdateTs(TS_2)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
