package app.planhive.services.feed.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.persistence.FeedDAO;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class FeedServiceImplTest {

  private static final String RESOURCE_ID = "123";
  private static final String RESOURCE_ID_2 = "234";
  private static final String USER_ID_1 = "A1";
  private static final String USER_ID_2 = "B2";
  private static final String RESOURCE_TYPE = "USER";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private FeedDAO feedDAO;
  private FeedService feedService;

  @BeforeEach
  public void setUp() {
    feedDAO = mock(FeedDAO.class);

    feedService = new FeedServiceImpl(feedDAO);
  }

  @Test
  public void createFeeds() {
    feedService.createFeeds(RESOURCE_ID, List.of(USER_ID_1, USER_ID_2), RESOURCE_TYPE, TS_1);

    ArgumentCaptor<List<Feed>> captor = ArgumentCaptor.forClass(List.class);

    verify(feedDAO).batchPutItem(captor.capture());
    var arg = captor.getValue();

    assertThat(arg.size()).isEqualTo(2);
    assertThat(arg).extracting(Feed::getResourceId).containsOnly(RESOURCE_ID);
    assertThat(arg).extracting(Feed::getUserId).containsOnly(USER_ID_1, USER_ID_2);
    assertThat(arg).extracting(Feed::getResourceType).containsOnly(RESOURCE_TYPE);
    assertThat(arg).extracting(Feed::getBeginTs).containsOnly(new Instant(0));
    assertThat(arg).extracting(Feed::getCreationTs).containsOnly(TS_1);
    assertThat(arg).extracting(Feed::getLastUpdateTs).containsOnly(TS_1);
  }

  @Test
  public void createFeedsWithBeginTs() {
    feedService.createFeeds(RESOURCE_ID, List.of(USER_ID_1, USER_ID_2), RESOURCE_TYPE, TS_1, TS_2);

    ArgumentCaptor<List<Feed>> captor = ArgumentCaptor.forClass(List.class);

    verify(feedDAO).batchPutItem(captor.capture());
    var arg = captor.getValue();

    assertThat(arg.size()).isEqualTo(2);
    assertThat(arg).extracting(Feed::getResourceId).containsOnly(RESOURCE_ID);
    assertThat(arg).extracting(Feed::getUserId).containsOnly(USER_ID_1, USER_ID_2);
    assertThat(arg).extracting(Feed::getResourceType).containsOnly(RESOURCE_TYPE);
    assertThat(arg).extracting(Feed::getBeginTs).containsOnly(TS_1);
    assertThat(arg).extracting(Feed::getCreationTs).containsOnly(TS_2);
    assertThat(arg).extracting(Feed::getLastUpdateTs).containsOnly(TS_2);
  }

  @Test
  public void refreshFeedsForResource() {
    var feed = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByResourceId(RESOURCE_ID)).thenReturn(List.of(feed));

    feedService.refreshFeedsForResource(RESOURCE_ID, TS_2);

    verify(feedDAO).refreshLastUpdateTs(
        List.of(new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_2).build()));
  }

  @Test
  public void batchRefreshFeedsForResource() {
    var feed1 = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByResourceId(RESOURCE_ID)).thenReturn(List.of(feed1));
    var feed2 = new Feed.Builder(RESOURCE_ID_2, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByResourceId(RESOURCE_ID_2)).thenReturn(List.of(feed2));

    feedService.batchRefreshFeedsForResource(List.of(RESOURCE_ID, RESOURCE_ID_2), TS_2);

    verify(feedDAO).refreshLastUpdateTs(
        List.of(new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_2).build()));
    verify(feedDAO).refreshLastUpdateTs(
        List.of(new Feed.Builder(RESOURCE_ID_2, USER_ID_1).withLastUpdateTs(TS_2).build()));
  }

  @Test
  public void getFeedOrThrow() throws Exception {
    var feed = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.getFeed(RESOURCE_ID, USER_ID_1)).thenReturn(feed);

    var result = feedService.getFeedOrThrow(RESOURCE_ID, USER_ID_1);

    Assertions.assertThat(result).isEqualTo(feed);
  }

  @Test
  public void getFeedOrThrowThrowsIfFeedDoesNotExist() throws Exception {
    when(feedDAO.getFeed(RESOURCE_ID, USER_ID_1)).thenReturn(null);

    assertThatThrownBy(() -> feedService.getFeedOrThrow(RESOURCE_ID, USER_ID_1))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String
            .format("No feed with resource id %s, user id %s exists", RESOURCE_ID, USER_ID_1));
  }

  @Test
  public void getFeedsForUser() {
    var feed = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByUserIdAndResourceType(USER_ID_1, RESOURCE_ID, TS_2, 100))
        .thenReturn(List.of(feed));

    var result = feedService.getFeedsForUser(USER_ID_1, RESOURCE_ID, TS_2, 100);

    Assertions.assertThat(result).containsOnly(feed);
  }

  @Test
  public void getHistoricalFeedsForUser() {
    var feed = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByUserIdAndResourceTypeHistorical(USER_ID_1, RESOURCE_ID, TS_2))
        .thenReturn(List.of(feed));

    var result = feedService.getHistoricalFeedsForUser(USER_ID_1, RESOURCE_ID, TS_2);

    Assertions.assertThat(result).containsOnly(feed);
  }

  @Test
  public void getFeedsByResourceId() {
    var feed = new Feed.Builder(RESOURCE_ID, USER_ID_1).withLastUpdateTs(TS_1).build();
    when(feedDAO.queryByResourceId(RESOURCE_ID)).thenReturn(List.of(feed));

    var result = feedService.getFeedsByResourceId(RESOURCE_ID);

    Assertions.assertThat(result).containsOnly(feed);
  }

  @Test
  public void updateBeginTs() {
    feedService.updateBeginTs(RESOURCE_ID, USER_ID_1, TS_1, TS_1);

    verify(feedDAO).updateBeginTs(RESOURCE_ID, USER_ID_1, TS_1, TS_1);
  }

  @Test
  public void batchHardDeleteFeed() {
    feedService.batchHardDeleteFeed(USER_ID_1, List.of(RESOURCE_ID));

    verify(feedDAO).batchHardDeleteFeed(USER_ID_1, List.of(RESOURCE_ID));
  }
}
