package app.planhive.services.event.notification;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class EventNotificationSenderImplTest {

  private static final String USER_NAME_1 = "Roger";
  private static final User USER_1 = new User.Builder("1").withName(USER_NAME_1).build();
  private static final User USER_2 = new User.Builder("2").build();
  private static final User USER_3 = new User.Builder("3").build();
  private static final String EVENT_NAME = "Rabbit";
  private static final String EVENT_DESCRIPTION = "What?";
  private static final NodeId EVENT_NODE_ID = new NodeId("123", NodeType.EVENT);
  private static final List<String> USER_IDS = List.of("1", "2");

  private NotificationService notificationService;
  private EventNotificationSender eventNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    eventNotificationSender = new EventNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendNewMemberNotificationZeroNewMembers() {
    eventNotificationSender
        .sendNewMemberNotification(EVENT_NODE_ID, EVENT_NAME, List.of(), List.of());

    verify(notificationService, never())
        .sendNotificationByNodeId(any(), any(), any(), anyBoolean());
  }

  @Test
  public void sendNewMemberNotificationOneNewMember() {
    eventNotificationSender
        .sendNewMemberNotification(EVENT_NODE_ID, EVENT_NAME, List.of(USER_1), List.of());

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s joined the event.", USER_NAME_1), NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("1"), true);
  }

  @Test
  public void sendNewMemberNotificationTwoNewMembers() {
    eventNotificationSender
        .sendNewMemberNotification(EVENT_NODE_ID, EVENT_NAME, List.of(USER_1, USER_2),
            List.of("55"));

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s and %s other person joined the event.", USER_NAME_1, 1),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("1", "2", "55"),
            true);
  }

  @Test
  public void sendNewMemberNotificationManyNewMembers() {
    eventNotificationSender
        .sendNewMemberNotification(EVENT_NODE_ID, EVENT_NAME, List.of(USER_1, USER_2, USER_3),
            List.of());

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        String.format("%s and %s other people joined the event.", USER_NAME_1, 2),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("1", "2", "3"),
            true);
  }

  @Test
  public void sendInvitedToEventNotification() {
    eventNotificationSender
        .sendInvitedToEventNotification(USER_NAME_1, EVENT_NODE_ID, EVENT_NAME,
            List.of(USER_1, USER_2));

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder("New event",
        String.format("%s added you to %s.", USER_NAME_1, EVENT_NAME),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService).sendNotificationByUserIds(expectedNotification, USER_IDS, true);
  }

  @Test
  public void sendEventDataChangedNotificationReturnsIfNewDataUnchanged() {
    var oldEventData = new EventData(EventState.ACTIVE, EVENT_NAME, EVENT_DESCRIPTION, 0L, null);
    var newEventData = new EventData(EventState.ACTIVE, EVENT_NAME, EVENT_DESCRIPTION, 0L, null);

    eventNotificationSender
        .sendEventDataChangedNotification("123", EVENT_NODE_ID, oldEventData, newEventData);

    verify(notificationService, never()).sendNotificationByUserIds(any(), any(), anyBoolean());
  }

  @Test
  public void sendEventDataChangedNotificationNameUpdated() {
    var oldEventData = new EventData(EventState.ACTIVE, EVENT_NAME, EVENT_DESCRIPTION, 0L, null);
    var newEventData = new EventData(EventState.ACTIVE, "New Name", EVENT_DESCRIPTION, 0L, null);

    eventNotificationSender
        .sendEventDataChangedNotification("123", EVENT_NODE_ID, oldEventData, newEventData);

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(
        String.format("%s (prev. %s)", "New Name", EVENT_NAME), "Event name updated.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();

    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("123"), true);
  }

  @Test
  public void sendEventDataChangedNotificationDescriptionUpdated() {
    var oldEventData = new EventData(EventState.ACTIVE, EVENT_NAME, EVENT_DESCRIPTION, 0L, null);
    var newEventData = new EventData(EventState.ACTIVE, EVENT_NAME, "New Description", 0L, null);

    eventNotificationSender
        .sendEventDataChangedNotification("123", EVENT_NODE_ID, oldEventData, newEventData);

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME, "Event description updated.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();

    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("123"), true);
  }

  @Test
  public void sendEventDataChangedNotificationNameAndDescriptionUpdated() {
    var oldEventData = new EventData(EventState.ACTIVE, EVENT_NAME, EVENT_DESCRIPTION, 0L, null);
    var newEventData = new EventData(EventState.ACTIVE, "New Name", "New Description", 0L, null);

    eventNotificationSender
        .sendEventDataChangedNotification("123", EVENT_NODE_ID, oldEventData, newEventData);

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(
        String.format("%s (prev. %s)", "New Name", EVENT_NAME),
        "Event name and description updated.", NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();

    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("123"), true);
  }

  @Test
  public void sendNewAdminNotification() {
    eventNotificationSender.sendNewAdminNotification(EVENT_NODE_ID, EVENT_NAME, "234");

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME, "You are now an admin.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of("234"), true);
  }

  @Test
  public void sendEventCanceledNotification() {
    eventNotificationSender.sendEventCanceledNotification(EVENT_NODE_ID, EVENT_NAME, "234");

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME, "Event has been canceled.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("234"), true);
  }

  @Test
  public void sendEventDeletedNotification() {
    eventNotificationSender.sendEventDeletedNotification(EVENT_NODE_ID, EVENT_NAME, "234");

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME, "Event has been deleted.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, EVENT_NODE_ID, List.of("234"), true);
  }
}
