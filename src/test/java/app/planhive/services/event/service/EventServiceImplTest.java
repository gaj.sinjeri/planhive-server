package app.planhive.services.event.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.event.notification.EventNotificationSender;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.participation.service.ParticipationService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.service.TimelineService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.assertj.core.api.Assertions;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.imageio.ImageIO;

@TestInstance(Lifecycle.PER_CLASS)
public class EventServiceImplTest {

  private static final String EVENT_ID = "e1v2e3n4t5";
  private static final String ENCRYPTED_EVENT_ID = "5t4n3e2v1e";
  private static final String UUID_1 = "1234";
  private static final String UUID_2 = "7890";
  private static final String USER_ID = "v34v-43-gj03g";
  private static final String USER_ID_2 = "cba";
  private static final String USER_NAME = "Isabella";
  private static final User USER = new User.Builder(USER_ID).withName(USER_NAME).build();
  private static final User USER_2 = new User.Builder(USER_ID_2).build();
  private static final String CONTACT_ID_1 = "11211";
  private static final String CONTACT_ID_2 = "22122";
  private static final String NAME = "Bowling night";
  private static final String DESCRIPTION = "Been a while. Let's get together!";
  private static final Instant TS = new Instant(1);
  private static final Instant TS_2 = new Instant(2);
  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);
  private static final ActivityData ACTIVITY_DATA = new ActivityData.Builder()
      .withTitle("Hello")
      .withActivityState(ActivityState.SUGGESTION)
      .withTime(ACTIVITY_TIME)
      .build();

  private PermissionService permissionService;
  private NodeService nodeService;
  private CloudStore cloudStore;
  private ParticipationService participationService;
  private TimelineService timelineService;
  private ThreadService threadService;
  private PhotoService photoService;
  private FeedService feedService;
  private UserService userService;
  private EventNotificationSender eventNotificationSender;
  private GroupService groupService;
  private EventService eventService;

  private Node participationNode;
  private Node participantNode;
  private Node timelineNode;
  private Node activityNode;
  private Node threadNode;
  private Node photosNode;

  private InputStream imageInputStream;
  private MultipartFile FILE;

  @BeforeEach
  public void setUp() throws Exception {
    SecureGenerator secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID_1);
    when(secureGenerator.encryptBase64String(EVENT_ID)).thenReturn(ENCRYPTED_EVENT_ID);
    when(secureGenerator.decryptBase64String(ENCRYPTED_EVENT_ID)).thenReturn(EVENT_ID);

    permissionService = mock(PermissionService.class);
    when(permissionService.createPermissionGroup(Map.of(USER_ID,
        AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE)), TS_2))
        .thenReturn(UUID_2);

    nodeService = mock(NodeService.class);
    cloudStore = mock(CloudStore.class);
    participationService = mock(ParticipationService.class);
    timelineService = mock(TimelineService.class);
    threadService = mock(ThreadService.class);
    photoService = mock(PhotoService.class);
    feedService = mock(FeedService.class);
    userService = mock(UserService.class);
    eventNotificationSender = mock(EventNotificationSender.class);
    groupService = mock(GroupService.class);

    eventService = new EventServiceImpl(secureGenerator, permissionService, nodeService, cloudStore,
        participationService, timelineService, threadService, photoService, feedService,
        userService, eventNotificationSender, groupService, 2048, 640);

    participationNode = constructNode(NodeType.PARTICIPATION);
    participantNode = constructNode(NodeType.PARTICIPANT);
    timelineNode = constructNode(NodeType.TIMELINE);
    activityNode = constructNode(NodeType.ACTIVITY);
    threadNode = constructNode(NodeType.THREAD);
    photosNode = constructNode(NodeType.PHOTOS);

    when(participationService.addParticipationWidget(UUID_1, UUID_2, USER_ID, List.of(), TS_2))
        .thenReturn(List.of(participationNode, participantNode));
    when(timelineService
        .addTimelineWidget(UUID_1, UUID_2, USER_ID, List.of(), Optional.of(ACTIVITY_DATA), TS_2))
        .thenReturn(List.of(timelineNode, activityNode));
    when(threadService
        .createThread(new NodeId(UUID_1, NodeType.EVENT), UUID_2, false, USER_ID, List.of(), TS_2))
        .thenReturn(threadNode);
    when(photoService.addPhotosWidget(UUID_1, UUID_2, USER_ID, List.of(), TS_2))
        .thenReturn(photosNode);

    imageInputStream = generateImageInputStream(2, 1);
    FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg", imageInputStream);
  }

  @Test
  public void createEventStoresEvent() throws Exception {
    var result = eventService
        .createEvent(USER, NAME, DESCRIPTION, FILE, Optional.of(ACTIVITY_DATA), TS_2);

    var expectedNodeId = new NodeId(UUID_1, NodeType.EVENT);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(UUID_2)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_2)
        .build();

    Assertions.assertThat(result)
        .containsExactlyInAnyOrder(expectedNode, participationNode, participantNode, timelineNode,
            activityNode, threadNode, photosNode);

    verify(nodeService).insertNode(expectedNode);
    verify(feedService)
        .createFeeds(expectedNodeId.toFeedItemKey(), List.of(USER_ID), NodeType.EVENT.name(), TS_2);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("events/%s/cover.jpeg", UUID_1)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("events/%s/t-cover.jpeg", UUID_1)), any());
  }

  @Test
  public void createEventDoesNotStoreImageIfNull() throws Exception {
    var result = eventService
        .createEvent(USER, NAME, DESCRIPTION, null, Optional.of(ACTIVITY_DATA), TS_2);

    var expectedNodeId = new NodeId(UUID_1, NodeType.EVENT);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(UUID_2)
        .withData(new EventData(EventState.ACTIVE, NAME, DESCRIPTION, 0L, null).toJson())
        .withCreationTs(TS_2)
        .withLastUpdateTs(TS_2)
        .build();

    Assertions.assertThat(result)
        .containsExactlyInAnyOrder(expectedNode, participationNode, participantNode, timelineNode,
            activityNode, threadNode, photosNode);

    verify(nodeService).insertNode(expectedNode);
    verify(cloudStore, never()).storeImage(any(), any());
    verify(feedService)
        .createFeeds(expectedNodeId.toFeedItemKey(), List.of(USER_ID), NodeType.EVENT.name(), TS_2);
  }

  @Test
  public void createEventRethrowsIfPhotoInaccessible() throws Exception {
    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(() -> eventService
        .createEvent(USER, NAME, DESCRIPTION, fileMock, Optional.of(ACTIVITY_DATA), TS))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error handling image. This could be either due to not being "
            + "able to access the input stream of the original image or create thumbnail");

    verify(cloudStore, never()).storeImage(any(), any());
    verify(permissionService, never()).createPermissionGroup(any(), any());
    verify(nodeService, never()).insertNode(any());
    verify(threadService, never()).createThread(any(), any(), anyBoolean(), any(), any(), any());
    verify(participationService, never()).addParticipationWidget(any(), any(), any(), any(), any());
    verify(timelineService, never()).addTimelineWidget(any(), any(), any(), any(), any(), any());
    verify(photoService, never()).addPhotosWidget(any(), any(), any(), any(), any());
    verify(feedService, never()).createFeeds(any(), any(), any(), any());
  }

  @Test
  public void createEventThrowsIfCoverPhotoFileFormatIncorrect() {
    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(() -> eventService
        .createEvent(USER, NAME, DESCRIPTION, fileMock, Optional.of(ACTIVITY_DATA), TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");
  }

  @Test
  public void createEventThrowsIfCoverPhotoResolutionTooHigh() throws Exception {
    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(() -> eventService
        .createEvent(USER, NAME, DESCRIPTION, file, Optional.of(ACTIVITY_DATA), TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");
  }

  @Test
  public void createEventThrowsIfCoverPhotoAspectRatioIncorrect() throws Exception {
    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(4, 1));

    assertThatThrownBy(() -> eventService
        .createEvent(USER, NAME, DESCRIPTION, file, Optional.of(ACTIVITY_DATA), TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Expected width-to-height ratio to be 2.0, but was 4.0");
  }

  @Test
  public void createEventRethrowsIfEventAlreadyExists() throws Exception {
    Mockito.doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(() -> eventService
        .createEvent(USER, NAME, DESCRIPTION, null, Optional.of(ACTIVITY_DATA), TS))
        .isInstanceOf(InternalError.class);

    verify(cloudStore, never()).storeImage(any(), any());
    verify(threadService, never()).createThread(any(), any(), anyBoolean(), any(), any(), any());
    verify(participationService, never()).addParticipationWidget(any(), any(), any(), any(), any());
    verify(timelineService, never()).addTimelineWidget(any(), any(), any(), any(), any(), any());
    verify(photoService, never()).addPhotosWidget(any(), any(), any(), any(), any());
    verify(feedService, never()).createFeeds(any(), any(), any(), any());
  }

  @Test
  public void getCoverPhoto() throws Exception {
    var nodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var node = new Node.Builder(nodeId).withPermissionGroupId(UUID_2).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);
    when(cloudStore.getFile(String.format("events/%s/cover.jpeg", EVENT_ID)))
        .thenReturn(FILE.getBytes());

    var result = eventService.getCoverPhoto(USER, EVENT_ID);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_2, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverPhotoThumbnail() throws Exception {
    var nodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var node = new Node.Builder(nodeId).withPermissionGroupId(UUID_2).build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(node);
    when(cloudStore.getFile(String.format("events/%s/t-cover.jpeg", EVENT_ID)))
        .thenReturn(FILE.getBytes());

    var result = eventService.getCoverPhotoThumbnail(USER, EVENT_ID);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_2, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void updateEventStateToActive() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.CHAT_GROUP);
    var eventData = new EventData(EventState.CANCELED, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var updatedEventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var updatedEventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(updatedEventData.toJson())
        .build();
    when(nodeService.updateNodeData(eventNodeId, updatedEventData.toJson(), TS))
        .thenReturn(updatedEventNode);

    var result = eventService.updateEventState(USER, eventNodeId, EventState.ACTIVE, TS);

    Assertions.assertThat(result).isEqualTo(updatedEventNode);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(eventNodeId.toFeedItemKey(), TS);
    verify(eventNotificationSender, never()).sendEventCanceledNotification(any(), any(), any());
  }

  @Test
  public void updateEventStateToCanceled() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.CHAT_GROUP);
    var eventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var updatedEventData = new EventData(EventState.CANCELED, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var updatedEventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(updatedEventData.toJson())
        .build();
    when(nodeService.updateNodeData(eventNodeId, updatedEventData.toJson(), TS))
        .thenReturn(updatedEventNode);

    var result = eventService.updateEventState(USER, eventNodeId, EventState.CANCELED, TS);

    Assertions.assertThat(result).isEqualTo(updatedEventNode);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(eventNodeId.toFeedItemKey(), TS);
    verify(eventNotificationSender).sendEventCanceledNotification(eventNodeId, NAME, USER_ID);
  }

  @Test
  public void updateEventData() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.CHAT_GROUP);
    var eventData = new EventData(EventState.ACTIVE, "Abc", "Def", TS_2.getMillis(),
        ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var updatedEventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS.getMillis(),
        null);
    var updatedEventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(updatedEventData.toJson())
        .build();
    when(nodeService.updateNodeData(eventNodeId, updatedEventData.toJson(), TS))
        .thenReturn(updatedEventNode);

    var result = eventService
        .updateEventData(USER, eventNodeId, NAME, DESCRIPTION, null, false, FILE, TS);

    Assertions.assertThat(result).isEqualTo(updatedEventNode);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(eventNodeId.toFeedItemKey(), TS);
    verify(eventNotificationSender)
        .sendEventDataChangedNotification(USER_ID, eventNodeId, eventData, updatedEventData);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("events/%s/cover.jpeg", EVENT_ID)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("events/%s/t-cover.jpeg", EVENT_ID)), any());
  }

  @Test
  public void updateEventDataWithNullCoverPhoto() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.CHAT_GROUP);
    var eventData = new EventData(EventState.ACTIVE, "Abc", "Def", TS.getMillis(),
        null);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var updatedEventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, 1L,
        ENCRYPTED_EVENT_ID);
    var updatedEventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(updatedEventData.toJson())
        .build();
    when(nodeService.updateNodeData(eventNodeId, updatedEventData.toJson(), TS))
        .thenReturn(updatedEventNode);

    var result = eventService
        .updateEventData(USER, eventNodeId, NAME, DESCRIPTION, 1L, true, null, TS);

    Assertions.assertThat(result).isEqualTo(updatedEventNode);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(eventNodeId.toFeedItemKey(), TS);
    verify(eventNotificationSender)
        .sendEventDataChangedNotification(USER_ID, eventNodeId, eventData, updatedEventData);

    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void updateEventDataEncryptsEventId() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.CHAT_GROUP);
    var eventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var updatedEventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS.getMillis(),
        ENCRYPTED_EVENT_ID);
    var updatedEventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(updatedEventData.toJson())
        .build();
    when(nodeService.updateNodeData(eventNodeId, updatedEventData.toJson(), TS))
        .thenReturn(updatedEventNode);

    var result = eventService
        .updateEventData(USER, eventNodeId, NAME, DESCRIPTION, TS.getMillis(), true, null, TS);

    Assertions.assertThat(result).isEqualTo(updatedEventNode);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(eventNodeId.toFeedItemKey(), TS);
    verify(eventNotificationSender, never())
        .sendEventDataChangedNotification(any(), any(), any(), any());

    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void acceptEventInvitation() throws Exception {
    var nodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, 0L, ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder((nodeId))
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(eventNode);

    when(groupService.addUsersToGroup(eventNode, List.of(USER_ID), new Instant(0), TS))
        .thenReturn(List.of(USER));

    var result = eventService.acceptEventInvitation(ENCRYPTED_EVENT_ID, USER, TS);

    assertThat(result).isEqualTo(eventNode);

    verify(eventNotificationSender)
        .sendNewMemberNotification(nodeId, NAME, List.of(USER), List.of());
  }

  @Test
  public void acceptEventInvitationThrowsIfPublicLinkNotEnabled() throws Exception {
    var nodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, 0L, null);
    var eventNode = new Node.Builder((nodeId))
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(eventNode);

    assertThatThrownBy(() -> eventService.acceptEventInvitation(ENCRYPTED_EVENT_ID, USER, TS))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining(String.format("Cannot join event with id %s because it does "
            + "not have a public link enabled", EVENT_ID));

    verify(groupService, never()).addUsersToGroup(any(), any(), any(), any());
    verify(eventNotificationSender, never()).sendNewMemberNotification(any(), any(), any(), any());
  }

  @Test
  public void acceptEventInvitationThrowsIfUserAlreadyMember() throws Exception {
    var nodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventData = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, 0L, ENCRYPTED_EVENT_ID);
    var eventNode = new Node.Builder((nodeId))
        .withData(eventData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(nodeId)).thenReturn(eventNode);

    when(groupService.addUsersToGroup(eventNode, List.of(USER_ID), new Instant(0), TS))
        .thenReturn(List.of());

    assertThatThrownBy(() -> eventService.acceptEventInvitation(ENCRYPTED_EVENT_ID, USER, TS))
        .isInstanceOf(InvalidOperationException.class)
        .hasMessageContaining(
            String.format("User %s is already a member of event %s", USER_ID, EVENT_ID));

    verify(eventNotificationSender, never()).sendNewMemberNotification(any(), any(), any(), any());
  }

  @Test
  public void inviteToEvent() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(String.format("{\"eventState\":\"ACTIVE\",\"name\":\"%s\", \"coverTs\":0}", NAME))
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    var contact = new User.Builder(CONTACT_ID_1).build();
    when(groupService.addUsersToGroup(eventNode, List.of(CONTACT_ID_1), new Instant(0), TS))
        .thenReturn(List.of(contact));

    eventService.inviteToEvent(EVENT_ID, USER, List.of(CONTACT_ID_1), TS);

    verify(permissionService).verifyAccessRight(USER_ID, UUID_1, AccessRight.WRITE);
    verify(eventNotificationSender)
        .sendInvitedToEventNotification(USER_NAME, eventNodeId, NAME, List.of(contact));
    verify(eventNotificationSender)
        .sendNewMemberNotification(eventNodeId, NAME, List.of(contact), List.of(USER_ID));
  }

  @Test
  public void setMemberAdminStatusMakeAdmin() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    eventService.setMemberAdminStatus(eventNodeId, USER, UUID_2, true, TS);

    verify(groupService).setMemberAdminStatus(eventNode, USER, UUID_2, true, TS);
    verify(eventNotificationSender).sendNewAdminNotification(eventNodeId, NAME, UUID_2);
  }


  @Test
  public void setMemberAdminStatusRemoveAdmin() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    eventService.setMemberAdminStatus(eventNodeId, USER, UUID_2, false, TS);

    verify(groupService).setMemberAdminStatus(eventNode, USER, UUID_2, false, TS);
    verify(eventNotificationSender, never()).sendNewAdminNotification(any(), any(), any());
  }

  @Test
  public void removeUserFromEvent() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    when(userService.getUser(USER_ID_2)).thenReturn(USER_2);

    eventService.removeUserFromEvent(eventNodeId, USER, USER_ID_2, TS);

    verify(groupService).removeUserFromGroup(eventNode, USER, USER_2, TS);
    verify(eventNotificationSender, never()).sendNewAdminNotification(any(), any(), any());
  }

  @Test
  public void removeUserFromEventSendsNotificationIfNewAdmin() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    when(userService.getUser(USER_ID_2)).thenReturn(USER_2);
    when(groupService.removeUserFromGroup(eventNode, USER, USER_2, TS))
        .thenReturn(Optional.of(UUID_2));

    eventService.removeUserFromEvent(eventNodeId, USER, USER_ID_2, TS);

    verify(eventNotificationSender).sendNewAdminNotification(eventNodeId, NAME, UUID_2);
  }

  @Test
  public void deleteEvent() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withPermissionGroupId(UUID_1)
        .withData(
            new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS_2.getMillis(), null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    eventService.deleteEvent(eventNodeId, USER, TS);

    verify(groupService).deleteGroup(eventNode, USER, TS);
    verify(eventNotificationSender).sendEventDeletedNotification(eventNodeId, NAME, USER_ID);
  }

  private Node constructNode(NodeType type) {
    return new Node.Builder(new NodeId("1", UUID_1, type))
        .withPermissionGroupId(UUID_2)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .build();
  }

  private InputStream generateImageInputStream(int width, int height) throws Exception {
    var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    var outputStream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpeg", outputStream);

    return new ByteArrayInputStream(outputStream.toByteArray());
  }
}
