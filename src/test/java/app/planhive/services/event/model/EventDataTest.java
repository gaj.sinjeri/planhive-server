package app.planhive.services.event.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import org.junit.jupiter.api.Test;

public class EventDataTest {

  private static final String NAME = "Birthday Party!";
  private static final String DESCRIPTION = "18!";
  private static final Long TS = 1L;
  private static final String ENCRYPTED_EVENT_ID = "123";

  @Test
  public void constructor() {
    var obj = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);

    assertThat(obj.getEventState()).isEqualTo(EventState.ACTIVE);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getCoverTs()).isEqualTo(TS);
    assertThat(obj.getEncryptedEventId()).isEqualTo(ENCRYPTED_EVENT_ID);
  }

  @Test
  public void constructorTrimsNameAndDescription() {
    var obj = new EventData(EventState.ACTIVE, String.format("  %s  ", NAME),
        String.format("  %s  ", DESCRIPTION), TS, ENCRYPTED_EVENT_ID);

    assertThat(obj.getEventState()).isEqualTo(EventState.ACTIVE);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getCoverTs()).isEqualTo(TS);
    assertThat(obj.getEncryptedEventId()).isEqualTo(ENCRYPTED_EVENT_ID);
  }

  @Test
  public void constructorThrowsIfEventStateNull() {
    assertThatThrownBy(() -> new EventData(null, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfNameNull() {
    assertThatThrownBy(
        () -> new EventData(EventState.ACTIVE, null, DESCRIPTION, TS, ENCRYPTED_EVENT_ID))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void constructorThrowsIfNameEmpty() {
    assertThatThrownBy(
        () -> new EventData(EventState.ACTIVE, "", DESCRIPTION, TS, ENCRYPTED_EVENT_ID))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void constructorThrowsIfNameLongerThanLimit() {
    assertThatThrownBy(
        () -> new EventData(EventState.ACTIVE, new String(new char[26]).replace('\0', ' '),
            DESCRIPTION, TS, ENCRYPTED_EVENT_ID))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void constructorThrowsIfDescriptionLongerThanLimit() {
    assertThatThrownBy(
        () -> new EventData(EventState.ACTIVE, new String(new char[513]).replace('\0', ' '),
            DESCRIPTION, TS, ENCRYPTED_EVENT_ID))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void constructorThrowsIfCoverTsNull() {
    assertThatThrownBy(
        () -> new EventData(EventState.ACTIVE, NAME, DESCRIPTION, null, ENCRYPTED_EVENT_ID))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);
    var obj2 = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);
    var obj2 = new EventData(EventState.CANCELED, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID);

    assertThat(obj.toJson()).isEqualTo(String.format("{\"eventState\":\"%s\",\"name\":\"%s\","
            + "\"description\":\"%s\",\"coverTs\":%s,\"encryptedEventId\":\"%s\"}",
        EventState.ACTIVE.name(), NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID));
  }

  @Test
  public void toJsonRequiredFieldsOnly() {
    var obj = new EventData(EventState.ACTIVE, NAME, null, TS, null);

    assertThat(obj.toJson()).isEqualTo(String
        .format("{\"eventState\":\"%s\",\"name\":\"%s\",\"coverTs\":%s}", EventState.ACTIVE.name(),
            NAME, TS));
  }

  @Test
  public void fromJson() {
    var obj = NodeData.fromJson(String.format("{\"eventState\":\"%s\",\"name\":\"%s\","
            + "\"description\":\"%s\",\"coverTs\":%s,\"encryptedEventId\":\"%s\"}",
        EventState.ACTIVE.name(), NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID), EventData.class);

    assertThat(obj)
        .isEqualTo(new EventData(EventState.ACTIVE, NAME, DESCRIPTION, TS, ENCRYPTED_EVENT_ID));
  }

  @Test
  public void fromJsonRequiredKeysOnly() {
    var obj = NodeData.fromJson(String
        .format("{\"eventState\":\"%s\",\"name\":\"%s\",\"coverTs\":%s}", EventState.ACTIVE.name(),
            NAME, TS), EventData.class);

    assertThat(obj).isEqualTo(new EventData(EventState.ACTIVE, NAME, null, TS, null));
  }

  @Test
  public void fromJsonFailsIfEventStateMissing() {
    assertThatThrownBy(() -> NodeData.fromJson(String.format("{\"name\":\"%s\","
        + "\"description\":\"%s\",\"coverTs\":%s}", NAME, DESCRIPTION, TS), EventData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'eventState'");
  }

  @Test
  public void fromJsonFailsIfNameMissing() {
    assertThatThrownBy(() -> NodeData.fromJson(
        String.format("{\"eventState\":\"%s\",\"description\":\"%s\",\"coverTs\":%s}",
            EventState.ACTIVE, DESCRIPTION, TS), EventData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void fromJsonFailsIfCoverTsMissing() {
    assertThatThrownBy(() -> NodeData.fromJson(String.format("{\"eventState\":\"%s\","
            + "\"name\":\"%s\",\"description\":\"%s\"}", EventState.ACTIVE, NAME, DESCRIPTION),
        EventData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'coverTs'");
  }
}
