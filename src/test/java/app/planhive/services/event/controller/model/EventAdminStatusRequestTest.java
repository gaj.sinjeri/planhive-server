package app.planhive.services.event.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class EventAdminStatusRequestTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new EventAdminStatusRequest(true);

    assertThat(obj.isSetAdmin()).isEqualTo(true);
  }

  @Test
  public void deserializationFailsIfSetAdminMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{}", EventAdminStatusRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'setAdmin'");
  }

  @Test
  public void equality() {
    var obj1 = new EventAdminStatusRequest(true);
    var obj2 = new EventAdminStatusRequest(true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new EventAdminStatusRequest(true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new EventAdminStatusRequest(true);
    var obj2 = new EventAdminStatusRequest(false);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
