package app.planhive.services.event.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.event.model.EventState;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UpdateEventStateRequestTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateEventStateRequest(EventState.ACTIVE);

    Assertions.assertThat(obj.getEventState()).isEqualTo(EventState.ACTIVE);
  }

  @Test
  public void deserializationFailsIfEventStateMissing() {
    assertThatThrownBy(() -> mapper.readValue("{}", UpdateEventStateRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'eventState'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateEventStateRequest(EventState.ACTIVE);
    var obj2 = new UpdateEventStateRequest(EventState.ACTIVE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateEventStateRequest(EventState.ACTIVE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateEventStateRequest(EventState.ACTIVE);
    var obj2 = new UpdateEventStateRequest(EventState.CANCELED);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
