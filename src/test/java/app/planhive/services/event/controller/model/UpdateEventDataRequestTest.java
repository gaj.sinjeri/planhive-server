package app.planhive.services.event.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class UpdateEventDataRequestTest {

  private static final String NAME = "Laser tag";
  private static final String DESCRIPTION = "Fire the \"laser\"";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);

    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getCoverTs()).isEqualTo(1L);
    assertThat(obj.isInvitationLinkEnabled()).isTrue();
  }

  @Test
  public void deserializationFailsIfNameMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"description\":\"a\",\"coverTs\":1,\"invitationLinkEnabled\":true}",
            UpdateEventDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void deserializationFailsIfDescriptionMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"name\":\"a\",\"coverTs\":1,\"invitationLinkEnabled\":true}",
            UpdateEventDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'description'");
  }

  @Test
  public void deserializationFailsIfCoverTsMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"name\":\"a\",\"description\":\"a\",\"invitationLinkEnabled\":true}",
            UpdateEventDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'coverTs'");
  }

  @Test
  public void deserializationFailsIfInvitationLinkEnabledMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"name\":\"a\",\"description\":\"a\",\"coverTs\":1}",
            UpdateEventDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'invitationLinkEnabled'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var obj2 = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var obj2 = new UpdateEventDataRequest(NAME, DESCRIPTION, 2L, true);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
