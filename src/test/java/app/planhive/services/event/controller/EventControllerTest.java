package app.planhive.services.event.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.event.controller.model.CreateEventRequest;
import app.planhive.services.event.controller.model.EventAdminStatusRequest;
import app.planhive.services.event.controller.model.InviteToEventRequest;
import app.planhive.services.event.controller.model.UpdateEventDataRequest;
import app.planhive.services.event.controller.model.UpdateEventStateRequest;
import app.planhive.services.event.model.EventState;
import app.planhive.services.event.service.EventService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@TestInstance(Lifecycle.PER_CLASS)
public class EventControllerTest {

  private static final User USER = new User.Builder("v34v-43-gj03g").build();
  private static final String USER_ID_2 = "234";
  private static final String NAME = "Bowling night";
  private static final String DESCRIPTION = "Been a while. Let's get together!";
  private static final MultipartFile FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg",
      new byte[1]);
  private static final String EVENT_ID = "123123";
  private static final String ENCRYPTED_EVENT_ID = "321321";
  private static final List<String> CONTACT_IDS = List.of("234", "345");
  private static final Node NODE = new Node.Builder(new NodeId("1", NodeType.EVENT)).build();

  private EventService eventService;
  private EventController eventController;

  @BeforeEach
  public void setUp() {
    eventService = mock(EventService.class);

    eventController = new EventController(eventService);
  }

  @Test
  public void createEventSuccessfulCall() throws Exception {
    var nodeId = new NodeId("1", NodeType.EVENT);
    var node = new Node.Builder(nodeId)
        .withPermissionGroupId("3")
        .withCreationTs(Instant.now())
        .withLastUpdateTs(Instant.now())
        .withData("{\":some\":\"data\"}")
        .build();

    when(eventService
        .createEvent(eq(USER), eq(NAME), eq(DESCRIPTION), eq(FILE), eq(Optional.empty()), any()))
        .thenReturn(List.of(node));

    var request = new CreateEventRequest(NAME, DESCRIPTION, 0L, 0L, null, null, null, null);
    var response = eventController.createEvent(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node)));
  }

  @Test
  public void createEventSuccessfulCallWithTime() throws Exception {
    var nodeId = new NodeId("1", NodeType.EVENT);
    var node = new Node.Builder(nodeId)
        .withPermissionGroupId("3")
        .withCreationTs(Instant.now())
        .withLastUpdateTs(Instant.now())
        .withData("{\":some\":\"data\"}")
        .build();
    var activityData = new ActivityData.Builder()
        .withTitle("Main Activity")
        .withActivityState(ActivityState.APPROVED)
        .withTime(new ActivityTime(1L, 2L))
        .build();

    when(eventService
        .createEvent(eq(USER), eq(NAME), eq(DESCRIPTION), eq(FILE), eq(Optional.of(activityData)),
            any()))
        .thenReturn(List.of(node));

    var request = new CreateEventRequest(NAME, DESCRIPTION, 1L, 2L, null, null, null, null);
    var response = eventController.createEvent(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node)));
  }

  @Test
  public void createEventSuccessfulCallWithTimeAndLocation() throws Exception {
    var nodeId = new NodeId("1", NodeType.EVENT);
    var node = new Node.Builder(nodeId)
        .withPermissionGroupId("3")
        .withCreationTs(Instant.now())
        .withLastUpdateTs(Instant.now())
        .withData("{\":some\":\"data\"}")
        .build();
    var activityData = new ActivityData.Builder()
        .withTitle("Main Activity")
        .withActivityState(ActivityState.APPROVED)
        .withTime(new ActivityTime(1L, 2L))
        .withLocation(new Location(null, null, "123", "234"))
        .build();

    when(eventService
        .createEvent(eq(USER), eq(NAME), eq(DESCRIPTION), eq(FILE), eq(Optional.of(activityData)),
            any()))
        .thenReturn(List.of(node));

    var request = new CreateEventRequest(NAME, DESCRIPTION, 1L, 2L, null, null, "123", "234");
    var response = eventController.createEvent(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node)));
  }

  @Test
  public void createEventReturnsErrorIfInvalidImage() throws Exception {
    Mockito.doThrow(new InvalidImageException("")).when(eventService)
        .createEvent(eq(USER), eq(NAME), eq(DESCRIPTION), eq(FILE), eq(Optional.empty()), any());

    var request = new CreateEventRequest(NAME, DESCRIPTION, 0L, 0L, null, null, null, null);
    var response = eventController.createEvent(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getCoverPhotoSuccessfulCall() throws Exception {
    when(eventService.getCoverPhoto(USER, EVENT_ID)).thenReturn(FILE.getBytes());

    var response = eventController.getCoverPhoto(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverPhotoReturnsErrorIfResourceNotFound() throws Exception {
    when(eventService.getCoverPhoto(USER, EVENT_ID))
        .thenThrow(new ResourceNotFoundException(""));

    var response = eventController.getCoverPhoto(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoReturnsErrorIfResourceDeleted() throws Exception {
    when(eventService.getCoverPhoto(USER, EVENT_ID))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = eventController.getCoverPhoto(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoReturnsErrorIfUserNotAuthorised() throws Exception {
    when(eventService.getCoverPhoto(USER, EVENT_ID)).thenThrow(new UnauthorisedException(""));

    var response = eventController.getCoverPhoto(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailSuccessfulCall() throws Exception {
    when(eventService.getCoverPhotoThumbnail(USER, EVENT_ID)).thenReturn(FILE.getBytes());

    var response = eventController.getCoverPhotoThumbnail(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfResourceNotFound() throws Exception {
    when(eventService.getCoverPhotoThumbnail(USER, EVENT_ID))
        .thenThrow(new ResourceNotFoundException(""));

    var response = eventController.getCoverPhotoThumbnail(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfResourceDeleted() throws Exception {
    when(eventService.getCoverPhotoThumbnail(USER, EVENT_ID))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = eventController.getCoverPhotoThumbnail(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfUserNotAuthorised() throws Exception {
    when(eventService.getCoverPhotoThumbnail(USER, EVENT_ID))
        .thenThrow(new UnauthorisedException(""));

    var response = eventController.getCoverPhotoThumbnail(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void updateEventStateSuccessfulCall() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();

    when(eventService.updateEventState(eq(USER), eq(eventNodeId), eq(EventState.CANCELED), any()))
        .thenReturn(eventNode);

    var request = new UpdateEventStateRequest(EventState.CANCELED);
    var response = eventController.updateEventState(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(eventNode));
  }

  @Test
  public void updateEventStateReturnsErrorIfResourceDoesNotExist() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new ResourceNotFoundException("")).when(eventService)
        .updateEventState(eq(USER), eq(eventNodeId), eq(EventState.CANCELED), any());

    var request = new UpdateEventStateRequest(EventState.CANCELED);
    var response = eventController.updateEventState(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventStateReturnsErrorIfResourceDeleted() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new ResourceDeletedException("", new Object())).when(eventService)
        .updateEventState(eq(USER), eq(eventNodeId), eq(EventState.CANCELED), any());

    var request = new UpdateEventStateRequest(EventState.CANCELED);
    var response = eventController.updateEventState(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventStateReturnsErrorIfUnauthorized() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new UnauthorisedException("")).when(eventService)
        .updateEventState(eq(USER), eq(eventNodeId), eq(EventState.CANCELED), any());

    var request = new UpdateEventStateRequest(EventState.CANCELED);
    var response = eventController.updateEventState(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventDataSuccessfulCall() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();

    when(eventService
        .updateEventData(eq(USER), eq(eventNodeId), eq(NAME), eq(DESCRIPTION), eq(1L), eq(true),
            eq(FILE), any()))
        .thenReturn(eventNode);

    var request = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var response = eventController.updateEventData(USER, EVENT_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(eventNode));
  }

  @Test
  public void updateEventDataReturnsErrorIfResourceDoesNotExist() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new ResourceNotFoundException("")).when(eventService)
        .updateEventData(eq(USER), eq(eventNodeId), eq(NAME), eq(DESCRIPTION), eq(1L), eq(true),
            eq(null), any());

    var request = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var response = eventController.updateEventData(USER, EVENT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventDataReturnsErrorIfResourceDeleted() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new ResourceDeletedException("", new Object())).when(eventService)
        .updateEventData(eq(USER), eq(eventNodeId), eq(NAME), eq(DESCRIPTION), eq(1L), eq(true),
            eq(null), any());

    var request = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var response = eventController.updateEventData(USER, EVENT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventDataReturnsErrorIfUnauthorized() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new UnauthorisedException("")).when(eventService)
        .updateEventData(eq(USER), eq(eventNodeId), eq(NAME), eq(DESCRIPTION), eq(1L), eq(true),
            eq(null), any());

    var request = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var response = eventController.updateEventData(USER, EVENT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateEventDataReturnsErrorIfInvalidImage() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    doThrow(new InvalidImageException("")).when(eventService)
        .updateEventData(eq(USER), eq(eventNodeId), eq(NAME), eq(DESCRIPTION), eq(1L), eq(true),
            eq(null), any());

    var request = new UpdateEventDataRequest(NAME, DESCRIPTION, 1L, true);
    var response = eventController.updateEventData(USER, EVENT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void acceptEventInvitationSuccessfulCall() throws Exception {
    when(eventService.acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any())).thenReturn(NODE);

    var response = eventController.acceptEventInvitation(USER, ENCRYPTED_EVENT_ID);

    verify(eventService).acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(NODE));
  }

  @Test
  public void acceptEventInvitationReturnsErrorIfEventDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("Bam!")).when(eventService)
        .acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any());

    var response = eventController.acceptEventInvitation(USER, ENCRYPTED_EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void acceptEventInvitationReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("Bam!", NODE)).when(eventService)
        .acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any());

    var response = eventController.acceptEventInvitation(USER, ENCRYPTED_EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void acceptEventInvitationReturnsErrorIfUserDoesNotHavePermission() throws Exception {
    doThrow(new UnauthorisedException("Bam!")).when(eventService)
        .acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any());

    var response = eventController.acceptEventInvitation(USER, ENCRYPTED_EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void acceptEventInvitationReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("Bam!")).when(eventService)
        .acceptEventInvitation(eq(ENCRYPTED_EVENT_ID), eq(USER), any());

    var response = eventController.acceptEventInvitation(USER, ENCRYPTED_EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
  
  @Test
  public void inviteToEventSuccessfulCall() throws Exception {
    var request = new InviteToEventRequest(CONTACT_IDS);
    var response = eventController.inviteToEvent(USER, EVENT_ID, request);

    verify(eventService).inviteToEvent(eq(EVENT_ID), eq(USER), eq(CONTACT_IDS), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToEventReturnsErrorIfEventDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("Bam!")).when(eventService)
        .inviteToEvent(eq(EVENT_ID), eq(USER), eq(CONTACT_IDS), any());

    var request = new InviteToEventRequest(CONTACT_IDS);
    var response = eventController.inviteToEvent(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToEventReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("Bam!", NODE)).when(eventService)
        .inviteToEvent(eq(EVENT_ID), eq(USER), eq(CONTACT_IDS), any());

    var request = new InviteToEventRequest(CONTACT_IDS);
    var response = eventController.inviteToEvent(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToEventReturnsErrorIfUserDoesNotHavePermission() throws Exception {
    doThrow(new UnauthorisedException("Bam!")).when(eventService)
        .inviteToEvent(eq(EVENT_ID), eq(USER), eq(CONTACT_IDS), any());

    var request = new InviteToEventRequest(CONTACT_IDS);
    var response = eventController.inviteToEvent(USER, EVENT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusSuccessfulCall() throws Exception {
    var request = new EventAdminStatusRequest(true);
    var response = eventController.setMemberAdminStatus(USER, EVENT_ID, "someone", request);

    verify(eventService)
        .setMemberAdminStatus(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq("someone"),
            eq(true), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfResourceNotFound() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(eventService)
        .setMemberAdminStatus(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new EventAdminStatusRequest(true);
    var response = eventController.setMemberAdminStatus(USER, EVENT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(eventService)
        .setMemberAdminStatus(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new EventAdminStatusRequest(true);
    var response = eventController.setMemberAdminStatus(USER, EVENT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(eventService)
        .setMemberAdminStatus(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new EventAdminStatusRequest(true);
    var response = eventController.setMemberAdminStatus(USER, EVENT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromEventSuccessfulCall() throws Exception {
    var response = eventController.removeFromEvent(USER, EVENT_ID, USER_ID_2);

    verify(eventService)
        .removeUserFromEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq(USER_ID_2),
            any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromEventReturnsErrorIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(eventService)
        .removeUserFromEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq(USER_ID_2),
            any());

    var response = eventController.removeFromEvent(USER, EVENT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromEventReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(eventService)
        .removeUserFromEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq(USER_ID_2),
            any());

    var response = eventController.removeFromEvent(USER, EVENT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromEventReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(eventService)
        .removeUserFromEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), eq(USER_ID_2),
            any());

    var response = eventController.removeFromEvent(USER, EVENT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteEventSuccessfulCall() throws Exception {
    var eventNodeId = new NodeId(EVENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId).build();
    when(eventService.deleteEvent(eq(eventNodeId), eq(USER), any())).thenReturn(List.of(eventNode));

    var response = eventController.deleteEvent(USER, EVENT_ID);

    verify(eventService).deleteEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(eventNode)));
  }

  @Test
  public void deleteEventReturnsErrorIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(eventService)
        .deleteEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), any());

    var response = eventController.deleteEvent(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteEventReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(eventService)
        .deleteEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), any());

    var response = eventController.deleteEvent(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteEventReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(eventService)
        .deleteEvent(eq(new NodeId(EVENT_ID, NodeType.EVENT)), eq(USER), any());

    var response = eventController.deleteEvent(USER, EVENT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
