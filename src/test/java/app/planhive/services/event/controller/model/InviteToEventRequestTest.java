package app.planhive.services.event.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

import java.util.List;

public class InviteToEventRequestTest {

  private static final List<String> CONTACT_IDS = List.of("1121", "2212");

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new InviteToEventRequest(CONTACT_IDS);

    assertThat(obj.getContactIds()).isEqualTo(CONTACT_IDS);
  }

  @Test
  public void deserializationFailsIfContactIdsMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{}", InviteToEventRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'contactIds'");
  }

  @Test
  public void equality() {
    var obj1 = new InviteToEventRequest(CONTACT_IDS);
    var obj2 = new InviteToEventRequest(CONTACT_IDS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new InviteToEventRequest(CONTACT_IDS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new InviteToEventRequest(CONTACT_IDS);
    var obj2 = new InviteToEventRequest(List.of());

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
