package app.planhive.services.event.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.timeline.model.ActivityTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class CreateEventRequestTest {

  private static final String EVENT_NAME = "Party at Bogey Lowenstein's house!";
  private static final String DESCRIPTION = "Don't call, just be there!";

  private static final ActivityTime ACTIVITY_TIME = new ActivityTime(1L, 2L);

  private static final String LOCATION_TITLE = "My House";
  private static final String ADDRESS = "On the right at the end of the street";
  private static final String LATITUDE = "4° 10' 31.7856'' N";
  private static final String LONGITUDE = "73° 30' 33.6456'' E";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new CreateEventRequest(EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj.getName()).isEqualTo(EVENT_NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(ACTIVITY_TIME.getEndTs());
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }

  @Test
  public void constructorRequiredArgumentsOnly() {
    var obj = new CreateEventRequest(EVENT_NAME, null, 1L, 2L, null, null, null, null);

    assertThat(obj.getName()).isEqualTo(EVENT_NAME);
    assertThat(obj.getDescription()).isNull();
    assertThat(obj.getStartTs()).isEqualTo(1L);
    assertThat(obj.getEndTs()).isEqualTo(2L);
    assertThat(obj.getLocationTitle()).isNull();
    assertThat(obj.getAddress()).isNull();
    assertThat(obj.getLatitude()).isNull();
    assertThat(obj.getLocationTitle()).isNull();
  }

  @Test
  public void constructorThrowsIfLocationPresentButTimeNot() {
    assertThatThrownBy(
        () -> new CreateEventRequest(EVENT_NAME, null, 0L, 0L, LOCATION_TITLE, null, null,
            null))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void constructorThrowsIfStartTsAfterEndTs() {
    assertThatThrownBy(
        () -> new CreateEventRequest(EVENT_NAME, null, ACTIVITY_TIME.getEndTs(),
            ACTIVITY_TIME.getStartTs(), null, null, null,
            null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Invalid start and/or end timestamp");
  }

  @Test
  public void deserializationFailsIfNameMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{\"startTs\":0,\"endTs\":0}", CreateEventRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void deserializationFailsIfStartTsMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{\"name\":\"Something\",\"endTs\":0}", CreateEventRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'startTs'");
  }

  @Test
  public void deserializationFailsIfEndTsMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{\"name\":\"Something\",\"startTs\":0}", CreateEventRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'endTs'");
  }

  @Test
  public void equality() {
    var obj1 = new CreateEventRequest(EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);
    var obj2 = new CreateEventRequest(EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new CreateEventRequest(EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new CreateEventRequest(EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);
    var obj2 = new CreateEventRequest("What", DESCRIPTION, ACTIVITY_TIME.getStartTs(),
        ACTIVITY_TIME.getEndTs(), LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void editorConstructsObjectFromString() {
    var editor = new CreateEventRequest.Editor();

    var s = String.format(
        "{\"name\":\"%s\",\"description\":\"%s\",\"startTs\":\"%s\",\"endTs\":\"%s\","
            + "\"locationTitle\":\"%s\",\"address\":\"%s\",\"latitude\":\"%s\","
            + "\"longitude\":\"%s\"}",
        EVENT_NAME, DESCRIPTION, ACTIVITY_TIME.getStartTs(), ACTIVITY_TIME.getEndTs(),
        LOCATION_TITLE, ADDRESS, LATITUDE, LONGITUDE);

    editor.setAsText(s);
    var obj = (CreateEventRequest) editor.getValue();

    assertThat(obj.getName()).isEqualTo(EVENT_NAME);
    assertThat(obj.getDescription()).isEqualTo(DESCRIPTION);
    assertThat(obj.getStartTs()).isEqualTo(ACTIVITY_TIME.getStartTs());
    assertThat(obj.getEndTs()).isEqualTo(ACTIVITY_TIME.getEndTs());
    assertThat(obj.getLocationTitle()).isEqualTo(LOCATION_TITLE);
    assertThat(obj.getAddress()).isEqualTo(ADDRESS);
    assertThat(obj.getLatitude()).isEqualTo(LATITUDE);
    assertThat(obj.getLongitude()).isEqualTo(LONGITUDE);
  }
}
