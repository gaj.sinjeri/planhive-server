package app.planhive.services.chat.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

import java.util.List;

public class InviteToChatRequestTest {

  private static final List<String> USER_IDS = List.of("1121", "2212");

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new InviteToChatRequest(USER_IDS);

    assertThat(obj.getUserIds()).isEqualTo(USER_IDS);
  }

  @Test
  public void deserializationFailsIfUserIdsMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{\"chatId\":\"1\"}", InviteToChatRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'userIds'");
  }

  @Test
  public void equality() {
    var obj1 = new InviteToChatRequest(USER_IDS);
    var obj2 = new InviteToChatRequest(USER_IDS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new InviteToChatRequest(USER_IDS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new InviteToChatRequest(USER_IDS);
    var obj2 = new InviteToChatRequest(List.of());

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
