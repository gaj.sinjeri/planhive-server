package app.planhive.services.chat.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class UpdateChatDataRequestTest {

  private static final String NAME = "Laser tag";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new UpdateChatDataRequest(NAME, 1L);

    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getCoverTs()).isEqualTo(1L);
  }

  @Test
  public void deserializationFailsIfNameMissing() {
    assertThatThrownBy(() -> mapper.readValue("{\"coverTs\":1}", UpdateChatDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void deserializationFailsIfCoverTsMissing() {
    assertThatThrownBy(() -> mapper.readValue("{\"name\":\"a\"}", UpdateChatDataRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'coverTs'");
  }

  @Test
  public void equality() {
    var obj1 = new UpdateChatDataRequest(NAME, 1L);
    var obj2 = new UpdateChatDataRequest(NAME, 1L);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new UpdateChatDataRequest(NAME, 1L);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new UpdateChatDataRequest(NAME, 1L);
    var obj2 = new UpdateChatDataRequest(NAME, 2L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
