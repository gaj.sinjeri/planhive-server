package app.planhive.services.chat.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.chat.controller.model.CreateGroupChatRequest;
import app.planhive.services.chat.controller.model.GroupChatAdminStatusRequest;
import app.planhive.services.chat.controller.model.InviteToChatRequest;
import app.planhive.services.chat.controller.model.UpdateChatDataRequest;
import app.planhive.services.chat.service.GroupChatService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class GroupChatControllerTest {

  private static final User USER = new User.Builder("123").build();
  private static final String USER_ID_2 = "234";
  private static final List<String> USER_IDS = List.of("123");
  private static final String NAME = "Skydiving";
  private static final String CHAT_ID = "10";
  private static final MultipartFile FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg",
      new byte[1]);
  private static final Node NODE = new Node.Builder(new NodeId("1", NodeType.EVENT)).build();

  private GroupChatService groupChatService;
  private GroupChatController groupChatController;

  @BeforeEach
  public void setUp() {
    groupChatService = mock(GroupChatService.class);
    groupChatController = new GroupChatController(groupChatService);
  }

  @Test
  public void createGroupChatSuccessfulCall() throws Exception {
    var node = new Node.Builder(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)).build();
    when(groupChatService.createGroupChat(eq(USER), eq(USER_IDS), eq(NAME), eq(FILE), any()))
        .thenReturn(List.of(node));

    var request = new CreateGroupChatRequest(USER_IDS, NAME);
    var response = groupChatController.createGroupChat(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node)));
  }

  @Test
  public void createGroupChatReturnsErrorIfInvalidImage() throws Exception {
    doThrow(new InvalidImageException("")).when(groupChatService)
        .createGroupChat(eq(USER), eq(USER_IDS), eq(NAME), eq(FILE), any());

    var request = new CreateGroupChatRequest(USER_IDS, NAME);
    var response = groupChatController.createGroupChat(USER, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getCoverPhotoSuccessfulCall() throws Exception {
    when(groupChatService.getCoverPhoto(USER, CHAT_ID)).thenReturn(FILE.getBytes());

    var response = groupChatController.getCoverPhoto(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverPhotoReturnsErrorIfResourceNotFound() throws Exception {
    when(groupChatService.getCoverPhoto(USER, CHAT_ID))
        .thenThrow(new ResourceNotFoundException(""));

    var response = groupChatController.getCoverPhoto(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoReturnsErrorIfResourceDeleted() throws Exception {
    when(groupChatService.getCoverPhoto(USER, CHAT_ID))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = groupChatController.getCoverPhoto(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoReturnsErrorIfUserNotAuthorised() throws Exception {
    when(groupChatService.getCoverPhoto(USER, CHAT_ID)).thenThrow(new UnauthorisedException(""));

    var response = groupChatController.getCoverPhoto(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailSuccessfulCall() throws Exception {
    when(groupChatService.getCoverPhotoThumbnail(USER, CHAT_ID)).thenReturn(FILE.getBytes());

    var response = groupChatController.getCoverPhotoThumbnail(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfResourceNotFound() throws Exception {
    when(groupChatService.getCoverPhotoThumbnail(USER, CHAT_ID))
        .thenThrow(new ResourceNotFoundException(""));

    var response = groupChatController.getCoverPhotoThumbnail(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfResourceDeleted() throws Exception {
    when(groupChatService.getCoverPhotoThumbnail(USER, CHAT_ID))
        .thenThrow(new ResourceDeletedException("", NODE));

    var response = groupChatController.getCoverPhotoThumbnail(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getCoverPhotoThumbnailReturnsErrorIfUserNotAuthorised() throws Exception {
    when(groupChatService.getCoverPhotoThumbnail(USER, CHAT_ID))
        .thenThrow(new UnauthorisedException(""));

    var response = groupChatController.getCoverPhotoThumbnail(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void updateChatDataSuccessfulCall() throws Exception {
    var chatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId).build();

    when(groupChatService
        .updateChatData(eq(USER), eq(chatNodeId), eq(NAME), eq(1L), eq(FILE), any()))
        .thenReturn(chatNode);

    var request = new UpdateChatDataRequest(NAME, 1L);
    var response = groupChatController.updateChatData(USER, CHAT_ID, request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(chatNode));
  }

  @Test
  public void updateChatDataReturnsErrorIfResourceDoesNotExist() throws Exception {
    var chatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    doThrow(new ResourceNotFoundException("")).when(groupChatService)
        .updateChatData(eq(USER), eq(chatNodeId), eq(NAME), eq(1L), eq(null), any());

    var request = new UpdateChatDataRequest(NAME, 1L);
    var response = groupChatController.updateChatData(USER, CHAT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateChatDataReturnsErrorIfResourceDeleted() throws Exception {
    var chatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    doThrow(new ResourceDeletedException("", NODE)).when(groupChatService)
        .updateChatData(eq(USER), eq(chatNodeId), eq(NAME), eq(1L), eq(null), any());

    var request = new UpdateChatDataRequest(NAME, 1L);
    var response = groupChatController.updateChatData(USER, CHAT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateChatDataReturnsErrorIfUnauthorized() throws Exception {
    var chatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    doThrow(new UnauthorisedException("")).when(groupChatService)
        .updateChatData(eq(USER), eq(chatNodeId), eq(NAME), eq(1L), eq(null), any());

    var request = new UpdateChatDataRequest(NAME, 1L);
    var response = groupChatController.updateChatData(USER, CHAT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void updateChatDataReturnsErrorIfInvalidImage() throws Exception {
    var chatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    doThrow(new InvalidImageException("")).when(groupChatService)
        .updateChatData(eq(USER), eq(chatNodeId), eq(NAME), eq(1L), eq(null), any());

    var request = new UpdateChatDataRequest(NAME, 1L);
    var response = groupChatController.updateChatData(USER, CHAT_ID, request, null);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToChatSuccessfulCall() throws Exception {
    var request = new InviteToChatRequest(USER_IDS);
    var response = groupChatController.inviteToChat(USER, CHAT_ID, request);

    verify(groupChatService)
        .inviteToChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_IDS), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToChatReturnsErrorIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(groupChatService)
        .inviteToChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_IDS), any());

    var request = new InviteToChatRequest(USER_IDS);
    var response = groupChatController.inviteToChat(USER, CHAT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToChatReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(groupChatService)
        .inviteToChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_IDS), any());

    var request = new InviteToChatRequest(USER_IDS);
    var response = groupChatController.inviteToChat(USER, CHAT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void inviteToChatReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(groupChatService)
        .inviteToChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_IDS), any());

    var request = new InviteToChatRequest(USER_IDS);
    var response = groupChatController.inviteToChat(USER, CHAT_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusSuccessfulCall() throws Exception {
    var request = new GroupChatAdminStatusRequest(true);
    var response = groupChatController.setMemberAdminStatus(USER, CHAT_ID, "someone", request);

    verify(groupChatService)
        .setMemberAdminStatus(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq("someone"),
            eq(true), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfResourceNotFound() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(groupChatService)
        .setMemberAdminStatus(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new GroupChatAdminStatusRequest(true);
    var response = groupChatController.setMemberAdminStatus(USER, CHAT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(groupChatService)
        .setMemberAdminStatus(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new GroupChatAdminStatusRequest(true);
    var response = groupChatController.setMemberAdminStatus(USER, CHAT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void setMemberAdminStatusReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(groupChatService)
        .setMemberAdminStatus(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq("someone"),
            eq(true), any());

    var request = new GroupChatAdminStatusRequest(true);
    var response = groupChatController.setMemberAdminStatus(USER, CHAT_ID, "someone", request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromChatSuccessfulCall() throws Exception {
    var response = groupChatController.removeFromChat(USER, CHAT_ID, USER_ID_2);

    verify(groupChatService)
        .removeUserFromChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_ID_2),
            any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromChatReturnsErrorIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(groupChatService)
        .removeUserFromChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_ID_2),
            any());

    var response = groupChatController.removeFromChat(USER, CHAT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromChatReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(groupChatService)
        .removeUserFromChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_ID_2),
            any());

    var response = groupChatController.removeFromChat(USER, CHAT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void removeFromChatReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(groupChatService)
        .removeUserFromChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), eq(USER_ID_2),
            any());

    var response = groupChatController.removeFromChat(USER, CHAT_ID, USER_ID_2);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteGroupChatSuccessfulCall() throws Exception {
    var groupChatNodeId = new NodeId(CHAT_ID, NodeType.CHAT_GROUP);
    var groupChatNode = new Node.Builder(groupChatNodeId).build();
    when(groupChatService.deleteChat(eq(groupChatNodeId), eq(USER), any()))
        .thenReturn(List.of(groupChatNode));

    var response = groupChatController.deleteGroupChat(USER, CHAT_ID);

    verify(groupChatService)
        .deleteChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(groupChatNode)));
  }

  @Test
  public void deleteGroupChatReturnsErrorIfResourceDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(groupChatService)
        .deleteChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), any());

    var response = groupChatController.deleteGroupChat(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteGroupChatReturnsErrorIfResourceDeleted() throws Exception {
    doThrow(new ResourceDeletedException("", NODE)).when(groupChatService)
        .deleteChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), any());

    var response = groupChatController.deleteGroupChat(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deleteGroupChatReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(groupChatService)
        .deleteChat(eq(new NodeId(CHAT_ID, NodeType.CHAT_GROUP)), eq(USER), any());

    var response = groupChatController.deleteGroupChat(USER, CHAT_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
