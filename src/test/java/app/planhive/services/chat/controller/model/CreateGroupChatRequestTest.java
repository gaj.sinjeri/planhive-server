package app.planhive.services.chat.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CreateGroupChatRequestTest {

  private static final List<String> USER_IDS = List.of("123");
  private static final String NAME = "Cinema!";

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new CreateGroupChatRequest(USER_IDS, NAME);

    assertThat(obj.getUserIds()).isEqualTo(USER_IDS);
    assertThat(obj.getChatName()).isEqualTo(NAME);
  }

  @Test
  public void deserializationFailsIfUserIdsMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"groupName\":\"1\"}", CreateGroupChatRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'userIds'");
  }

  @Test
  public void deserializationFailsIfChatNameMissing() {
    assertThatThrownBy(() -> mapper
        .readValue("{\"userIds\":[]}", CreateGroupChatRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'chatName'");
  }

  @Test
  public void equality() {
    var obj1 = new CreateGroupChatRequest(USER_IDS, NAME);
    var obj2 = new CreateGroupChatRequest(USER_IDS, NAME);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new CreateGroupChatRequest(USER_IDS, NAME);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new CreateGroupChatRequest(USER_IDS, NAME);
    var obj2 = new CreateGroupChatRequest(USER_IDS, "Theatre!");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
