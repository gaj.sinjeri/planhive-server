package app.planhive.services.chat.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.model.RestResponse;
import app.planhive.services.chat.service.DirectChatService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class DirectChatControllerTest {

  private static final User USER = new User.Builder("123").build();
  private static final String CHAT_ID = "10";

  private DirectChatService directChatService;
  private DirectChatController directChatController;

  @BeforeEach
  public void setUp() {
    directChatService = mock(DirectChatService.class);
    directChatController = new DirectChatController(directChatService);
  }

  @Test
  public void getDirectChatSuccessfulCall() throws Exception {
    var node = new Node.Builder(new NodeId(CHAT_ID, NodeType.CHAT_DIRECT)).build();
    when(directChatService.getDirectChat(eq(USER), eq("147"), any())).thenReturn(List.of(node));

    var response = directChatController.getDirectChat(USER, "147");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(node)));
  }

  @Test
  public void getDirectChatReturnsErrorIfResourceDeleted() throws Exception {
    var node = new Node.Builder(new NodeId(CHAT_ID, NodeType.CHAT_DIRECT)).build();
    doThrow(new ResourceDeletedException("", node)).when(directChatService)
        .getDirectChat(eq(USER), eq("147"), any());

    var response = directChatController.getDirectChat(USER, "147");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
