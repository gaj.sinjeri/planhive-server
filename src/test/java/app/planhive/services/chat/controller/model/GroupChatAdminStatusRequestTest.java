package app.planhive.services.chat.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class GroupChatAdminStatusRequestTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new GroupChatAdminStatusRequest(true);

    assertThat(obj.isSetAdmin()).isEqualTo(true);
  }

  @Test
  public void deserializationFailsIfSetAdminMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{}", GroupChatAdminStatusRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'setAdmin'");
  }

  @Test
  public void equality() {
    var obj1 = new GroupChatAdminStatusRequest(true);
    var obj2 = new GroupChatAdminStatusRequest(true);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new GroupChatAdminStatusRequest(true);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new GroupChatAdminStatusRequest(true);
    var obj2 = new GroupChatAdminStatusRequest(false);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
