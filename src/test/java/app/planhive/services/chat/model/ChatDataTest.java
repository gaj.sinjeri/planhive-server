package app.planhive.services.chat.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.services.node.model.NodeData;

import org.junit.jupiter.api.Test;

public class ChatDataTest {

  private static final String NAME = "Cinema Club";
  private static final Long TS = 1L;

  @Test
  public void constructor() {
    var obj = new ChatData(NAME, TS);

    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getCoverTs()).isEqualTo(TS);
  }

  @Test
  public void constructorTrimsName() {
    var obj = new ChatData("  A  ", TS);

    assertThat(obj.getName()).isEqualTo("A");
    assertThat(obj.getCoverTs()).isEqualTo(TS);
  }

  @Test
  public void constructorThrowsIfNameNull() {
    assertThatThrownBy(() -> new ChatData(null, TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Group chat name does not match pattern");
  }

  @Test
  public void constructorThrowsIfNameEmpty() {
    assertThatThrownBy(() -> new ChatData("", TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Group chat name does not match pattern");
  }

  @Test
  public void constructorThrowsIfNameLongerThanLimit() {
    assertThatThrownBy(() -> new ChatData(new String(new char[26]).replace('\0', ' '), TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Group chat name does not match pattern");
  }

  @Test
  public void constructorThrowsIfCoverTsNull() {
    assertThatThrownBy(() -> new ChatData(NAME, null)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new ChatData(NAME, TS);
    var obj2 = new ChatData(NAME, TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ChatData(NAME, TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ChatData(NAME, TS);
    var obj2 = new ChatData("Hmm?", TS);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void toJson() {
    var obj = new ChatData(NAME, TS);

    assertThat(obj.toJson()).isEqualTo(String.format("{\"name\":\"%s\",\"coverTs\":%s}", NAME, TS));
  }

  @Test
  public void fromJson() {
    var obj = NodeData
        .fromJson(String.format("{\"name\":\"%s\",\"coverTs\":%s}", NAME, TS), ChatData.class);

    var expectedObj = new ChatData(NAME, TS);
    assertThat(obj).isEqualTo(expectedObj);
  }

  @Test
  public void fromJsonFailsIfNameMissing() {
    assertThatThrownBy(() -> NodeData.fromJson("{\"coverTs\":1}", ChatData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void fromJsonFailsIfCoverTsMissing() {
    assertThatThrownBy(() -> NodeData.fromJson("{\"name\":\"Tim\"}", ChatData.class))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Missing required creator property 'coverTs'");
  }
}
