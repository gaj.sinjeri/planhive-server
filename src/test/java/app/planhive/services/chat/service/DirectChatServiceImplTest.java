package app.planhive.services.chat.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class DirectChatServiceImplTest {

  private static final String UUID = "1234";
  private static final String ID_1 = "2345";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "abc";
  private static final String USER_ID_2 = "cba";
  private static final String NAME = "Fawlty";
  private static final User USER = new User.Builder(USER_ID).withName(NAME).build();
  private static final Instant TS = Instant.now();

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private ThreadService threadService;
  private DirectChatService directChatService;


  @BeforeEach
  public void setUp() {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    threadService = mock(ThreadService.class);

    directChatService = new DirectChatServiceImpl(nodeService, feedService, permissionService,
        threadService);
  }

  @Test
  public void getDirectChatNodeExists() throws Exception {
    var chatId = String.format("%s~%s", USER_ID, USER_ID_2);
    var chatNodeId = new NodeId(chatId, NodeType.CHAT_DIRECT);
    var chatNode = new Node.Builder(chatNodeId).build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    var threadNode = new Node.Builder(new NodeId("A", chatId, NodeType.THREAD)).build();
    when(nodeService.getChildNodes(chatId)).thenReturn(List.of(threadNode));

    var result = directChatService.getDirectChat(USER, USER_ID_2, TS);

    assertThat(result).containsOnly(chatNode, threadNode);
  }

  @Test
  public void getDirectChatCreatesDeterministicNodeIdWhenUserIdsFlipped() throws Exception {
    var chatId = String.format("%s~%s", USER_ID, USER_ID_2);
    var chatNodeId = new NodeId(chatId, NodeType.CHAT_DIRECT);
    var chatNode = new Node.Builder(chatNodeId).build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    var threadNode = new Node.Builder(new NodeId("A", chatId, NodeType.THREAD)).build();
    when(nodeService.getChildNodes(chatId)).thenReturn(List.of(threadNode));

    var result = directChatService.getDirectChat(new User.Builder(USER_ID_2).build(), USER_ID, TS);

    assertThat(result).containsOnly(chatNode, threadNode);
  }

  @Test
  public void getDirectChatNodeDoesNotExist() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(nodeService).getNodeOrThrow(any());

    when(permissionService.createPermissionGroup(
        Map.of(USER_ID, AccessControl.withRights(AccessRight.READ),
            USER_ID_2, AccessControl.withRights(AccessRight.READ)), TS))
        .thenReturn(PERMISSION_GROUP_ID);

    var chatId = String.format("%s~%s", USER_ID, USER_ID_2);

    var threadNodeId = new NodeId(ID_1, chatId, NodeType.THREAD);
    var threadNode = new Node.Builder(threadNodeId).build();
    when(threadService
        .createThread(new NodeId(chatId, NodeType.CHAT_DIRECT), PERMISSION_GROUP_ID, false, USER_ID,
            List.of(USER_ID_2), TS))
        .thenReturn(threadNode);

    var result = directChatService.getDirectChat(USER, USER_ID_2, TS);

    var captor = ArgumentCaptor.forClass(Node.class);
    verify(nodeService).insertNode(captor.capture());

    var arg = captor.getValue();

    assertThat(arg.getNodeId()).isEqualTo(new NodeId(chatId, NodeType.CHAT_DIRECT));
    assertThat(arg.getCreatorId()).isEqualTo(USER_ID);
    assertThat(arg.getPrivateNode()).isNull();
    assertThat(arg.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(arg.getCreationTs()).isEqualTo(TS);
    assertThat(arg.getLastUpdateTs()).isEqualTo(TS);
    assertThat(arg.getData()).isNull();
    assertThat(arg.getDeleted()).isNull();

    assertThat(result).isEqualTo(List.of(arg, threadNode));

    ArgumentCaptor<List<String>> captor2 = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(eq(arg.getNodeId().toFeedItemKey()), captor2.capture(),
        eq(NodeType.CHAT_DIRECT.name()), eq(TS));
    assertThat(captor2.getValue()).containsOnly(USER_ID, USER_ID_2);
  }
}
