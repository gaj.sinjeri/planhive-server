package app.planhive.services.chat.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.chat.model.ChatData;
import app.planhive.services.chat.notification.GroupChatNotificationSender;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.imageio.ImageIO;

@TestInstance(Lifecycle.PER_CLASS)
public class GroupChatServiceImplTest {

  private static final String CHAT_NAME = "Cinema tomorrow!";
  private static final String UUID = "1234";
  private static final String NODE_ID = "5555";
  private static final String ID_1 = "2345";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "abc";
  private static final String USER_ID_2 = "cba";
  private static final String NAME = "Fawlty";
  private static final User USER = new User.Builder(USER_ID).withName(NAME).build();
  private static final User USER_2 = new User.Builder(USER_ID_2).build();
  private static final Instant TS = Instant.now();

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private GroupChatNotificationSender groupChatNotificationSender;
  private ThreadService threadService;
  private UserService userService;
  private GroupService groupService;
  private CloudStore cloudStore;
  private GroupChatService groupChatService;

  private InputStream imageInputStream;
  private MultipartFile FILE;

  @BeforeEach
  public void setUp() throws Exception {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    groupChatNotificationSender = mock(GroupChatNotificationSender.class);
    threadService = mock(ThreadService.class);
    userService = mock(UserService.class);
    groupService = mock(GroupService.class);
    cloudStore = mock(CloudStore.class);

    groupChatService = new GroupChatServiceImpl(secureGenerator, nodeService, feedService,
        permissionService, groupChatNotificationSender, threadService, userService, groupService,
        cloudStore, 2048, 256);

    imageInputStream = generateImageInputStream(2, 1);
    FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg", imageInputStream);
  }

  @Test
  public void createGroupChat() throws Exception {
    when(permissionService.createPermissionGroup(
        Map.of(USER_ID,
            AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE),
            USER_ID_2, AccessControl.withRights(AccessRight.READ)), TS))
        .thenReturn(PERMISSION_GROUP_ID);

    var threadNodeId = new NodeId(ID_1, UUID, NodeType.THREAD);
    var threadNode = new Node.Builder(threadNodeId).build();
    when(threadService
        .createThread(new NodeId(UUID, NodeType.CHAT_GROUP), PERMISSION_GROUP_ID, false, USER_ID,
            List.of(USER_ID_2), TS)).thenReturn(threadNode);

    var result = groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, FILE, TS);

    var captor = ArgumentCaptor.forClass(Node.class);
    verify(nodeService).insertNode(captor.capture());

    var arg = captor.getValue();

    assertThat(arg.getNodeId()).isEqualTo(new NodeId(UUID, NodeType.CHAT_GROUP));
    assertThat(arg.getCreatorId()).isEqualTo(USER_ID);
    assertThat(arg.getPrivateNode()).isNull();
    assertThat(arg.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(arg.getCreationTs()).isEqualTo(TS);
    assertThat(arg.getLastUpdateTs()).isEqualTo(TS);
    assertThat(arg.getDeleted()).isNull();

    var chatData = NodeData.fromJson(arg.getData(), ChatData.class);
    assertThat(chatData.getName()).isEqualTo(CHAT_NAME);
    assertThat(chatData.getCoverTs()).isNotEqualTo(0L);

    assertThat(result).isEqualTo(List.of(arg, threadNode));

    ArgumentCaptor<List<String>> captor2 = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(eq(arg.getNodeId().toFeedItemKey()), captor2.capture(),
        eq(NodeType.CHAT_GROUP.name()), eq(TS));
    assertThat(captor2.getValue()).containsOnly(USER_ID, USER_ID_2);

    verify(groupChatNotificationSender)
        .sendNewChatNotification(NAME, new NodeId(UUID, NodeType.CHAT_GROUP), CHAT_NAME,
            threadNodeId, List.of(USER_ID_2));

    var captor3 = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("chats/%s/cover.jpeg", UUID)), captor3.capture());

    var capturedImage = captor3.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("chats/%s/t-cover.jpeg", UUID)), any());
  }

  @Test
  public void createGroupChatDoesNotStoreCoverPhotoIfNull() throws Exception {
    when(permissionService.createPermissionGroup(
        Map.of(USER_ID,
            AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE),
            USER_ID_2, AccessControl.withRights(AccessRight.READ)), TS))
        .thenReturn(PERMISSION_GROUP_ID);

    var threadNodeId = new NodeId(ID_1, UUID, NodeType.THREAD);
    var threadNode = new Node.Builder(threadNodeId).build();
    when(threadService
        .createThread(new NodeId(UUID, NodeType.CHAT_GROUP), PERMISSION_GROUP_ID, false, USER_ID,
            List.of(USER_ID_2), TS)).thenReturn(threadNode);

    var result = groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, null, TS);

    var captor = ArgumentCaptor.forClass(Node.class);
    verify(nodeService).insertNode(captor.capture());

    var arg = captor.getValue();

    var expectedChatData = new ChatData(CHAT_NAME, 0L);
    assertThat(arg.getNodeId()).isEqualTo(new NodeId(UUID, NodeType.CHAT_GROUP));
    assertThat(arg.getCreatorId()).isEqualTo(USER_ID);
    assertThat(arg.getPrivateNode()).isNull();
    assertThat(arg.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(arg.getCreationTs()).isEqualTo(TS);
    assertThat(arg.getLastUpdateTs()).isEqualTo(TS);
    assertThat(arg.getData()).isEqualTo(expectedChatData.toJson());
    assertThat(arg.getDeleted()).isNull();

    assertThat(result).isEqualTo(List.of(arg, threadNode));

    ArgumentCaptor<List<String>> captor2 = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(eq(arg.getNodeId().toFeedItemKey()), captor2.capture(),
        eq(NodeType.CHAT_GROUP.name()), eq(TS));
    assertThat(captor2.getValue()).containsOnly(USER_ID, USER_ID_2);

    verify(groupChatNotificationSender)
        .sendNewChatNotification(NAME, new NodeId(UUID, NodeType.CHAT_GROUP), CHAT_NAME,
            threadNodeId, List.of(USER_ID_2));
    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void createGroupChatRethrowsIfPhotoInaccessible() throws Exception {
    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(
        () -> groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, fileMock, TS))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error handling image. This could be either due to not being "
            + "able to access the input stream of the original image or create thumbnail");

    verify(cloudStore, never()).storeImage(any(), any());
    verify(permissionService, never()).createPermissionGroup(any(), any());
    verify(nodeService, never()).insertNode(any());
    verify(feedService, never()).createFeeds(any(), any(), any(), any());
    verify(threadService, never()).createThread(any(), any(), anyBoolean(), any(), any(), any());
    verify(groupChatNotificationSender, never())
        .sendNewChatNotification(any(), any(), any(), any(), any());
  }

  @Test
  public void createGroupChatThrowsIfCoverPhotoFileFormatIncorrect() {
    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(
        () -> groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, fileMock, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");
  }

  @Test
  public void createGroupChatThrowsIfCoverPhotoResolutionTooHigh() throws Exception {
    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(
        () -> groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");
  }

  @Test
  public void createGroupChatThrowsIfCoverPhotoAspectRatioIncorrect() throws Exception {
    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(4, 1));

    assertThatThrownBy(
        () -> groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Expected width-to-height ratio to be 2.0, but was 4.0");
  }

  @Test
  public void createGroupChatRethrowsIfThreadAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(
        () -> groupChatService.createGroupChat(USER, List.of(USER_ID_2), CHAT_NAME, FILE, TS))
        .isInstanceOf(InternalError.class);

    verify(feedService, never()).createFeeds(any(), any(), any(), any());
    verify(threadService, never()).createThread(any(), any(), anyBoolean(), any(), any(), any());
    verify(groupChatNotificationSender, never())
        .sendNewChatNotification(any(), any(), any(), any(), any());
  }

  @Test
  public void getCoverPhoto() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);
    when(cloudStore.getFile(String.format("chats/%s/cover.jpeg", NODE_ID)))
        .thenReturn(FILE.getBytes());

    var result = groupChatService.getCoverPhoto(USER, NODE_ID);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getCoverThumbnail() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);
    when(cloudStore.getFile(String.format("chats/%s/t-cover.jpeg", NODE_ID)))
        .thenReturn(FILE.getBytes());

    var result = groupChatService.getCoverPhotoThumbnail(USER, NODE_ID);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);

    assertThat(result).isEqualTo(FILE.getBytes());
  }

  @Test
  public void updateChatData() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    var chatData = new ChatData(CHAT_NAME, TS.getMillis());
    var updatedChatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.updateNodeData(chatNodeId, chatData.toJson(), TS)).thenReturn(updatedChatNode);

    var result = groupChatService.updateChatData(USER, chatNodeId, CHAT_NAME, null, FILE, TS);

    assertThat(result).isEqualTo(updatedChatNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(chatNodeId.toFeedItemKey(), TS);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("chats/%s/cover.jpeg", NODE_ID)), captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore).storeImage(eq(String.format("chats/%s/t-cover.jpeg", NODE_ID)), any());
  }

  @Test
  public void updateChatDataWithNullCoverPhoto() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    var chatData = new ChatData(CHAT_NAME, 1L);
    var updatedChatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.updateNodeData(chatNodeId, chatData.toJson(), TS)).thenReturn(updatedChatNode);

    var result = groupChatService.updateChatData(USER, chatNodeId, CHAT_NAME, 1L, null, TS);

    assertThat(result).isEqualTo(updatedChatNode);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(feedService).refreshFeedsForResource(chatNodeId.toFeedItemKey(), TS);

    verify(cloudStore, never()).storeImage(any(), any());
  }

  @Test
  public void inviteToChat() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    var invitee = new User.Builder(ID_1).build();
    when(groupService.addUsersToGroup(chatNode, List.of(ID_1), TS, TS))
        .thenReturn(List.of(invitee));

    groupChatService.inviteToChat(chatNodeId, USER, List.of(ID_1), TS);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.WRITE);
    verify(groupChatNotificationSender)
        .sendInvitedToChatNotification(NAME, chatNodeId, CHAT_NAME, List.of(invitee));
    verify(groupChatNotificationSender)
        .sendNewMemberNotification(chatNodeId, CHAT_NAME, List.of(invitee), List.of(USER_ID));
  }

  @Test
  public void setMemberAdminStatusMakeAdmin() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    groupChatService.setMemberAdminStatus(chatNodeId, USER, USER_ID_2, true, TS);

    verify(groupService).setMemberAdminStatus(chatNode, USER, USER_ID_2, true, TS);
    verify(groupChatNotificationSender).sendNewAdminNotification(chatNodeId, CHAT_NAME, USER_ID_2);
  }

  @Test
  public void setMemberAdminStatusRemoveAdmin() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    groupChatService.setMemberAdminStatus(chatNodeId, USER, USER_ID_2, false, TS);

    verify(groupService).setMemberAdminStatus(chatNode, USER, USER_ID_2, false, TS);
    verify(groupChatNotificationSender, never()).sendNewAdminNotification(any(), any(), any());
  }

  @Test
  public void removeUserFromChat() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    when(userService.getUser(USER_ID_2)).thenReturn(USER_2);

    groupChatService.removeUserFromChat(chatNodeId, USER, USER_ID_2, TS);

    verify(groupService).removeUserFromGroup(chatNode, USER, USER_2, TS);
    verify(groupChatNotificationSender, never()).sendNewAdminNotification(any(), any(), any());
  }

  @Test
  public void removeUserFromChatSendsNotificationIfNewAdmin() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    when(userService.getUser(USER_ID_2)).thenReturn(USER_2);
    when(groupService.removeUserFromGroup(chatNode, USER, USER_2, TS))
        .thenReturn(Optional.of(UUID));

    groupChatService.removeUserFromChat(chatNodeId, USER, USER_ID_2, TS);

    verify(groupChatNotificationSender).sendNewAdminNotification(chatNodeId, CHAT_NAME, UUID);
  }

  @Test
  public void deleteChat() throws Exception {
    var chatNodeId = new NodeId(NODE_ID, NodeType.CHAT_GROUP);
    var chatData = new ChatData(CHAT_NAME, 0L);
    var chatNode = new Node.Builder(chatNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withData(chatData.toJson())
        .build();
    when(nodeService.getNodeOrThrow(chatNodeId)).thenReturn(chatNode);

    groupChatService.deleteChat(chatNodeId, USER, TS);

    verify(groupService).deleteGroup(chatNode, USER, TS);
    verify(groupChatNotificationSender)
        .sendGroupChatDeletedNotification(chatNodeId, CHAT_NAME, USER_ID);
  }

  private InputStream generateImageInputStream(int width, int height) throws Exception {
    var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    var outputStream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpeg", outputStream);

    return new ByteArrayInputStream(outputStream.toByteArray());
  }
}
