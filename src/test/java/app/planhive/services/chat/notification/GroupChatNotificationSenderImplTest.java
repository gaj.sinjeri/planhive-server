package app.planhive.services.chat.notification;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class GroupChatNotificationSenderImplTest {

  private static final String USER_NAME_1 = "Roger";
  private static final User USER_1 = new User.Builder("1").withName(USER_NAME_1).build();
  private static final User USER_2 = new User.Builder("2").build();
  private static final User USER_3 = new User.Builder("3").build();
  private static final String CHAT_NAME = "Rabbit";
  private static final NodeId CHAT_NODE_ID = new NodeId("123", NodeType.CHAT_GROUP);
  private static final NodeId THREAD_NODE_ID = new NodeId("234", "123", NodeType.THREAD);
  private static final List<String> USER_IDS = List.of("1", "2");

  private NotificationService notificationService;
  private GroupChatNotificationSender groupChatNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    groupChatNotificationSender = new GroupChatNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendNewMemberNotificationZeroNewMembers() {
    groupChatNotificationSender
        .sendNewMemberNotification(CHAT_NODE_ID, CHAT_NAME, List.of(), List.of());

    verify(notificationService, never())
        .sendNotificationByNodeId(any(), any(), any(), anyBoolean());
  }

  @Test
  public void sendNewMemberNotificationOneNewMember() {
    groupChatNotificationSender
        .sendNewMemberNotification(CHAT_NODE_ID, CHAT_NAME, List.of(USER_1), List.of());

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(CHAT_NAME,
        String.format("%s joined the group chat.", USER_NAME_1), NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, CHAT_NODE_ID, List.of("1"), true);
  }

  @Test
  public void sendNewMemberNotificationTwoNewMembers() {
    groupChatNotificationSender
        .sendNewMemberNotification(CHAT_NODE_ID, CHAT_NAME, List.of(USER_1, USER_2), List.of("55"));

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(CHAT_NAME,
        String.format("%s and %s other person joined the group chat.", USER_NAME_1, 1),
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, CHAT_NODE_ID, List.of("1", "2", "55"),
            true);
  }

  @Test
  public void sendNewMemberNotificationManyNewMembers() {
    groupChatNotificationSender
        .sendNewMemberNotification(CHAT_NODE_ID, CHAT_NAME, List.of(USER_1, USER_2, USER_3),
            List.of());

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(CHAT_NAME,
        String.format("%s and %s other people joined the group chat.", USER_NAME_1, 2),
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, CHAT_NODE_ID, List.of("1", "2", "3"), true);
  }

  @Test
  public void sendNewChatNotification() {
    groupChatNotificationSender
        .sendNewChatNotification(USER_NAME_1, CHAT_NODE_ID, CHAT_NAME, THREAD_NODE_ID, USER_IDS);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "123",
        NodeType.CHAT_GROUP.name(), "234", "123", NodeType.THREAD.name());
    var expectedNotification = new Notification.Builder(
        USER_NAME_1, String.format("Created a new chat: %s", CHAT_NAME),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService).sendNotificationByUserIds(expectedNotification, USER_IDS, true);
  }

  @Test
  public void sendInvitedToChatNotification() {
    groupChatNotificationSender
        .sendInvitedToChatNotification(USER_NAME_1, CHAT_NODE_ID, CHAT_NAME,
            List.of(USER_1, USER_2));

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(
        "New group chat", String.format("%s added you to %s.", USER_NAME_1, CHAT_NAME),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService).sendNotificationByUserIds(expectedNotification, USER_IDS, true);
  }

  @Test
  public void sendNewAdminNotification() {
    groupChatNotificationSender.sendNewAdminNotification(CHAT_NODE_ID, CHAT_NAME, "234");

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(
        CHAT_NAME, "You are now an admin.", NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByUserIds(expectedNotification, List.of("234"), true);
  }

  @Test
  public void sendGroupChatDeletedNotification() {
    groupChatNotificationSender.sendGroupChatDeletedNotification(CHAT_NODE_ID, CHAT_NAME, "234");

    var expectedNodeIds = String
        .format("[{\"id\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.CHAT_GROUP.name());
    var expectedNotification = new Notification.Builder(CHAT_NAME, "Group chat has been deleted.",
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, CHAT_NODE_ID, List.of("234"), true);
  }
}
