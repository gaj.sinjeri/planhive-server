package app.planhive.services.permission.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.permission.controller.model.GetPermissionUpdatesRequest;
import app.planhive.services.permission.controller.model.GetPermissionsRequest;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class PermissionControllerTest {

  private static final User USER = new User.Builder("v34v-43-gj03g").build();
  private static final String PERMISSION_GROUP_ID = "123";
  private static final Instant TS = new Instant(1);

  private PermissionService permissionService;
  private PermissionController permissionController;

  @BeforeEach
  public void setUp() {
    permissionService = mock(PermissionService.class);
    permissionController = new PermissionController(permissionService);
  }

  @Test
  public void getPermissionsSuccessfulCall() throws Exception {
    var permission = new Permission.Builder(USER.getId(), PERMISSION_GROUP_ID).build();
    when(permissionService.getPermissions(USER, PERMISSION_GROUP_ID, TS))
        .thenReturn(List.of(permission));

    var request = new GetPermissionsRequest(TS.getMillis());
    var response = permissionController.getPermissions(USER, PERMISSION_GROUP_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(permission)));
  }

  @Test
  public void getPermissionsReturnsErrorIfUserNotInPermissionGroup() throws Exception {
    doThrow(new UnauthorisedException("")).when(permissionService)
        .getPermissions(USER, PERMISSION_GROUP_ID, TS);

    var request = new GetPermissionsRequest(TS.getMillis());
    var response = permissionController.getPermissions(USER, PERMISSION_GROUP_ID, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getPermissionUpdatesSuccessfulCall() {
    var permission = new Permission.Builder(USER.getId(), PERMISSION_GROUP_ID).build();
    when(permissionService.getPermissionUpdates(USER, TS)).thenReturn(List.of(permission));

    var request = new GetPermissionUpdatesRequest(TS.getMillis());
    var response = permissionController.getPermissionUpdates(USER, request);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(permission)));
  }
}
