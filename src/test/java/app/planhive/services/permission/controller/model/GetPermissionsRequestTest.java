package app.planhive.services.permission.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class GetPermissionsRequestTest {

  private static final Long TS = Instant.now().getMillis();

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new GetPermissionsRequest(TS);

    assertThat(obj.getLastUpdateTs()).isEqualTo(TS);
  }

  @Test
  public void deserializationFailsIfLastUpdateTsMissing() {
    assertThatThrownBy(
        () -> mapper.readValue("{}", GetPermissionsRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'lastUpdateTs'");
  }

  @Test
  public void equality() {
    var obj1 = new GetPermissionsRequest(TS);
    var obj2 = new GetPermissionsRequest(TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new GetPermissionsRequest(TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new GetPermissionsRequest(TS);
    var obj2 = new GetPermissionsRequest(0L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
