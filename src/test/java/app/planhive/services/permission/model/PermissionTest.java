package app.planhive.services.permission.model;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

import java.util.Base64;

public class PermissionTest {

  private static final String USER_ID = "111";
  private static final String PERMISSION_GROUP_ID = "222";
  private static final AccessControl ACCESS_CONTROL = AccessControl.withRights(AccessRight.READ);
  private static final Instant TS = Instant.now();

  @Test
  public void builder() {
    var obj = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();

    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(obj.getAccessControl()).isEqualTo(ACCESS_CONTROL);
    assertThat(obj.getLastUpdateTs()).isEqualTo(TS);
    assertThat(obj.getDeleted()).isEqualTo(true);
  }

  @Test
  public void equality() {
    var obj1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();
    var obj2 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();
    var obj2 = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(false)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void serialize() throws Exception {
    var obj = new Permission.Builder(USER_ID, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL)
        .withLastUpdateTs(TS)
        .setDeleted(true)
        .build();

    var mapper = new ObjectMapper();
    var jsonStr = mapper.writeValueAsString(obj);

    assertThat(jsonStr).isEqualTo(String.format(
        "{\"userId\":\"%s\",\"permissionGroupId\":\"%s\",\"permissionFlags\":\"%s\","
            + "\"lastUpdateTs\":%s,\"deleted\":%s}", USER_ID, PERMISSION_GROUP_ID,
        new String(Base64.getEncoder().encode(ACCESS_CONTROL.getRights().toByteArray())),
        TS.getMillis(), true));
  }
}
