package app.planhive.services.permission.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.BitSet;

public class AccessControlTest {

  @Test
  public void withRights() {
    var obj = AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);

    var bs = new BitSet();
    bs.set(AccessRight.READ.getValue());
    bs.set(AccessRight.WRITE.getValue());
    bs.set(AccessRight.DELETE.getValue());

    assertThat(obj.getRights()).isEqualTo(bs);
  }

  @Test
  public void withRightsEmpty() {
    var obj = AccessControl.withRights();

    assertThat(obj.getRights()).isEqualTo(new BitSet());
  }

  @Test
  public void fromBytes() {
    var obj = AccessControl.fromBytes(BigInteger.valueOf(7).toByteArray());

    var bs = new BitSet();
    bs.set(AccessRight.READ.getValue());
    bs.set(AccessRight.WRITE.getValue());
    bs.set(AccessRight.DELETE.getValue());

    assertThat(obj.getRights()).isEqualTo(bs);
  }

  @Test
  public void fromBytesNoRights() {
    var obj = AccessControl.fromBytes(BigInteger.valueOf(0).toByteArray());

    assertThat(obj.getRights()).isEqualTo(new BitSet());
  }

  @Test
  public void hasRightReturnsCorrectBoolean() {
    var obj = AccessControl.withRights(AccessRight.READ);

    assertThat(obj.hasRight(AccessRight.READ)).isTrue();
    assertThat(obj.hasRight(AccessRight.WRITE)).isFalse();
    assertThat(obj.hasRight(AccessRight.DELETE)).isFalse();
  }

  @Test
  public void equality() {
    var obj1 = AccessControl.withRights(
        AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);
    var obj2 = AccessControl.withRights(
        AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = AccessControl.withRights(AccessRight.WRITE, AccessRight.DELETE);
    var obj2 = AccessControl.withRights(AccessRight.READ, AccessRight.DELETE);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}