package app.planhive.services.permission.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
public class PermissionDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final String PERMISSION_GROUP_ID = "123";
  private static final AccessControl ACCESS_CONTROL = AccessControl.withRights(AccessRight.READ);
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private DynamoDbTestUtils dynamoDbTestUtils;
  private PermissionDAO permissionDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());
    var dynamoDB = dynamoDbTestUtils.getDynamoDB();
    var table = dynamoDbTestUtils.getTable();

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getDynamoDB()).thenReturn(dynamoDB);
    when(dynamoDBClientMock.getTable()).thenReturn(table);

    permissionDAO = new PermissionDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void batchPutItemWritesItemsCorrectly() {
    var id = UUID.randomUUID().toString();
    var permission1 = new Permission.Builder("111", id)
        .withAccessControl(ACCESS_CONTROL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .build();
    var permission2 = new Permission.Builder("222", id)
        .withAccessControl(ACCESS_CONTROL)
        .withCreationTs(TS_1)
        .withLastUpdateTs(TS_1)
        .build();
    permissionDAO.batchPutItem(List.of(permission1, permission2));

    var storedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "111",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", id)),
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "222",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", id)))
    );

    assertThat(storedItems.size()).isEqualTo(2);
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("111", "222");
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("perm|%s", id));
    assertThat(storedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("perm|%s", id));
    assertThat(storedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_1.getMillis());
    assertThat(storedItems).extracting(it -> it.getBinary("AccCtrl"))
        .containsOnly(ACCESS_CONTROL.getRights().toByteArray());
    assertThat(storedItems).extracting(it -> it.get("Del")).containsOnlyNulls();
  }

  @Test
  public void batchPutItemEmptyList() {
    // This test is to ensure that an empty list input does not cause an error.
    permissionDAO.batchPutItem(List.of());
  }

  @Test
  public void readItemReturnsRequestedItem() {
    var result = permissionDAO.readItem("1", PERMISSION_GROUP_ID);

    assertThat(result.getUserId()).isEqualTo("1");
    assertThat(result.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(result.getAccessControl()).isEqualTo(ACCESS_CONTROL);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_1);
    assertThat(result.getDeleted()).isEqualTo(true);
  }

  @Test
  public void readItemReturnsNullIfItemDoesNotExist() {
    var result = permissionDAO.readItem("missing", "missing");

    assertThat(result).isNull();
  }

  @Test
  public void updateAccessControl() throws Exception {
    var accessControl = AccessControl
        .withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);
    var result = permissionDAO.updateAccessControl("4", PERMISSION_GROUP_ID, accessControl, TS_2);

    assertThat(result.getUserId()).isEqualTo("4");
    assertThat(result.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(result.getAccessControl()).isEqualTo(accessControl);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(result.getDeleted()).isEqualTo(false);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("4");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("perm|%s", PERMISSION_GROUP_ID));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("perm|%s", PERMISSION_GROUP_ID));
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-C"))
        .contains(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getBinary("AccCtrl"))
        .containsOnly(accessControl.getRights().toByteArray());
    assertThat(retrievedItems).extracting(it -> it.getBoolean("Del")).containsOnly(false);
  }

  @Test
  public void updateAccessControlThrowsIfItemDoesNotExist() {
    assertThatThrownBy(
        () -> permissionDAO.updateAccessControl("missing", "not-here", ACCESS_CONTROL, TS_1))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining("Attempted to update access control for user id missing, "
            + "permission group id not-here, but the item does not exist or has been deleted");
  }

  @Test
  public void queryByPermissionGroupIdReturnsPermissions() {
    var result = permissionDAO
        .queryByPermissionGroupId(PERMISSION_GROUP_ID, new Instant(0));

    assertThat(result.size()).isEqualTo(4);
    assertThat(result).extracting(Permission::getUserId).contains("1", "2", "3", "4");
    assertThat(result).extracting(Permission::getPermissionGroupId).contains(PERMISSION_GROUP_ID);
    assertThat(result).extracting(Permission::getAccessControl).contains(ACCESS_CONTROL);
    assertThat(result).extracting(Permission::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(Permission::getLastUpdateTs).contains(TS_1);
    assertThat(result).extracting(Permission::getDeleted).contains(true);
  }

  @Test
  public void queryByPermissionGroupIdReturnsPermissionsFiltersByLastUpdateTs() {
    var result = permissionDAO.queryByPermissionGroupId(PERMISSION_GROUP_ID, TS_1);

    assertThat(result.size()).isEqualTo(1);
    assertThat(result).extracting(Permission::getUserId).contains("2");
    assertThat(result).extracting(Permission::getPermissionGroupId).contains(PERMISSION_GROUP_ID);
    assertThat(result).extracting(Permission::getAccessControl).contains(ACCESS_CONTROL);
    assertThat(result).extracting(Permission::getCreationTs).containsOnly(TS_1);
    assertThat(result).extracting(Permission::getLastUpdateTs).contains(TS_2);
    assertThat(result).extracting(Permission::getDeleted).contains(true);
  }

  @Test
  public void softDeletePermission() throws ResourceNotFoundException {
    var result = permissionDAO.softDeletePermission("3", PERMISSION_GROUP_ID, TS_2);

    assertThat(result.getUserId()).isEqualTo("3");
    assertThat(result.getPermissionGroupId()).isEqualTo(PERMISSION_GROUP_ID);
    assertThat(result.getAccessControl()).isEqualTo(ACCESS_CONTROL);
    assertThat(result.getCreationTs()).isEqualTo(TS_1);
    assertThat(result.getLastUpdateTs()).isEqualTo(TS_2);
    assertThat(result.getDeleted()).isEqualTo(true);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly("3");
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly(String.format("perm|%s", PERMISSION_GROUP_ID));
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.GSI1_H))
        .containsOnly(String.format("perm|%s", PERMISSION_GROUP_ID));
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-C"))
        .containsOnly(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getLong(DynamoDBClient.GSI1_R))
        .containsOnly(TS_2.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getBinary("AccCtrl"))
        .containsOnly(ACCESS_CONTROL.getRights().toByteArray());
    assertThat(retrievedItems).extracting(it -> it.get("Del")).containsOnly(true);
  }

  @Test
  public void softDeleteNodeThrowsIfResourceDoesNotExist() {
    assertThatThrownBy(() -> permissionDAO.softDeletePermission("missing", "not-here", TS_2))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String.format("Attempted to mark permission item as deleted for "
                + "permission group id %s, user %s, but the item does not exist", "not-here",
            "missing"));
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "1",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withString(DynamoDBClient.GSI1_H, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withLong("Ts-C", TS_1.getMillis())
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withBinary("AccCtrl", ACCESS_CONTROL.getRights().toByteArray())
        .withBoolean("Del", true));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "2",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withString(DynamoDBClient.GSI1_H, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withLong("Ts-C", TS_1.getMillis())
        .withLong(DynamoDBClient.GSI1_R, TS_2.getMillis())
        .withBinary("AccCtrl", ACCESS_CONTROL.getRights().toByteArray())
        .withBoolean("Del", true));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "3",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withString(DynamoDBClient.GSI1_H, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withLong("Ts-C", TS_1.getMillis())
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withBinary("AccCtrl", ACCESS_CONTROL.getRights().toByteArray()));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "4",
            DynamoDBClient.PRIMARY_KEY_R, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withString(DynamoDBClient.GSI1_H, String.format("perm|%s", PERMISSION_GROUP_ID))
        .withLong("Ts-C", TS_1.getMillis())
        .withLong(DynamoDBClient.GSI1_R, TS_1.getMillis())
        .withBinary("AccCtrl", ACCESS_CONTROL.getRights().toByteArray())
        .withBoolean("Del", false));

    return items;
  }
}
