package app.planhive.services.permission.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.permission.persistence.PermissionDAO;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class PermissionServiceImplTest {

  private static final String UUID = "123";
  private static final String USER_ID_1 = "A123";
  private static final String USER_ID_2 = "B123";
  private static final User USER_1 = new User.Builder(USER_ID_1).build();
  private static final User USER_2 = new User.Builder(USER_ID_2).build();
  private static final String PERMISSION_GROUP_ID = "p12";
  private static final String PERMISSION_GROUP_ID_2 = "p17";
  private static final AccessControl ACCESS_CONTROL_READ = AccessControl
      .withRights(AccessRight.READ);
  private static final AccessControl ACCESS_CONTROL_WRITE = AccessControl
      .withRights(AccessRight.WRITE);
  private static final Instant TS_1 = new Instant(1);

  private PermissionDAO permissionDAO;
  private SecureGenerator secureGenerator;
  private FeedService feedService;
  private PermissionService permissionService;

  @BeforeAll
  public void setUpAll() {
    secureGenerator = mock(SecureGenerator.class);
  }

  @BeforeEach
  public void setUpEach() {
    permissionDAO = mock(PermissionDAO.class);
    feedService = mock(FeedService.class);

    permissionService = new PermissionServiceImpl(permissionDAO, secureGenerator, feedService);
  }

  @Test
  public void createPermissionGroupCreatesPermissionsAndReturnsId() {
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    var result = permissionService.createPermissionGroup(
        Map.of(USER_ID_1, ACCESS_CONTROL_READ, USER_ID_2, ACCESS_CONTROL_WRITE), TS_1);

    assertThat(result).isEqualTo(UUID);

    ArgumentCaptor<List<Permission>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionDAO).batchPutItem(captor.capture());
    var arg = captor.getValue();

    assertThat(arg).hasSize(2);
    assertThat(arg).extracting(Permission::getUserId).containsOnly(USER_ID_1, USER_ID_2);
    assertThat(arg).extracting(Permission::getPermissionGroupId).containsOnly(UUID);
    assertThat(arg).extracting(Permission::getAccessControl)
        .containsOnly(ACCESS_CONTROL_READ, ACCESS_CONTROL_WRITE);
    assertThat(arg).extracting(Permission::getCreationTs).containsOnly(TS_1);
    assertThat(arg).extracting(Permission::getLastUpdateTs).containsOnly(TS_1);
    assertThat(arg).extracting(Permission::getDeleted).containsOnlyNulls();

    ArgumentCaptor<List<String>> captor2 = ArgumentCaptor.forClass(List.class);
    verify(feedService).createFeeds(eq(UUID), captor2.capture(), eq("perm"), eq(TS_1));
    var arg2 = captor2.getValue();

    assertThat(arg2).hasSize(2);
    assertThat(arg2).containsOnly(USER_ID_1, USER_ID_2);
  }

  @Test
  public void addUsersToPermissionGroup() {
    permissionService
        .addUsersToPermissionGroup(Map.of(USER_ID_2, ACCESS_CONTROL_WRITE), PERMISSION_GROUP_ID,
            TS_1);

    ArgumentCaptor<List<Permission>> captor = ArgumentCaptor.forClass(List.class);
    verify(permissionDAO).batchPutItem(captor.capture());
    var arg = captor.getValue();

    assertThat(arg.size()).isEqualTo(1);
    assertThat(arg).extracting(Permission::getUserId).containsOnly(USER_ID_2);
    assertThat(arg).extracting(Permission::getPermissionGroupId).containsOnly(PERMISSION_GROUP_ID);
    assertThat(arg).extracting(Permission::getAccessControl).containsOnly(ACCESS_CONTROL_WRITE);
    assertThat(arg).extracting(Permission::getCreationTs).containsOnly(TS_1);
    assertThat(arg).extracting(Permission::getLastUpdateTs).containsOnly(TS_1);
    assertThat(arg).extracting(Permission::getDeleted).containsOnlyNulls();

    ArgumentCaptor<List<String>> captor2 = ArgumentCaptor.forClass(List.class);
    verify(feedService)
        .createFeeds(eq(PERMISSION_GROUP_ID), captor2.capture(), eq("perm"), eq(TS_1));
    var arg2 = captor2.getValue();

    assertThat(arg2).hasSize(1);
    assertThat(arg2).containsOnly(USER_ID_2);

    verify(feedService).refreshFeedsForResource(PERMISSION_GROUP_ID, TS_1);
  }

  @Test
  public void updateAccessControl() throws ResourceNotFoundException {
    var permission = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    when(permissionDAO
        .updateAccessControl(USER_ID_1, PERMISSION_GROUP_ID, ACCESS_CONTROL_READ, TS_1))
        .thenReturn(permission);

    var result = permissionService
        .updateAccessControl(USER_ID_1, PERMISSION_GROUP_ID, ACCESS_CONTROL_READ, TS_1);

    assertThat(result).isEqualTo(permission);
    verify(feedService).refreshFeedsForResource(PERMISSION_GROUP_ID, TS_1);
  }

  @Test
  public void verifyAccessRightDoesNotThrowIfAuthorized() throws UnauthorisedException {
    var dbItem = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL_READ)
        .withLastUpdateTs(TS_1)
        .setDeleted(false)
        .build();

    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(dbItem);

    permissionService.verifyAccessRight(USER_ID_1, PERMISSION_GROUP_ID, AccessRight.READ);
  }

  @Test
  public void verifyAccessRightThrowsIfUnauthorized() {
    var dbItem = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID)
        .withAccessControl(ACCESS_CONTROL_READ)
        .withLastUpdateTs(TS_1)
        .setDeleted(false)
        .build();

    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(dbItem);

    assertThatThrownBy(() -> permissionService
        .verifyAccessRight(USER_ID_1, PERMISSION_GROUP_ID, AccessRight.WRITE))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining(String.format("User %s does not have %s permission for permission "
            + "group id %s", USER_ID_1, AccessRight.WRITE, PERMISSION_GROUP_ID));
  }

  @Test
  public void verifyAccessRightThrowsIfNoPermissionItem() {
    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(null);

    assertThatThrownBy(() -> permissionService
        .verifyAccessRight(USER_ID_1, PERMISSION_GROUP_ID, AccessRight.WRITE))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining(String.format("User %s does not have %s permission for permission "
            + "group id %s", USER_ID_1, AccessRight.WRITE, PERMISSION_GROUP_ID));
  }

  @Test
  public void verifyAccessRightThrowsIfPermissionItemDeleted() {
    var dbItem = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID)
        .setDeleted(true)
        .build();

    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(dbItem);

    assertThatThrownBy(() -> permissionService
        .verifyAccessRight(USER_ID_1, PERMISSION_GROUP_ID, AccessRight.WRITE))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining(String.format("User %s does not have %s permission for permission "
            + "group id %s", USER_ID_1, AccessRight.WRITE, PERMISSION_GROUP_ID));
  }

  @Test
  public void getPermissions() {
    var permission = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    when(permissionDAO.queryByPermissionGroupId(PERMISSION_GROUP_ID, TS_1))
        .thenReturn(List.of(permission));

    var result = permissionService.getPermissions(PERMISSION_GROUP_ID, TS_1);

    assertThat(result).containsOnly(permission);
  }

  @Test
  public void getPermissionsWithUserCheck() throws UnauthorisedException {
    var permission = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(permission);
    when(permissionDAO.queryByPermissionGroupId(PERMISSION_GROUP_ID, TS_1))
        .thenReturn(List.of(permission));

    var result = permissionService.getPermissions(USER_1, PERMISSION_GROUP_ID, TS_1);

    assertThat(result).containsOnly(permission);
  }

  @Test
  public void getPermissionsThrowsIfUserNotPartOfPermissionGroup() {
    when(permissionDAO.readItem(USER_ID_1, PERMISSION_GROUP_ID)).thenReturn(null);

    assertThatThrownBy(() -> permissionService.getPermissions(USER_2, PERMISSION_GROUP_ID, TS_1))
        .isInstanceOf(UnauthorisedException.class).hasMessageContaining(String
        .format("User %s is not part of permission group %s", USER_ID_2, PERMISSION_GROUP_ID));
  }

  @Test
  public void getPermissionUpdates() {
    var feed = new Feed.Builder(PERMISSION_GROUP_ID, USER_ID_1)
        .withBeginTs(new Instant(0))
        .withLastUpdateTs(TS_1)
        .build();
    when(feedService.getFeedsForUser(USER_ID_1, "perm", TS_1, 100))
        .thenReturn(List.of(feed));

    var permission = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    when(permissionDAO.queryByPermissionGroupId(PERMISSION_GROUP_ID, TS_1))
        .thenReturn(List.of(permission));

    var result = permissionService.getPermissionUpdates(USER_1, TS_1);

    assertThat(result).containsOnly(permission);
  }

  @Test
  public void batchSoftDeletePermission() throws Exception {
    var permission1 = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    var permission2 = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID_2).build();
    when(permissionDAO.softDeletePermission(USER_ID_1, PERMISSION_GROUP_ID, TS_1))
        .thenReturn(permission1);
    when(permissionDAO.softDeletePermission(USER_ID_1, PERMISSION_GROUP_ID_2, TS_1))
        .thenReturn(permission2);

    var result = permissionService
        .batchSoftDeletePermissions(USER_ID_1, List.of(PERMISSION_GROUP_ID, PERMISSION_GROUP_ID_2),
            TS_1);

    assertThat(result).containsOnly(permission1, permission2);

    verify(feedService).refreshFeedsForResource(PERMISSION_GROUP_ID, TS_1);
    verify(feedService).refreshFeedsForResource(PERMISSION_GROUP_ID_2, TS_1);
  }

  @Test
  public void batchSoftDeletePermissionIgnoresExceptionIfItemDoesNotExist() throws Exception {
    var permission = new Permission.Builder(USER_ID_1, PERMISSION_GROUP_ID).build();
    when(permissionDAO.softDeletePermission(USER_ID_1, PERMISSION_GROUP_ID, TS_1))
        .thenReturn(permission);

    doThrow(new ResourceNotFoundException("")).when(permissionDAO)
        .softDeletePermission(USER_ID_1, PERMISSION_GROUP_ID_2, TS_1);

    var result = permissionService
        .batchSoftDeletePermissions(USER_ID_1, List.of(PERMISSION_GROUP_ID, PERMISSION_GROUP_ID_2),
            TS_1);

    assertThat(result).containsOnly(permission);

    verify(feedService).refreshFeedsForResource(PERMISSION_GROUP_ID, TS_1);
  }
}
