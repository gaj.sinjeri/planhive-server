package app.planhive.services.registration.controller.model;

import static org.assertj.core.api.Assertions.assertThat;

import app.planhive.services.user.model.User;

import org.junit.jupiter.api.Test;

public class RegisterUserResponseTest {

  private static final User USER = new User.Builder("1234").build();
  private static final String SESSION_ID = "123";

  @Test
  public void constructor() {
    var obj = new RegisterUserResponse(USER, SESSION_ID);

    assertThat(obj.getUser()).isEqualTo(USER);
    assertThat(obj.getSessionId()).isEqualTo(SESSION_ID);
  }

  @Test
  public void equality() {
    var obj1 = new RegisterUserResponse(USER, SESSION_ID);
    var obj2 = new RegisterUserResponse(USER, SESSION_ID);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new RegisterUserResponse(USER, SESSION_ID);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new RegisterUserResponse(USER, SESSION_ID);
    var obj2 = new RegisterUserResponse(USER, "231");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
