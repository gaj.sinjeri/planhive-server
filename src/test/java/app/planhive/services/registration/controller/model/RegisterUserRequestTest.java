package app.planhive.services.registration.controller.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.junit.jupiter.api.Test;

public class RegisterUserRequestTest {

  private static final String USER_ID = "1asd-3d3q-d3-d3q";
  private static final String USER_NAME = "aldo.agent";
  private static final String NAME = "Aldo";
  private static final String MAC = "h4rd5h%EH4gW$#";
  private static final Long TS = 1L;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void constructor() {
    var obj = new RegisterUserRequest(USER_ID, USER_NAME, NAME, MAC, TS);

    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getUsername()).isEqualTo(USER_NAME);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getMac()).isEqualTo(MAC);
    assertThat(obj.getMacCreationTs()).isEqualTo(TS);
  }

  @Test
  public void constructorThrowsIfUsernameInvalid() {
    assertThatThrownBy(() -> new RegisterUserRequest(USER_ID, "O", NAME, MAC, TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Username O does not match pattern");
  }

  @Test
  public void constructorThrowsIfNameInvalid() {
    assertThatThrownBy(
        () -> new RegisterUserRequest(USER_ID, USER_NAME, "1", MAC, TS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Name 1 does not match pattern");
  }

  @Test
  public void deserializationFailsIfUserIdMissing() {
    assertThatThrownBy(() -> mapper.readValue(
        "{\"name\":\"1\",\"username\":\"1\",\"mac\":\"1\",\"macCreationTs\":1}",
        RegisterUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'userId'");
  }

  @Test
  public void deserializationFailsIfUsernameMissing() {
    assertThatThrownBy(() -> mapper.readValue(
        "{\"userId\":\"1\",\"name\":\"1\",\"mac\":\"1\",\"macCreationTs\":1}",
        RegisterUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'username'");
  }

  @Test
  public void deserializationFailsIfNameMissing() {
    assertThatThrownBy(() -> mapper.readValue(
        "{\"userId\":\"1\",\"username\":\"1\",\"mac\":\"1\",\"macCreationTs\":1}",
        RegisterUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'name'");
  }

  @Test
  public void deserializationFailsIfMacMissing() {
    assertThatThrownBy(() -> mapper.readValue(
        "{\"userId\":\"1\",\"name\":\"1\",\"username\":\"1\",\"macCreationTs\":1}",
        RegisterUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'mac'");
  }

  @Test
  public void deserializationFailsIfMacCreationTsMissing() {
    assertThatThrownBy(() -> mapper.readValue(
        "{\"userId\":\"1\",\"name\":\"1\",\"username\":\"1\",\"mac\":\"1\"}",
        RegisterUserRequest.class))
        .isInstanceOf(MismatchedInputException.class)
        .hasMessageContaining("Missing required creator property 'macCreationTs'");
  }

  @Test
  public void equality() {
    var obj1 = new RegisterUserRequest(USER_ID, USER_NAME, NAME, MAC, TS);
    var obj2 = new RegisterUserRequest(USER_ID, USER_NAME, NAME, MAC, TS);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new RegisterUserRequest(USER_ID, USER_NAME, NAME, MAC, TS);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new RegisterUserRequest(USER_ID, USER_NAME, NAME, MAC, TS);
    var obj2 = new RegisterUserRequest(USER_ID, USER_NAME, "Susan", MAC, TS);

    assertThat(obj1).isNotEqualTo(obj2);
  }

  @Test
  public void editorConstructsObjectFromString() {
    var editor = new RegisterUserRequest.Editor();

    var s = String.format("{\"userId\":\"%s\",\"username\":\"%s\",\"name\":\"%s\","
        + "\"mac\":\"%s\",\"macCreationTs\":%s}", USER_ID, USER_NAME, NAME, MAC, TS);

    editor.setAsText(s);
    var obj = (RegisterUserRequest) editor.getValue();

    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getUsername()).isEqualTo(USER_NAME);
    assertThat(obj.getName()).isEqualTo(NAME);
    assertThat(obj.getMac()).isEqualTo(MAC);
    assertThat(obj.getMacCreationTs()).isEqualTo(TS);
  }
}
