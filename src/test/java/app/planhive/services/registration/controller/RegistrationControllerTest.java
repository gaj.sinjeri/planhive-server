package app.planhive.services.registration.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.ResourceType;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.EmailAddress;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.registration.controller.model.RegisterUserRequest;
import app.planhive.services.registration.controller.model.RegisterUserResponse;
import app.planhive.services.registration.controller.model.SendVerificationCodeResponse;
import app.planhive.services.registration.model.VerificationSuccess;
import app.planhive.services.registration.service.RegistrationService;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@TestInstance(Lifecycle.PER_CLASS)
public class RegistrationControllerTest {

  private static final EmailAddress EMAIL = new EmailAddress("admin@planhive.app");
  private static final String CODE = "123";
  private static final String USER_ID = "a1a1a1";
  private static final String SESSION_ID = "l1380h2489";
  private static final String USER_NAME = "joe.moe";
  private static final String NAME = "Joe";
  private static final MultipartFile FILE = new MockMultipartFile("myFile", new byte[1]);
  private static final Long TS = 1L;

  private RegistrationService registrationService;
  private RegistrationController registrationController;

  @BeforeEach
  public void setUp() {
    registrationService = mock(RegistrationService.class);
    registrationController = new RegistrationController(registrationService);
  }

  @Test
  public void sendVerificationCodeSuccessfulCall() throws Exception {
    when(registrationService.sendVerificationCode(eq(EMAIL), any())).thenReturn(1);

    var response = registrationController.sendVerificationCode(EMAIL.getValue());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(new SendVerificationCodeResponse(1)));
  }

  @Test
  public void sendVerificationCodeReturnsErrorIfResourceAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("")).when(registrationService)
        .sendVerificationCode(eq(EMAIL), any());

    var response = registrationController.sendVerificationCode(EMAIL.getValue());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void sendVerificationCodeReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("", "it's all too much")).when(registrationService)
        .sendVerificationCode(eq(EMAIL), any());

    var response = registrationController.sendVerificationCode(EMAIL.getValue());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(
        RestResponse.errorResponse(ErrorCode.MAX_VERIFICATION_CODES_REACHED, "it's all too much"));
  }

  @Test
  public void sendVerificationCodeReturnsErrorIfDatabaseUpdateException() throws Exception {
    doThrow(new DatabaseUpdateException("")).when(registrationService)
        .sendVerificationCode(eq(EMAIL), any());

    var response = registrationController.sendVerificationCode(EMAIL.getValue());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void verifyCodeSuccessfulCall() throws Exception {
    var verificationSuccess = new VerificationSuccess.Builder("123")
        .withSessionId("321")
        .build();
    when(registrationService.verifyCode(eq(EMAIL), eq(CODE), any()))
        .thenReturn(verificationSuccess);

    var response = registrationController.verifyCode(EMAIL.getValue(), CODE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(verificationSuccess));
  }

  @Test
  public void verifyCodeReturnsErrorIfResourceNotFound() throws Exception {
    doThrow(new ResourceNotFoundException("")).when(registrationService)
        .verifyCode(eq(EMAIL), eq(CODE), any());

    var response = registrationController.verifyCode(EMAIL.getValue(), CODE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void verifyCodeReturnsErrorIfUnauthorized() throws Exception {
    doThrow(new UnauthorisedException("")).when(registrationService)
        .verifyCode(eq(EMAIL), eq(CODE), any());

    var response = registrationController.verifyCode(EMAIL.getValue(), CODE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void verifyCodeReturnsErrorIfDatabaseUpdateException() throws Exception {
    doThrow(new DatabaseUpdateException("")).when(registrationService)
        .verifyCode(eq(EMAIL), eq(CODE), any());

    var response = registrationController.verifyCode(EMAIL.getValue(), CODE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void registerUserSuccessfulCall() throws Exception {
    var user = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();
    when(registrationService
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any()))
        .thenReturn(user);

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.successResponse(new RegisterUserResponse(user, SESSION_ID)));
  }

  @Test
  public void registerUserReturnsErrorIfUnauthorised() throws Exception {
    doThrow(new UnauthorisedException("")).when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.errorResponse(ErrorCode.MAC_INVALID));
  }

  @Test
  public void registerUserReturnsErrorIfInvalidImage() throws Exception {
    doThrow(new InvalidImageException("")).when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void registerUserReturnsErrorIfInvalidOperation() throws Exception {
    doThrow(new InvalidOperationException("")).when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.errorResponse(ErrorCode.MAC_EXPIRED));
  }

  @Test
  public void registerUserReturnsErrorIfUsernameTaken() throws Exception {
    doThrow(new ResourceAlreadyExistsException("", ResourceType.DB_USERNAME_ITEM))
        .when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.errorResponse(ErrorCode.USERNAME_TAKEN));
  }

  @Test
  public void registerUserReturnsErrorIfEmailTaken() throws Exception {
    doThrow(new ResourceAlreadyExistsException("", ResourceType.DB_USER_EMAIL_ITEM))
        .when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.errorResponse(ErrorCode.EMAIL_TAKEN));
  }

  @Test
  public void registerUserReturnsErrorIfUserAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("", ResourceType.DB_USER_ITEM))
        .when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody())
        .isEqualTo(RestResponse.errorResponse(ErrorCode.USER_ALREADY_EXISTS));
  }

  @Test
  public void registerUserReturnsErrorIfOtherResourceAlreadyTaken() throws Exception {
    doThrow(new ResourceAlreadyExistsException("", null))
        .when(registrationService)
        .registerUser(eq(EMAIL), eq(USER_ID), eq(CODE), eq(TS), eq(USER_NAME), eq(NAME),
            eq(FILE), any());

    var request = new RegisterUserRequest(USER_ID, USER_NAME, NAME, CODE, TS);
    var response = registrationController.registerUser(EMAIL.getValue(), request, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
