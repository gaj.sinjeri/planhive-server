package app.planhive.services.registration.controller.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class SendVerificationCodeResponseTest {

  @Test
  public void constructor() {
    var obj = new SendVerificationCodeResponse(1);

    assertThat(obj.getCodesRemaining()).isEqualTo(1);
  }

  @Test
  public void equality() {
    var obj1 = new SendVerificationCodeResponse(1);
    var obj2 = new SendVerificationCodeResponse(1);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new SendVerificationCodeResponse(1);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new SendVerificationCodeResponse(1);
    var obj2 = new SendVerificationCodeResponse(2);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
