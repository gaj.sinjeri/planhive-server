package app.planhive.services.registration.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import app.planhive.DynamoDbTestUtils;
import app.planhive.LocalDbCreationExtension;
import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.persistence.model.VersionedItem;
import app.planhive.services.registration.model.EmailAddressVerification;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.ArrayList;
import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class RegistrationDAOImplTest {

  @RegisterExtension
  public LocalDbCreationExtension extension = new LocalDbCreationExtension();

  private static final EmailAddress EMAIL = new EmailAddress("someone@planhive.app");
  private static final String CODE = "fe09420f32krm";
  private static final Instant TS_1 = new Instant(1000);
  private static final Instant TS_2 = new Instant(2000);

  private DynamoDbTestUtils dynamoDbTestUtils;
  private RegistrationDAO registrationDAO;

  @BeforeAll
  public void setUp() {
    dynamoDbTestUtils = new DynamoDbTestUtils(getTestData());
    var amazonDynamoDB = dynamoDbTestUtils.getAmazonDynamoDB();
    var table = dynamoDbTestUtils.getTable();

    var dynamoDBClientMock = mock(DynamoDBClient.class);
    when(dynamoDBClientMock.getAmazonDynamoDB()).thenReturn(amazonDynamoDB);
    when(dynamoDBClientMock.getTable()).thenReturn(table);

    registrationDAO = new RegistrationDAOImpl(dynamoDBClientMock);
  }

  @Test
  public void insertNewItem() throws Exception {
    var email = new EmailAddress("abc@planhive.app");
    var item = new EmailAddressVerification.Builder(email)
        .withCode(CODE)
        .withNumberOfEmailsSent(1)
        .setReminderSent(false)
        .withTimeToLive(TS_2)
        .build();

    registrationDAO.insertNewItem(item);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, email.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "reg")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(email.getValue());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("reg");
    assertThat(retrievedItems).extracting(it -> it.getString("Code")).containsOnly(CODE);
    assertThat(retrievedItems).extracting(it -> it.getInt("NumSent")).containsOnly(1);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Reminder")).containsOnly(false);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Ts-LV")).containsOnly(false);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ttl"))
        .containsOnly(TS_2.getMillis() / 1000);
    assertThat(retrievedItems).extracting(it -> it.getInt("Version")).containsOnly(0);
  }

  @Test
  public void insertNewItemThrowsIfItemAlreadyExistsInDB() {
    var item = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(1)
        .setReminderSent(false)
        .withTimeToLive(TS_2)
        .build();

    assertThatThrownBy(() -> registrationDAO.insertNewItem(item))
        .isInstanceOf(ResourceAlreadyExistsException.class)
        .hasMessageContaining(String.format("Email verification item for email %s already exists",
            EMAIL.getValue()));
  }

  @Test
  public void readItem() {
    var item = registrationDAO.readItem(EMAIL);

    assertThat(item.getVersion()).isEqualTo(1);
    assertThat(item.getItem().getEmailAddress()).isEqualTo(EMAIL);
    assertThat(item.getItem().getCode()).isEqualTo(CODE);
    assertThat(item.getItem().getNumberOfEmailsSent()).isEqualTo(1);
    assertThat(item.getItem().getReminderSent()).isFalse();
    assertThat(item.getItem().getLastVerifiedTimestamp()).isEqualTo(TS_1);
    assertThat(item.getItem().getTimeToLive()).isEqualTo(TS_2);
  }

  @Test
  public void readItemRequiredAttributesOnly() {
    var email = new EmailAddress("admin@planhive.app");
    var item = registrationDAO.readItem(email);

    assertThat(item.getVersion()).isEqualTo(1);
    assertThat(item.getItem().getEmailAddress()).isEqualTo(email);
    assertThat(item.getItem().getCode()).isEqualTo(CODE);
    assertThat(item.getItem().getNumberOfEmailsSent()).isEqualTo(2);
    assertThat(item.getItem().getReminderSent()).isTrue();
    assertThat(item.getItem().getLastVerifiedTimestamp()).isNull();
    assertThat(item.getItem().getTimeToLive()).isEqualTo(TS_2);
  }

  @Test
  public void readItemReturnsNullIfItemNotFound() {
    var item = registrationDAO.readItem(new EmailAddress("ghost@planhive.app"));

    assertThat(item).isNull();
  }

  @Test
  public void updateItem() throws Exception {
    var email = new EmailAddress("user@planhive.app");
    var ts1 = new Instant(123);

    var verificationItem = new EmailAddressVerification.Builder(email)
        .withCode("123")
        .withNumberOfEmailsSent(7)
        .withLastVerifiedTimestamp(ts1)
        .withTimeToLive(TS_1)
        .setReminderSent(true)
        .build();
    var versionedItem = new VersionedItem<>(1, verificationItem);

    registrationDAO.updateItem(versionedItem);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, email.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "reg")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(email.getValue());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("reg");
    assertThat(retrievedItems).extracting(it -> it.getString("Code")).containsOnly("123");
    assertThat(retrievedItems).extracting(it -> it.getInt("NumSent")).containsOnly(7);
    assertThat(retrievedItems).extracting(it -> it.getBOOL("Reminder")).containsOnly(true);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-LV")).containsOnly(ts1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getLong("Ttl"))
        .containsOnly(TS_1.getMillis() / 1000);
    assertThat(retrievedItems).extracting(it -> it.getInt("Version")).containsOnly(2);
  }

  @Test
  public void updateItemRequiredAttributesOnly() throws Exception {
    var email = new EmailAddress("intruder@planhive.app");
    var verificationItem = new EmailAddressVerification.Builder(email).build();
    var versionedItem = new VersionedItem<>(1, verificationItem);

    registrationDAO.updateItem(versionedItem);

    var retrievedItems = dynamoDbTestUtils.batchGetItem(List.of(
        new PrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, email.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "reg")));

    assertThat(retrievedItems.size()).isEqualTo(1);
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_H))
        .containsOnly(email.getValue());
    assertThat(retrievedItems).extracting(it -> it.getString(DynamoDBClient.PRIMARY_KEY_R))
        .containsOnly("reg");
    assertThat(retrievedItems).extracting(it -> it.getString("Code")).containsOnly(CODE);
    assertThat(retrievedItems).extracting(it -> it.getInt("NumSent")).containsOnly(4);
    assertThat(retrievedItems).extracting(it -> it.hasAttribute("Reminder")).containsOnly(false);
    assertThat(retrievedItems).extracting(it -> it.getLong("Ts-LV")).containsOnly(TS_1.getMillis());
    assertThat(retrievedItems).extracting(it -> it.getLong("Ttl"))
        .containsOnly(TS_2.getMillis() / 1000);
    assertThat(retrievedItems).extracting(it -> it.getInt("Version")).containsOnly(2);
  }

  @Test
  public void updateItemThrowsIfVersionMismatch() {
    var verificationItem = new EmailAddressVerification.Builder(
        new EmailAddress("admin@planhive.app"))
        .build();
    var versionedItem = new VersionedItem<>(777, verificationItem);

    assertThatThrownBy(() -> registrationDAO.updateItem(versionedItem))
        .isInstanceOf(DatabaseUpdateException.class)
        .hasMessageContaining(String
            .format("Could not update email verification item for %s due to version mismatch",
                "admin@planhive.app"));
  }

  @Test
  public void getNonVerifiedItems() {
    var result = registrationDAO.getNonVerifiedItems(TS_2);

    assertThat(result).hasSize(1);
    assertThat(result).extracting(VersionedItem::getVersion).containsOnly(1);

    var verificationItem = result.get(0).getItem();

    assertThat(verificationItem.getEmailAddress()).isEqualTo(new EmailAddress("non-ver1@planhive.app"));
    assertThat(verificationItem.getCode()).isEqualTo(CODE);
    assertThat(verificationItem.getNumberOfEmailsSent()).isEqualTo(5);
    assertThat(verificationItem.getReminderSent()).isFalse();
    assertThat(verificationItem.getLastVerifiedTimestamp()).isNull();
    assertThat(verificationItem.getTimeToLive()).isEqualTo(TS_1);
  }

  private List<Item> getTestData() {
    var items = new ArrayList<Item>();

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, EMAIL.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 1)
        .withLong("Ts-LV", TS_1.getMillis())
        .withLong("Ttl", TS_2.getMillis() / 1000)
        .withInt("Version", 1));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "admin@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 2)
        .withLong("Ttl", TS_2.getMillis() / 1000)
        .withInt("Version", 1)
        .withBoolean("Reminder", true));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "user@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 1)
        .withLong("Ttl", TS_2.getMillis() / 1000)
        .withInt("Version", 1)
        .withBoolean("Reminder", true));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "intruder@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 4)
        .withLong("Ts-LV", TS_1.getMillis())
        .withLong("Ttl", TS_2.getMillis() / 1000)
        .withInt("Version", 1));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "non-ver1@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 5)
        .withLong("Ttl", TS_1.getMillis() / 1000)
        .withInt("Version", 1));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "ver1@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 6)
        .withLong("Ts-LV", TS_1.getMillis())
        .withLong("Ttl", TS_1.getMillis() / 1000)
        .withInt("Version", 1));

    items.add(new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, "rem1@planhive.app",
            DynamoDBClient.PRIMARY_KEY_R, "reg")
        .withString("Code", CODE)
        .withLong("NumSent", 7)
        .withBoolean("Reminder", true)
        .withLong("Ttl", TS_1.getMillis() / 1000)
        .withInt("Version", 1));

    return items;
  }
}
