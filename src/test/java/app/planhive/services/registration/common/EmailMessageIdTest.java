package app.planhive.services.registration.common;

import static org.assertj.core.api.Assertions.assertThat;

import app.planhive.model.EmailAddress;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class EmailMessageIdTest {

  private static final EmailAddress EMAIL = new EmailAddress("admin@planhive.app");
  private static final Instant TS_1 = new Instant(1);

  @Test
  public void generate() {
    var result = EmailMessageId.generate(EMAIL, TS_1, 7);

    assertThat(result)
        .isEqualTo(String.format("%s.%s.%s", "YWRtaW5AcGxhbmhpdmUuYXBw", TS_1.getMillis(), 7));
  }

  @Test
  public void generateSimple() {
    var result = EmailMessageId.generate(EMAIL, TS_1);

    assertThat(result)
        .isEqualTo(String.format("%s.%s", "YWRtaW5AcGxhbmhpdmUuYXBw", TS_1.getMillis()));
  }
}
