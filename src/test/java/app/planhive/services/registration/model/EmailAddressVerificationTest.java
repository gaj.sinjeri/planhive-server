package app.planhive.services.registration.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.model.EmailAddress;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class EmailAddressVerificationTest {

  private static final EmailAddress EMAIL = new EmailAddress("admin@planhive.app");
  private static final String CODE = "fe09420f32krm";
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  @Test
  public void builder() {
    var obj = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(7)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();

    assertThat(obj.getEmailAddress()).isEqualTo(EMAIL);
    assertThat(obj.getCode()).isEqualTo(CODE);
    assertThat(obj.getNumberOfEmailsSent()).isEqualTo(7);
    assertThat(obj.getReminderSent()).isTrue();
    assertThat(obj.getLastVerifiedTimestamp()).isEqualTo(TS_1);
    assertThat(obj.getTimeToLive()).isEqualTo(TS_2);
  }

  @Test
  public void builderThrowsIfEmailNull() {
    assertThatThrownBy(() -> new EmailAddressVerification.Builder(null).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(7)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();
    var obj2 = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(7)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(7)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();
    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(7)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();
    var obj2 = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(8)
        .setReminderSent(true)
        .withLastVerifiedTimestamp(TS_1)
        .withTimeToLive(TS_2)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
