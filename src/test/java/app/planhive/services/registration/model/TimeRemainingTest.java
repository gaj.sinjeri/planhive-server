package app.planhive.services.registration.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class TimeRemainingTest {

  @Test
  public void constructor() {
    var obj = new TimeRemaining(1L);

    assertThat(obj.getTimeRemaining()).isEqualTo(1L);
  }

  @Test
  public void equality() {
    var obj1 = new TimeRemaining(1L);
    var obj2 = new TimeRemaining(1L);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new TimeRemaining(1L);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new TimeRemaining(1L);
    var obj2 = new TimeRemaining(2L);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
