package app.planhive.services.registration.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.joda.time.Instant;
import org.junit.jupiter.api.Test;

public class VerificationSuccessTest {

  private static final String USER_ID = "1asd-3d3q-d3-d3q";
  private static final String SESSION_ID = "123123";
  private static final String MAC = "awt43G5e4g4eWG";
  private static final Long TS = Instant.now().getMillis();

  @Test
  public void builderWithSessionId() {
    var obj = new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();

    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getSessionId()).isEqualTo(SESSION_ID);
    assertThat(obj.getMac()).isNull();
    assertThat(obj.getMacCreationTs()).isNull();
  }

  @Test
  public void builderWithMac() {
    var obj = new VerificationSuccess.Builder(USER_ID)
        .withMac(MAC, TS)
        .build();

    assertThat(obj.getUserId()).isEqualTo(USER_ID);
    assertThat(obj.getSessionId()).isNull();
    assertThat(obj.getMac()).isEqualTo(MAC);
    assertThat(obj.getMacCreationTs()).isEqualTo(TS);
  }

  @Test
  public void builderThrowsIfUserIdNull() {
    assertThatThrownBy(() -> new VerificationSuccess.Builder(null).build())
        .isInstanceOf(NullPointerException.class);
  }


  @Test
  public void builderThrowsIfBothSessionIdAndMacSet() {
    assertThatThrownBy(() -> new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withMac(MAC, TS)
        .build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Only one of [sessionId, [mac, macCreationTs]] can be set");
  }

  @Test
  public void builderThrowsIfBothSessionIdAndMacNull() {
    assertThatThrownBy(() -> new VerificationSuccess.Builder(USER_ID).build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Either sessionId or [mac, macCreationTs] must be set");
  }

  @Test
  public void builderThrowsIfMacSetWithoutCreationTs() {
    assertThatThrownBy(() -> new VerificationSuccess.Builder(USER_ID)
        .withMac(MAC, null)
        .build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Either sessionId or [mac, macCreationTs] must be set");
  }

  @Test
  public void builderThrowsIfMacCreationTsSetWithoutMac() {
    assertThatThrownBy(() -> new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .withMac(null, TS)
        .build())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Only one of [sessionId, [mac, macCreationTs]] can be set");
  }

  @Test
  public void equality() {
    var obj1 = new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();
    var obj2 = new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new VerificationSuccess.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();
    var obj2 = new VerificationSuccess.Builder(USER_ID)
        .withMac(MAC, TS)
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
