package app.planhive.services.registration.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.EmailAddress;
import app.planhive.notification.EmailClient;
import app.planhive.notification.model.Email;
import app.planhive.persistence.model.VersionedItem;
import app.planhive.security.SecureGenerator;
import app.planhive.services.registration.common.EmailMessageId;
import app.planhive.services.registration.model.EmailAddressVerification;
import app.planhive.services.registration.model.TimeRemaining;
import app.planhive.services.registration.persistence.RegistrationDAO;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class RegistrationServiceImplTest {

  private static final String UUID = "1234";
  private static final String UUID_2 = "2a2ab";
  private static final String MAC = "a1b2c3";
  private static final EmailAddress EMAIL = new EmailAddress("admin@planhive.app");
  private static final String CODE = "123456";
  private static final Instant TS_1 = new Instant(1601325841574L).minus(10);
  private static final Instant TS_2 = new Instant(1601325841574L);
  private static final String USER_NAME = "a.b.c";
  private static final String NAME = "Fred";
  private static final MultipartFile FILE = new MockMultipartFile("myFile", new byte[1]);

  private static final String APP_NAME = "PlanHive";
  private static final String API_URL_VERIFICATION = "verify.planhive.app/email";
  private static final EmailAddress REGISTRATION_EMAIL_ADDRESS = new EmailAddress(
      "hello@planhive.app");
  private static final String EMAIL_MESSAGE_FORMAT = "%link %link";

  private SecureGenerator secureGenerator;
  private EmailClient emailClient;
  private RegistrationDAO registrationDAO;
  private UserService userService;

  private RegistrationService registrationService;

  @BeforeEach
  public void setUp() {
    secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    emailClient = mock(EmailClient.class);
    registrationDAO = mock(RegistrationDAO.class);
    userService = mock(UserService.class);

    registrationService = new RegistrationServiceImpl(secureGenerator, emailClient, registrationDAO,
        userService, APP_NAME, API_URL_VERIFICATION, REGISTRATION_EMAIL_ADDRESS,
        EMAIL_MESSAGE_FORMAT, EMAIL_MESSAGE_FORMAT);
  }

  @Test
  public void sendVerificationCodeWhenNoItemAlreadyExists() throws Exception {
    when(registrationDAO.readItem(EMAIL)).thenReturn(null);
    when(secureGenerator.generate128BitKey()).thenReturn(CODE);

    var result = registrationService.sendVerificationCode(EMAIL, TS_1);

    assertThat(result).isEqualTo(9);

    var item = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(1)
        .withTimeToLive(TS_1.toDateTime().plusDays(1).toInstant())
        .build();
    verify(registrationDAO).insertNewItem(item);

    var expectedMessageId = EmailMessageId
        .generate(EMAIL, TS_1.toDateTime().plusDays(1).toInstant(), 1);
    var expectedEmail = new Email.Builder(expectedMessageId, REGISTRATION_EMAIL_ADDRESS, EMAIL)
        .withSenderName(APP_NAME)
        .withSubject(RegistrationServiceImpl.VERIFICATION_EMAIL_SUBJECT)
        .withHtmlBody(generateEmailBody())
        .withTextBody(generateEmailBody())
        .build();
    verify(emailClient).sendEmail(expectedEmail);
  }

  @Test
  public void sendVerificationCodeWhenItemAlreadyExists() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .withNumberOfEmailsSent(1)
        .withTimeToLive(Instant.now())
        .build();
    var existingVersionedItem = new VersionedItem<>(0, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    var result = registrationService.sendVerificationCode(EMAIL, TS_2);

    assertThat(result).isEqualTo(8);

    var updatedItem = new EmailAddressVerification.Builder(EMAIL)
        .withNumberOfEmailsSent(2)
        .build();
    var updatedVersionedItem = new VersionedItem<>(0, updatedItem);
    verify(registrationDAO).updateItem(updatedVersionedItem);

    var expectedMessageId = EmailMessageId.generate(EMAIL, existingItem.getTimeToLive(), 2);
    var expectedEmail = new Email.Builder(expectedMessageId, REGISTRATION_EMAIL_ADDRESS, EMAIL)
        .withSenderName(APP_NAME)
        .withSubject(RegistrationServiceImpl.VERIFICATION_EMAIL_SUBJECT)
        .withHtmlBody(generateEmailBody())
        .withTextBody(generateEmailBody())
        .build();
    verify(emailClient).sendEmail(expectedEmail);

    verify(secureGenerator, never()).generate128BitKey();
  }

  @Test
  public void sendVerificationCodeThrowsIfMaximumNumberOfEmailSent() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withNumberOfEmailsSent(10)
        .withTimeToLive(TS_2)
        .build();
    var existingVersionedItem = new VersionedItem<>(1, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    assertThatThrownBy(() -> registrationService.sendVerificationCode(EMAIL, TS_1))
        .isInstanceOf(InvalidOperationException.class)
        .hasMessageContaining(String
            .format("Maximum number of resend requests reached for email %s",
                EMAIL.getValue()))
        .hasFieldOrPropertyWithValue("data", new TimeRemaining(10L));

    verify(secureGenerator, never()).generateUUID();
    verify(registrationDAO, never()).updateItem(any());
    verify(emailClient, never()).sendEmail(any());
  }

  @Test
  public void sendVerificationCodeSetsTimeRemainingToZeroIfTtlNotExecutedYet() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withNumberOfEmailsSent(10)
        .withTimeToLive(TS_1)
        .build();
    var existingVersionedItem = new VersionedItem<>(1, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    assertThatThrownBy(() -> registrationService.sendVerificationCode(EMAIL, TS_2))
        .isInstanceOf(InvalidOperationException.class)
        .hasMessageContaining(String
            .format("Maximum number of resend requests reached for email %s",
                EMAIL.getValue()))
        .hasFieldOrPropertyWithValue("data", new TimeRemaining(0L));

    verify(secureGenerator, never()).generateUUID();
    verify(registrationDAO, never()).updateItem(any());
    verify(emailClient, never()).sendEmail(any());
  }

  @Test
  public void verifyCodeIfUserExists() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .build();
    var existingVersionedItem = new VersionedItem<>(1, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    when(userService.getUserId(EMAIL)).thenReturn(UUID);
    when(secureGenerator.generate256BitKey()).thenReturn(UUID_2);

    var updatedUser = new User.Builder(UUID)
        .withSessionId(UUID_2)
        .build();
    when(userService.updateSessionId(UUID, UUID_2, TS_1)).thenReturn(updatedUser);

    var result = registrationService.verifyCode(EMAIL, CODE, TS_1);

    assertThat(result.getUserId()).isEqualTo(UUID);
    assertThat(result.getSessionId()).isEqualTo(UUID_2);

    var updatedItem = new EmailAddressVerification.Builder(EMAIL)
        .withLastVerifiedTimestamp(TS_1)
        .build();
    var updatedVersionedItem = new VersionedItem<>(1, updatedItem);
    verify(registrationDAO).updateItem(updatedVersionedItem);

    verify(secureGenerator, never()).generateUUID();
    verify(secureGenerator, never()).generateMAC(anyList());
  }

  @Test
  public void verifyCodeIfUserDoesNotExist() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .build();
    var existingVersionedItem = new VersionedItem<>(1, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    when(secureGenerator
        .generateMAC(List.of(EMAIL.getValue(), UUID, Long.toString(TS_1.getMillis()))))
        .thenReturn(MAC);

    var result = registrationService.verifyCode(EMAIL, CODE, TS_1);

    assertThat(result.getUserId()).isEqualTo(UUID);
    assertThat(result.getMac()).isEqualTo(MAC);
    assertThat(result.getMacCreationTs()).isEqualTo(TS_1.getMillis());

    var updatedItem = new EmailAddressVerification.Builder(EMAIL)
        .withLastVerifiedTimestamp(TS_1)
        .build();
    var updatedVersionedItem = new VersionedItem<>(1, updatedItem);
    verify(registrationDAO).updateItem(updatedVersionedItem);

    verify(secureGenerator, never()).generate256BitKey();
    verify(userService, never()).updateSessionId(any(), any(), any());
  }

  @Test
  public void verifyCodeThrowsIfVerificationItemDoesNotExist() throws Exception {
    when(registrationDAO.readItem(EMAIL)).thenReturn(null);

    assertThatThrownBy(() -> registrationService.verifyCode(EMAIL, CODE, TS_1))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(
            String.format("No registration item found for email address %s", EMAIL.getValue()));

    verify(userService, never()).getUserId(any(EmailAddress.class));
    verify(secureGenerator, never()).generate256BitKey();
    verify(userService, never()).updateSessionId(any(), any(), any());
    verify(secureGenerator, never()).generateUUID();
    verify(secureGenerator, never()).generateMAC(anyList());
    verify(registrationDAO, never()).updateItem(any());
  }

  @Test
  public void verifyCodeThrowsIfCodeMismatch() throws Exception {
    var existingItem = new EmailAddressVerification.Builder(EMAIL)
        .withCode(CODE)
        .build();
    var existingVersionedItem = new VersionedItem<>(1, existingItem);
    when(registrationDAO.readItem(EMAIL)).thenReturn(existingVersionedItem);

    assertThatThrownBy(() -> registrationService.verifyCode(EMAIL, "1-1-1", TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Invalid verification code");

    verify(userService, never()).getUserId(any(EmailAddress.class));
    verify(secureGenerator, never()).generate256BitKey();
    verify(userService, never()).updateSessionId(any(), any(), any());
    verify(secureGenerator, never()).generateUUID();
    verify(secureGenerator, never()).generateMAC(anyList());
    verify(registrationDAO, never()).updateItem(any());
  }

  @Test
  public void registerUser() throws Exception {
    when(secureGenerator
        .generateMAC(List.of(EMAIL.getValue(), UUID, Long.toString(TS_2.getMillis()))))
        .thenReturn(MAC);

    var user = new User.Builder(UUID).build();
    when(userService.createUser(UUID, USER_NAME, NAME, EMAIL, FILE, TS_1)).thenReturn(user);

    var result = registrationService
        .registerUser(EMAIL, UUID, MAC, TS_2.getMillis(), USER_NAME, NAME, FILE, TS_1);

    assertThat(result).isEqualTo(user);
  }

  @Test
  public void registerUserThrowsIfMacMismatch() throws Exception {
    when(secureGenerator.generateMAC(List.of(EMAIL.getValue(), UUID))).thenReturn(MAC);

    assertThatThrownBy(() -> registrationService
        .registerUser(EMAIL, UUID, "impostor", TS_2.getMillis(), USER_NAME, NAME, FILE,
            TS_1))
        .isInstanceOf(UnauthorisedException.class)
        .hasMessageContaining("Provided mac does not match expected mac");

    verify(userService, never()).createUser(any(), any(), any(), any(), any(), any());
  }

  @Test
  public void registerUserThrowsIfMacExpired() throws Exception {
    when(secureGenerator.generateMAC(List.of(EMAIL.getValue(), UUID, Long.toString(1L))))
        .thenReturn(MAC);

    assertThatThrownBy(
        () -> registrationService
            .registerUser(EMAIL, UUID, MAC, 1L, USER_NAME, NAME, FILE, TS_1))
        .isInstanceOf(InvalidOperationException.class)
        .hasMessageContaining("Mac has expired");

    verify(userService, never()).createUser(any(), any(), any(), any(), any(), any());
  }
  
  private String generateEmailBody() {
    var url = String.format("https://%s/%s", API_URL_VERIFICATION, CODE);

    return EMAIL_MESSAGE_FORMAT.replaceAll("%link", url);
  }
}
