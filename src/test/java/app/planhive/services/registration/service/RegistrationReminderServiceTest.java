package app.planhive.services.registration.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.model.EmailAddress;
import app.planhive.notification.EmailClient;
import app.planhive.notification.model.Email;
import app.planhive.persistence.model.VersionedItem;
import app.planhive.services.registration.common.EmailMessageId;
import app.planhive.services.registration.model.EmailAddressVerification;
import app.planhive.services.registration.persistence.RegistrationDAO;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class RegistrationReminderServiceTest {

  private static final String CODE = "123";
  private static final EmailAddress EMAIL_1 = new EmailAddress("admin@planhive.app");
  private static final EmailAddress EMAIL_2 = new EmailAddress("user@planhive.app");
  private static final Instant TS_1 = new Instant(1);
  private static final Instant TS_2 = new Instant(2);

  private static final String APP_NAME = "PlanHive";
  private static final EmailAddress SUPPORT_EMAIL_ADDRESS = new EmailAddress(
      "support@planhive.app");
  private static final String EMAIL_TEXT = "Hello";

  private RegistrationDAO registrationDAO;
  private EmailClient emailClient;
  private RegistrationReminderService registrationReminderService;

  @BeforeEach
  public void setUp() {
    registrationDAO = mock(RegistrationDAO.class);
    emailClient = mock(EmailClient.class);

    registrationReminderService = new RegistrationReminderService(registrationDAO, emailClient,
        APP_NAME, SUPPORT_EMAIL_ADDRESS, EMAIL_TEXT);
  }

  @Test
  public void sendReminderEmails() throws Exception {
    var existingItem1 = new EmailAddressVerification.Builder(EMAIL_1)
        .withCode(CODE)
        .withTimeToLive(TS_1)
        .build();
    var existingVersionedItem1 = new VersionedItem<>(0, existingItem1);

    var existingItem2 = new EmailAddressVerification.Builder(EMAIL_2)
        .withCode(CODE)
        .withTimeToLive(TS_2)
        .build();
    var existingVersionedItem2 = new VersionedItem<>(0, existingItem2);

    when(registrationDAO.getNonVerifiedItems(any()))
        .thenReturn(List.of(existingVersionedItem1, existingVersionedItem2));

    registrationReminderService.sendReminderEmails();

    var expectedMessageId1 = EmailMessageId.generate(EMAIL_1, TS_1);
    var expectedEmail1 = new Email.Builder(expectedMessageId1, SUPPORT_EMAIL_ADDRESS, EMAIL_1)
        .withSenderName(APP_NAME)
        .withSubject("Reminder: Verify your email address")
        .withTextBody(EMAIL_TEXT)
        .build();
    verify(emailClient).sendEmail(expectedEmail1);

    var expectedMessageId2 = EmailMessageId.generate(EMAIL_2, TS_2);
    var expectedEmail2 = new Email.Builder(expectedMessageId2, SUPPORT_EMAIL_ADDRESS, EMAIL_2)
        .withSenderName(APP_NAME)
        .withSubject("Reminder: Verify your email address")
        .withTextBody(EMAIL_TEXT)
        .build();
    verify(emailClient).sendEmail(expectedEmail2);

    var updatedItem1 = new EmailAddressVerification.Builder(EMAIL_1)
        .setReminderSent(true)
        .build();
    var updatedVersionedItem1 = new VersionedItem<>(0, updatedItem1);
    verify(registrationDAO).updateItem(updatedVersionedItem1);

    var updatedItem2 = new EmailAddressVerification.Builder(EMAIL_2)
        .setReminderSent(true)
        .build();
    var updatedVersionedItem2 = new VersionedItem<>(0, updatedItem2);
    verify(registrationDAO).updateItem(updatedVersionedItem2);
  }
}
