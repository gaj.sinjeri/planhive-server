package app.planhive.services.photo.notification;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.util.List;
import java.util.Map;

@TestInstance(Lifecycle.PER_CLASS)
public class PhotoNotificationSenderImplTest {

  private static final User USER = new User.Builder("123").withName("Roger").build();
  private static final NodeId EVENT_NODE_ID = new NodeId("123", NodeType.EVENT);
  private static final String EVENT_NAME = "Las Vegas";
  private static final NodeId PHOTOS_NODE_ID = new NodeId("234", "123",
      NodeType.PHOTOS);

  private NotificationService notificationService;
  private PhotoNotificationSender photoNotificationSender;

  @BeforeEach
  public void setUp() {
    notificationService = mock(NotificationService.class);

    photoNotificationSender = new PhotoNotificationSenderImpl(notificationService);
  }

  @Test
  public void sendNewPhotosNotification() {
    photoNotificationSender.sendNewPhotosNotification(USER, EVENT_NODE_ID, EVENT_NAME,
        PHOTOS_NODE_ID);

    var expectedNodeIds = String.format("[{\"id\":\"%s\",\"type\":\"%s\"}"
            + ",{\"id\":\"%s\",\"parentId\":\"%s\",\"type\":\"%s\"}]", "123", NodeType.EVENT.name(),
        "234", "123", NodeType.PHOTOS.name());
    var expectedNotification = new Notification.Builder(EVENT_NAME,
        "Roger has uploaded new photos to the event album!", NotificationCategory.ALBUMS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(), expectedNodeIds))
        .build();
    verify(notificationService)
        .sendNotificationByNodeId(expectedNotification, PHOTOS_NODE_ID, List.of(USER.getId()),
            true);
  }
}
