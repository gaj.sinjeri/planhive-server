package app.planhive.services.photo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.user.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class PhotoControllerTest {

  private static final String EVENT_NODE_ID = "123";
  private static final String PHOTOS_NODE_ID = "234";
  private static final String PHOTO_NODE_ID = "345";
  private static final User USER = new User.Builder("v34v-43-gj03g").build();
  private static final byte[] BYTES = new byte[]{1};
  private static final MultipartFile FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg",
      BYTES);
  private static final Node NODE = new Node.Builder(new NodeId("1", NodeType.PHOTO)).build();

  private PhotoService photoService;
  private PhotoController photoController;

  @BeforeEach
  public void setUp() {
    photoService = mock(PhotoService.class);

    photoController = new PhotoController(photoService);
  }

  @Test
  public void addPhotoSuccessfulCall() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId).build();

    when(photoService.addPhoto(eq(photosNodeId), eq(USER), eq(FILE), any()))
        .thenReturn(List.of(photoNode));

    var response = photoController.addPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.successResponse(List.of(photoNode)));
  }

  @Test
  public void addPhotoReturnsErrorIfInvalidImage() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new InvalidImageException("")).when(photoService)
        .addPhoto(eq(photosNodeId), eq(USER), eq(FILE), any());

    var response = photoController.addPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addPhotoReturnsErrorIfResourceDoesNotExist() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new ResourceNotFoundException("")).when(photoService)
        .addPhoto(eq(photosNodeId), eq(USER), eq(FILE), any());

    var response = photoController.addPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addPhotoReturnsErrorIfResourceDeleted() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new ResourceDeletedException("", NODE)).when(photoService)
        .addPhoto(eq(photosNodeId), eq(USER), eq(FILE), any());

    var response = photoController.addPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void addPhotoReturnsErrorIfUnauthorized() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new UnauthorisedException("")).when(photoService)
        .addPhoto(eq(photosNodeId), eq(USER), eq(FILE), any());

    var response = photoController.addPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, FILE);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void sendPhotoUploadNotificationSuccessfulCall() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);

    var response = photoController.sendPhotoUploadNotification(USER, EVENT_NODE_ID, PHOTOS_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());

    verify(photoService).sendNewPhotosNotification(USER, photosNodeId);
  }

  @Test
  public void sendPhotoUploadNotificationReturnsErrorIfResourceDoesNotExist() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new ResourceNotFoundException("")).when(photoService)
        .sendNewPhotosNotification(USER, photosNodeId);

    var response = photoController.sendPhotoUploadNotification(USER, EVENT_NODE_ID, PHOTOS_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void sendPhotoUploadNotificationReturnsErrorIfResourceDeleted() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    doThrow(new ResourceDeletedException("", NODE)).when(photoService)
        .sendNewPhotosNotification(USER, photosNodeId);

    var response = photoController.sendPhotoUploadNotification(USER, EVENT_NODE_ID, PHOTOS_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void getPhotoSuccessfulCall() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);

    when(photoService.retrievePhoto(photosNodeId, photoNodeId, USER)).thenReturn(BYTES);

    var response = photoController.getPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getPhotoReturnsErrorIfResourceDoesNotExist() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceNotFoundException("")).when(photoService)
        .retrievePhoto(photosNodeId, photoNodeId, USER);

    var response = photoController.getPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getPhotoReturnsErrorIfResourceDeleted() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceDeletedException("", NODE)).when(photoService)
        .retrievePhoto(photosNodeId, photoNodeId, USER);

    var response = photoController.getPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getPhotoReturnsErrorIfUnauthorized() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new UnauthorisedException("")).when(photoService)
        .retrievePhoto(photosNodeId, photoNodeId, USER);

    var response = photoController.getPhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getThumbnailSuccessfulCall() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);

    when(photoService.retrieveThumbnail(photosNodeId, photoNodeId, USER)).thenReturn(BYTES);

    var response = photoController.getThumbnail(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(FILE.getBytes());
  }

  @Test
  public void getThumbnailReturnsErrorIfResourceDoesNotExist() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceNotFoundException("")).when(photoService)
        .retrieveThumbnail(photosNodeId, photoNodeId, USER);

    var response = photoController.getThumbnail(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getThumbnailReturnsErrorIfResourceDeleted() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceDeletedException("", NODE)).when(photoService)
        .retrieveThumbnail(photosNodeId, photoNodeId, USER);

    var response = photoController.getThumbnail(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void getThumbnailReturnsErrorIfUnauthorized() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new UnauthorisedException("")).when(photoService)
        .retrieveThumbnail(photosNodeId, photoNodeId, USER);

    var response = photoController.getThumbnail(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(new byte[0]);
  }

  @Test
  public void deletePhotoSuccessfulCall() throws Exception {
    var response = photoController.deletePhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    verify(photoService).deletePhoto(eq(photosNodeId), eq(photoNodeId), eq(USER), any());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deletePhotoReturnsErrorIfResourceDoesNotExist() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceNotFoundException("")).when(photoService)
        .deletePhoto(eq(photosNodeId), eq(photoNodeId), eq(USER), any());

    var response = photoController.deletePhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deletePhotoReturnsErrorIfResourceDeleted() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new ResourceDeletedException("", NODE)).when(photoService)
        .deletePhoto(eq(photosNodeId), eq(photoNodeId), eq(USER), any());

    var response = photoController.deletePhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }

  @Test
  public void deletePhotoReturnsErrorIfUnauthorized() throws Exception {
    var photosNodeId = new NodeId(PHOTOS_NODE_ID, EVENT_NODE_ID, NodeType.PHOTOS);
    var photoNodeId = new NodeId(PHOTO_NODE_ID, PHOTOS_NODE_ID, NodeType.PHOTO);
    doThrow(new UnauthorisedException("")).when(photoService)
        .deletePhoto(eq(photosNodeId), eq(photoNodeId), eq(USER), any());

    var response = photoController.deletePhoto(USER, EVENT_NODE_ID, PHOTOS_NODE_ID, PHOTO_NODE_ID);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    assertThat(response.getBody()).isEqualTo(RestResponse.emptyResponse());
  }
}
