package app.planhive.services.photo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.notification.PhotoNotificationSender;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;

@TestInstance(Lifecycle.PER_CLASS)
public class PhotoServiceImplTest {

  private static final String UUID = "1234";
  private static final String NODE_ID = "ksnrgoirw";
  private static final String PARENT_ID = "v34v-43-gj03g";
  private static final String PERMISSION_GROUP_ID = "adf34f42-fwefed";
  private static final String USER_ID = "123";
  private static final User USER = new User.Builder(USER_ID).build();
  private static final byte[] BYTES = new byte[]{1};
  private static final Instant TS = Instant.now();

  private NodeService nodeService;
  private FeedService feedService;
  private PermissionService permissionService;
  private CloudStore cloudStore;
  private PhotoNotificationSender photoNotificationSender;
  private PhotoService photoService;

  private InputStream imageInputStream;
  private MultipartFile FILE;

  @BeforeEach
  public void setUp() throws Exception {
    var secureGenerator = mock(SecureGenerator.class);
    when(secureGenerator.generateUUID()).thenReturn(UUID);

    nodeService = mock(NodeService.class);
    feedService = mock(FeedService.class);
    permissionService = mock(PermissionService.class);
    cloudStore = mock(CloudStore.class);
    photoNotificationSender = mock(PhotoNotificationSender.class);

    photoService = new PhotoServiceImpl(secureGenerator, nodeService, feedService,
        permissionService, cloudStore, photoNotificationSender, 2048, 512);

    imageInputStream = generateImageInputStream(2, 1);
    FILE = new MockMultipartFile("myFile", "myFile", "image/jpeg", imageInputStream);
  }

  @Test
  public void addPhotosWidgetStoresWidget() throws Exception {
    var result = photoService
        .addPhotosWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(), TS);

    var expectedNodeId = new NodeId(UUID, PARENT_ID, NodeType.PHOTOS);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .build();
    assertThat(result).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);
    verify(feedService).createFeeds(expectedNodeId.toFeedItemKey(), List.of(USER_ID),
        NodeType.PHOTOS.name(), TS);
  }

  @Test
  public void addPhotosWidgetRethrowsIfWidgetAlreadyExists() throws Exception {
    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(() -> photoService
        .addPhotosWidget(PARENT_ID, PERMISSION_GROUP_ID, USER_ID, List.of(USER_ID), TS))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void addPhoto() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var updatedPhotosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withLastUpdateTs(TS)
        .build();
    when(nodeService.refreshLastUpdateTs(photosNodeId, TS)).thenReturn(updatedPhotosNode);

    var result = photoService.addPhoto(photosNodeId, USER, FILE, TS);

    assertThat(result.get(0)).isEqualTo(updatedPhotosNode);

    var expectedNodeId = new NodeId(UUID, NODE_ID, NodeType.PHOTO);
    var expectedNode = new Node.Builder(expectedNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .withCreationTs(TS)
        .withLastUpdateTs(TS)
        .build();
    assertThat(result.get(1)).isEqualTo(expectedNode);

    verify(nodeService).insertNode(expectedNode);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
    verify(feedService).refreshFeedsForResource(photosNodeId.toFeedItemKey(), TS);

    var captor = ArgumentCaptor.forClass(BufferedImage.class);
    verify(cloudStore)
        .storeImage(eq(String.format("events/%s/photos/%s/%s.jpeg", PARENT_ID, NODE_ID, UUID)),
            captor.capture());

    var capturedImage = captor.getValue();

    imageInputStream.reset();
    var expectedImage = ImageIO.read(imageInputStream);

    for (int y = 0; y < capturedImage.getHeight(); y++) {
      for (int x = 0; x < capturedImage.getWidth(); x++) {
        assertThat(capturedImage.getRGB(x, y)).isEqualTo(expectedImage.getRGB(x, y));
      }
    }

    // TODO(#208): Figure out a way to test for thumbnail
    verify(cloudStore)
        .storeImage(eq(String.format("events/%s/photos/%s/t-%s.jpeg", PARENT_ID, NODE_ID, UUID)),
            any());
  }

  @Test
  public void addPhotoRethrowsIfPhotoInaccessible() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var fileMock = mock(MultipartFile.class);
    when(fileMock.getContentType()).thenReturn("image/jpeg");
    doThrow(new IOException("")).when(fileMock).getInputStream();

    assertThatThrownBy(() -> photoService.addPhoto(photosNodeId, USER, fileMock, TS))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Error handling image. This could be either due to not being "
            + "able to access the input stream of the original image or create thumbnail");
  }

  @Test
  public void addPhotoThrowsIfFileFormatIncorrect() {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);

    var fileMock = new MockMultipartFile("myFile", "myFile", "x", new byte[1]);

    assertThatThrownBy(() -> photoService.addPhoto(photosNodeId, USER, fileMock, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Image must be in jpeg format");
  }

  @Test
  public void addPhotoThrowsIfResolutionTooHigh() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        generateImageInputStream(2049, 2049));

    assertThatThrownBy(() -> photoService.addPhoto(photosNodeId, USER, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("Provided image exceeds maximum allowed resolution");
  }

  @Test
  public void addPhotoThrowsIfFormatNotRecognized() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var file = new MockMultipartFile("myFile", "myFile", "image/jpeg",
        new ByteArrayInputStream(new byte[1024 * 1024]));

    assertThatThrownBy(() -> photoService.addPhoto(photosNodeId, USER, file, TS))
        .isInstanceOf(InvalidImageException.class)
        .hasMessageContaining("The provided image wasn't saved in a supported format");
  }

  @Test
  public void addPhotoRethrowsIfPhotoAlreadyExists() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    doThrow(new ResourceAlreadyExistsException("")).when(nodeService).insertNode(any());

    assertThatThrownBy(() -> photoService.addPhoto(photosNodeId, USER, FILE, TS))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void sendNewPhotosNotification() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);

    var eventNodeId = new NodeId(PARENT_ID, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withData(new EventData(EventState.ACTIVE, "Huh", "Heh", 0L, null).toJson())
        .build();
    when(nodeService.getNodeOrThrow(eventNodeId)).thenReturn(eventNode);

    photoService.sendNewPhotosNotification(USER, photosNodeId);

    verify(photoNotificationSender)
        .sendNewPhotosNotification(USER, eventNodeId, "Huh", photosNodeId);
  }

  @Test
  public void retrievePhoto() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var photoNodeId = new NodeId(UUID, NODE_ID, NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photoNodeId)).thenReturn(photoNode);

    when(cloudStore.getFile(String.format("events/%s/photos/%s/%s.jpeg", PARENT_ID, NODE_ID, UUID)))
        .thenReturn(BYTES);

    var result = photoService.retrievePhoto(photosNodeId, photoNodeId, USER);

    assertThat(result).isEqualTo(BYTES);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
  }

  @Test
  public void retrieveThumbnail() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTOS);
    var photosNode = new Node.Builder(photosNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photosNodeId)).thenReturn(photosNode);

    var photoNodeId = new NodeId(UUID, NODE_ID, NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photoNodeId)).thenReturn(photoNode);

    when(
        cloudStore
            .getFile(String.format("events/%s/photos/%s/t-%s.jpeg", PARENT_ID, NODE_ID, UUID)))
        .thenReturn(BYTES);

    var result = photoService.retrieveThumbnail(photosNodeId, photoNodeId, USER);

    assertThat(result).isEqualTo(BYTES);

    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.READ);
  }

  @Test
  public void deletePhotoIfCallerIsCreator() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTO);

    var photoNodeId = new NodeId(UUID, NODE_ID, NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId)
        .withCreatorId(USER_ID)
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photoNodeId)).thenReturn(photoNode);

    photoService.deletePhoto(photosNodeId, photoNodeId, USER, TS);

    verify(nodeService).getNodeOrThrow(photosNodeId);
    verify(permissionService, never()).verifyAccessRight(any(), any(), any());
    verify(nodeService).softDeleteNode(photoNodeId, TS);
    verify(feedService).refreshFeedsForResource(photosNodeId.toFeedItemKey(), TS);
  }

  @Test
  public void deletePhotoIfCallerIsNotCreator() throws Exception {
    var photosNodeId = new NodeId(NODE_ID, PARENT_ID, NodeType.PHOTO);

    var photoNodeId = new NodeId(UUID, NODE_ID, NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId)
        .withCreatorId("something-something-something")
        .withPermissionGroupId(PERMISSION_GROUP_ID)
        .build();
    when(nodeService.getNodeOrThrow(photoNodeId)).thenReturn(photoNode);

    photoService.deletePhoto(photosNodeId, photoNodeId, USER, TS);

    verify(nodeService).getNodeOrThrow(photosNodeId);
    verify(permissionService).verifyAccessRight(USER_ID, PERMISSION_GROUP_ID, AccessRight.DELETE);
    verify(nodeService).softDeleteNode(photoNodeId, TS);
    verify(feedService).refreshFeedsForResource(photosNodeId.toFeedItemKey(), TS);
  }

  private InputStream generateImageInputStream(int width, int height) throws Exception {
    var image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    var outputStream = new ByteArrayOutputStream();
    ImageIO.write(image, "jpeg", outputStream);

    return new ByteArrayInputStream(outputStream.toByteArray());
  }
}
