package app.planhive.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class EmailAddressTest {

  private static final String EMAIL_1 = "admin@planhive.app";
  private static final String EMAIL_2 = "user@planhive.app";

  @Test
  public void constructor() {
    var obj = new EmailAddress(EMAIL_1);

    assertThat(obj.getValue()).isEqualTo(EMAIL_1);
  }

  @Test
  public void constructorTrimsEmailAddress() {
    var obj = new EmailAddress(String.format("       %s       ", EMAIL_1));

    assertThat(obj.getValue()).isEqualTo(EMAIL_1);
  }

  @Test
  public void constructorChangesEmailAddressToLowerCase() {
    var obj = new EmailAddress(EMAIL_1.toUpperCase());

    assertThat(obj.getValue()).isEqualTo(EMAIL_1);
  }

  @Test
  public void constructorThrowsIfEmailAddressNull() {
    assertThatThrownBy(() -> new EmailAddress(null)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfEmailAddressInvalid() {
    assertThatThrownBy(() -> new EmailAddress("invalid@email"))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("Incorrect email address format for invalid@email");
  }

  @Test
  public void equality() {
    var obj1 = new EmailAddress(EMAIL_1);
    var obj2 = new EmailAddress(EMAIL_1);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new EmailAddress(EMAIL_1);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new EmailAddress(EMAIL_1);
    var obj2 = new EmailAddress(EMAIL_2);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
