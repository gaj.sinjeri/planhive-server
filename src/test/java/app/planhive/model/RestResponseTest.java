package app.planhive.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

import java.util.Map;

public class RestResponseTest {

  private static final String SUCCESS_MESSAGE = "Welcome to the world of widgets";
  private static final ErrorCode CODE = ErrorCode.MAC_EXPIRED;
  private static final Map<String, Object> DATA = Map.of("attemptsRemaining", "1");

  @Test
  public void emptyResponseConstructsObjectWithNullFields() {
    var response = RestResponse.emptyResponse();

    assertThat(response.getError()).isNull();
    assertThat(response.getResult()).isNull();
  }

  @Test
  public void successResponseOnlyPopulatesResultField() {
    var response = RestResponse.successResponse(SUCCESS_MESSAGE);

    assertThat(response.getResult()).isEqualTo(SUCCESS_MESSAGE);
    assertThat(response.getError()).isNull();
  }

  @Test
  public void successResponseThrowsIfResultIsNull() {
    assertThatThrownBy(() -> RestResponse.successResponse(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void errorResponseOnlyPopulatesErrorField() {
    var response = RestResponse.errorResponse(CODE);

    assertThat(response.getResult()).isNull();
    assertThat(response.getError().getCode()).isEqualTo(CODE.getValue());
    assertThat(response.getError().getData()).isNull();
  }

  @Test
  public void errorResponseOnlyPopulatesErrorFieldWithData() {
    var response = RestResponse.errorResponse(CODE, DATA);

    assertThat(response.getResult()).isNull();
    assertThat(response.getError().getCode()).isEqualTo(CODE.getValue());
    assertThat(response.getError().getData()).isEqualTo(DATA);
  }

  @Test
  public void equalitySuccessResponse() {
    var response1 = RestResponse.successResponse(SUCCESS_MESSAGE);
    var response2 = RestResponse.successResponse(SUCCESS_MESSAGE);

    assertThat(response1).isEqualTo(response2);
  }

  @Test
  public void equalitySuccessResponseSelf() {
    var response = RestResponse.successResponse(SUCCESS_MESSAGE);

    assertThat(response).isEqualTo(response);
  }

  @Test
  public void inequalitySuccessResponse() {
    var response1 = RestResponse.successResponse(SUCCESS_MESSAGE);
    var response2 = RestResponse.successResponse("Shalalalala");

    assertThat(response1).isNotEqualTo(response2);
  }

  @Test
  public void equalityErrorResponse() {
    var response1 = RestResponse.errorResponse(CODE, DATA);
    var response2 = RestResponse.errorResponse(CODE, DATA);

    assertThat(response1).isEqualTo(response2);
  }

  @Test
  public void equalityErrorResponseSelf() {
    var response = RestResponse.errorResponse(CODE, DATA);

    assertThat(response).isEqualTo(response);
  }

  @Test
  public void inequalityErrorResponse() {
    var response1 = RestResponse.errorResponse(CODE, DATA);
    var response2 = RestResponse.errorResponse(CODE);

    assertThat(response1).isNotEqualTo(response2);
  }
}
