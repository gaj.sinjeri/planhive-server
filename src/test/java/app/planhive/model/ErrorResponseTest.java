package app.planhive.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

import java.util.Map;

public class ErrorResponseTest {

  private static final ErrorCode CODE = ErrorCode.MAC_EXPIRED;
  private static final Map<String, Object> DATA = Map.of("attemptsRemaining", "1");

  @Test
  public void constructor() {
    var obj = new ErrorResponse(CODE);

    assertThat(obj.getCode()).isEqualTo(CODE.getValue());
    assertThat(obj.getData()).isNull();
  }

  @Test
  public void constructorWithData() {
    var obj = new ErrorResponse(CODE, DATA);

    assertThat(obj.getCode()).isEqualTo(CODE.getValue());
    assertThat(obj.getData()).isEqualTo(DATA);
  }

  @Test
  public void constructorThrowsIfCodeNull() {
    assertThatThrownBy(() -> new ErrorResponse(null)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new ErrorResponse(CODE, DATA);
    var obj2 = new ErrorResponse(CODE, DATA);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new ErrorResponse(CODE, DATA);

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new ErrorResponse(CODE, DATA);
    var obj2 = new ErrorResponse(ErrorCode.USERNAME_TAKEN, DATA);

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
