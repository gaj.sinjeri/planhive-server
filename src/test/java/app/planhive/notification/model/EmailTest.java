package app.planhive.notification.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.model.EmailAddress;

import org.junit.jupiter.api.Test;

public class EmailTest {

  private static final String MESSAGE_ID = "abcxyz";
  private static final EmailAddress EMAIL_1 = new EmailAddress("admin@planhive.app");
  private static final EmailAddress EMAIL_2 = new EmailAddress("user@planhive.app");
  private static final String SENDER_NAME = "PlanHive";
  private static final String SUBJECT = "Verification";
  private static final String HTML_BODY = "Verify!";
  private static final String TEXT_BODY = "Me!";

  @Test
  public void constructor() {
    var obj = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody(TEXT_BODY)
        .build();

    assertThat(obj.getMessageId()).isEqualTo(MESSAGE_ID);
    assertThat(obj.getSourceAddress()).isEqualTo(EMAIL_1);
    assertThat(obj.getDestinationAddress()).isEqualTo(EMAIL_2);
    assertThat(obj.getSenderName()).isEqualTo(SENDER_NAME);
    assertThat(obj.getSubject()).isEqualTo(SUBJECT);
    assertThat(obj.getHtmlBody()).isEqualTo(HTML_BODY);
    assertThat(obj.getTextBody()).isEqualTo(TEXT_BODY);
  }

  @Test
  public void constructorThrowsIfMessageIdNull() {
    assertThatThrownBy(() -> new Email.Builder(null, EMAIL_1, EMAIL_2).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfSourceAddressNull() {
    assertThatThrownBy(() -> new Email.Builder(MESSAGE_ID, null, EMAIL_2).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void constructorThrowsIfDestinationAddressNull() {
    assertThatThrownBy(() -> new Email.Builder(MESSAGE_ID, EMAIL_1, null).build())
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void equality() {
    var obj1 = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody(TEXT_BODY)
        .build();
    var obj2 = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody(TEXT_BODY)
        .build();

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody(TEXT_BODY)
        .build();

    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody(TEXT_BODY)
        .build();
    var obj2 = new Email.Builder(MESSAGE_ID, EMAIL_1, EMAIL_2)
        .withSenderName(SENDER_NAME)
        .withSubject(SUBJECT)
        .withHtmlBody(HTML_BODY)
        .withTextBody("")
        .build();

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
