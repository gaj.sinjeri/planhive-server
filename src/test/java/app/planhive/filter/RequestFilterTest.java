package app.planhive.filter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.AuthenticationFailedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.authentication.Authenticator;
import app.planhive.services.user.model.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.FilterChain;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

@TestInstance(Lifecycle.PER_CLASS)
public class RequestFilterTest {

  private static final String USER_ID = "ABC";
  private static final String SESSION_ID = "ABC";

  private MockHttpServletRequest request;

  private HttpServletResponse response;
  private ServletOutputStream outputStream;

  private FilterChain filterChain;
  private Authenticator authenticator;

  private RequestFilter requestFilter;

  @BeforeEach
  public void setUp() throws Exception {
    request = new MockHttpServletRequest();
    request.setRequestURI("/authenticated/");

    response = mock(HttpServletResponse.class);
    outputStream = mock(ServletOutputStream.class);
    when(response.getOutputStream()).thenReturn(outputStream);

    filterChain = mock(FilterChain.class);
    authenticator = mock(Authenticator.class);

    requestFilter = new RequestFilter(authenticator);
  }

  @Test
  public void doFilterCallsFilterChainForNonAuthenticatedEndpoints() throws Exception {
    request.setRequestURI("/");

    requestFilter.doFilter(request, response, filterChain);

    verify(response, never()).setHeader(any(), any());
    verify(response, never()).setStatus(anyInt());
    verify(outputStream, never()).write(any());
    verify(filterChain).doFilter(request, response);
  }

  @Test
  public void doFilterAddsUserAsRequestAttribute() throws Exception {
    var user = new User.Builder(USER_ID)
        .withSessionId(SESSION_ID)
        .build();
    when(authenticator.authenticateUser(USER_ID, SESSION_ID)).thenReturn(user);

    request.addHeader("User-Id", USER_ID);
    request.addHeader("Session-Id", SESSION_ID);

    requestFilter.doFilter(request, response, filterChain);

    assertThat(request.getAttribute("user")).isEqualTo(user);

    verify(response, never()).setHeader(any(), any());
    verify(response, never()).setStatus(anyInt());
    verify(outputStream, never()).write(any());
    verify(filterChain).doFilter(request, response);
  }

  @Test
  public void doFilterReturnsErrorIfUserIdMissingFromHeader() throws Exception {
    var expectedBytes = new ObjectMapper()
        .writeValueAsString(RestResponse.errorResponse(ErrorCode.MISSING_HEADER_USER_ID))
        .getBytes();

    requestFilter.doFilter(request, response, filterChain);

    verify(response).setHeader("Content-Type", "application/json");
    verify(response).setStatus(400);
    verify(outputStream).write(expectedBytes);
    verify(filterChain, never()).doFilter(any(), any());
  }

  @Test
  public void doFilterReturnsErrorIfSessionIdMissingFromHeader() throws Exception {
    request.addHeader("User-Id", USER_ID);
    var expectedBytes = new ObjectMapper()
        .writeValueAsString(RestResponse.errorResponse(ErrorCode.MISSING_HEADER_SESSION_ID))
        .getBytes();

    requestFilter.doFilter(request, response, filterChain);

    verify(response).setHeader("Content-Type", "application/json");
    verify(response).setStatus(400);
    verify(outputStream).write(expectedBytes);
    verify(filterChain, never()).doFilter(any(), any());
  }

  @Test
  public void doFilterReturnsErrorIfUserDoesNotExist() throws Exception {
    request.addHeader("User-Id", USER_ID);
    request.addHeader("Session-Id", SESSION_ID);
    doThrow(new ResourceNotFoundException("Kapow!")).when(authenticator)
        .authenticateUser(USER_ID, SESSION_ID);

    var expectedBytes = new ObjectMapper()
        .writeValueAsString(RestResponse.errorResponse(ErrorCode.USER_DOES_NOT_EXIST))
        .getBytes();

    requestFilter.doFilter(request, response, filterChain);

    verify(response).setHeader("Content-Type", "application/json");
    verify(response).setStatus(401);
    verify(outputStream).write(expectedBytes);
    verify(filterChain, never()).doFilter(any(), any());
  }

  @Test
  public void doFilterReturnsErrorIfAuthenticationFails() throws Exception {
    request.addHeader("User-Id", USER_ID);
    request.addHeader("Session-Id", SESSION_ID);
    doThrow(new AuthenticationFailedException("Kapow!")).when(authenticator)
        .authenticateUser(USER_ID, SESSION_ID);

    var expectedBytes = new ObjectMapper()
        .writeValueAsString(RestResponse.errorResponse(ErrorCode.INVALID_SESSION_ID))
        .getBytes();

    requestFilter.doFilter(request, response, filterChain);

    verify(response).setHeader("Content-Type", "application/json");
    verify(response).setStatus(401);
    verify(outputStream).write(expectedBytes);
    verify(filterChain, never()).doFilter(any(), any());
  }
}
