package app.planhive.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import app.planhive.security.exception.InvalidCiphertextException;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class SecureGeneratorTest {

  private static final String MAC_GEN_KEY = "X1Jn75SRRnoZIwjfhbpXBOP9ToARHka10M+Tzcb060I=";
  private static final String EMAIL_1 = "abc@planhive.app";
  private static final String EMAIL_2 = "def@planhive.app";

  @Test
  public void generateMacIsDeterministic() {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var mac1 = generator.generateMAC(EMAIL_1);
    var mac2 = generator.generateMAC(EMAIL_1);

    assertThat(mac1).isNotNull();
    assertThat(mac2).isNotNull();
    assertThat(mac1).isEqualTo(mac2);
  }

  @Test
  public void generateMacReturnsDifferentResultForDifferentInput() {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var mac1 = generator.generateMAC(EMAIL_1);
    var mac2 = generator.generateMAC("Not an email");

    assertThat(mac1).isNotNull();
    assertThat(mac2).isNotNull();
    assertThat(mac1).isNotEqualTo(mac2);
  }

  @Test
  public void generateMacFromListIsDeterministic() {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var mac1 = generator.generateMAC(List.of(EMAIL_1, EMAIL_2));
    var mac2 = generator.generateMAC(List.of(EMAIL_1, EMAIL_2));

    assertThat(mac1).isNotNull();
    assertThat(mac2).isNotNull();
    assertThat(mac1).isEqualTo(mac2);
  }

  @Test
  public void generateMacFromListReturnsDifferentResultForDifferentInput() {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var mac1 = generator.generateMAC(List.of(EMAIL_1));
    var mac2 = generator.generateMAC(List.of(EMAIL_1, EMAIL_2));

    assertThat(mac1).isNotNull();
    assertThat(mac2).isNotNull();
    assertThat(mac1).isNotEqualTo(mac2);
  }

  @Test
  public void decryptOutputEqualsEncryptInput() throws InvalidCiphertextException {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var plaintext = Base64.getUrlEncoder().withoutPadding()
        .encodeToString("This is a test string".getBytes());

    assertThat(plaintext)
        .isEqualTo(generator.decryptBase64String(generator.encryptBase64String(plaintext)));
  }

  @Test
  public void decryptThrowsForTamperedCiphertext() {
    var generator = new SecureGenerator(MAC_GEN_KEY);

    var plaintext = Base64.getUrlEncoder().withoutPadding()
        .encodeToString("This is a test string".getBytes());

    final var ciphertext = generator.encryptBase64String(plaintext) + "hello";

    assertThatThrownBy(() -> generator.decryptBase64String(ciphertext))
        .isInstanceOf(InvalidCiphertextException.class);
  }
}
