package app.planhive.threading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskRunnerSync implements TaskRunner {

  private static final Logger logger = LoggerFactory.getLogger(TaskRunnerImpl.class);

  public void run(final Runnable task) {
    try {
      task.run();
    } catch (Throwable e) {
      logger.error("Unhandled exception thrown by task", e);
    }
  }
}
