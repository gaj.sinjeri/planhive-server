package app.planhive;

import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class LocalDbCreationExtension implements BeforeAllCallback, AfterAllCallback {

  public static final String PORT = "8001";
  private DynamoDBProxyServer server;

  public LocalDbCreationExtension() {
    System.setProperty("sqlite4java.library.path", "native-libs");
  }

  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    server = ServerRunner.createServerFromCommandLineArgs(
        new String[]{"-inMemory", "-port", PORT});
    server.start();
  }

  @Override
  public void afterAll(ExtensionContext context) {
    this.stopUnchecked(server);
  }

  private void stopUnchecked(DynamoDBProxyServer dynamoDbServer) {
    try {
      dynamoDbServer.stop();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}