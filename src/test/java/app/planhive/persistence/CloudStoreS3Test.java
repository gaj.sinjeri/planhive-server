package app.planhive.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.planhive.exception.ResourceNotFoundException;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.ArgumentCaptor;

import java.awt.image.BufferedImage;
import java.io.IOException;

@TestInstance(Lifecycle.PER_CLASS)
public class CloudStoreS3Test {

  private static final String BUCKET_NAME = "app.planhive.store";

  private final String FILEPATH = "scotty/avatar.jpeg";
  private final byte[] BYTES = new byte[]{1};
  private final Instant TS_1 = new Instant(1);
  private final BufferedImage IMAGE = new BufferedImage(256, 256, BufferedImage.TYPE_INT_RGB);

  private AmazonS3 s3Client;
  private CloudStoreS3 cloudStoreS3;
  private S3ObjectInputStream s3ObjectInputStream;

  @BeforeEach
  public void setUp() throws IOException {
    s3Client = mock(AmazonS3.class);
    cloudStoreS3 = new CloudStoreS3(BUCKET_NAME, s3Client);

    var s3Object = mock(S3Object.class);

    s3ObjectInputStream = mock(S3ObjectInputStream.class);
    when(s3ObjectInputStream.readAllBytes()).thenReturn(BYTES);
    when(s3Object.getObjectContent()).thenReturn(s3ObjectInputStream);

    var objectMetadata = new ObjectMetadata();
    objectMetadata.setLastModified(TS_1.toDate());
    when(s3Client.getObjectMetadata(BUCKET_NAME, FILEPATH)).thenReturn(objectMetadata);
    when(s3Client.getObject(BUCKET_NAME, FILEPATH)).thenReturn(s3Object);
  }

  @Test
  public void storeImageCallsS3Client() {
    cloudStoreS3.storeImage(FILEPATH, IMAGE);

    var captor = ArgumentCaptor.forClass(PutObjectRequest.class);
    verify(s3Client).putObject(captor.capture());
    var args = captor.getValue();

    assertThat(args.getBucketName()).isEqualTo(BUCKET_NAME);
    assertThat(args.getKey()).isEqualTo(FILEPATH);
    assertThat(args.getMetadata().getContentType()).isEqualTo("image/jpeg");
    assertThat(args.getMetadata().getContentLength()).isEqualTo(1651L);

    // TODO(#207): Test BufferedImage is converted to InputStream
  }

  @Test
  public void storeImageThrowsIfFilePathNull() {
    assertThatThrownBy(() -> cloudStoreS3.storeImage(null, IMAGE))
        .isInstanceOf(NullPointerException.class);

    verify(s3Client, never()).putObject(any());
  }

  @Test
  public void storeImageThrowsIfInputStreamNull() {
    assertThatThrownBy(() -> cloudStoreS3.storeImage(FILEPATH, null))
        .isInstanceOf(NullPointerException.class);

    verify(s3Client, never()).putObject(any());
  }

  @Test
  public void storeImageThrowsIfS3CannotProcessRequest() {
    when(s3Client.putObject(any())).thenThrow(new AmazonServiceException(""));

    assertThatThrownBy(() -> cloudStoreS3.storeImage(FILEPATH, IMAGE))
        .isInstanceOf(InternalError.class)
        .hasMessageContaining("AWS S3 could not process the request to store jpeg image");
  }

  @Test
  public void storeImageThrowsIfS3CannotBeContacted() {
    when(s3Client.putObject(any())).thenThrow(new SdkClientException(""));

    assertThatThrownBy(() -> cloudStoreS3.storeImage(FILEPATH, IMAGE))
        .isInstanceOf(InternalError.class).hasMessageContaining(
        "AWS S3 could not be contacted or the client could not parse the response");
  }

  @Test
  public void storeImageThrowsIfRuntimeException() {
    when(s3Client.putObject(any())).thenThrow(new RuntimeException());

    assertThatThrownBy(() -> cloudStoreS3.storeImage(FILEPATH, IMAGE))
        .isInstanceOf(InternalError.class);
  }

  @Test
  public void getFileCallsS3Client() throws ResourceNotFoundException {
    var result = cloudStoreS3.getFile(FILEPATH);

    assertThat(result).isEqualTo(BYTES);
  }

  @Test
  public void getFileThrowsIfFilePathNull() {
    assertThatThrownBy(() -> cloudStoreS3.getFile(null)).isInstanceOf(NullPointerException.class);

    verify(s3Client, never()).getObject(anyString(), anyString());
  }

  @Test
  public void getFileThrowsIfObjectDoesNotExistInS3() {
    var exception = new AmazonS3Exception("");
    exception.setErrorCode("NoSuchKey");

    when(s3Client.getObject(BUCKET_NAME, FILEPATH)).thenThrow(exception);

    assertThatThrownBy(() -> cloudStoreS3.getFile(FILEPATH))
        .isInstanceOf(ResourceNotFoundException.class)
        .hasMessageContaining(String.format("No file exists in S3 at filepath %s", FILEPATH));
  }

  @Test
  public void getFileThrowsIfReadingObjectContentBytesFails() throws IOException {
    when(s3ObjectInputStream.readAllBytes()).thenThrow(new IOException());

    assertThatThrownBy(() -> cloudStoreS3.getFile(FILEPATH)).isInstanceOf(InternalError.class);
  }
}
