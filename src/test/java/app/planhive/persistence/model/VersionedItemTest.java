package app.planhive.persistence.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class VersionedItemTest {

  private static final int VERSION = 1;
  private static final String ITEM = "item";

  @Test
  public void builder() {
    var obj = new VersionedItem<>(VERSION, ITEM);

    assertThat(obj.getVersion()).isEqualTo(VERSION);
    assertThat(obj.getItem()).isEqualTo(ITEM);
  }

  @Test
  public void builderThrowsIfItemNull() {
    assertThatThrownBy(() -> new VersionedItem<>(1, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void withNewItem() {
    var obj = new VersionedItem<>(VERSION, ITEM);
    var newObj = obj.withNewItem("new-item");

    assertThat(obj != newObj);
    assertThat(newObj.getVersion()).isEqualTo(VERSION);
    assertThat(newObj.getItem()).isEqualTo("new-item");
  }

  @Test
  public void equality() {
    var obj1 = new VersionedItem<>(VERSION, ITEM);
    var obj2 = new VersionedItem<>(VERSION, ITEM);

    assertThat(obj1).isEqualTo(obj2);
  }

  @Test
  public void equalitySelf() {
    var obj = new VersionedItem<>(VERSION, ITEM);
    assertThat(obj).isEqualTo(obj);
  }

  @Test
  public void inequality() {
    var obj1 = new VersionedItem<>(VERSION, ITEM);
    var obj2 = new VersionedItem<>(VERSION, "no-item");

    assertThat(obj1).isNotEqualTo(obj2);
  }
}
