package app.planhive.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class DynamoDAOTest {

  private static final String PREFIX = "reg";
  private static final String EMAIL = "admin@planhive.app";
  private static final String EMAIL_ID = "reg|admin@planhive.app";

  private static class MockDynamoDAO extends DynamoDAO {

    MockDynamoDAO() {
      super(PREFIX);
    }
  }

  private final MockDynamoDAO mockDAO = new MockDynamoDAO();

  @Test
  public void addPrefix() {
    var result = mockDAO.addPrefix(EMAIL);

    assertThat(result).isEqualTo(EMAIL_ID);
  }

  @Test
  public void addPrefixThrowsIfValueNull() {
    assertThatThrownBy(() -> mockDAO.addPrefix(null)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void removePrefix() {
    var result = mockDAO.removePrefix(EMAIL_ID);

    assertThat(result).isEqualTo(EMAIL);
  }

  @Test
  public void removePrefixThrowsIfValueNull() {
    assertThatThrownBy(() -> mockDAO.removePrefix(null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void addPrefixExplicit() {
    var result = DynamoDAO.addPrefix(PREFIX, EMAIL);

    assertThat(result).isEqualTo(EMAIL_ID);
  }

  @Test
  public void addPrefixExplicitThrowsIfPrefixNull() {
    assertThatThrownBy(() -> DynamoDAO.addPrefix(null, EMAIL))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void addPrefixExplicitThrowsIfValueNull() {
    assertThatThrownBy(() -> DynamoDAO.addPrefix(PREFIX, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void removePrefixExplicit() {
    var result = DynamoDAO.removePrefix(PREFIX, EMAIL_ID);

    assertThat(result).isEqualTo(EMAIL);
  }

  @Test
  public void removePrefixExplicitThrowsIfPrefixNull() {
    assertThatThrownBy(() -> DynamoDAO.removePrefix(PREFIX, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void removePrefixExplicitThrowsIfValueNull() {
    assertThatThrownBy(() -> DynamoDAO.removePrefix(null, EMAIL))
        .isInstanceOf(NullPointerException.class);
  }
}
