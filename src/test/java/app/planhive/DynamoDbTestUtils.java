package app.planhive;

import app.planhive.persistence.DynamoDBClient;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.LocalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;

import java.util.ArrayList;
import java.util.List;

public class DynamoDbTestUtils {

  private AmazonDynamoDB amazonDynamoDB;
  private DynamoDB dynamoDB;
  private Table table;

  public DynamoDbTestUtils() {
    setUp();
  }

  public DynamoDbTestUtils(List<Item> testData) {
    setUp();
    populateTableWithTestData(testData);
  }

  private void setUp() {
    var awsCredentials = new BasicAWSCredentials("fakeMyKeyId", "fakeSecretAccessKey");
    amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
        .withEndpointConfiguration(
            new AwsClientBuilder
                .EndpointConfiguration(
                String.format("http://localhost:%s", LocalDbCreationExtension.PORT), "us-east-1"))
        .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
        .build();
    dynamoDB = new DynamoDB(amazonDynamoDB);

    this.table = createTable();
  }

  public AmazonDynamoDB getAmazonDynamoDB() {
    return amazonDynamoDB;
  }

  public DynamoDB getDynamoDB() {
    return dynamoDB;
  }

  public Table getTable() {
    return table;
  }

  private Table createTable() {
    var attributeDefinitions = new ArrayList<AttributeDefinition>();
    attributeDefinitions.add(
        new AttributeDefinition()
            .withAttributeName(DynamoDBClient.PRIMARY_KEY_H)
            .withAttributeType("S"));
    attributeDefinitions.add(
        new AttributeDefinition()
            .withAttributeName(DynamoDBClient.PRIMARY_KEY_R)
            .withAttributeType("S"));
    attributeDefinitions.add(
        new AttributeDefinition()
            .withAttributeName(DynamoDBClient.GSI1_H)
            .withAttributeType("S"));
    attributeDefinitions.add(
        new AttributeDefinition()
            .withAttributeName(DynamoDBClient.GSI1_R)
            .withAttributeType("N"));
    attributeDefinitions.add(
        new AttributeDefinition()
            .withAttributeName(DynamoDBClient.LSI1_R)
            .withAttributeType("S"));

    var keySchema = new ArrayList<KeySchemaElement>();
    keySchema.add(
        new KeySchemaElement()
            .withAttributeName(DynamoDBClient.PRIMARY_KEY_H)
            .withKeyType(KeyType.HASH));
    keySchema.add(
        new KeySchemaElement()
            .withAttributeName(DynamoDBClient.PRIMARY_KEY_R)
            .withKeyType(KeyType.RANGE));

    var globalSecondaryIndexes = new ArrayList<GlobalSecondaryIndex>();
    globalSecondaryIndexes.add(new GlobalSecondaryIndex()
        .withIndexName(DynamoDBClient.GSI1_INDEX)
        .withKeySchema(List.of(
            new KeySchemaElement()
                .withAttributeName(DynamoDBClient.GSI1_H)
                .withKeyType(KeyType.HASH),
            new KeySchemaElement()
                .withAttributeName(DynamoDBClient.GSI1_R)
                .withKeyType(KeyType.RANGE)))
        .withProvisionedThroughput(
            new ProvisionedThroughput()
                .withReadCapacityUnits(5L)
                .withWriteCapacityUnits(6L))
        .withProjection(
            new Projection()
                .withProjectionType(ProjectionType.ALL)));

    var localSecondaryIndexes = new ArrayList<LocalSecondaryIndex>();
    localSecondaryIndexes.add(new LocalSecondaryIndex()
        .withIndexName(DynamoDBClient.LSI1_INDEX)
        .withKeySchema(List.of(
            new KeySchemaElement()
                .withAttributeName(DynamoDBClient.PRIMARY_KEY_H)
                .withKeyType(KeyType.HASH),
            new KeySchemaElement()
                .withAttributeName(DynamoDBClient.LSI1_R)
                .withKeyType(KeyType.RANGE)))
        .withProjection(
            new Projection()
                .withProjectionType(ProjectionType.ALL)));

    var createTableRequest = new CreateTableRequest()
        .withTableName(DynamoDBClient.DB_TABLE_NAME)
        .withKeySchema(keySchema)
        .withAttributeDefinitions(attributeDefinitions)
        .withGlobalSecondaryIndexes(globalSecondaryIndexes)
        .withLocalSecondaryIndexes(localSecondaryIndexes)
        .withProvisionedThroughput(
            new ProvisionedThroughput()
                .withReadCapacityUnits(5L)
                .withWriteCapacityUnits(6L));

    var table = dynamoDB.createTable(createTableRequest);

    try {
      table.waitForActive();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    return table;
  }

  public void populateTableWithTestData(List<Item> items) {
    var tableWriteItems = new TableWriteItems(DynamoDBClient.DB_TABLE_NAME).withItemsToPut(items);

    var outcome = dynamoDB.batchWriteItem(tableWriteItems);
    while (!outcome.getUnprocessedItems().isEmpty()) {
      outcome = dynamoDB.batchWriteItemUnprocessed(outcome.getUnprocessedItems());
    }
  }

  public List<Item> batchGetItem(List<PrimaryKey> keys) {
    var tableKeysAndAttributes = new TableKeysAndAttributes(DynamoDBClient.DB_TABLE_NAME);
    for (var key : keys) {
      tableKeysAndAttributes.addPrimaryKey(key);
    }

    var outcome = dynamoDB.batchGetItem(tableKeysAndAttributes);
    return outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME);
  }
}
