package app.planhive.exception;

public enum ResourceType {
  DB_USER_ITEM,
  DB_USERNAME_ITEM,
  DB_USER_EMAIL_ITEM
}
