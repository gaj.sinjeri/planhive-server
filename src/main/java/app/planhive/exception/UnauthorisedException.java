package app.planhive.exception;

public class UnauthorisedException extends Exception {

  public UnauthorisedException(String errorMessage) {
    super(errorMessage);
  }

}
