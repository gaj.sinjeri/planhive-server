package app.planhive.exception;

public class ResourceNotFoundException extends Exception {

  private final ResourceType resourceType;

  public ResourceNotFoundException(String errorMessage) {
    this(errorMessage, null);
  }

  public ResourceNotFoundException(String errorMessage, ResourceType resourceType) {
    super(errorMessage);

    this.resourceType = resourceType;
  }

  public ResourceType getResourceType() {
    return resourceType;
  }
}
