package app.planhive.exception;

public class InvalidImageException extends Exception {

  public InvalidImageException(String errorMessage) {
    super(errorMessage);
  }

}
