package app.planhive.exception;

public class DatabaseUpdateException extends Exception {

  public DatabaseUpdateException(String errorMessage) {
    super(errorMessage);
  }

}
