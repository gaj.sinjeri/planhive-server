package app.planhive.exception;

public class ResourceDeletedException extends Exception {

  private final Object resource;

  public ResourceDeletedException(String errorMessage, Object resource) {
    super(errorMessage);
    this.resource = resource;
  }

  public Object getResource() {
    return resource;
  }
}
