package app.planhive.exception;

public class ResourceAlreadyExistsException extends Exception {

  private final ResourceType resourceType;

  public ResourceAlreadyExistsException(String errorMessage) {
    this(errorMessage, null);
  }

  public ResourceAlreadyExistsException(String errorMessage, ResourceType resourceType) {
    super(errorMessage);

    this.resourceType = resourceType;
  }

  public ResourceType getResourceType() {
    return resourceType;
  }
}
