package app.planhive.exception;

public class InvalidOperationException extends Exception {

  private final Object data;

  public InvalidOperationException(String errorMessage) {
    this(errorMessage, null);
  }

  public InvalidOperationException(String errorMessage, Object data) {
    super(errorMessage);

    this.data = data;
  }

  public Object getData() {
    return data;
  }
}
