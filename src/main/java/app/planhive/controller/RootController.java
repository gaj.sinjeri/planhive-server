package app.planhive.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

  @GetMapping(path = "/", produces = "text/plain")
  public ResponseEntity<String> root() {
    return new ResponseEntity<>("Hollo World", HttpStatus.OK);
  }
}
