package app.planhive.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class NotificationClientStub implements NotificationClient {

  private static final Logger logger = LoggerFactory.getLogger(NotificationClientStub.class);

  public NotificationClientStub() {
    logger.info("[STUB] Notification Client initialized");
  }

  @Override
  public void sendNotifications(Map<String, Integer> registrationTokensWithCount, String title,
      String body, Map<String, String> data, NotificationCategory category) {
    logger.info(String
        .format("[STUB] Sent message with title %s, body %s, data %s", title, body, data));
  }
}
