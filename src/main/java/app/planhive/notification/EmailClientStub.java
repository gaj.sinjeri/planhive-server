package app.planhive.notification;

import app.planhive.notification.model.Email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailClientStub implements EmailClient {

  private static final Logger logger = LoggerFactory.getLogger(EmailClientStub.class);

  public EmailClientStub() {
    logger.info("[STUB] Email Client initialized");
  }

  @Override
  public void sendEmail(Email email) {
    logger.info(String.format("Sent email to %s with sender name %s, subject %s, body %s",
        email.getDestinationAddress().getValue(), email.getSenderName(), email.getSenderName(),
        email.getHtmlBody()));
  }
}
