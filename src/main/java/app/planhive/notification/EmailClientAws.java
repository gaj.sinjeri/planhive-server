package app.planhive.notification;

import app.planhive.notification.model.Email;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

public class EmailClientAws implements EmailClient {

  private final AmazonSimpleEmailService client;

  private static final Logger logger = LoggerFactory.getLogger(EmailClientAws.class);

  public EmailClientAws() {
    client = AmazonSimpleEmailServiceClientBuilder.standard()
        .withRegion(Regions.US_EAST_1)
        .build();

    logger.info("[AWS SES] Email Client initialized");
  }

  @Override
  @Async
  public void sendEmail(Email email) {
    var request = new SendEmailRequest()
        .withDestination(
            new Destination()
                .withToAddresses(email.getDestinationAddress().getValue()));

    if (email.getSubject() != null || email.getHtmlBody() != null || email.getTextBody() != null) {
      request.withMessage(constructMessage(email));
    }

    request.putCustomRequestHeader("Message-ID",
        String.format("<%s@planhive.app>", email.getMessageId()));

    var sourceAddress = email.getSourceAddress().getValue();
    request.withSource(email.getSenderName() != null ? String
        .format("%s <%s>", email.getSenderName(), sourceAddress) : sourceAddress);

    client.sendEmail(request);

    logger.debug(String
        .format("Sent email to %s with subject %s", email.getDestinationAddress().getValue(),
            email.getSubject()));
  }

  private Message constructMessage(Email email) {
    var message = new Message();

    if (email.getSubject() != null) {
      message.withSubject(new Content()
          .withCharset("UTF-8").withData(email.getSubject()));
    }

    if (email.getHtmlBody() != null || email.getTextBody() != null) {
      var body = new Body();

      if (email.getHtmlBody() != null) {
        body.withHtml(new Content().withCharset("UTF-8").withData(email.getHtmlBody()));
      }

      if (email.getTextBody() != null) {
        body.withText(new Content().withCharset("UTF-8").withData(email.getTextBody()));
      }

      message.withBody(body);
    }

    return message;
  }
}
