package app.planhive.notification.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.model.EmailAddress;

import java.util.Objects;

public class Email {

  private final String messageId;
  private final EmailAddress sourceAddress;
  private final EmailAddress destinationAddress;
  private final String senderName;
  private final String subject;
  private final String htmlBody;
  private final String textBody;

  private Email(String messageId, EmailAddress sourceAddress, EmailAddress destinationAddress,
      String senderName, String subject, String htmlBody, String textBody) {
    this.messageId = messageId;
    this.sourceAddress = sourceAddress;
    this.destinationAddress = destinationAddress;
    this.senderName = senderName;
    this.subject = subject;
    this.htmlBody = htmlBody;
    this.textBody = textBody;
  }

  public static class Builder {

    private final String messageId;
    private final EmailAddress sourceAddress;
    private final EmailAddress destinationAddress;
    private String senderName;
    private String subject;
    private String htmlBody;
    private String textBody;

    public Builder(String messageId, EmailAddress sourceAddress, EmailAddress destinationAddress) {
      checkNotNull(messageId);
      checkNotNull(sourceAddress);
      checkNotNull(destinationAddress);

      this.messageId = messageId;
      this.sourceAddress = sourceAddress;
      this.destinationAddress = destinationAddress;
    }

    public Builder withSenderName(String senderName) {
      this.senderName = senderName;
      return this;
    }

    public Builder withSubject(String subject) {
      this.subject = subject;
      return this;
    }

    public Builder withHtmlBody(String htmlBody) {
      this.htmlBody = htmlBody;
      return this;
    }

    public Builder withTextBody(String textBody) {
      this.textBody = textBody;
      return this;
    }

    public Email build() {
      return new Email(messageId, sourceAddress, destinationAddress, senderName, subject, htmlBody,
          textBody);
    }
  }

  public String getMessageId() {
    return messageId;
  }

  public EmailAddress getSourceAddress() {
    return sourceAddress;
  }

  public EmailAddress getDestinationAddress() {
    return destinationAddress;
  }

  public String getSenderName() {
    return senderName;
  }

  public String getSubject() {
    return subject;
  }

  public String getHtmlBody() {
    return htmlBody;
  }

  public String getTextBody() {
    return textBody;
  }

  @Override
  public String toString() {
    return "Email{" +
        "messageId='" + messageId + '\'' +
        ", sourceAddress=" + sourceAddress +
        ", destinationAddress=" + destinationAddress +
        ", senderName='" + senderName + '\'' +
        ", subject='" + subject + '\'' +
        ", htmlBody='" + htmlBody + '\'' +
        ", textBody='" + textBody + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Email email = (Email) o;
    return Objects.equals(messageId, email.messageId) &&
        Objects.equals(sourceAddress, email.sourceAddress) &&
        Objects.equals(destinationAddress, email.destinationAddress) &&
        Objects.equals(senderName, email.senderName) &&
        Objects.equals(subject, email.subject) &&
        Objects.equals(htmlBody, email.htmlBody) &&
        Objects.equals(textBody, email.textBody);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(messageId, sourceAddress, destinationAddress, senderName, subject, htmlBody,
            textBody);
  }
}
