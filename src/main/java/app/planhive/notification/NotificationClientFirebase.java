package app.planhive.notification;

import com.amazonaws.services.s3.AmazonS3;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidConfig.Priority;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class NotificationClientFirebase implements NotificationClient {

  private static final String BUCKET_NAME = "app.planhive.top-secret";

  private final FirebaseMessaging firebaseMessaging;

  private static final Logger logger = LoggerFactory.getLogger(NotificationClientFirebase.class);

  public NotificationClientFirebase(AmazonS3 s3Client) {
    GoogleCredentials credentials;
    try (var obj = s3Client.getObject(BUCKET_NAME, "firebase/secret.json")) {
      credentials = GoogleCredentials.fromStream(obj.getObjectContent());
    } catch (IOException e) {
      throw new InternalError(e);
    }

    var options = new FirebaseOptions.Builder()
        .setCredentials(credentials)
        .build();
    FirebaseApp.initializeApp(options);

    firebaseMessaging = FirebaseMessaging.getInstance();
    logger.info("[Firebase] Notification Client initialized");
  }

  @Override
  public void sendNotifications(Map<String, Integer> registrationTokensWithCount, String title,
      String body, Map<String, String> data, NotificationCategory category) {
    logger.debug(String
        .format("Sending notification to tokens {%s} with title {%s}, body {%s}, data {%s}",
            registrationTokensWithCount.keySet(), title, body, data));

    if (registrationTokensWithCount.isEmpty()) {
      return;
    }

    var messages = new ArrayList<Message>();
    registrationTokensWithCount.forEach((token, count) -> messages.add(Message.builder()
        .setToken(token)
        .setNotification(Notification.builder()
            .setTitle(title)
            .setBody(body)
            .build())
        .putAllData(data)
        // We will keep the android configuration hardcoded until there is a use case for a
        // different configuration.
        .setAndroidConfig(AndroidConfig.builder()
            .setNotification(AndroidNotification.builder()
                .setChannelId(category.getAndroidChannel())
                .setClickAction("FLUTTER_NOTIFICATION_CLICK")
                .setSound("default")
                .build())
            .setPriority(Priority.HIGH)
            .build())
        .setApnsConfig(ApnsConfig.builder()
            .setAps(Aps.builder()
                .setBadge(count)
                .setSound("default")
                .build())
            .build())
        .build()));

    try {
      var response = firebaseMessaging.sendAll(messages);
      logger.debug(String
          .format("Notifications sent. Successful (%s), failed (%s)", response.getSuccessCount(),
              response.getFailureCount()));

      response.getResponses().forEach(r -> logger.debug(String
          .format("Success status of message with id {%s} is {%s}. Exception {%s}",
              r.getMessageId(), r.isSuccessful(), r.getException())));
    } catch (FirebaseMessagingException e) {
      throw new InternalError(e);
    }
  }
}
