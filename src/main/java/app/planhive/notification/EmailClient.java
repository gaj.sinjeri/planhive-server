package app.planhive.notification;


import app.planhive.notification.model.Email;

public interface EmailClient {

  void sendEmail(Email email);

}
