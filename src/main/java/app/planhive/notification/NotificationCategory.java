package app.planhive.notification;

public enum NotificationCategory {
  EVENT_UPDATES("event_updates"),
  ALBUMS("albums"),
  INVITES("invites"),
  CONTACTS("contacts"),
  DIRECT_CHATS("direct_chats"),
  GROUP_CHATS("group_chats"),
  EVENT_CHATS("event_chats");

  String androidChannel;

  NotificationCategory(String androidChannel) {
    this.androidChannel = androidChannel;
  }

  public String getAndroidChannel() {
    return this.androidChannel;
  }
}
