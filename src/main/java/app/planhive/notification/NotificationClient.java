package app.planhive.notification;

import java.util.Map;

public interface NotificationClient {

  void sendNotifications(Map<String, Integer> registrationTokensWithCount, String title,
      String body, Map<String, String> data, NotificationCategory category);

}
