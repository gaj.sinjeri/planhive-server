package app.planhive.notification.configuration;

import static app.planhive.common.Flags.APP_ENV_KEY;
import static app.planhive.common.Flags.DEVELOPMENT_ENV;
import static app.planhive.common.Flags.PRODUCTION_ENV;

import app.planhive.notification.EmailClient;
import app.planhive.notification.EmailClientAws;
import app.planhive.notification.EmailClientStub;
import app.planhive.notification.NotificationClient;
import app.planhive.notification.NotificationClientFirebase;
import app.planhive.notification.NotificationClientStub;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NotificationConfiguration {

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = PRODUCTION_ENV)
  public NotificationClient notificationClientFirebase(AmazonS3 amazonS3) {
    return new NotificationClientFirebase(amazonS3);
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = DEVELOPMENT_ENV)
  public NotificationClient notificationClientStub() {
    return new NotificationClientStub();
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = PRODUCTION_ENV)
  public EmailClient emailClientAws() {
    return new EmailClientAws();
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = DEVELOPMENT_ENV)
  public EmailClient emailClientStub() {
    return new EmailClientStub();
  }
}
