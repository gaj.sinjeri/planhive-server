package app.planhive.persistence;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public interface DynamoDBClient {

  String DB_TABLE_NAME = "TheHive";
  String PRIMARY_KEY_H = "H";                   // S
  String PRIMARY_KEY_R = "R";                   // S

  String GSI1_INDEX = "GSI1";
  String GSI1_H = "GSI1H";                      // S
  String GSI1_R = "GSI1R";                      // N

  String LSI1_INDEX = "LSI1";
  String LSI1_R = "LSI1R";                      // S

  String ATTRIBUTE_TTL = "Ttl";                 // N

  int MAXIMUM_BATCH_PUT_SIZE = 25;
  int MAXIMUM_BATCH_GET_SIZE = 100;

  AmazonDynamoDB getAmazonDynamoDB();

  DynamoDB getDynamoDB();

  Table getTable();
}
