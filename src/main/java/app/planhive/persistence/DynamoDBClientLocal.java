package app.planhive.persistence;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamoDBClientLocal implements DynamoDBClient {

  private static final String DB_TABLE_NAME = "TheHive";
  private static final String SERVICE_ENDPOINT = "http://localhost:8000";

  private final AmazonDynamoDB amazonDynamoDB;
  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(DynamoDBClientAws.class);

  public DynamoDBClientLocal() {
    amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
        .withEndpointConfiguration(
            new AwsClientBuilder.EndpointConfiguration(SERVICE_ENDPOINT,
                Regions.US_EAST_1.getName()))
        .build();

    dynamoDB = new DynamoDB(amazonDynamoDB);
    table = dynamoDB.getTable(DB_TABLE_NAME);

    logger.info("[Local] Dynamo DB Client initialized");
  }

  @Override
  public AmazonDynamoDB getAmazonDynamoDB() {
    return amazonDynamoDB;
  }

  @Override
  public DynamoDB getDynamoDB() {
    return dynamoDB;
  }

  @Override
  public Table getTable() {
    return table;
  }
}
