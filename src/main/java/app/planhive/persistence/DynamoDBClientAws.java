package app.planhive.persistence;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamoDBClientAws implements DynamoDBClient {

  private final AmazonDynamoDB amazonDynamoDB;
  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(DynamoDBClientAws.class);

  public DynamoDBClientAws() {
    amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
        .withRegion(Regions.US_EAST_1)
        .build();

    dynamoDB = new DynamoDB(amazonDynamoDB);
    table = dynamoDB.getTable(DB_TABLE_NAME);

    logger.info("[AWS] Dynamo DB Client initialized");
  }

  @Override
  public AmazonDynamoDB getAmazonDynamoDB() {
    return amazonDynamoDB;
  }

  @Override
  public DynamoDB getDynamoDB() {
    return dynamoDB;
  }

  @Override
  public Table getTable() {
    return table;
  }
}
