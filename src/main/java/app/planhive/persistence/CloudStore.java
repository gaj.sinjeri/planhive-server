package app.planhive.persistence;

import app.planhive.exception.ResourceNotFoundException;

import java.awt.image.BufferedImage;

public interface CloudStore {

  void storeImage(String filePath, BufferedImage image);

  byte[] getFile(String filePath) throws ResourceNotFoundException;

}
