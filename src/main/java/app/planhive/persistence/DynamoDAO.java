package app.planhive.persistence;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.regex.Pattern;

public abstract class DynamoDAO {

  public static final String CONDITIONAL_CHECK_FAILED_CODE = "ConditionalCheckFailed";

  private final String prefix;

  public DynamoDAO(String prefix) {
    this.prefix = prefix;
  }

  public String addPrefix(String value) {
    checkNotNull(value);

    return String.format("%s|%s", prefix, value);
  }

  public String removePrefix(String value) {
    checkNotNull(value);

    return value.replaceFirst(Pattern.quote(String.format("%s|", prefix)), "");
  }

  public static String addPrefix(String prefix, String value) {
    checkNotNull(prefix);
    checkNotNull(value);

    return String.format("%s|%s", prefix, value);
  }

  public static String removePrefix(String prefix, String value) {
    checkNotNull(prefix);
    checkNotNull(value);

    return value.replaceFirst(Pattern.quote(String.format("%s|", prefix)), "");
  }
}
