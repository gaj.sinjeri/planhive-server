package app.planhive.persistence.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

public class VersionedItem<T> {

  private final int version;
  private final T item;

  public VersionedItem(int version, T item) {
    checkNotNull(item);

    this.version = version;
    this.item = item;
  }

  public VersionedItem<T> withNewItem(T item) {
    return new VersionedItem<>(version, item);
  }

  public int getVersion() {
    return version;
  }

  public T getItem() {
    return item;
  }

  @Override
  public String toString() {
    return "VersionedItem{" +
        "version=" + version +
        ", item=" + item +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VersionedItem<?> that = (VersionedItem<?>) o;
    return version == that.version &&
        Objects.equals(item, that.item);
  }

  @Override
  public int hashCode() {
    return Objects.hash(version, item);
  }
}
