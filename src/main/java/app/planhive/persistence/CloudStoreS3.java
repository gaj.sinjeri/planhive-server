package app.planhive.persistence;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.common.Image;
import app.planhive.exception.ResourceNotFoundException;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class CloudStoreS3 implements CloudStore {

  private final String bucketName;
  private final AmazonS3 s3Client;

  private static final Logger logger = LoggerFactory.getLogger(CloudStoreS3.class);

  public CloudStoreS3(String bucketName, AmazonS3 s3Client) {
    this.bucketName = bucketName;
    this.s3Client = s3Client;

    logger.info("[AWS] S3 Client initialized");
  }

  @Override
  public void storeImage(String filePath, BufferedImage image) {
    checkNotNull(filePath);
    checkNotNull(image);

    var bytes = Image.imageToBytes(image);

    var metadata = new ObjectMetadata();
    metadata.setContentType("image/jpeg");
    metadata.setContentLength(bytes.length);

    try (var inputStream = new ByteArrayInputStream(bytes)) {
      var request = new PutObjectRequest(bucketName, filePath, inputStream, metadata);
      s3Client.putObject(request);
    } catch (AmazonServiceException e) {
      throw new InternalError("AWS S3 could not process the request to store jpeg image", e);
    } catch (SdkClientException e) {
      throw new InternalError(
          "AWS S3 could not be contacted or the client could not parse the response", e);
    } catch (Exception e) {
      throw new InternalError(e);
    }
  }

  @Override
  public byte[] getFile(String filePath) throws ResourceNotFoundException {
    checkNotNull(filePath);

    try (var object = s3Client.getObject(bucketName, filePath)) {
      return object.getObjectContent().readAllBytes();
    } catch (AmazonS3Exception e) {
      if (e.getErrorCode() != null && (e.getErrorCode().equals("NoSuchKey") || e.getErrorCode()
          .equals("404 Not Found"))) {
        throw new ResourceNotFoundException(
            String.format("No file exists in S3 at filepath %s", filePath));
      }
      throw e;
    } catch (IOException e) {
      logger.error(String.format("Could not read bytes for file from %s", filePath));
      throw new InternalError(e);
    }
  }
}
