package app.planhive.persistence.configuration;

import static app.planhive.common.Flags.APP_ENV_KEY;
import static app.planhive.common.Flags.DEVELOPMENT_ENV;
import static app.planhive.common.Flags.PRODUCTION_ENV;

import app.planhive.persistence.CloudStore;
import app.planhive.persistence.CloudStoreS3;
import app.planhive.persistence.CloudStoreS3Stub;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.persistence.DynamoDBClientAws;
import app.planhive.persistence.DynamoDBClientLocal;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceConfiguration {

  @Value("${s3.bucket.name}")
  private String bucketName;

  @Bean
  public AmazonS3 amazonS3() {
    return AmazonS3ClientBuilder.standard()
        .withRegion(Regions.US_EAST_1)
        .build();
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = PRODUCTION_ENV)
  public CloudStore cloudStore() {
    return new CloudStoreS3(bucketName, amazonS3());
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = DEVELOPMENT_ENV)
  public CloudStore cloudStoreStub() {
    return new CloudStoreS3Stub();
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = PRODUCTION_ENV)
  public DynamoDBClient dynamoDBClientAws() {
    return new DynamoDBClientAws();
  }

  @Bean
  @ConditionalOnProperty(value = APP_ENV_KEY, havingValue = DEVELOPMENT_ENV)
  public DynamoDBClient dynamoDBClientLocal() {
    return new DynamoDBClientLocal();
  }
}
