package app.planhive.persistence;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;

public class CloudStoreS3Stub implements CloudStore {

  private static final Logger logger = LoggerFactory.getLogger(CloudStoreS3Stub.class);

  public CloudStoreS3Stub() {
    logger.info("[STUB] S3 Client initialized");
  }

  @Override
  public void storeImage(String filePath, BufferedImage image) {
    checkNotNull(filePath);
    checkNotNull(image);

    logger.info(String.format("[STUB] Stored image in %s", filePath));
  }

  @Override
  public byte[] getFile(String filePath) {
    checkNotNull(filePath);

    logger.info(String.format("[STUB] Returning empty file from %s", filePath));

    return new byte[]{};
  }
}
