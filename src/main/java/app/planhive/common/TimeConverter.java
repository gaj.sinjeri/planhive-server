package app.planhive.common;

import org.joda.time.Instant;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class TimeConverter {

  public static Long instantToSeconds(Instant i) {
    return i.getMillis() / 1000;
  }

  public static Instant secondsToInstant(Long s) {
    return new Instant(s * 1000);
  }

  public static Instant millisToInstant(Long m) {
    return new Instant(m);
  }

  public static List<Long> instantListToMillisList(List<Instant> l) {
    return l.stream()
        .map(Instant::getMillis)
        .collect(Collectors.toUnmodifiableList());
  }

  public static List<Instant> bigDecimalListToInstantList(List<BigDecimal> l) {
    return l.stream()
        .map(i -> new Instant(i.longValue()))
        .collect(Collectors.toUnmodifiableList());
  }

}
