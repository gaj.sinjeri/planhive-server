package app.planhive.common;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public abstract class ObjectSerializer {

  private static final ObjectMapper mapper = new ObjectMapper();

  protected String serializeObject(Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (IOException e) {
      throw new InternalError(e);
    }
  }
}
