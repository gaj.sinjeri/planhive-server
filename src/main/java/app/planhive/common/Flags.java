package app.planhive.common;

public class Flags {

  public static final String APP_ENV_KEY = "APP_ENV";
  public static final String DEVELOPMENT_ENV = "dev";
  public static final String PRODUCTION_ENV = "prod";

}
