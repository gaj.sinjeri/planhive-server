package app.planhive.common;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.exception.InvalidImageException;

import org.imgscalr.Scalr;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

// TODO(#209): Write unit tests for this class
public class Image {

  public static BufferedImage resize(BufferedImage original, int maxResolution) {
    checkNotNull(original);
    checkArgument(maxResolution > 0, "Maximum resolution must be a positive integer");

    var scaleFactor = Double
        .min(1, maxResolution / Double.max(original.getHeight(), original.getWidth()));

    if (scaleFactor < 1) {
      var targetHeight = (int) (original.getHeight() * scaleFactor);
      var targetWidth = (int) (original.getWidth() * scaleFactor);

      return Scalr.resize(original, targetWidth, targetHeight);
    }

    return original;
  }

  public static void verifyResolution(BufferedImage image, int maxResolution)
      throws InvalidImageException {
    if (image.getWidth() > maxResolution || image.getHeight() > maxResolution) {
      throw new InvalidImageException("Provided image exceeds maximum allowed resolution");
    }
  }

  public static void verifyAspectRatio(BufferedImage image, double widthToHeightRatio)
      throws InvalidImageException {
    var actualRatio = ((double) image.getWidth()) / ((double) image.getHeight());

    var deviation = Math.abs((actualRatio / widthToHeightRatio) - 1);
    if (deviation > 0.02) {
      throw new InvalidImageException(String
          .format("Expected width-to-height ratio to be %s, but was %s", widthToHeightRatio,
              actualRatio));
    }
  }

  public static byte[] imageToBytes(BufferedImage image) {
    byte[] bytes;

    try (var outputStream = new ByteArrayOutputStream()) {
      ImageIO.write(image, "jpeg", outputStream);
      bytes = outputStream.toByteArray();
    } catch (IOException e) {
      throw new InternalError("Could not write image to output stream", e);
    }

    return bytes;
  }
}
