package app.planhive.health;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * Controller that defines an endpoint that can be configured as a health check for a load balancer
 */
@RestController
public class HealthCheckController {

  @GetMapping("/health")
  public String health() {
    return "I'm healthy, thanks for checking!";
  }

}
