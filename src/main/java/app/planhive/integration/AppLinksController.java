package app.planhive.integration;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static org.springframework.util.MimeTypeUtils.TEXT_HTML_VALUE;

import com.google.common.io.CharStreams;
import java.io.InputStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 * These endpoints are called by Apple or Google when the app is installed. They return a JSON file
 * that describes the paths that should open the app instead of the website.
 * <p>
 * Starting with iOS 13 Apple changed the way app ids and paths are declared, so both versions need
 * to be included to support older iOS versions. This can be done in the same document by using both
 * the old and new properties (because the keys are different).
 * <p>
 * Apple doc: https://developer.apple.com/documentation/safariservices/supporting_associated_domains_in_your_app
 * Google doc: https://developer.android.com/training/app-links/verify-site-associations
 */
@RestController
public class AppLinksController {

  private final byte[] appleAppSiteAssociationJson;
  private final byte[] assetLinksJson;
  private final String verificationRedirectHtml;
  private final String apiUrlSchemeName;
  private final String apiUrlVerification;

  @Autowired
  AppLinksController(
      @Value("apple-app-site-association.json") ClassPathResource appleJson,
      @Value("assetlinks.json") ClassPathResource googleJson,
      @Value("verification-redirect.html") ClassPathResource verificationRedirectHtml,
      @Value("${api.url.scheme.name}") String apiUrlSchemeName,
      @Value("${api.url.verification}") String apiUrlVerification
  ) throws IOException {
    appleAppSiteAssociationJson = appleJson.getInputStream().readAllBytes();
    assetLinksJson = googleJson.getInputStream().readAllBytes();

    try (var reader = new InputStreamReader(verificationRedirectHtml.getInputStream())) {
      this.verificationRedirectHtml = CharStreams.toString(reader);
    }

    this.apiUrlSchemeName = apiUrlSchemeName;
    this.apiUrlVerification = apiUrlVerification;
  }

  @GetMapping(value = "/email/{code}")
  public void emailValidationRedirect(@PathVariable("code") String code,
      HttpServletResponse httpResponse) throws IOException {
    var redirectUri = String.format("%s://%s/%s", apiUrlSchemeName, apiUrlVerification, code);
    httpResponse.setStatus(200);
    httpResponse.setContentType(TEXT_HTML_VALUE);
    httpResponse.getWriter().write(String.format(verificationRedirectHtml, redirectUri));
  }

  @GetMapping(value = "/.well-known/apple-app-site-association", produces = APPLICATION_JSON_VALUE)
  public byte[] appleAppSiteAssociationJson() {
    return appleAppSiteAssociationJson;
  }

  @GetMapping(value = "/apple-app-site-association", produces = APPLICATION_JSON_VALUE)
  public byte[] appleAppSiteAssociationJsonRoot() {
    return appleAppSiteAssociationJson;
  }

  @GetMapping(value = "/.well-known/assetlinks.json", produces = APPLICATION_JSON_VALUE)
  public byte[] assetLinks() {
    return assetLinksJson;
  }
}
