package app.planhive.threading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.annotation.PreDestroy;

@Service
public class TaskRunnerImpl implements TaskRunner {

  private static final Logger logger = LoggerFactory.getLogger(TaskRunnerImpl.class);

  private final ExecutorService executorService;

  public TaskRunnerImpl() {
    executorService = Executors.newCachedThreadPool();
  }

  public void run(final Runnable task) {
    executorService.execute(() -> {
      try {
        task.run();
      } catch (Throwable e) {
        logger.error("Unhandled exception thrown by task", e);
      }
    });
  }

  @PreDestroy
  private void ensureCompletion() throws InterruptedException {
    logger.info("Ensuring all tasks complete before shutdown...");

    executorService.shutdown();
    if (!executorService.awaitTermination(5, TimeUnit.MINUTES)) {
      logger.error("Remaining tasks didn't complete in the required time of 5 minutes");
    }
  }
}
