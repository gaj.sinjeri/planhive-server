package app.planhive.threading;

public interface TaskRunner {

  void run(Runnable runnable);
}
