package app.planhive.security.configuration;

import app.planhive.security.SecureGenerator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class SecurityConfiguration {

  @Value("#{environment.MAC_GEN_KEY}")
  private String macGenKey;

  @Bean
  public SecureGenerator secureGenerator() throws NoSuchAlgorithmException, InvalidKeyException {
    return new SecureGenerator(macGenKey);
  }
}
