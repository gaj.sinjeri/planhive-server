package app.planhive.security;

import app.planhive.security.exception.InvalidCiphertextException;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

// TODO(#251): This class now deals with encryption, random number generation, and authentication.
//  We should split it into different classes, each with a single responsibility.
public class SecureGenerator {

  private static final int MAX_GENERATED_BYTES = 10240; // 10 kB

  private SecureRandom sr;
  private final SecretKey secretKey;
  private int generatedBytes = 0;

  private final Base64.Encoder base64Encoder = Base64.getUrlEncoder().withoutPadding();
  private final Base64.Decoder base64Decoder = Base64.getUrlDecoder();

  public SecureGenerator(String macGenKey) {
    sr = new SecureRandom();
    secretKey = new SecretKeySpec(Base64.getDecoder().decode(macGenKey), "AES");
  }

  private synchronized void checkAndRefreshSeed() {
    if (generatedBytes >= MAX_GENERATED_BYTES) {
      sr = new SecureRandom();
      generatedBytes = 0;
    }
  }

  private byte[] generateSecureRandomKey(int byteLength) {
    checkAndRefreshSeed();
    generatedBytes += byteLength;

    var bytes = new byte[byteLength];
    sr.nextBytes(bytes);

    return bytes;
  }

  public String generate128BitKey() {
    return base64Encoder.encodeToString(generateSecureRandomKey(16));
  }

  public String generate256BitKey() {
    return base64Encoder.encodeToString(generateSecureRandomKey(32));
  }

  public String generateMAC(String s) {
    return base64Encoder.encodeToString(generateMAC(s.getBytes(StandardCharsets.UTF_8)));
  }

  public String generateMAC(List<String> l) {
    var stringBuilder = new StringBuilder();
    l.forEach(stringBuilder::append);

    return generateMAC(stringBuilder.toString());
  }

  private byte[] generateMAC(byte[] bytes) {
    try {
      var mac = Mac.getInstance("HmacSHA256");
      mac.init(secretKey);
      return mac.doFinal(bytes);
    } catch (NoSuchAlgorithmException | InvalidKeyException e) {
      throw new RuntimeException(e);
    }
  }

  public String generateUUID() {
    // TODO: ensure this isn't blocking.
    return base64Encoder.encodeToString(uuidToBytes(UUID.randomUUID()));
  }

  public String encryptBase64String(String source) {
    return base64Encoder.encodeToString(encrypt(base64Decoder.decode(source)));
  }

  public String decryptBase64String(String source) throws InvalidCiphertextException {
    return base64Encoder.encodeToString(decrypt(base64Decoder.decode(source)));
  }

  private byte[] encrypt(byte[] plaintext) {
    try {
      var cipher = Cipher.getInstance("AES/GCM/NoPadding");
      var iv = generateSecureRandomKey(16);
      var parameterSpec = new GCMParameterSpec(128, iv);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
      var encryptedBytes = cipher.doFinal(plaintext);
      ByteBuffer cipherMessage = ByteBuffer.allocate(iv.length + encryptedBytes.length);
      cipherMessage.put(iv);
      cipherMessage.put(encryptedBytes);
      return cipherMessage.array();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private byte[] decrypt(byte[] ciphertext) throws InvalidCiphertextException {
    try {
      var cipher = Cipher.getInstance("AES/GCM/NoPadding");
      var parameterSpec = new GCMParameterSpec(128, ciphertext, 0, 16);
      cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
      return cipher.doFinal(ciphertext, 16, ciphertext.length - 16);
    } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException e) {
      throw new RuntimeException(e);
    } catch (BadPaddingException | IllegalBlockSizeException e) {
      throw new InvalidCiphertextException();
    }
  }

  public static byte[] uuidToBytes(UUID uuid) {
    var bb = ByteBuffer.wrap(new byte[16]);
    bb.putLong(uuid.getMostSignificantBits());
    bb.putLong(uuid.getLeastSignificantBits());
    return bb.array();
  }
}
