package app.planhive.filter;

import app.planhive.exception.AuthenticationFailedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.AuthenticationHeader;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.authentication.Authenticator;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestFilter implements Filter {

  private static final Pattern AUTH_REQUIRED_PATTERN = Pattern.compile("^/authenticated/");
  public static final String USER_ATTRIBUTE = "user";

  private final Authenticator authenticator;

  private static final Logger logger = LoggerFactory.getLogger(RequestFilter.class);

  @Inject
  RequestFilter(Authenticator authenticator) {
    this.authenticator = authenticator;
  }

  @Override
  public void init(FilterConfig filterConfig) {
    logger.info("########## Initiating Rest Request filter ##########");
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {
    var request = (HttpServletRequest) servletRequest;

    logger.debug(String.format("Received request with URI: %s", request.getRequestURI()));

    String path;
    try {
      path = new URI(request.getRequestURI()).getPath();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    if (AUTH_REQUIRED_PATTERN.matcher(path).find()) {
      logger.debug("Authenticating request");

      var userId = ((HttpServletRequest) servletRequest).getHeader(AuthenticationHeader.USER_ID);
      var sessionId = ((HttpServletRequest) servletRequest)
          .getHeader(AuthenticationHeader.SESSION_ID);

      if (userId == null || userId.isEmpty()) {
        logger.warn(String.format("[%s: %s] Missing header User-Id", request.getMethod(),
            path));

        var errorResponse = RestResponse.errorResponse(ErrorCode.MISSING_HEADER_USER_ID);

        writeServletResponse(servletResponse, errorResponse, HttpStatus.BAD_REQUEST);
        return;
      } else if (sessionId == null || sessionId.isEmpty()) {
        logger.warn(String.format("[%s: %s] Missing header Session-Id", request.getMethod(),
            path));

        var errorResponse = RestResponse.errorResponse(ErrorCode.MISSING_HEADER_SESSION_ID);

        writeServletResponse(servletResponse, errorResponse, HttpStatus.BAD_REQUEST);
        return;
      }

      try {
        var user = authenticator.authenticateUser(userId, sessionId);
        servletRequest.setAttribute(USER_ATTRIBUTE, user);
      } catch (ResourceNotFoundException e) {
        logger.warn(String.format("[%s: %s] No user exists with user id %s", request.getMethod(),
            path, userId));

        var errorResponse = RestResponse.errorResponse(ErrorCode.USER_DOES_NOT_EXIST);

        writeServletResponse(servletResponse, errorResponse, HttpStatus.UNAUTHORIZED);
        return;
      } catch (AuthenticationFailedException e) {
        logger.warn(String.format("[%s: %s] Invalid session id", request.getMethod(), path));

        var errorResponse = RestResponse.errorResponse(ErrorCode.INVALID_SESSION_ID);

        writeServletResponse(servletResponse, errorResponse, HttpStatus.UNAUTHORIZED);
        return;
      }
    }

    filterChain.doFilter(servletRequest, servletResponse);
  }

  private void writeServletResponse(ServletResponse servletResponse, RestResponse obj,
      HttpStatus status) throws IOException {
    var responseToSend = new ObjectMapper().writeValueAsString(obj).getBytes();

    ((HttpServletResponse) servletResponse).setHeader("Content-Type", "application/json");
    ((HttpServletResponse) servletResponse).setStatus(status.value());
    servletResponse.getOutputStream().write(responseToSend);
  }
}
