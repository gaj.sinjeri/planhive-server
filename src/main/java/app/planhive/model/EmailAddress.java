package app.planhive.model;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.Objects;

public class EmailAddress {

  private final String value;

  public EmailAddress(String value) {
    checkNotNull(value);

    var formattedValue = value.trim().toLowerCase();

    if (!EmailValidator.getInstance().isValid(formattedValue)) {
      throw new InternalError(
          String.format("Incorrect email address format for %s", formattedValue));
    }

    this.value = formattedValue;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "EmailAddress{" +
        "value='" + value + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmailAddress that = (EmailAddress) o;
    return Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }
}
