package app.planhive.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

public class RestResponse {

  private final Object result;
  private final ErrorResponse error;

  private RestResponse(Object result, ErrorResponse error) {
    this.result = result;
    this.error = error;
  }

  public static RestResponse emptyResponse() {
    return new RestResponse(null, null);
  }

  public static RestResponse successResponse(Object result) {
    checkNotNull(result);
    return new RestResponse(result, null);
  }

  public static RestResponse errorResponse(ErrorCode code) {
    return new RestResponse(null, new ErrorResponse(code));
  }

  public static RestResponse errorResponse(ErrorCode code, Object data) {
    return new RestResponse(null, new ErrorResponse(code, data));
  }

  public Object getResult() {
    return result;
  }

  public ErrorResponse getError() {
    return error;
  }

  @Override
  public String toString() {
    return "RestResponse{" +
        "result=" + result +
        ", error=" + error +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RestResponse that = (RestResponse) o;
    return Objects.equals(result, that.result) &&
        Objects.equals(error, that.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(result, error);
  }
}
