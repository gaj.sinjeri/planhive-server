package app.planhive.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

class ErrorResponse {

  private final ErrorCode code;
  private final Object data;

  ErrorResponse(ErrorCode code) {
    this(code, null);
  }

  ErrorResponse(ErrorCode code, Object data) {
    checkNotNull(code);

    this.code = code;
    this.data = data;
  }

  public int getCode() {
    return code.getValue();
  }

  public Object getData() {
    return data;
  }

  @Override
  public String toString() {
    return "ErrorResponse{" +
        "code=" + code +
        ", data=" + data +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorResponse that = (ErrorResponse) o;
    return code == that.code &&
        Objects.equals(data, that.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, data);
  }
}
