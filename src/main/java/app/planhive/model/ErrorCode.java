package app.planhive.model;

public enum ErrorCode {

  MAC_EXPIRED(0),
  MAC_INVALID(1),
  USERNAME_TAKEN(2),
  EMAIL_TAKEN(3),
  USER_ALREADY_EXISTS(4),
  MAX_VERIFICATION_CODES_REACHED(5),
  MISSING_HEADER_USER_ID(6),
  MISSING_HEADER_SESSION_ID(7),
  USER_DOES_NOT_EXIST(8),
  INVALID_SESSION_ID(9);

  private final int value;

  ErrorCode(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
