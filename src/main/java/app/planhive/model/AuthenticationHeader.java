package app.planhive.model;

public class AuthenticationHeader {

  public static final String USER_ID = "User-Id";
  public static final String SESSION_ID = "Session-Id";

}
