package app.planhive;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Optional;
import java.util.Properties;

@EnableRetry
@EnableAsync
@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    var props = new Properties();

    // Define the port for the HTTP server.
    // AWS Elastic Beanstalk defines an env variable PORT, which is the port http requests are forwarded to.
    props.setProperty("server.port", Optional.ofNullable(System.getenv("PORT")).orElse("5000"));

    new SpringApplicationBuilder(Application.class)
        .properties(props)
        .run(args);
  }
}
