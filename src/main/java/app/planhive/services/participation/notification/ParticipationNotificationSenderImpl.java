package app.planhive.services.participation.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.user.model.Notification.Builder;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ParticipationNotificationSenderImpl extends ObjectSerializer implements
    ParticipationNotificationSender {

  private final NotificationService notificationService;

  @Autowired
  ParticipationNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendParticipationStatusUpdatedNotification(User user, NodeId eventNodeId,
      String eventName, NodeId participationNodeId, ParticipationStatus participationStatus) {

    var notificationMessage = generateNotificationMessageForNewParticipationStatus(
        participationStatus, user.getName(), eventName);
    var notification = new Builder(eventName, notificationMessage, NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId, participationNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, participationNodeId, List.of(user.getId()), true);
  }

  private String generateNotificationMessageForNewParticipationStatus(
      ParticipationStatus participationStatus, String username, String eventName) {
    switch (participationStatus) {
      case ACCEPTED:
        return String.format("%s accepted the invitation for %s", username, eventName);
      case MAYBE:
        return String.format("%s might be attending %s", username, eventName);
      case DECLINED:
        return String.format("%s declined the invitation for %s", username, eventName);
      default:
        throw new InternalError("Invalid participation status.");
    }
  }
}
