package app.planhive.services.participation.notification;

import app.planhive.services.node.model.NodeId;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.user.model.User;

public interface ParticipationNotificationSender {

  void sendParticipationStatusUpdatedNotification(User user, NodeId EventNodeId, String eventName,
      NodeId participationNodeId, ParticipationStatus participationStatus);

}
