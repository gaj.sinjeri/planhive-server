package app.planhive.services.participation.controller.model;

import app.planhive.services.participation.model.ParticipationStatus;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateParticipationStatusRequest {

  private final ParticipationStatus status;

  @JsonCreator
  public UpdateParticipationStatusRequest(
      @JsonProperty(value = "participationStatus", required = true) ParticipationStatus status) {
    this.status = status;
  }

  public ParticipationStatus getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return "UpdateParticipationStatusRequest{" +
        "status=" + status +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateParticipationStatusRequest that = (UpdateParticipationStatusRequest) o;
    return status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(status);
  }
}
