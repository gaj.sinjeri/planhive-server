package app.planhive.services.participation.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.participation.controller.model.UpdateParticipationStatusRequest;
import app.planhive.services.participation.service.ParticipationService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authenticated")
public class ParticipationController {

  private static final Logger logger = LoggerFactory.getLogger(ParticipationController.class);

  private final ParticipationService participationService;

  @Autowired
  ParticipationController(ParticipationService participationService) {
    this.participationService = participationService;
  }

  @PutMapping(path = "events/{eventId}/participation/{participationId}/status", consumes = "application/json")
  public ResponseEntity<RestResponse> updateParticipationStatus(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("eventId") String eventId,
      @PathVariable("participationId") String participationId,
      @RequestBody UpdateParticipationStatusRequest request) {
    logger.debug(
        String.format("Received request to update event %s participation status to %s for user %s",
            eventId, request.getStatus(), user.getId()));

    try {
      var participationNodeId = new NodeId(participationId, eventId, NodeType.PARTICIPATION);
      var participantNode = participationService
          .updateParticipationStatus(user, participationNodeId, request.getStatus(), Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(participantNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }
}
