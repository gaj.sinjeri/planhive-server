package app.planhive.services.participation.model;

public enum ParticipationStatus {
  ACCEPTED,
  MAYBE,
  DECLINED
}
