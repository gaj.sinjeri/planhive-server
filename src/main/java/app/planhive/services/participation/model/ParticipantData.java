package app.planhive.services.participation.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ParticipantData extends NodeData {

  private final ParticipationStatus status;

  @JsonCreator
  public ParticipantData(
      @JsonProperty(value = "status", required = true) ParticipationStatus status) {
    checkNotNull(status);

    this.status = status;
  }

  public ParticipationStatus getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return "ParticipantData{" +
        "status=" + status +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ParticipantData that = (ParticipantData) o;
    return status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(status);
  }
}
