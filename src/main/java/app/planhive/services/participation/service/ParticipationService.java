package app.planhive.services.participation.service;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface ParticipationService {

  List<Node> addParticipationWidget(String parentId, String permissionGroupId, String creatorId,
      List<String> userIds, Instant requestTs);

  Node updateParticipationStatus(User user, NodeId participationNodeId,
      ParticipationStatus participationStatus, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

}
