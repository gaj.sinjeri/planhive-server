package app.planhive.services.participation.service;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.common.LeafNode;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.participation.model.ParticipantData;
import app.planhive.services.participation.model.ParticipationStatus;
import app.planhive.services.participation.notification.ParticipationNotificationSender;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ParticipationServiceImpl implements ParticipationService {

  private final SecureGenerator secureGenerator;
  private final NodeService nodeService;
  private final PermissionService permissionService;
  private final FeedService feedService;
  private final ParticipationNotificationSender participationNotificationSender;

  @Autowired
  ParticipationServiceImpl(SecureGenerator secureGenerator, NodeService nodeService,
      PermissionService permissionService, FeedService feedService,
      ParticipationNotificationSender participationNotificationSender) {
    this.secureGenerator = secureGenerator;
    this.nodeService = nodeService;
    this.permissionService = permissionService;
    this.feedService = feedService;
    this.participationNotificationSender = participationNotificationSender;
  }

  @Override
  public List<Node> addParticipationWidget(String parentId, String permissionGroupId,
      String creatorId, List<String> userIds, Instant requestTs) {

    var participationId = secureGenerator.generateUUID();
    var participationNodeId = new NodeId(participationId, parentId, NodeType.PARTICIPATION);
    var participationNode = new Node.Builder(participationNodeId)
        .withCreatorId(creatorId)
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .build();

    try {
      nodeService.insertNode(participationNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    var participantNode = createParticipantNode(participationNodeId, creatorId,
        ParticipationStatus.ACCEPTED, requestTs);

    feedService.createFeeds(participationNodeId.toFeedItemKey(),
        Stream.concat(userIds.stream(), Stream.of(creatorId))
            .collect(Collectors.toUnmodifiableList()),
        participationNodeId.getType().name(), requestTs);

    return List.of(participationNode, participantNode);
  }

  @Override
  public Node updateParticipationStatus(User user, NodeId participationNodeId,
      ParticipationStatus participationStatus, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNodeId = new NodeId(participationNodeId.getParentId(), NodeType.EVENT);
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var participationNode = nodeService.getNodeOrThrow(participationNodeId);

    // TODO(#113): Ideally, all users should have WRITE permissions for participation widgets.
    //  We will check for read permissions for now as the participation widget only has one purpose
    //   at this point.
    permissionService.verifyAccessRight(user.getId(), participationNode.getPermissionGroupId(),
        AccessRight.READ);

    var participantNode = createParticipantNode(participationNodeId, user.getId(),
        participationStatus, requestTs);
    feedService
        .refreshFeedsForResource(participationNode.getNodeId().toFeedItemKey(), requestTs);

    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    participationNotificationSender
        .sendParticipationStatusUpdatedNotification(user, eventNodeId, eventData.getName(),
            participationNodeId, participationStatus);

    return participantNode;
  }

  private Node createParticipantNode(NodeId participationNodeId, String userId,
      ParticipationStatus participationStatus, Instant requestTs) {
    var participantNodeId = new NodeId(
        LeafNode.generateLeafNodeId(participationNodeId.getId(), userId),
        participationNodeId.getId(), NodeType.PARTICIPANT);
    return nodeService.insertNodeWithDataOnly(participantNodeId, userId,
        new ParticipantData(participationStatus).toJson(), requestTs);
  }
}
