package app.planhive.services.thread.controller;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.filter.RequestFilter;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.thread.controller.model.AddMessageRequest;
import app.planhive.services.thread.controller.model.ClearThreadForUserRequest;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(path = "authenticated/threads")
public class ThreadController {

  private static final Logger logger = LoggerFactory.getLogger(ThreadController.class);

  private final ThreadService threadService;

  @Autowired
  ThreadController(ThreadService threadService) {
    this.threadService = threadService;
  }

  @PostMapping(path = "{threadId}/messages")
  public ResponseEntity<RestResponse> addMessage(@RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user,
      @PathVariable("threadId") String threadId, @RequestParam("request") AddMessageRequest request,
      @RequestParam(name = "image", required = false) MultipartFile image) {
    logger.debug(String
        .format("Received request to add message to thread %s, user %s", threadId, user.getId()));

    var threadNodeId = new NodeId(threadId, request.getParentNodeId(), NodeType.THREAD);

    try {
      List<Node> nodes;
      if (request.getParentNodeType() == NodeType.CHAT_DIRECT) {
        nodes = threadService
            .addMessageToDirectThread(user, threadNodeId, request.getTextContent(), image, Instant
                .now());
      } else {
        var groupNodeId = new NodeId(request.getParentNodeId(),
            request.getParentNodeType() == NodeType.EVENT ? NodeType.EVENT : NodeType.CHAT_GROUP);
        nodes = threadService.addMessageToGroupThread(user, groupNodeId, threadNodeId,
            request.getTextContent(), image, Instant.now());
      }
      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    } catch (InvalidImageException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(path = "{threadId}/messages/{messageId}/image",
      produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getImage(@RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user,
      @PathVariable("threadId") String threadId, @PathVariable("messageId") String messageId) {
    logger.debug(
        String.format("Received request to get photo for message %s", messageId));

    try {
      var image = threadService.getImage(user, new NodeId(messageId, threadId, NodeType.MESSAGE));
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "{threadId}/messages/{messageId}/image-thumbnail",
      produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getImageThumbnail(@RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user,
      @PathVariable("threadId") String threadId, @PathVariable("messageId") String messageId) {
    logger.debug(
        String.format("Received request to get photo for message %s", messageId));

    try {
      var image = threadService
          .getImageThumbnail(user, new NodeId(messageId, threadId, NodeType.MESSAGE));
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "{threadId}/users/{userId}", consumes = "application/json")
  public ResponseEntity<RestResponse> clearThreadForUser(
      @RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user, @PathVariable("threadId") String threadId,
      @RequestBody ClearThreadForUserRequest request) {
    logger.debug(
        String.format("Received request to clear thread %s, user %s", threadId, user.getId()));

    var threadNodeId = new NodeId(threadId, request.getParentNodeId(), NodeType.THREAD);

    try {
      threadService.clearThreadForUser(user, threadNodeId, Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(AddMessageRequest.class, new AddMessageRequest.Editor());
  }
}
