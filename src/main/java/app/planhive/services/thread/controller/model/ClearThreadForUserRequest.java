package app.planhive.services.thread.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ClearThreadForUserRequest {

  private final String parentNodeId;

  @JsonCreator
  public ClearThreadForUserRequest(
      @JsonProperty(value = "parentNodeId", required = true) String parentNodeId) {

    this.parentNodeId = parentNodeId;
  }

  public String getParentNodeId() {
    return parentNodeId;
  }

  @Override
  public String toString() {
    return "ClearThreadForUserRequest{" +
        "parentNodeId='" + parentNodeId + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClearThreadForUserRequest that = (ClearThreadForUserRequest) o;
    return Objects.equals(parentNodeId, that.parentNodeId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(parentNodeId);
  }
}
