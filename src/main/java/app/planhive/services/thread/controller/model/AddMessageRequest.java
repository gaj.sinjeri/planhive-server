package app.planhive.services.thread.controller.model;

import static com.google.common.base.Preconditions.checkArgument;

import app.planhive.services.node.model.NodeType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class AddMessageRequest {

  private final NodeType parentNodeType;
  private final String parentNodeId;
  private final String textContent;

  @JsonCreator
  public AddMessageRequest(
      @JsonProperty(value = "parentNodeType", required = true) NodeType parentNodeType,
      @JsonProperty(value = "parentNodeId", required = true) String parentNodeId,
      @JsonProperty(value = "textContent") String textContent) {
    checkArgument(parentNodeType == NodeType.EVENT
        || parentNodeType == NodeType.CHAT_DIRECT
        || parentNodeType == NodeType.CHAT_GROUP, "Parent node type not allowed");

    this.parentNodeType = parentNodeType;
    this.parentNodeId = parentNodeId;
    this.textContent = textContent;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, AddMessageRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public NodeType getParentNodeType() {
    return parentNodeType;
  }

  public String getParentNodeId() {
    return parentNodeId;
  }

  public String getTextContent() {
    return textContent;
  }

  @Override
  public String toString() {
    return "AddMessageRequest{" +
        "parentNodeType=" + parentNodeType +
        ", parentNodeId='" + parentNodeId + '\'' +
        ", textContent='" + textContent + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddMessageRequest that = (AddMessageRequest) o;
    return parentNodeType == that.parentNodeType &&
        Objects.equals(parentNodeId, that.parentNodeId) &&
        Objects.equals(textContent, that.textContent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(parentNodeType, parentNodeId, textContent);
  }
}
