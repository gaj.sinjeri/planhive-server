package app.planhive.services.thread.notification;

import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

public interface ThreadNotificationSender {

  void sendNewDirectMessageNotification(User sender, NodeId groupNodeId, NodeId threadNodeId,
      NodeId messageNodeId, String textContent);

  void sendNewGroupMessageNotification(User sender, Node groupNode, NodeId threadNodeId,
      NodeId messageNodeId, String textContent);

}
