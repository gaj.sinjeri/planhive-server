package app.planhive.services.thread.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.chat.model.ChatData;
import app.planhive.services.event.model.EventData;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ThreadNotificationSenderImpl extends ObjectSerializer implements
    ThreadNotificationSender {

  private final NotificationService notificationService;

  @Autowired
  ThreadNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendNewDirectMessageNotification(User sender, NodeId groupNodeId, NodeId threadNodeId,
      NodeId messageNodeId, String textContent) {
    var body = textContent != null ? textContent : "Shared a photo.";
    var notification = new Notification.Builder(sender.getName(), body, NotificationCategory.DIRECT_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(
            NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(groupNodeId, threadNodeId, messageNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, threadNodeId, List.of(sender.getId()), true);
  }

  @Override
  public void sendNewGroupMessageNotification(User sender, Node groupNode, NodeId threadNodeId,
      NodeId messageNodeId, String textContent) {
    String groupName;
    var isEvent = groupNode.getNodeId().getType() == NodeType.EVENT;
    if (isEvent) {
      groupName = NodeData.fromJson(groupNode.getData(), EventData.class).getName();
    } else if (groupNode.getNodeId().getType() == NodeType.CHAT_GROUP) {
      groupName = NodeData.fromJson(groupNode.getData(), ChatData.class).getName();
    } else {
      throw new InternalError(
          String.format("Unsupported node type: %s", groupNode.getNodeId().getType().name()));
    }

    var body = textContent != null ? textContent : "Shared a photo.";
    var notification = new Notification.Builder(groupName,
        String.format("%s: %s", sender.getName(), body), isEvent ? NotificationCategory.EVENT_CHATS
        : NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(),
                serializeObject(List.of(groupNode.getNodeId(), threadNodeId, messageNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, threadNodeId, List.of(sender.getId()), true);
  }
}
