package app.planhive.services.thread.model;

import static com.google.common.base.Preconditions.checkArgument;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;

import java.util.Objects;

@JsonInclude(Include.NON_EMPTY)
public class MessageData extends NodeData {

  private final String textContent;
  private final boolean imagePresent;

  @JsonCreator
  public MessageData(@JsonProperty("textContent") String textContent,
      @JsonProperty("imagePresent") boolean imagePresent) {
    checkArgument(!Strings.isNullOrEmpty(textContent) || imagePresent,
        "Either textContent has to be present or imagePresent true");

    this.textContent = textContent;
    this.imagePresent = imagePresent;
  }

  public String getTextContent() {
    return textContent;
  }

  public boolean isImagePresent() {
    return imagePresent;
  }

  @Override
  public String toString() {
    return "MessageData{" +
        "textContent='" + textContent + '\'' +
        ", imagePresent=" + imagePresent +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MessageData that = (MessageData) o;
    return imagePresent == that.imagePresent &&
        Objects.equals(textContent, that.textContent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(textContent, imagePresent);
  }
}
