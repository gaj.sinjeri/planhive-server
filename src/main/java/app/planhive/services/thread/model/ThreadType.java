package app.planhive.services.thread.model;

public enum ThreadType {
  MAIN,
  CUSTOM
}
