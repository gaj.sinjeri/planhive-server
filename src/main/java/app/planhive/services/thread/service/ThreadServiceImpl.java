package app.planhive.services.thread.service;

import app.planhive.common.Image;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.model.MessageData;
import app.planhive.services.thread.notification.ThreadNotificationSender;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

public class ThreadServiceImpl implements ThreadService {

  private static final String IMAGE_URI = "threads/%s/messages/%s/image.jpeg";
  private static final String IMAGE_THUMBNAIL_URI = "threads/%s/messages/%s/t-image.jpeg";

  private final SecureGenerator secureGenerator;
  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final ThreadNotificationSender threadNotificationSender;
  private final CloudStore cloudStore;
  private final int maxImageResolution;
  private final int thumbnailResolution;

  private static final Logger logger = LoggerFactory.getLogger(ThreadServiceImpl.class);

  public ThreadServiceImpl(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService,
      ThreadNotificationSender threadNotificationSender, CloudStore cloudStore,
      int maxImageResolution, int thumbnailResolution) {
    this.secureGenerator = secureGenerator;
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.threadNotificationSender = threadNotificationSender;
    this.cloudStore = cloudStore;
    this.maxImageResolution = maxImageResolution;
    this.thumbnailResolution = thumbnailResolution;
  }

  @Override
  public Node createThread(NodeId parentNodeId, String permissionGroupId, boolean privateThread,
      String creatorId, List<String> userIds, Instant requestTs) {
    var threadId = secureGenerator.generateUUID();
    var threadNodeId = new NodeId(threadId, parentNodeId.getId(), NodeType.THREAD);
    var node = new Node.Builder(threadNodeId)
        .withCreatorId(creatorId)
        .setPrivateNode(privateThread ? true : null)
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .build();

    try {
      nodeService.insertNode(node);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService.createFeeds(node.getNodeId().toFeedItemKey(),
        Stream.concat(userIds.stream(), Stream.of(creatorId))
            .collect(Collectors.toUnmodifiableList()),
        node.getNodeId().getType().name(), requestTs);

    return node;
  }

  @Override
  public List<Node> addMessageToDirectThread(User sender, NodeId threadNodeId, String textContent,
      MultipartFile image, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException {
    var directChatNode = nodeService
        .getNodeOrThrow(new NodeId(threadNodeId.getParentId(), NodeType.CHAT_DIRECT));
    var threadNode = nodeService.getNodeOrThrow(threadNodeId);
    permissionService
        .verifyAccessRight(sender.getId(), threadNode.getPermissionGroupId(), AccessRight.READ);

    var messageNode = storeMessage(sender.getId(), threadNodeId,
        threadNode.getPermissionGroupId(), textContent, image, requestTs);
    var updatedThreadNode = nodeService.refreshLastUpdateTs(threadNodeId, requestTs);

    threadNotificationSender
        .sendNewDirectMessageNotification(sender, directChatNode.getNodeId(), threadNodeId,
            messageNode.getNodeId(), textContent);

    return List.of(updatedThreadNode, messageNode);
  }

  @Override
  public List<Node> addMessageToGroupThread(User sender, NodeId groupNodeId, NodeId threadNodeId,
      String textContent, MultipartFile image, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException {
    var groupNode = nodeService.getNodeOrThrow(groupNodeId);
    permissionService
        .verifyAccessRight(sender.getId(), groupNode.getPermissionGroupId(), AccessRight.READ);

    var threadNode = nodeService.getNodeOrThrow(threadNodeId);
    permissionService
        .verifyAccessRight(sender.getId(), threadNode.getPermissionGroupId(), AccessRight.READ);

    var messageNode = storeMessage(sender.getId(), threadNodeId,
        threadNode.getPermissionGroupId(), textContent, image, requestTs);
    var updatedThreadNode = nodeService.refreshLastUpdateTs(threadNodeId, requestTs);

    threadNotificationSender
        .sendNewGroupMessageNotification(sender, groupNode, threadNodeId, messageNode.getNodeId(),
            textContent);

    return List.of(updatedThreadNode, messageNode);
  }

  private Node storeMessage(String senderId, NodeId threadNodeId, String permissionGroupId,
      String textContent, MultipartFile image, Instant requestTs) throws InvalidImageException {
    var messageId = secureGenerator.generateUUID();

    if (image != null) {
      storeImage(image, threadNodeId.getId(), messageId);
    }

    var messageNodeId = new NodeId(messageId, threadNodeId.getId(), NodeType.MESSAGE);
    var messageNode = new Node.Builder(messageNodeId)
        .withCreatorId(senderId)
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .withData(new MessageData(textContent, image != null).toJson())
        .build();

    try {
      nodeService.insertNode(messageNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService.refreshFeedsForResource(threadNodeId.toFeedItemKey(), requestTs);

    return messageNode;
  }

  private void storeImage(MultipartFile image, String threadNodeId, String messageId)
      throws InvalidImageException {
    if (!Objects.equals(image.getContentType(), "image/jpeg")) {
      throw new InvalidImageException("Image must be in jpeg format");
    }

    BufferedImage original;
    BufferedImage thumbnail;

    try (var imageInputStream = image.getInputStream()) {
      original = ImageIO.read(imageInputStream);
      Image.verifyResolution(original, maxImageResolution);

      thumbnail = Image.resize(original, thumbnailResolution);
    } catch (IOException e) {
      var msg = "Error accessing image input stream";
      logger.error(msg);
      throw new InternalError(msg, e);
    }

    var originalFileName = String.format(IMAGE_URI, threadNodeId, messageId);
    var thumbnailFileName = String.format(IMAGE_THUMBNAIL_URI, threadNodeId, messageId);
    cloudStore.storeImage(originalFileName, original);
    cloudStore.storeImage(thumbnailFileName, thumbnail);
  }

  @Override
  public byte[] getImage(User user, NodeId messageNodeId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var messageNode = nodeService.getNodeOrThrow(messageNodeId);

    permissionService
        .verifyAccessRight(user.getId(), messageNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String.format(IMAGE_URI, messageNodeId.getParentId(), messageNodeId.getId());
    return cloudStore.getFile(filePath);
  }

  @Override
  public byte[] getImageThumbnail(User user, NodeId messageNodeId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var messageNode = nodeService.getNodeOrThrow(messageNodeId);

    permissionService
        .verifyAccessRight(user.getId(), messageNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String
        .format(IMAGE_THUMBNAIL_URI, messageNodeId.getParentId(), messageNodeId.getId());
    return cloudStore.getFile(filePath);
  }

  @Override
  public void clearThreadForUser(User user, NodeId threadNodeId, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException {
    var threadNode = nodeService.getNodeOrThrow(threadNodeId);

    feedService
        .updateBeginTs(threadNode.getNodeId().toFeedItemKey(), user.getId(), requestTs, requestTs);
  }
}
