package app.planhive.services.thread.service;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ThreadService {

  Node createThread(NodeId parentNodeId, String permissionGroupId, boolean privateThread,
      String creatorId, List<String> userIds, Instant requestTs);

  List<Node> addMessageToDirectThread(User sender, NodeId threadNodeId, String textContent,
      MultipartFile image, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException;

  List<Node> addMessageToGroupThread(User sender, NodeId groupNodeId, NodeId threadNodeId,
      String textContent, MultipartFile image, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException;

  byte[] getImage(User user, NodeId messageNodeId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  byte[] getImageThumbnail(User user, NodeId messageNodeId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void clearThreadForUser(User user, NodeId threadNodeId, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException;

}
