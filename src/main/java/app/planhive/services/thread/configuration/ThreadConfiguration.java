package app.planhive.services.thread.configuration;

import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.notification.ThreadNotificationSender;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.thread.service.ThreadServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThreadConfiguration {

  @Value("${message.image.max.resolution}")
  private int maxImageResolution;

  @Value("${message.image.max.resolution}")
  private int thumbnailResolution;

  @Bean
  public ThreadService threadService(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService,
      ThreadNotificationSender threadNotificationSender, CloudStore cloudStore) {
    return new ThreadServiceImpl(secureGenerator, nodeService, feedService, permissionService,
        threadNotificationSender, cloudStore, maxImageResolution, thumbnailResolution);
  }
}
