package app.planhive.services.authentication;

import app.planhive.exception.AuthenticationFailedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatorImpl implements Authenticator {

  private final UserDAO userDAO;

  private static final Logger logger = LoggerFactory.getLogger(AuthenticatorImpl.class);

  public AuthenticatorImpl(UserDAO userDAO) {
    this.userDAO = userDAO;
  }

  public User authenticateUser(String userId, String sessionId)
      throws ResourceNotFoundException, AuthenticationFailedException {
    var user = userDAO.getUser(userId);
    if (user == null) {
      var msg = String.format("User %s does not exist", userId);
      logger.error(msg);
      throw new ResourceNotFoundException(msg);
    }
    if (!user.getSessionId().equals(sessionId)) {
      throw new AuthenticationFailedException(
          String.format("Invalid session id for user %s", userId));
    }
    return user;
  }
}
