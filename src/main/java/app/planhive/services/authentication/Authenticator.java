package app.planhive.services.authentication;

import app.planhive.exception.AuthenticationFailedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.user.model.User;

public interface Authenticator {

  User authenticateUser(String userId, String sessionId)
      throws ResourceNotFoundException, AuthenticationFailedException;
}
