package app.planhive.services.permission.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class GetPermissionsRequest {

  private final long lastUpdateTs;

  @JsonCreator
  public GetPermissionsRequest(
      @JsonProperty(value = "lastUpdateTs", required = true) long lastUpdateTs) {
    this.lastUpdateTs = lastUpdateTs;
  }

  public long getLastUpdateTs() {
    return lastUpdateTs;
  }

  @Override
  public String toString() {
    return "GetPermissionsRequest{" +
        "lastUpdateTs=" + lastUpdateTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetPermissionsRequest that = (GetPermissionsRequest) o;
    return lastUpdateTs == that.lastUpdateTs;
  }

  @Override
  public int hashCode() {
    return Objects.hash(lastUpdateTs);
  }
}
