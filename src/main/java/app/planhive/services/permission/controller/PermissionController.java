package app.planhive.services.permission.controller;

import app.planhive.exception.UnauthorisedException;
import app.planhive.filter.RequestFilter;
import app.planhive.model.RestResponse;
import app.planhive.services.permission.controller.model.GetPermissionUpdatesRequest;
import app.planhive.services.permission.controller.model.GetPermissionsRequest;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authenticated/permissions")
public class PermissionController {

  private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);

  private final PermissionService permissionService;

  @Autowired
  PermissionController(PermissionService permissionService) {
    this.permissionService = permissionService;
  }

  @GetMapping(path = "{permissionGroupId}", consumes = "application/json")
  public ResponseEntity<RestResponse> getPermissions(
      @RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user,
      @PathVariable("permissionGroupId") String permissionGroupId,
      @RequestBody GetPermissionsRequest request) {
    logger.debug(String.format("Received request to get permissions in group %s for user %s",
        permissionGroupId, user.getId()));

    try {
      var permissions = permissionService
          .getPermissions(user, permissionGroupId, new Instant(request.getLastUpdateTs()));
      return new ResponseEntity<>(RestResponse.successResponse(permissions), HttpStatus.OK);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "updates", consumes = "application/json")
  public ResponseEntity<RestResponse> getPermissionUpdates(
      @RequestAttribute(RequestFilter.USER_ATTRIBUTE) User user,
      @RequestBody GetPermissionUpdatesRequest request) {
    logger.debug(
        String.format("Received request to get permission updates for user %s", user.getId()));

    var permissions = permissionService
        .getPermissionUpdates(user, new Instant(request.getLastUpdateTs()));
    return new ResponseEntity<>(RestResponse.successResponse(permissions), HttpStatus.OK);
  }
}
