package app.planhive.services.permission.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;
import java.util.Map;

public interface PermissionService {

  String createPermissionGroup(Map<String, AccessControl> usersWithAccessControl,
      Instant requestTs);

  void addUsersToPermissionGroup(Map<String, AccessControl> usersWithAccessControl,
      String permissionGroupId, Instant requestTs);

  Permission updateAccessControl(String userId, String permissionGroupId,
      AccessControl accessControl, Instant requestTs) throws ResourceNotFoundException;

  void verifyAccessRight(String userId, String permissionGroupId, AccessRight accessRight)
      throws UnauthorisedException;

  List<Permission> getPermissions(String permissionGroupId, Instant lastUpdateTs);

  List<Permission> getPermissions(User user, String permissionGroupId, Instant lastUpdateTs)
      throws UnauthorisedException;

  List<Permission> getPermissionUpdates(User user, Instant lastUpdateTs);

  List<Permission> batchSoftDeletePermissions(String userId, List<String> permissionGroupIds,
      Instant requestTs);

}
