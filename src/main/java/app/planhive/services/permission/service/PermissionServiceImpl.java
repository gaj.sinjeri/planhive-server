package app.planhive.services.permission.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.permission.persistence.PermissionDAO;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PermissionServiceImpl implements PermissionService {

  private static final int FEED_QUERY_SIZE_LIMIT = 100;
  private static final String FEED_ITEM_RESOURCE_TYPE_KEY = "perm";

  private final PermissionDAO permissionDAO;
  private final SecureGenerator secureGenerator;
  private final FeedService feedService;

  private static final Logger logger = LoggerFactory.getLogger(PermissionServiceImpl.class);

  @Autowired
  PermissionServiceImpl(PermissionDAO permissionDAO, SecureGenerator secureGenerator,
      FeedService feedService) {
    this.permissionDAO = permissionDAO;
    this.secureGenerator = secureGenerator;
    this.feedService = feedService;
  }

  @Override
  public String createPermissionGroup(Map<String, AccessControl> usersWithAccessControl,
      Instant requestTs) {
    var permissionGroupId = secureGenerator.generateUUID();

    createNewPermissions(usersWithAccessControl, permissionGroupId, requestTs);

    return permissionGroupId;
  }

  @Override
  public void addUsersToPermissionGroup(Map<String, AccessControl> usersWithAccessControl,
      String permissionGroupId, Instant requestTs) {
    createNewPermissions(usersWithAccessControl, permissionGroupId, requestTs);

    feedService.refreshFeedsForResource(permissionGroupId, requestTs);
  }

  private void createNewPermissions(Map<String, AccessControl> usersWithAccessControl,
      String permissionGroupId, Instant requestTs) {
    var userIds = new ArrayList<>(usersWithAccessControl.keySet());

    var newPermissions = new ArrayList<Permission>();
    userIds.forEach(userId -> newPermissions
        .add(new Permission.Builder(userId, permissionGroupId)
            .withAccessControl(usersWithAccessControl.get(userId))
            .withCreationTs(requestTs)
            .withLastUpdateTs(requestTs)
            .build()));
    permissionDAO.batchPutItem(newPermissions);

    feedService.createFeeds(permissionGroupId, userIds, FEED_ITEM_RESOURCE_TYPE_KEY, requestTs);
  }

  @Override
  public Permission updateAccessControl(String userId, String permissionGroupId,
      AccessControl accessControl, Instant requestTs) throws ResourceNotFoundException {
    var updatedPermission = permissionDAO
        .updateAccessControl(userId, permissionGroupId, accessControl, requestTs);

    feedService.refreshFeedsForResource(permissionGroupId, requestTs);

    return updatedPermission;
  }

  @Override
  public void verifyAccessRight(String userId, String permissionGroupId, AccessRight accessRight)
      throws UnauthorisedException {
    var item = permissionDAO.readItem(userId, permissionGroupId);

    if (item == null || item.getDeleted() || !item.getAccessControl().hasRight(accessRight)) {
      throw new UnauthorisedException(String
          .format("User %s does not have %s permission for permission group id %s", userId,
              accessRight, permissionGroupId));
    }
  }

  @Override
  public List<Permission> getPermissions(String permissionGroupId, Instant lastUpdateTs) {
    return permissionDAO.queryByPermissionGroupId(permissionGroupId, lastUpdateTs);
  }

  @Override
  public List<Permission> getPermissions(User user, String permissionGroupId,
      Instant lastUpdateTs) throws UnauthorisedException {
    var userPermission = permissionDAO.readItem(user.getId(), permissionGroupId);
    if (userPermission == null) {
      throw new UnauthorisedException(String
          .format("User %s is not part of permission group %s", user.getId(), permissionGroupId));
    }

    return permissionDAO.queryByPermissionGroupId(permissionGroupId, lastUpdateTs);
  }

  @Override
  public List<Permission> getPermissionUpdates(User user, Instant lastUpdateTs) {
    var feeds = feedService.getFeedsForUser(user.getId(), FEED_ITEM_RESOURCE_TYPE_KEY, lastUpdateTs,
        FEED_QUERY_SIZE_LIMIT);

    var permissions = new ArrayList<Permission>();
    feeds.forEach(feed ->
        permissions
            .addAll(permissionDAO.queryByPermissionGroupId(feed.getResourceId(), lastUpdateTs))
    );

    return permissions;
  }

  @Override
  public List<Permission> batchSoftDeletePermissions(String userId,
      List<String> permissionGroupIds, Instant requestTs) {
    var permissions = new ArrayList<Permission>();

    for (var permissionGroupId : permissionGroupIds) {
      try {
        permissions.add(permissionDAO.softDeletePermission(userId, permissionGroupId, requestTs));
        feedService.refreshFeedsForResource(permissionGroupId, requestTs);
      } catch (ResourceNotFoundException e) {
        logger.error(String.format("Could not delete permission item for permission group id %s, "
            + "user id %s", permissionGroupId, userId), e);
      }
    }

    return permissions;
  }
}
