package app.planhive.services.permission.persistence;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.Permission;

import org.joda.time.Instant;

import java.util.List;

public interface PermissionDAO {

  void batchPutItem(List<Permission> permissions);

  Permission readItem(String userId, String permissionGroupId);

  /**
   * @param userId            the id of the user
   * @param permissionGroupId the id of the permission group
   * @param accessControl     the rights assigned to the user for the permission group
   * @param requestTs         the timestamp to use for the lastUpdateTs field
   * @return the permission item
   * @throws ResourceNotFoundException if item does not exist or has been deleted
   */
  Permission updateAccessControl(String userId, String permissionGroupId,
      AccessControl accessControl, Instant requestTs) throws ResourceNotFoundException;

  List<Permission> queryByPermissionGroupId(String permissionGroupId, Instant lastUpdateTs);

  Permission softDeletePermission(String userId, String permissionGroupID, Instant requestTs)
      throws ResourceNotFoundException;

}
