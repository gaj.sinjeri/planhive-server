package app.planhive.services.permission.persistence;

import static app.planhive.common.TimeConverter.millisToInstant;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.B;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.BOOL;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.N;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.Permission;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.collect.Lists;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * PermissionDAOImpl provides methods for manipulating Permission items in the database.
 *
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {User Id}                           S
 *    Range Key:        perm|{Permission Group Id}          S
 *  GSI1:
 *    Hash Key:         perm|{Permission Group Id}          S
 *    Range Key:        {Last Update Timestamp}             N
 *  Other attributes:
 *    AccCtrl:          {Access Control}                    S (Binary)
 *    Ts-C:             {Creation Timestamp}                N
 *    Del:              {Deleted}                           B                   Optional
 */
@Repository
public class PermissionDAOImpl extends DynamoDAO implements PermissionDAO {

  private static final String ID_PREFIX = "perm";

  private static final String ATTRIBUTE_ACCESS_CONTROL = "AccCtrl";
  private static final String ATTRIBUTE_CREATION_TS = "Ts-C";
  private static final String ATTRIBUTE_DELETED = "Del";

  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(PermissionDAOImpl.class);

  @Autowired
  PermissionDAOImpl(DynamoDBClient dynamoDBClient) {
    super(ID_PREFIX);

    this.dynamoDB = dynamoDBClient.getDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public void batchPutItem(List<Permission> permissions) {
    for (var partition : Lists.partition(permissions, DynamoDBClient.MAXIMUM_BATCH_PUT_SIZE)) {
      var dbItems = partition.stream()
          .map(item -> new Item()
              .withPrimaryKey(
                  DynamoDBClient.PRIMARY_KEY_H, item.getUserId(),
                  DynamoDBClient.PRIMARY_KEY_R, addPrefix(item.getPermissionGroupId()))
              .withString(DynamoDBClient.GSI1_H, addPrefix(item.getPermissionGroupId()))
              .withLong(ATTRIBUTE_CREATION_TS, item.getCreationTs().getMillis())
              .withLong(DynamoDBClient.GSI1_R, item.getLastUpdateTs().getMillis())
              .withBinary(ATTRIBUTE_ACCESS_CONTROL,
                  item.getAccessControl().getRights().toByteArray())
          ).collect(Collectors.toUnmodifiableList());
      var itemsToWrite = new TableWriteItems(DynamoDBClient.DB_TABLE_NAME).withItemsToPut(dbItems);

      try {
        logger.debug("Starting batchPut operation for permissions");
        var outcome = dynamoDB.batchWriteItem(itemsToWrite);

        // TODO(#59): Replace loop with better solution
        while (!outcome.getUnprocessedItems().isEmpty()) {
          logger.debug("BatchPutting unprocessed permission items");
          outcome = dynamoDB.batchWriteItemUnprocessed(outcome.getUnprocessedItems());
        }
      } catch (Exception e) {
        logger.error("Error executing batchPut operation for permission items");
        throw e;
      }
    }
  }

  @Override
  public Permission readItem(String userId, String permissionGroupId) {
    var spec = new GetItemSpec()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userId,
            DynamoDBClient.PRIMARY_KEY_R, addPrefix(permissionGroupId));

    var item = table.getItem(spec);
    if (item == null) {
      return null;
    }

    return constructPermissionFromDbItem(item);
  }

  @Override
  public Permission updateAccessControl(String userId, String permissionGroupId,
      AccessControl accessControl, Instant requestTs) throws ResourceNotFoundException {
    logger.debug(String
        .format("Updating access control for user id %s, permission group id %s", userId,
            permissionGroupId));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(B(ATTRIBUTE_ACCESS_CONTROL).set(accessControl.getRights().toByteArray()))
        .addUpdate(N(DynamoDBClient.GSI1_R).set(requestTs.getMillis()))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, userId,
        DynamoDBClient.PRIMARY_KEY_R, addPrefix(permissionGroupId))
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var outcome = table.updateItem(updateItemSpec);
      logger.debug(String
          .format("Successfully updated access control for user id %s, permission group id %s",
              userId, permissionGroupId));
      return constructPermissionFromDbItem(outcome.getItem());
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("Attempted to update access control for user id %s, permission group "
          + "id %s, but the item does not exist or has been deleted", userId, permissionGroupId);
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(String
          .format("Error updating access control for user id %s, permission group id %s", userId,
              permissionGroupId));
      throw e;
    }
  }

  @Override
  public List<Permission> queryByPermissionGroupId(String permissionGroupId, Instant lastUpdateTs) {
    logger.debug(String.format(
        "Starting queryByPermissionGroupId operation for permissions with permission group id %s",
        permissionGroupId));

    var index = table.getIndex(DynamoDBClient.GSI1_INDEX);
    var items = new ArrayList<Permission>();

    try {
      var querySpec = new QuerySpec().withKeyConditionExpression("#hk = :id and #rk > :ts")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.GSI1_H)
              .with("#rk", DynamoDBClient.GSI1_R))
          .withValueMap(new ValueMap()
              .withString(":id", addPrefix(permissionGroupId))
              .withLong(":ts", lastUpdateTs.getMillis()));

      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructPermissionFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format("Error executing queryByPermissionGroupId operation "
          + "for permissions with permission group id %s", permissionGroupId));
      throw e;
    }

    return items;
  }

  @Override
  public Permission softDeletePermission(String userId, String permissionGroupId, Instant requestTs)
      throws ResourceNotFoundException {
    logger.debug(String.format("Marking permission item as deleted for permission group "
        + "id %s, user %s", permissionGroupId, userId));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(BOOL(ATTRIBUTE_DELETED).set(true))
        .addUpdate(N(DynamoDBClient.GSI1_R).set(requestTs.getMillis()))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, userId,
        DynamoDBClient.PRIMARY_KEY_R, addPrefix(permissionGroupId))
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var outcome = table.updateItem(updateItemSpec);
      logger.debug(String.format("Successfully marked permission item as deleted for permission "
          + "group id %s, user %s", permissionGroupId, userId));
      return constructPermissionFromDbItem(outcome.getItem());
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("Attempted to mark permission item as deleted for permission group "
          + "id %s, user %s, but the item does not exist", permissionGroupId, userId);
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(String.format("Error marking permission item as deleted for permission group "
          + "id %s, user %s", permissionGroupId, userId));
      throw e;
    }
  }

  private Permission constructPermissionFromDbItem(Item item) {
    return new Permission.Builder(item.getString(DynamoDBClient.PRIMARY_KEY_H),
        removePrefix(item.getString(DynamoDBClient.PRIMARY_KEY_R)))
        .withAccessControl(AccessControl.fromBytes(item.getBinary(ATTRIBUTE_ACCESS_CONTROL)))
        .withCreationTs(millisToInstant(item.getLong(ATTRIBUTE_CREATION_TS)))
        .withLastUpdateTs(millisToInstant(item.getLong(DynamoDBClient.GSI1_R)))
        .setDeleted(item.isPresent(ATTRIBUTE_DELETED) && item.getBoolean(ATTRIBUTE_DELETED))
        .build();
  }
}
