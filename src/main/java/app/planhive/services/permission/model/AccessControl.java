package app.planhive.services.permission.model;

import java.util.BitSet;
import java.util.Objects;

public class AccessControl {

  private final BitSet rights;

  private AccessControl(BitSet rights) {
    this.rights = rights;
  }

  private AccessControl(byte[] bytes) {
    this.rights = BitSet.valueOf(bytes);
  }

  public static AccessControl fromBytes(byte[] bytes) {
    return new AccessControl(bytes);
  }

  public static AccessControl withRights(AccessRight... rights) {
    var bs = new BitSet();
    for (var right : rights) {
      bs.set(right.getValue());
    }
    return new AccessControl(bs);
  }

  public BitSet getRights() {
    return rights;
  }

  public boolean hasRight(AccessRight right) {
    return rights.get(right.getValue());
  }

  @Override
  public String toString() {
    return "AccessControl{" +
        "rights=" + rights +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccessControl that = (AccessControl) o;
    return Objects.equals(rights, that.rights);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rights);
  }
}
