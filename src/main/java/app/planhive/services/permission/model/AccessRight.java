package app.planhive.services.permission.model;

public enum AccessRight {
  READ(0),
  WRITE(1),
  DELETE(2);

  private final int value;

  AccessRight(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
