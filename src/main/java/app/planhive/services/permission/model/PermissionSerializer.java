package app.planhive.services.permission.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class PermissionSerializer extends JsonSerializer<Permission> {

  @Override
  public void serialize(Permission permission, JsonGenerator generator, SerializerProvider provider)
      throws IOException {

    generator.writeStartObject();
    generator.writeStringField("userId", permission.getUserId());
    generator.writeStringField("permissionGroupId", permission.getPermissionGroupId());
    generator.writeBinaryField("permissionFlags",
        permission.getAccessControl().getRights().toByteArray());
    generator.writeNumberField("lastUpdateTs", permission.getLastUpdateTs().getMillis());
    generator.writeBooleanField("deleted", permission.getDeleted());
    generator.writeEndObject();
  }

}
