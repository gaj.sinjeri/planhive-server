package app.planhive.services.permission.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.Instant;

import java.util.Objects;

@JsonSerialize(using = PermissionSerializer.class)
public class Permission {

  private final String userId;
  private final String permissionGroupId;
  private final AccessControl accessControl;
  private final Instant creationTs;
  private final Instant lastUpdateTs;
  private final Boolean deleted;

  private Permission(String userId, String permissionGroupId, AccessControl accessControl,
      Instant creationTs, Instant lastUpdateTs, Boolean deleted) {
    checkNotNull(userId);
    checkNotNull(permissionGroupId);

    this.userId = userId;
    this.permissionGroupId = permissionGroupId;
    this.accessControl = accessControl;
    this.creationTs = creationTs;
    this.lastUpdateTs = lastUpdateTs;
    this.deleted = deleted;
  }

  public static class Builder {

    private final String userId;
    private final String permissionGroupId;
    private AccessControl accessControl;
    private Instant creationTs;
    private Instant lastUpdateTs;
    private Boolean deleted;

    public Builder(String userId, String permissionGroupId) {
      this.userId = userId;
      this.permissionGroupId = permissionGroupId;
    }

    public Builder withAccessControl(AccessControl accessControl) {
      this.accessControl = accessControl;
      return this;
    }

    public Builder withCreationTs(Instant creationTs) {
      this.creationTs = creationTs;
      return this;
    }

    public Builder withLastUpdateTs(Instant lastUpdateTs) {
      this.lastUpdateTs = lastUpdateTs;
      return this;
    }

    public Builder setDeleted(boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public Permission build() {
      return new Permission(userId, permissionGroupId, accessControl, creationTs, lastUpdateTs,
          deleted);
    }
  }

  public String getUserId() {
    return userId;
  }

  public String getPermissionGroupId() {
    return permissionGroupId;
  }

  public AccessControl getAccessControl() {
    return accessControl;
  }

  public Instant getCreationTs() {
    return creationTs;
  }

  public Instant getLastUpdateTs() {
    return lastUpdateTs;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  @Override
  public String toString() {
    return "Permission{" +
        "userId='" + userId + '\'' +
        ", permissionGroupId='" + permissionGroupId + '\'' +
        ", accessControl=" + accessControl +
        ", creationTs=" + creationTs +
        ", lastUpdateTs=" + lastUpdateTs +
        ", deleted=" + deleted +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Permission that = (Permission) o;
    return Objects.equals(userId, that.userId) &&
        Objects.equals(permissionGroupId, that.permissionGroupId) &&
        Objects.equals(accessControl, that.accessControl) &&
        Objects.equals(creationTs, that.creationTs) &&
        Objects.equals(lastUpdateTs, that.lastUpdateTs) &&
        Objects.equals(deleted, that.deleted);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(userId, permissionGroupId, accessControl, creationTs, lastUpdateTs, deleted);
  }
}
