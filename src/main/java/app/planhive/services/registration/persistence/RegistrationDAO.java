package app.planhive.services.registration.persistence;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.model.VersionedItem;
import app.planhive.services.registration.model.EmailAddressVerification;

import org.joda.time.Instant;

import java.util.List;

public interface RegistrationDAO {

  void insertNewItem(EmailAddressVerification item) throws ResourceAlreadyExistsException;

  void updateItem(VersionedItem<EmailAddressVerification> item) throws DatabaseUpdateException;

  VersionedItem<EmailAddressVerification> readItem(EmailAddress email);

  /**
   * Scans the database for email address verification items that have not yet been verified.
   * @param maxTtlTs the maximum TTL timestamp up to which the scan should filter.
   * @return list of versioned email address verification items.
   * The items will only contain the email address attribute.
   */
  List<VersionedItem<EmailAddressVerification>> getNonVerifiedItems(Instant maxTtlTs);

}
