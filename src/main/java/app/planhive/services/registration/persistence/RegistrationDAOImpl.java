package app.planhive.services.registration.persistence;

import static app.planhive.common.TimeConverter.instantToSeconds;
import static app.planhive.common.TimeConverter.millisToInstant;
import static app.planhive.common.TimeConverter.secondsToInstant;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.BOOL;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.N;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.persistence.model.VersionedItem;
import app.planhive.services.registration.model.EmailAddressVerification;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.base.Strings;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * RegistrationDAOImpl provides methods for manipulating Registration items in the database.
 *
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {Email}                             S
 *    Range Key:        "reg"                               S
 *  Other attributes:
 *    Code:             {Registration Code}                 S
 *    NumSent:          {Number of Emails Sent}             N
 *    Reminder:         {Reminder Sent}                     B
 *    Ts-LV:            {Last Verified Timestamp}           N
 *    Ttl:              {Time to Live}                      N
 *    Version:          {Item Version}                      N
 */
@Repository
public class RegistrationDAOImpl extends DynamoDAO implements RegistrationDAO {

  private static final String ID_PREFIX = "reg";

  private static final String ATTRIBUTE_CODE = "Code";
  private static final String ATTRIBUTE_NUMBER_OF_EMAILS_SENT = "NumSent";
  private static final String ATTRIBUTE_REMINDER_SENT = "Reminder";
  private static final String ATTRIBUTE_LAST_VERIFIED_TIMESTAMP = "Ts-LV";
  private static final String ATTRIBUTE_VERSION = "Version";

  private final AmazonDynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(RegistrationDAOImpl.class);

  @Autowired
  RegistrationDAOImpl(DynamoDBClient dynamoDBClient) {
    super(ID_PREFIX);

    this.dynamoDB = dynamoDBClient.getAmazonDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public void insertNewItem(EmailAddressVerification item)
      throws ResourceAlreadyExistsException {
    logger.debug(String.format("Inserting new email verification item for email %s",
        item.getEmailAddress().getValue()));

    var tableItem = new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, item.getEmailAddress().getValue(),
            DynamoDBClient.PRIMARY_KEY_R, ID_PREFIX)
        .withString(ATTRIBUTE_CODE, item.getCode())
        .withInt(ATTRIBUTE_NUMBER_OF_EMAILS_SENT, item.getNumberOfEmailsSent())
        .withLong(DynamoDBClient.ATTRIBUTE_TTL, instantToSeconds(item.getTimeToLive()))
        .withInt(ATTRIBUTE_VERSION, 0);

    try {
      var putItemSpec = new PutItemSpec()
          .withItem(tableItem)
          .withConditionExpression(String
              .format("attribute_not_exists(%s) and attribute_not_exists(%s)",
                  DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R));
      table.putItem(putItemSpec);
      logger.debug(String
          .format("Successfully inserted new email verification item for email %s",
              item.getEmailAddress().getValue()));
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("Email verification item for email %s already exists",
          item.getEmailAddress().getValue());
      logger.warn(msg);
      throw new ResourceAlreadyExistsException(msg);
    } catch (Exception e) {
      logger.error(String
          .format("Error inserting new email verification item for email %s",
              item.getEmailAddress().getValue()));
      throw e;
    }
  }

  @Override
  public void updateItem(VersionedItem<EmailAddressVerification> item)
      throws DatabaseUpdateException {
    var verificationItem = item.getItem();

    logger.debug(String.format("Updating email verification item for email %s",
        verificationItem.getEmailAddress().getValue()));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(N(ATTRIBUTE_VERSION).set(item.getVersion() + 1))
        .withCondition(N(ATTRIBUTE_VERSION).eq(item.getVersion()));

    if (!Strings.isNullOrEmpty(verificationItem.getCode())) {
      xspec.addUpdate(S(ATTRIBUTE_CODE).set(verificationItem.getCode()));
    }
    if (verificationItem.getNumberOfEmailsSent() != null) {
      xspec.addUpdate(
          N(ATTRIBUTE_NUMBER_OF_EMAILS_SENT).set(verificationItem.getNumberOfEmailsSent()));
    }
    if (verificationItem.getReminderSent() != null) {
      xspec.addUpdate(BOOL(ATTRIBUTE_REMINDER_SENT).set(verificationItem.getReminderSent()));
    }
    if (verificationItem.getLastVerifiedTimestamp() != null) {
      xspec.addUpdate(N(ATTRIBUTE_LAST_VERIFIED_TIMESTAMP)
          .set(verificationItem.getLastVerifiedTimestamp().getMillis()));
    }
    if (verificationItem.getTimeToLive() != null) {
      xspec.addUpdate(
          N(DynamoDBClient.ATTRIBUTE_TTL).set(instantToSeconds(verificationItem.getTimeToLive())));
    }

    try {
      table.updateItem(
          DynamoDBClient.PRIMARY_KEY_H, verificationItem.getEmailAddress().getValue(),
          DynamoDBClient.PRIMARY_KEY_R, ID_PREFIX,
          xspec.buildForUpdate());
      logger.debug(String.format("Successfully updated email verification item for email %s",
          verificationItem.getEmailAddress().getValue()));
    } catch (ConditionalCheckFailedException e) {
      var msg = String
          .format("Could not update email verification item for %s due to version mismatch",
              verificationItem.getEmailAddress().getValue());
      logger.warn(msg);
      throw new DatabaseUpdateException(msg);
    } catch (Exception e) {
      logger.error(String.format("Error updating email verification item for email %s",
          verificationItem.getEmailAddress().getValue()));
      throw e;
    }
  }

  @Override
  public VersionedItem<EmailAddressVerification> readItem(EmailAddress emailAddress) {
    var spec = new GetItemSpec()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, emailAddress.getValue(),
            DynamoDBClient.PRIMARY_KEY_R, ID_PREFIX);

    var item = table.getItem(spec);
    if (item == null) {
      return null;
    }

    return constructItemFromDbItem(item);
  }

  @Override
  public List<VersionedItem<EmailAddressVerification>> getNonVerifiedItems(Instant maxTtlTs) {
    var expressionAttributeNames = new HashMap<String, String>();
    expressionAttributeNames.put("#r", DynamoDBClient.PRIMARY_KEY_R);
    expressionAttributeNames.put("#lver", ATTRIBUTE_LAST_VERIFIED_TIMESTAMP);
    expressionAttributeNames.put("#rem", ATTRIBUTE_REMINDER_SENT);
    expressionAttributeNames.put("#ttl", DynamoDBClient.ATTRIBUTE_TTL);

    var expressionAttributeValues = new HashMap<String, AttributeValue>();
    expressionAttributeValues.put(":r", new AttributeValue().withS(ID_PREFIX));
    expressionAttributeValues
        .put(":ttl", new AttributeValue().withN(instantToSeconds(maxTtlTs).toString()));

    Map<String, AttributeValue> lastKeyEvaluated = null;
    var versionedItems = new ArrayList<VersionedItem<EmailAddressVerification>>();

    do {
      var scanRequest = new ScanRequest()
          .withTableName(DynamoDBClient.DB_TABLE_NAME)
          .withFilterExpression("#r = :r and attribute_not_exists(#lver) and "
              + "attribute_not_exists(#rem) and #ttl < :ttl")
          .withExpressionAttributeNames(expressionAttributeNames)
          .withExpressionAttributeValues(expressionAttributeValues)
          .withExclusiveStartKey(lastKeyEvaluated);

      var result = dynamoDB.scan(scanRequest);
      for (var item : result.getItems()) {
        var emailVerificationItem = new EmailAddressVerification.Builder(
            new EmailAddress(item.get(DynamoDBClient.PRIMARY_KEY_H).getS()))
            .withNumberOfEmailsSent(
                Integer.parseInt(item.get(ATTRIBUTE_NUMBER_OF_EMAILS_SENT).getN()))
            .setReminderSent(
                item.containsKey(ATTRIBUTE_REMINDER_SENT) && item.get(ATTRIBUTE_REMINDER_SENT)
                    .getBOOL())
            .withCode(item.get(ATTRIBUTE_CODE).getS())
            .withTimeToLive(
                secondsToInstant(Long.parseLong(item.get(DynamoDBClient.ATTRIBUTE_TTL).getN())))
            .build();

      versionedItems.add(new VersionedItem<>(Integer.parseInt(item.get(ATTRIBUTE_VERSION).getN()),
          emailVerificationItem));
      }

      lastKeyEvaluated = result.getLastEvaluatedKey();
    } while (lastKeyEvaluated != null);

    return versionedItems;
  }

  private VersionedItem<EmailAddressVerification> constructItemFromDbItem(Item item) {
    var verificationItemBuilder = new EmailAddressVerification.Builder(
        new EmailAddress(item.getString(DynamoDBClient.PRIMARY_KEY_H)))
        .withNumberOfEmailsSent(item.getInt(ATTRIBUTE_NUMBER_OF_EMAILS_SENT))
        .setReminderSent(
            item.hasAttribute(ATTRIBUTE_REMINDER_SENT) && item.getBoolean(ATTRIBUTE_REMINDER_SENT))
        .withCode(item.getString(ATTRIBUTE_CODE))
        .withTimeToLive(secondsToInstant(item.getLong(DynamoDBClient.ATTRIBUTE_TTL)));

    if (item.hasAttribute(ATTRIBUTE_LAST_VERIFIED_TIMESTAMP)) {
      verificationItemBuilder
          .withLastVerifiedTimestamp(
              millisToInstant(item.getLong(ATTRIBUTE_LAST_VERIFIED_TIMESTAMP)));
    }

    return new VersionedItem<>(item.getInt(ATTRIBUTE_VERSION), verificationItemBuilder.build());
  }
}
