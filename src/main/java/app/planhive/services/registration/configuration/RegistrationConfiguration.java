package app.planhive.services.registration.configuration;

import app.planhive.model.EmailAddress;
import app.planhive.notification.EmailClient;
import app.planhive.security.SecureGenerator;
import app.planhive.services.registration.persistence.RegistrationDAO;
import app.planhive.services.registration.service.RegistrationReminderService;
import app.planhive.services.registration.service.RegistrationService;
import app.planhive.services.registration.service.RegistrationServiceImpl;
import app.planhive.services.user.service.UserService;

import com.google.common.io.CharStreams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStreamReader;

@Configuration
public class RegistrationConfiguration {

  @Value("${app.name}")
  private String appName;

  @Value("${api.url.verification}")
  private String apiUrlVerification;

  @Value("${registration.email.address}")
  private String registrationEmailAddress;

  @Value("${support.email.address}")
  private String supportEmailAddress;

  @Value("verification-email.html")
  ClassPathResource verificationEmailHtml;

  @Value("verification-email.txt")
  ClassPathResource verificationEmailText;

  @Value("verification-reminder-email.txt")
  ClassPathResource verificationReminderEmailText;

  @Bean
  public RegistrationService registrationService(SecureGenerator secureGenerator,
      EmailClient emailClient, RegistrationDAO registrationDAO, UserService userService)
      throws IOException {
    try (var htmlEmailReader = new InputStreamReader(verificationEmailHtml.getInputStream());
        var textEmailReader = new InputStreamReader(verificationEmailText.getInputStream())) {

      return new RegistrationServiceImpl(secureGenerator, emailClient, registrationDAO, userService,
          appName, apiUrlVerification, new EmailAddress(registrationEmailAddress),
          CharStreams.toString(htmlEmailReader), CharStreams.toString(textEmailReader));
    }
  }

  @Bean
  public RegistrationReminderService registrationReminderService(RegistrationDAO registrationDAO,
      EmailClient emailClient) throws IOException {
    try (var textEmailReader = new InputStreamReader(
        verificationReminderEmailText.getInputStream())) {

      return new RegistrationReminderService(registrationDAO, emailClient, appName,
          new EmailAddress(supportEmailAddress), CharStreams.toString(textEmailReader));
    }
  }
}
