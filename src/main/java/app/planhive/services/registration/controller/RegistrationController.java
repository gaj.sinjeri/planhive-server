package app.planhive.services.registration.controller;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.ResourceType;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.EmailAddress;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.registration.controller.model.RegisterUserRequest;
import app.planhive.services.registration.controller.model.RegisterUserResponse;
import app.planhive.services.registration.controller.model.SendVerificationCodeResponse;
import app.planhive.services.registration.service.RegistrationService;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class RegistrationController {

  private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

  private final RegistrationService registrationService;

  @Autowired
  RegistrationController(RegistrationService registrationService) {
    this.registrationService = registrationService;
  }

  @PostMapping(path = "/user-registration/{emailAddress}/codes")
  public ResponseEntity<RestResponse> sendVerificationCode(
      @PathVariable("emailAddress") String emailAddress) {
    logger.debug(String.format("Received request to start verification for %s", emailAddress));

    try {
      var codesRemaining = registrationService
          .sendVerificationCode(new EmailAddress(emailAddress), Instant.now());
      logger.debug(String.format("Verification code sent for %s", emailAddress));

      return new ResponseEntity<>(
          RestResponse.successResponse(new SendVerificationCodeResponse(codesRemaining)),
          HttpStatus.OK);
    } catch (ResourceAlreadyExistsException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (InvalidOperationException e) {
      logger.debug(e.getMessage());
      return new ResponseEntity<>(
          RestResponse.errorResponse(ErrorCode.MAX_VERIFICATION_CODES_REACHED, e.getData()),
          HttpStatus.FORBIDDEN);
    } catch (DatabaseUpdateException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(path = "/user-registration/{emailAddress}/codes/{code}")
  public ResponseEntity<RestResponse> verifyCode(@PathVariable("emailAddress") String emailAddress,
      @PathVariable("code") String code) {
    logger.debug(String.format("Received request to verify code for %s", emailAddress));

    try {
      var verificationSuccess = registrationService
          .verifyCode(new EmailAddress(emailAddress), code, Instant.now());
      logger.debug(String.format("Code verified for %s", emailAddress));
      return new ResponseEntity<>(RestResponse.successResponse(verificationSuccess), HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    } catch (DatabaseUpdateException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping(path = "/user-registration/{emailAddress}/user", consumes = "multipart/form-data")
  public ResponseEntity<RestResponse> registerUser(
      @PathVariable("emailAddress") String emailAddress,
      @RequestParam("request") RegisterUserRequest request,
      @RequestParam(name = "avatar", required = false) MultipartFile avatar) {
    logger.debug(String.format("Received request to register user for %s", emailAddress));

    try {
      var user = registrationService
          .registerUser(new EmailAddress(emailAddress), request.getUserId(), request.getMac(),
              request.getMacCreationTs(), request.getUsername(), request.getName(), avatar,
              Instant.now());
      var response = new RegisterUserResponse(user, user.getSessionId());
      logger.debug(String.format("User registered for email %s", emailAddress));
      return new ResponseEntity<>(RestResponse.successResponse(response), HttpStatus.OK);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.MAC_INVALID),
          HttpStatus.FORBIDDEN);
    } catch (InvalidImageException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    } catch (InvalidOperationException e) {
      logger.debug(e.getMessage());
      return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.MAC_EXPIRED),
          HttpStatus.FORBIDDEN);
    } catch (ResourceAlreadyExistsException e) {
      if (e.getResourceType() == ResourceType.DB_USERNAME_ITEM) {
        logger.debug(e.getMessage());
        return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.USERNAME_TAKEN),
            HttpStatus.FORBIDDEN);
      } else if (e.getResourceType() == ResourceType.DB_USER_EMAIL_ITEM) {
        logger.debug(e.getMessage());
        return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.EMAIL_TAKEN),
            HttpStatus.FORBIDDEN);
      } else if (e.getResourceType() == ResourceType.DB_USER_ITEM) {
        logger.debug(e.getMessage());
        return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.USER_ALREADY_EXISTS),
            HttpStatus.FORBIDDEN);
      } else {
        logger.error(e.getMessage());
        return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(RegisterUserRequest.class, new RegisterUserRequest.Editor());
  }
}
