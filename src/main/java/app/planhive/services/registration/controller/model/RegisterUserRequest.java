package app.planhive.services.registration.controller.model;

import app.planhive.services.user.model.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class RegisterUserRequest {

  private final String userId;
  private final String username;
  private final String name;
  private final String mac;
  private final Long macCreationTs;

  @JsonCreator
  public RegisterUserRequest(@JsonProperty(value = "userId", required = true) String userId,
      @JsonProperty(value = "username", required = true) String username,
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "mac", required = true) String mac,
      @JsonProperty(value = "macCreationTs", required = true) Long macCreationTs) {
    Preconditions.checkArgument(User.USERNAME_PATTERN.matcher(username).matches(),
        String.format("Username %s does not match pattern", username));
    Preconditions.checkArgument(User.NAME_PATTERN.matcher(name).matches(),
        String.format("Name %s does not match pattern", name));

    this.userId = userId;
    this.username = username;
    this.name = name;
    this.mac = mac;
    this.macCreationTs = macCreationTs;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, RegisterUserRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public String getUserId() {
    return userId;
  }

  public String getUsername() {
    return username;
  }

  public String getName() {
    return name;
  }

  public String getMac() {
    return mac;
  }

  public Long getMacCreationTs() {
    return macCreationTs;
  }

  @Override
  public String toString() {
    return "RegisterUserRequest{" +
        "userId='" + userId + '\'' +
        ", username='" + username + '\'' +
        ", name='" + name + '\'' +
        ", mac='" + mac + '\'' +
        ", macCreationTs=" + macCreationTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterUserRequest that = (RegisterUserRequest) o;
    return Objects.equals(userId, that.userId) &&
        Objects.equals(username, that.username) &&
        Objects.equals(name, that.name) &&
        Objects.equals(mac, that.mac) &&
        Objects.equals(macCreationTs, that.macCreationTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, username, name, mac, macCreationTs);
  }
}
