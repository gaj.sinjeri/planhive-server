package app.planhive.services.registration.controller.model;

import app.planhive.services.user.model.User;

import java.util.Objects;

public class RegisterUserResponse {

  private final User user;
  private final String sessionId;

  public RegisterUserResponse(User user, String sessionId) {
    this.user = user;
    this.sessionId = sessionId;
  }

  public User getUser() {
    return user;
  }

  public String getSessionId() {
    return sessionId;
  }

  @Override
  public String toString() {
    return "RegisterUserResponse{" +
        "user=" + user +
        ", sessionId='" + sessionId + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterUserResponse that = (RegisterUserResponse) o;
    return Objects.equals(user, that.user) &&
        Objects.equals(sessionId, that.sessionId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, sessionId);
  }
}
