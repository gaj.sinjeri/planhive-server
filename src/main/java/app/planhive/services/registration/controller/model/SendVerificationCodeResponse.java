package app.planhive.services.registration.controller.model;

import java.util.Objects;

public class SendVerificationCodeResponse {

  private final int codesRemaining;

  public SendVerificationCodeResponse(int codesRemaining) {
    this.codesRemaining = codesRemaining;
  }

  public int getCodesRemaining() {
    return codesRemaining;
  }

  @Override
  public String toString() {
    return "SendVerificationCodeResponse{" +
        "codesRemaining=" + codesRemaining +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SendVerificationCodeResponse that = (SendVerificationCodeResponse) o;
    return codesRemaining == that.codesRemaining;
  }

  @Override
  public int hashCode() {
    return Objects.hash(codesRemaining);
  }
}
