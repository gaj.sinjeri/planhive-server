package app.planhive.services.registration.common;

import app.planhive.model.EmailAddress;

import org.joda.time.Instant;

import java.util.Base64;

public class EmailMessageId {

  public static String generate(EmailAddress emailAddress, Instant timeToLive,
      int numberOfEmailsSent) {
    // the email address contains invalid characters for a Message-ID, so we encode it to Base64
    var encodedEmailAddress = encodeEmailAddress(emailAddress);

    return String
        .format("%s.%s.%s", encodedEmailAddress, timeToLive.getMillis(), numberOfEmailsSent);
  }

  public static String generate(EmailAddress emailAddress, Instant timeToLive) {
    // the email address contains invalid characters for a Message-ID, so we encode it to Base64
    var encodedEmailAddress = encodeEmailAddress(emailAddress);

    return String.format("%s.%s", encodedEmailAddress, timeToLive.getMillis());
  }

  private static String encodeEmailAddress(EmailAddress emailAddress) {
    return Base64.getUrlEncoder().encodeToString(emailAddress.getValue().getBytes());
  }
}
