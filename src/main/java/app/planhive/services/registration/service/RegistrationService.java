package app.planhive.services.registration.service;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.EmailAddress;
import app.planhive.services.registration.model.VerificationSuccess;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

public interface RegistrationService {

  /**
   * @param emailAddress the email address of the user requesting a verification code.
   * @param requestTs    the timestamp of the request.
   * @return the number of new codes remaining.
   * @throws ResourceAlreadyExistsException if a registration item already exists. This should only
   *                                        ever occur if there are two concurrent requests.
   * @throws InvalidOperationException      if maximum number of codes issued reached.
   * @throws DatabaseUpdateException        if item was updated by another process after this method
   *                                        read it, but before it updated it.
   */
  int sendVerificationCode(EmailAddress emailAddress, Instant requestTs)
      throws ResourceAlreadyExistsException, InvalidOperationException, DatabaseUpdateException;

  VerificationSuccess verifyCode(EmailAddress emailAddress, String code, Instant requestTs)
      throws ResourceNotFoundException, UnauthorisedException, DatabaseUpdateException;

  User registerUser(EmailAddress emailAddress, String userId, String mac, Long macCreationTs,
      String username, String name, MultipartFile avatar, Instant requestTs)
      throws UnauthorisedException, InvalidOperationException, InvalidImageException,
      ResourceAlreadyExistsException;

}
