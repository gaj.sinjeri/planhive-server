package app.planhive.services.registration.service;

import app.planhive.exception.DatabaseUpdateException;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.EmailAddress;
import app.planhive.notification.EmailClient;
import app.planhive.notification.model.Email;
import app.planhive.security.SecureGenerator;
import app.planhive.services.registration.common.EmailMessageId;
import app.planhive.services.registration.model.EmailAddressVerification;
import app.planhive.services.registration.model.TimeRemaining;
import app.planhive.services.registration.model.VerificationSuccess;
import app.planhive.services.registration.persistence.RegistrationDAO;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class RegistrationServiceImpl implements RegistrationService {

  private static final int MAX_NUMBER_OF_EMAILS_SENT = 10;
  private static final int CODE_LIFESPAN = 86400000; // 24 hours
  private static final int MAC_LIFESPAN = 86400000; // 24 hours

  public static final String VERIFICATION_EMAIL_SUBJECT = "Verify your email";

  private final SecureGenerator secureGenerator;
  private final EmailClient emailClient;
  private final RegistrationDAO registrationDAO;
  private final UserService userService;
  private final String appName;
  private final String apiUrlVerification;
  private final EmailAddress registrationEmailAddress;
  private final String verificationEmailHtml;
  private final String verificationEmailText;

  public RegistrationServiceImpl(SecureGenerator secureGenerator, EmailClient emailClient,
      RegistrationDAO registrationDAO, UserService userService, String appName,
      String apiUrlVerification, EmailAddress registrationEmailAddress,
      String verificationEmailHtml, String verificationEmailText) {
    this.secureGenerator = secureGenerator;
    this.emailClient = emailClient;
    this.registrationDAO = registrationDAO;
    this.userService = userService;
    this.appName = appName;
    this.apiUrlVerification = apiUrlVerification;
    this.registrationEmailAddress = registrationEmailAddress;
    this.verificationEmailHtml = verificationEmailHtml;
    this.verificationEmailText = verificationEmailText;
  }

  @Override
  public int sendVerificationCode(EmailAddress emailAddress, Instant requestTs)
      throws ResourceAlreadyExistsException, InvalidOperationException, DatabaseUpdateException {
    var versionedItem = registrationDAO.readItem(emailAddress);

    if (versionedItem == null) {
      var code = secureGenerator.generate128BitKey();

      var newVerificationItem = new EmailAddressVerification.Builder(emailAddress)
          .withCode(code)
          .withNumberOfEmailsSent(1)
          .withTimeToLive(requestTs.toDateTime().plusMillis(CODE_LIFESPAN).toInstant())
          .build();
      registrationDAO.insertNewItem(newVerificationItem);

      var emailId = EmailMessageId
          .generate(newVerificationItem.getEmailAddress(), newVerificationItem.getTimeToLive(),
              newVerificationItem.getNumberOfEmailsSent());
      var email = new Email.Builder(emailId, registrationEmailAddress, emailAddress)
          .withSenderName(appName)
          .withSubject(VERIFICATION_EMAIL_SUBJECT)
          .withHtmlBody(generateVerificationEmailHtml(code))
          .withTextBody(generateVerificationEmailText(code))
          .build();
      emailClient.sendEmail(email);

      return MAX_NUMBER_OF_EMAILS_SENT - 1;
    } else {
      var verificationItem = versionedItem.getItem();

      if (verificationItem.getNumberOfEmailsSent() >= MAX_NUMBER_OF_EMAILS_SENT) {
        var timeRemaining = new TimeRemaining(
            timeRemainingUntilExpiryInMillis(verificationItem, requestTs));
        throw new InvalidOperationException(String
            .format("Maximum number of resend requests reached for email %s",
                verificationItem.getEmailAddress().getValue()), timeRemaining);
      }

      var updatedVerificationItem = new EmailAddressVerification.Builder(
          verificationItem.getEmailAddress())
          .withNumberOfEmailsSent(verificationItem.getNumberOfEmailsSent() + 1)
          .build();
      var updatedVersionedItem = versionedItem.withNewItem(updatedVerificationItem);

      registrationDAO.updateItem(updatedVersionedItem);

      var emailId = EmailMessageId
          .generate(verificationItem.getEmailAddress(), verificationItem.getTimeToLive(),
              verificationItem.getNumberOfEmailsSent() + 1);
      var email = new Email.Builder(emailId, registrationEmailAddress, emailAddress)
          .withSenderName(appName)
          .withSubject(VERIFICATION_EMAIL_SUBJECT)
          .withHtmlBody(generateVerificationEmailHtml(verificationItem.getCode()))
          .withTextBody(generateVerificationEmailText(verificationItem.getCode()))
          .build();
      emailClient.sendEmail(email);

      return MAX_NUMBER_OF_EMAILS_SENT - updatedVerificationItem.getNumberOfEmailsSent();
    }
  }

  @Override
  public VerificationSuccess verifyCode(EmailAddress emailAddress, String code, Instant requestTs)
      throws ResourceNotFoundException, UnauthorisedException, DatabaseUpdateException {
    var versionedItem = registrationDAO.readItem(emailAddress);

    if (versionedItem == null) {
      throw new ResourceNotFoundException(String
          .format("No registration item found for email address %s", emailAddress.getValue()));
    }

    if (!versionedItem.getItem().getCode().equals(code)) {
      throw new UnauthorisedException("Invalid verification code");
    }

    var updatedVerificationItem = new EmailAddressVerification.Builder(emailAddress)
        .withLastVerifiedTimestamp(requestTs)
        .build();
    var updatedVersionedItem = versionedItem.withNewItem(updatedVerificationItem);
    registrationDAO.updateItem(updatedVersionedItem);

    var existingUserId = userService.getUserId(emailAddress);
    if (existingUserId != null) {
      var newSessionId = secureGenerator.generate256BitKey();
      var updatedUser = userService.updateSessionId(existingUserId, newSessionId, requestTs);

      return new VerificationSuccess.Builder(existingUserId)
          .withSessionId(updatedUser.getSessionId())
          .build();
    } else {
      var userId = secureGenerator.generateUUID();
      var mac = secureGenerator.generateMAC(
          List.of(emailAddress.getValue(), userId, Long.toString(requestTs.getMillis())));

      return new VerificationSuccess.Builder(userId)
          .withMac(mac, requestTs.getMillis())
          .build();
    }
  }

  @Override
  public User registerUser(EmailAddress emailAddress, String userId, String mac, Long macCreationTs,
      String username, String name, MultipartFile avatar, Instant requestTs)
      throws UnauthorisedException, InvalidOperationException, InvalidImageException,
      ResourceAlreadyExistsException {
    var expectedMac = secureGenerator
        .generateMAC(List.of(emailAddress.getValue(), userId, Long.toString(macCreationTs)));

    if (!mac.equals(expectedMac)) {
      throw new UnauthorisedException("Provided mac does not match expected mac");
    }

    if ((requestTs.getMillis() - macCreationTs) > MAC_LIFESPAN) {
      throw new InvalidOperationException("Mac has expired");
    }

    return userService.createUser(userId, username, name, emailAddress, avatar, requestTs);
  }

  private String generateVerificationEmailHtml(String code) {
    var url = generateVerificationUrl(code);

    return verificationEmailHtml.replaceAll("%link", url);
  }

  private String generateVerificationEmailText(String code) {
    var url = generateVerificationUrl(code);

    return verificationEmailText.replaceAll("%link", url);
  }

  private String generateVerificationUrl(String code) {
    return String.format("https://%s/%s", apiUrlVerification, code);
  }

  private Long timeRemainingUntilExpiryInMillis(EmailAddressVerification item, Instant requestTs) {
    return Math.max(0, item.getTimeToLive().getMillis() - requestTs.getMillis());
  }
}
