package app.planhive.services.registration.service;

import app.planhive.model.EmailAddress;
import app.planhive.notification.EmailClient;
import app.planhive.notification.model.Email;
import app.planhive.services.registration.common.EmailMessageId;
import app.planhive.services.registration.model.EmailAddressVerification;
import app.planhive.services.registration.persistence.RegistrationDAO;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class RegistrationReminderService {

  private static final int SCHEDULE_INTERVAL_IN_MINUTES = 60;
  private static final int MAXIMUM_VERIFICATION_ITEM_TTL_IN_HOURS = 23;

  private static final String EMAIL_VERIFICATION_REMINDER_SUBJECT =
      "Reminder: Verify your email address";

  private final RegistrationDAO registrationDAO;
  private final EmailClient emailClient;
  private final String appName;
  private final EmailAddress supportEmailAddress;
  private final String verificationReminderEmailText;

  private static final Logger logger = LoggerFactory.getLogger(RegistrationReminderService.class);

  public RegistrationReminderService(RegistrationDAO registrationDAO, EmailClient emailClient,
      String appName, EmailAddress supportEmailAddress, String verificationReminderEmailText) {
    this.emailClient = emailClient;
    this.registrationDAO = registrationDAO;
    this.appName = appName;
    this.supportEmailAddress = supportEmailAddress;
    this.verificationReminderEmailText = verificationReminderEmailText;

    var executorService = Executors.newSingleThreadScheduledExecutor();
    executorService.scheduleAtFixedRate(this::sendReminderEmails, 1, SCHEDULE_INTERVAL_IN_MINUTES,
        TimeUnit.MINUTES);

    logger.info("Registration Reminder Service initialized");
  }

  public void sendReminderEmails() {
    try {
      var maxTtlTs = DateTime.now().plusHours(MAXIMUM_VERIFICATION_ITEM_TTL_IN_HOURS).toInstant();

      var unverifiedEmailAddressItems = registrationDAO.getNonVerifiedItems(maxTtlTs);

      for (var unverifiedEmailAddressItem : unverifiedEmailAddressItems) {
        var verificationItem = unverifiedEmailAddressItem.getItem();

        var emailMessageId = EmailMessageId
            .generate(verificationItem.getEmailAddress(), verificationItem.getTimeToLive());
        var email = new Email.Builder(emailMessageId, supportEmailAddress,
            verificationItem.getEmailAddress())
            .withSenderName(appName)
            .withSubject(EMAIL_VERIFICATION_REMINDER_SUBJECT)
            .withTextBody(verificationReminderEmailText)
            .build();
        emailClient.sendEmail(email);

        var updatedVerificationItem = new EmailAddressVerification.Builder(
            verificationItem.getEmailAddress())
            .setReminderSent(true)
            .build();
        var updatedVersionedItem = unverifiedEmailAddressItem.withNewItem(updatedVerificationItem);

        registrationDAO.updateItem(updatedVersionedItem);
      }
    } catch (Exception e) {
      logger.error("Could not send verification reminder email", e);
    }
  }
}
