package app.planhive.services.registration.model;

import java.util.Objects;

public class TimeRemaining {

  private final Long timeRemaining;

  public TimeRemaining(Long timeRemaining) {
    this.timeRemaining = timeRemaining;
  }

  public Long getTimeRemaining() {
    return timeRemaining;
  }

  @Override
  public String toString() {
    return "TimeRemaining{" +
        "timeRemaining=" + timeRemaining +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TimeRemaining that = (TimeRemaining) o;
    return Objects.equals(timeRemaining, that.timeRemaining);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timeRemaining);
  }
}
