package app.planhive.services.registration.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.model.EmailAddress;

import org.joda.time.Instant;

import java.util.Objects;

public class EmailAddressVerification {

  private final EmailAddress emailAddress;
  private final String code;
  private final Integer numberOfEmailsSent;
  private final Boolean reminderSent;
  private final Instant lastVerifiedTimestamp;
  private final Instant timeToLive;

  private EmailAddressVerification(EmailAddress emailAddress, String code,
      Integer numberOfEmailsSent, Boolean reminderSent, Instant lastVerifiedTimestamp,
      Instant timeToLive) {
    checkNotNull(emailAddress);

    this.emailAddress = emailAddress;
    this.code = code;
    this.numberOfEmailsSent = numberOfEmailsSent;
    this.reminderSent = reminderSent;
    this.lastVerifiedTimestamp = lastVerifiedTimestamp;
    this.timeToLive = timeToLive;
  }

  public static class Builder {

    private final EmailAddress emailAddress;
    private String code;
    private Integer numberOfEmailsSent;
    private Boolean reminderSent;
    private Instant lastVerifiedTimestamp;
    private Instant timeToLive;

    public Builder(EmailAddress emailAddress) {
      this.emailAddress = emailAddress;
    }

    public Builder withCode(String code) {
      this.code = code;
      return this;
    }

    public Builder withNumberOfEmailsSent(Integer numberOfEmailsSent) {
      this.numberOfEmailsSent = numberOfEmailsSent;
      return this;
    }

    public Builder setReminderSent(Boolean reminderSent) {
      this.reminderSent = reminderSent;
      return this;
    }

    public Builder withLastVerifiedTimestamp(Instant lastVerifiedTimestamp) {
      this.lastVerifiedTimestamp = lastVerifiedTimestamp;
      return this;
    }

    public Builder withTimeToLive(Instant timeToLive) {
      this.timeToLive = timeToLive;
      return this;
    }

    public EmailAddressVerification build() {
      return new EmailAddressVerification(emailAddress, code, numberOfEmailsSent, reminderSent,
          lastVerifiedTimestamp, timeToLive);
    }
  }

  public EmailAddress getEmailAddress() {
    return emailAddress;
  }

  public String getCode() {
    return code;
  }

  public Integer getNumberOfEmailsSent() {
    return numberOfEmailsSent;
  }

  public Boolean getReminderSent() {
    return reminderSent;
  }

  public Instant getLastVerifiedTimestamp() {
    return lastVerifiedTimestamp;
  }

  public Instant getTimeToLive() {
    return timeToLive;
  }

  @Override
  public String toString() {
    return "EmailAddressVerification{" +
        "emailAddress=" + emailAddress +
        ", code='" + code + '\'' +
        ", numberOfEmailsSent=" + numberOfEmailsSent +
        ", reminderSent=" + reminderSent +
        ", lastVerifiedTimestamp=" + lastVerifiedTimestamp +
        ", timeToLive=" + timeToLive +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmailAddressVerification that = (EmailAddressVerification) o;
    return Objects.equals(emailAddress, that.emailAddress) &&
        Objects.equals(code, that.code) &&
        Objects.equals(numberOfEmailsSent, that.numberOfEmailsSent) &&
        Objects.equals(reminderSent, that.reminderSent) &&
        Objects.equals(lastVerifiedTimestamp, that.lastVerifiedTimestamp) &&
        Objects.equals(timeToLive, that.timeToLive);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emailAddress, code, numberOfEmailsSent, reminderSent, lastVerifiedTimestamp,
        timeToLive);
  }
}
