package app.planhive.services.registration.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

public class VerificationSuccess {

  private final String userId;
  private final String sessionId;
  private final String mac;
  private final Long macCreationTs;

  private VerificationSuccess(String userId, String sessionId, String mac, Long macCreationTs) {
    checkNotNull(userId);
    checkArgument(sessionId != null || (mac != null && macCreationTs != null),
        "Either sessionId or [mac, macCreationTs] must be set");
    checkArgument(sessionId == null || (mac == null && macCreationTs == null),
        "Only one of [sessionId, [mac, macCreationTs]] can be set");

    this.userId = userId;
    this.sessionId = sessionId;
    this.mac = mac;
    this.macCreationTs = macCreationTs;
  }

  public static class Builder {

    private final String userId;
    private String sessionId;
    private String mac;
    private Long macCreationTs;

    public Builder(String userId) {
      this.userId = userId;
    }

    public Builder withSessionId(String sessionId) {
      this.sessionId = sessionId;
      return this;
    }

    public Builder withMac(String mac, Long macCreationTs) {
      this.mac = mac;
      this.macCreationTs = macCreationTs;
      return this;
    }

    public VerificationSuccess build() {
      return new VerificationSuccess(userId, sessionId, mac, macCreationTs);
    }
  }

  public String getUserId() {
    return userId;
  }

  public String getSessionId() {
    return sessionId;
  }

  public String getMac() {
    return mac;
  }

  public Long getMacCreationTs() {
    return macCreationTs;
  }

  @Override
  public String toString() {
    return "VerificationSuccess{" +
        "userId='" + userId + '\'' +
        ", sessionId='" + sessionId + '\'' +
        ", mac='" + mac + '\'' +
        ", macCreationTs=" + macCreationTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VerificationSuccess that = (VerificationSuccess) o;
    return Objects.equals(userId, that.userId) &&
        Objects.equals(sessionId, that.sessionId) &&
        Objects.equals(mac, that.mac) &&
        Objects.equals(macCreationTs, that.macCreationTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, sessionId, mac, macCreationTs);
  }
}
