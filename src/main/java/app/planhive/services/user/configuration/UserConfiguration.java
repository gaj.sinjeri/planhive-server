package app.planhive.services.user.configuration;

import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.user.notification.UserNotificationSender;
import app.planhive.services.user.notification.UserNotificationSenderImpl;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.services.user.service.ContactService;
import app.planhive.services.user.service.NotificationService;
import app.planhive.services.user.service.UserService;
import app.planhive.services.user.service.UserServiceImpl;
import app.planhive.threading.TaskRunner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfiguration {

  @Value("${user.avatar.max.resolution}")
  private int maxAvatarResolution;

  @Value("${user.avatar.thumbnail.resolution}")
  private int maxAvatarThumbnailResolution;

  @Value("${user.avatar.micro.resolution}")
  private int maxAvatarMicroResolution;

  @Bean
  public UserService userService(UserDAO userDAO, ContactService contactService,
      CloudStore cloudStore, SecureGenerator secureGenerator, TaskRunner taskRunner) {
    return new UserServiceImpl(userDAO, contactService, cloudStore, secureGenerator, taskRunner,
        maxAvatarResolution, maxAvatarThumbnailResolution, maxAvatarMicroResolution);
  }

  @Bean
  public UserNotificationSender userNotificationSender(NotificationService notificationService) {
    return new UserNotificationSenderImpl(notificationService);
  }
}
