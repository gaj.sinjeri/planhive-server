package app.planhive.services.user.persistence;

import static app.planhive.persistence.DynamoDAO.addPrefix;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_CREATION_TS;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_EMAIL;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_LAST_AVATAR_UPDATE_TS;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_LAST_UPDATE_TS;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_NAME;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_NOTIFICATION_COUNT;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_SESSION_ID;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_USERNAME;
import static app.planhive.services.user.persistence.UserDAOImpl.ATTRIBUTE_USER_ID;
import static app.planhive.services.user.persistence.UserDAOImpl.USERNAME_ITEM_PREFIX;
import static app.planhive.services.user.persistence.UserDAOImpl.USER_EMAIL_ITEM_PREFIX;
import static app.planhive.services.user.persistence.UserDAOImpl.USER_ITEM_PREFIX;

import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.model.User;

import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Delete;
import com.amazonaws.services.dynamodbv2.model.Put;
import com.amazonaws.services.dynamodbv2.model.TransactWriteItem;
import com.amazonaws.services.dynamodbv2.model.Update;
import org.joda.time.Instant;

import java.util.HashMap;
import java.util.Map;

public class UserDAOTransactionAction {

  public static TransactWriteItem generatePutUserItemAction(User user) {
    var item = new HashMap<String, AttributeValue>();
    item.put(DynamoDBClient.PRIMARY_KEY_H, new AttributeValue(user.getId()));
    item.put(DynamoDBClient.PRIMARY_KEY_R, new AttributeValue(USER_ITEM_PREFIX));
    item.put(ATTRIBUTE_USERNAME, new AttributeValue(user.getUsername()));
    item.put(ATTRIBUTE_EMAIL, new AttributeValue(user.getEmailAddress().getValue()));
    item.put(ATTRIBUTE_SESSION_ID, new AttributeValue(user.getSessionId()));

    var notificationCount = new AttributeValue();
    notificationCount.setN(Integer.toString(user.getNotificationCount()));
    item.put(ATTRIBUTE_NOTIFICATION_COUNT, notificationCount);

    item.put(ATTRIBUTE_NAME, new AttributeValue(user.getName()));

    var creationTs = new AttributeValue();
    creationTs.setN(Long.toString(user.getCreationTs().getMillis()));
    item.put(ATTRIBUTE_CREATION_TS, creationTs);

    var lastUpdateTs = new AttributeValue();
    lastUpdateTs.setN(Long.toString(user.getLastUpdateTs().getMillis()));
    item.put(ATTRIBUTE_LAST_UPDATE_TS, lastUpdateTs);

    var lastAvatarUpdateTs = new AttributeValue();
    lastAvatarUpdateTs.setN(Long.toString(user.getLastAvatarUpdateTs().getMillis()));
    item.put(ATTRIBUTE_LAST_AVATAR_UPDATE_TS, lastAvatarUpdateTs);

    return new TransactWriteItem().withPut(
        new Put()
            .withTableName(DynamoDBClient.DB_TABLE_NAME)
            .withItem(item)
            .withConditionExpression(String
                .format("attribute_not_exists(%s) and attribute_not_exists(%s)",
                    DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R)));
  }

  public static TransactWriteItem generateUpdateUserItemAction(String userId, String username,
      Instant requestTs) {
    var key = Map.of(
        DynamoDBClient.PRIMARY_KEY_H, new AttributeValue(userId),
        DynamoDBClient.PRIMARY_KEY_R, new AttributeValue("user")
    );

    return new TransactWriteItem().withUpdate(
        new Update()
            .withTableName(DynamoDBClient.DB_TABLE_NAME)
            .withKey(key)
            .withUpdateExpression("SET #username = :username, #ts = :ts")
            .withExpressionAttributeNames(
                new NameMap()
                    .with("#username", ATTRIBUTE_USERNAME)
                    .with("#ts", ATTRIBUTE_LAST_UPDATE_TS))
            .withExpressionAttributeValues(
                Map.of(
                    ":username", new AttributeValue(username),
                    ":ts", new AttributeValue(Long.toString(requestTs.getMillis())))
            )
            .withConditionExpression(String.format("attribute_exists(%s) and attribute_exists(%s)",
                DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R)));
  }

  public static TransactWriteItem generatePutUsernameItemAction(String userId, String username) {
    var item = new HashMap<String, AttributeValue>();
    item
        .put(DynamoDBClient.PRIMARY_KEY_H, new AttributeValue(username.substring(0, 3)));
    item
        .put(DynamoDBClient.PRIMARY_KEY_R,
            new AttributeValue(addPrefix(USERNAME_ITEM_PREFIX, username.substring(3))));
    item.put(ATTRIBUTE_USER_ID, new AttributeValue(userId));

    return new TransactWriteItem().withPut(
        new Put()
            .withTableName(DynamoDBClient.DB_TABLE_NAME)
            .withItem(item)
            .withConditionExpression(String
                .format("attribute_not_exists(%s) and attribute_not_exists(%s)",
                    DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R)));
  }

  public static TransactWriteItem generateDeleteUsernameItemAction(String username) {
    var key = Map.of(
        DynamoDBClient.PRIMARY_KEY_H,
        new AttributeValue(username.substring(0, 3)),
        DynamoDBClient.PRIMARY_KEY_R,
        new AttributeValue(addPrefix(USERNAME_ITEM_PREFIX, username.substring(3)))
    );

    return new TransactWriteItem().withDelete(
        new Delete()
            .withTableName(DynamoDBClient.DB_TABLE_NAME)
            .withKey(key)
            .withConditionExpression(String.format("attribute_exists(%s) and attribute_exists(%s)",
                DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R)));
  }

  public static TransactWriteItem generatePutUserEmailItemAction(User user) {
    var item = new HashMap<String, AttributeValue>();
    item.put(DynamoDBClient.PRIMARY_KEY_H, new AttributeValue(user.getEmailAddress().getValue()));
    item.put(DynamoDBClient.PRIMARY_KEY_R,
        new AttributeValue(addPrefix(USER_EMAIL_ITEM_PREFIX, user.getId())));

    return new TransactWriteItem().withPut(
        new Put()
            .withTableName(DynamoDBClient.DB_TABLE_NAME)
            .withItem(item)
            .withConditionExpression(String
                .format("attribute_not_exists(%s) and attribute_not_exists(%s)",
                    DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R)));
  }
}
