package app.planhive.services.user.persistence;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.EmailAddress;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface UserDAO {

  User putItem(User user) throws ResourceAlreadyExistsException;

  String updateUsername(String userId, String oldUsername, String newUsername, Instant requestTs)
      throws ResourceAlreadyExistsException;

  User updateUser(User user, Instant requestTs) throws ResourceNotFoundException;

  User getUser(String userId);

  List<User> batchGetUser(List<String> userIds);

  String getUserId(String username);

  String getUserId(EmailAddress emailAddress);

  List<String> queryUserIdsByUsername(String beginsWith, int maxResultSize);

  void updateNotificationCount(String userId, int notificationCount)
      throws ResourceNotFoundException;

  List<User> batchGetUserWithNotificationCountIncrement(List<String> userIds);

  void removeNotificationToken(String userId);

}
