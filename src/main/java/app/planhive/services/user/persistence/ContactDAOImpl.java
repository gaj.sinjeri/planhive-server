package app.planhive.services.user.persistence;

import static app.planhive.common.TimeConverter.millisToInstant;
import static app.planhive.persistence.DynamoDBClient.MAXIMUM_BATCH_GET_SIZE;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.exception.InvalidOperationException;
import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;
import app.planhive.services.user.persistence.model.ContactUpdate;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.TransactWriteItemsRequest;
import com.amazonaws.services.dynamodbv2.model.TransactionCanceledException;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.collect.Lists;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * ContactDAOImpl provides methods for manipulating Contact items in the database.
 *
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {User Id}                           S
 *    Range Key:        ct|{Contact's User Id}              S
 *  LSI1:
 *    Hash Key:         {Primary Key Hash Key}              S
 *    Range Key:        ct|{Last Update Timestamp}          S
 *  Other attributes:
 *    ConState:         {Contact State}                     S
 */
@Repository
public class ContactDAOImpl extends DynamoDAO implements ContactDAO {

  protected static final String CONTACT_ID_PREFIX = "ct";

  protected static final String ATTRIBUTE_CONTACT_STATE = "ConState";

  private final AmazonDynamoDB amazonDynamoDB;
  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(ContactDAOImpl.class);

  @Autowired
  ContactDAOImpl(DynamoDBClient dynamoDBClient) {
    super(CONTACT_ID_PREFIX);

    this.amazonDynamoDB = dynamoDBClient.getAmazonDynamoDB();
    this.dynamoDB = dynamoDBClient.getDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public void updateContacts(List<ContactUpdate> updates) throws InvalidOperationException {
    var actions = updates
        .stream()
        .map(ContactDAOTransactionAction::generateUpdateContactItemAction)
        .collect(Collectors.toUnmodifiableList());

    var transactionRequest = new TransactWriteItemsRequest().withTransactItems(actions);

    try {
      amazonDynamoDB.transactWriteItems(transactionRequest);
      logger.debug("Successfully updated contacts");
    } catch (TransactionCanceledException e) {
      var conditionsFailed = e.getCancellationReasons()
          .stream()
          .filter(reason -> reason.getCode().equals(CONDITIONAL_CHECK_FAILED_CODE))
          .count();
      if (conditionsFailed > 0) {
        var msg = String.format("%s condition(s) failed. Could not update contacts from update "
            + "items: %s", conditionsFailed, updates);
        logger.warn(msg);
        throw new InvalidOperationException(msg);
      } else {
        throw e;
      }
    } catch (Exception e) {
      logger.error(String.format("Could not update contacts from update items: %s", updates));
      throw e;
    }
  }

  @Override
  public void refreshLastUpdateTs(String userId, String contactId, Instant requestTs) {
    logger.debug(String
        .format("Refreshing contact lastUpdateTs for user id %s, contact id %s", userId,
            contactId));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(
            S(DynamoDBClient.LSI1_R).set(addPrefix(Long.toString(requestTs.getMillis(), 36))))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists())
            .and(S(DynamoDBClient.LSI1_R).lt(addPrefix(Long.toString(requestTs.getMillis(), 36)))));

    try {
      table.updateItem(
          DynamoDBClient.PRIMARY_KEY_H, userId,
          DynamoDBClient.PRIMARY_KEY_R, addPrefix(contactId),
          xspec.buildForUpdate());
      logger.debug(String
          .format("Successfully refreshed contact lastUpdateTs for user id %s, contact id %s",
              userId, contactId));
    } catch (ConditionalCheckFailedException e) {
      logger.warn(String.format("Could not refresh contact lastUpdateTs for user id %s, "
          + "contact id %s because either the item does not exist or an item with "
          + "a newer timestamp already exists", userId, contactId));
    } catch (Exception e) {
      logger.error(String
          .format("Error refreshing contact lastUpdateTs for user id %s, contact id %s", userId,
              contactId));
      throw e;
    }
  }

  @Override
  public List<Contact> queryByUserId(String userId) {
    checkNotNull(userId);

    logger.debug(String.format("Starting queryByUserId operation for user %s", userId));
    var querySpec = new QuerySpec()
        .withKeyConditionExpression("#n = :v and begins_with(#p, :p)")
        .withNameMap(
            new NameMap()
                .with("#n", DynamoDBClient.PRIMARY_KEY_H)
                .with("#p", DynamoDBClient.PRIMARY_KEY_R))
        .withValueMap(
            new ValueMap()
                .withString(":v", userId)
                .withString(":p", String.format("%s|", CONTACT_ID_PREFIX)));

    var contacts = new ArrayList<Contact>();

    try {
      var dbItems = table.query(querySpec);
      dbItems.forEach(dbItem -> contacts.add(constructContactFromDbItem(dbItem)));
    } catch (Exception e) {
      logger
          .error(String.format("Error executing queryByUserId operation for user %s", userId));
      throw e;
    }

    return contacts;
  }

  @Override
  public List<Contact> queryByUserIdWithTsFilter(String userId, Instant fromTs) {
    logger.debug(
        String.format("Starting queryByUserIdWithTsFilter operation for user %s", userId));
    var querySpec = new QuerySpec()
        .withKeyConditionExpression("#n = :v and #rk > :ts")
        .withNameMap(
            new NameMap()
                .with("#n", DynamoDBClient.PRIMARY_KEY_H)
                .with("#rk", DynamoDBClient.LSI1_R))
        .withValueMap(
            new ValueMap()
                .withString(":v", userId)
                .withString(":ts", addPrefix(Long.toString(fromTs.getMillis(), 36))));

    var index = table.getIndex(DynamoDBClient.LSI1_INDEX);
    var contacts = new ArrayList<Contact>();

    try {
      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> contacts.add(constructContactFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String
          .format("Error executing queryByUserIdWithTsFilter operation for user %s", userId));
      throw e;
    }

    return contacts;
  }

  @Override
  public List<Contact> batchGetItem(String userId, List<String> contactIds) {
    logger.debug(String.format("Starting batchGetItem operation for user id %s", userId));

    if (contactIds.isEmpty()) {
      return List.of();
    }

    var contacts = new ArrayList<Contact>();

    for (var partition : Lists.partition(contactIds, MAXIMUM_BATCH_GET_SIZE)) {
      var tableKeysAndAttributes = new TableKeysAndAttributes(DynamoDBClient.DB_TABLE_NAME);
      for (var contactId : partition) {
        tableKeysAndAttributes.addHashAndRangePrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userId,
            DynamoDBClient.PRIMARY_KEY_R, addPrefix(contactId));
      }

      try {
        var outcome = dynamoDB.batchGetItem(tableKeysAndAttributes);
        for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
          contacts.add(constructContactFromDbItem(item));
        }

        while (!outcome.getUnprocessedKeys().isEmpty()) {
          outcome = dynamoDB.batchGetItemUnprocessed(outcome.getUnprocessedKeys());
          for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
            contacts.add(constructContactFromDbItem(item));
          }
        }
      } catch (Exception e) {
        logger.error(
            String.format("Error executing batchGetItem operation for user id %s", userId));
        throw e;
      }
    }

    return contacts;
  }

  private Contact constructContactFromDbItem(Item item) {
    return new Contact(
        ContactState.valueOf(item.getString(ATTRIBUTE_CONTACT_STATE)),
        item.getString(DynamoDBClient.PRIMARY_KEY_H),
        removePrefix(item.getString(DynamoDBClient.PRIMARY_KEY_R)),
        millisToInstant(Long.parseLong(removePrefix(item.getString(DynamoDBClient.LSI1_R)), 36)));
  }
}
