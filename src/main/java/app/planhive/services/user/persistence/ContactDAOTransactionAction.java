package app.planhive.services.user.persistence;

import static app.planhive.persistence.DynamoDAO.addPrefix;

import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.persistence.model.ContactUpdate;
import app.planhive.services.user.persistence.model.ContactUpdateComparisonOperator;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Put;
import com.amazonaws.services.dynamodbv2.model.TransactWriteItem;

import java.util.HashMap;
import java.util.Map;

public class ContactDAOTransactionAction {

  public static TransactWriteItem generateUpdateContactItemAction(ContactUpdate update) {
    var contact = update.getContact();

    var item = new HashMap<String, AttributeValue>();

    item.put(DynamoDBClient.PRIMARY_KEY_H, new AttributeValue(contact.getUserId()));
    item.put(DynamoDBClient.PRIMARY_KEY_R,
        new AttributeValue(addPrefix(ContactDAOImpl.CONTACT_ID_PREFIX, contact.getContactId())));
    item.put(DynamoDBClient.LSI1_R, new AttributeValue(
        addPrefix(ContactDAOImpl.CONTACT_ID_PREFIX, Long.toString(contact.getLastUpdateTs().getMillis(), 36))));
    item.put(ContactDAOImpl.ATTRIBUTE_CONTACT_STATE, new AttributeValue(contact.getContactState().name()));

    if (update.getCondition() == null) {
      return new TransactWriteItem().withPut(
          new Put()
              .withTableName(DynamoDBClient.DB_TABLE_NAME)
              .withItem(item));
    } else {
      return new TransactWriteItem().withPut(
          new Put()
              .withTableName(DynamoDBClient.DB_TABLE_NAME)
              .withItem(item)
              .withConditionExpression(generateUpdateExpression(update.getOperator()))
              .withExpressionAttributeNames(Map.of("#k", ContactDAOImpl.ATTRIBUTE_CONTACT_STATE))
              .withExpressionAttributeValues(
                  Map.of(":v", new AttributeValue(update.getCondition().name()))));
    }
  }

  private static String generateUpdateExpression(ContactUpdateComparisonOperator operator) {
    if (operator == ContactUpdateComparisonOperator.EQUALS) {
      return "#k=:v";
    } else if (operator == ContactUpdateComparisonOperator.NOT_EQUALS) {
      return "#k<>:v";
    } else {
      throw new InternalError(String.format("Unsupported enum value: %s", operator));
    }
  }
}
