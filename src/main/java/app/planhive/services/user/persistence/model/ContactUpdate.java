package app.planhive.services.user.persistence.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;

import java.util.Objects;

public class ContactUpdate {

  private final Contact contact;
  private final ContactState condition;
  private final ContactUpdateComparisonOperator operator;

  public ContactUpdate(Contact contact) {
    checkNotNull(contact);

    this.contact = contact;
    this.condition = null;
    this.operator = null;
  }

  public ContactUpdate(Contact contact, ContactState condition,
      ContactUpdateComparisonOperator operator) {
    checkNotNull(contact);
    checkNotNull(condition);
    checkNotNull(operator);

    this.contact = contact;
    this.condition = condition;
    this.operator = operator;
  }

  public Contact getContact() {
    return contact;
  }

  public ContactState getCondition() {
    return condition;
  }

  public ContactUpdateComparisonOperator getOperator() {
    return operator;
  }

  @Override
  public String toString() {
    return "ContactUpdate{" +
        "contact=" + contact +
        ", condition=" + condition +
        ", operator=" + operator +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContactUpdate that = (ContactUpdate) o;
    return Objects.equals(contact, that.contact) &&
        condition == that.condition &&
        operator == that.operator;
  }

  @Override
  public int hashCode() {
    return Objects.hash(contact, condition, operator);
  }
}
