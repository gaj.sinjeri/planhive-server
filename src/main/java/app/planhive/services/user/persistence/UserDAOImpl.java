package app.planhive.services.user.persistence;

import static app.planhive.common.TimeConverter.millisToInstant;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.N;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.ResourceType;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.user.model.User;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.TransactWriteItemsRequest;
import com.amazonaws.services.dynamodbv2.model.TransactionCanceledException;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.collect.Lists;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/*
 * UserDAOImpl provides methods for manipulating User items in the database.
 *
 * USER ITEM:
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {User Id}                           S
 *    Range Key:        "user"                              S
 *  Other attributes:
 *    Username:         {Username}                          S
 *    Email:            {Email}                             S
 *    Session:          {Session Id}                        S
 *    NotTok:           {Notification Token}                S                   Optional
 *    NotC:             {Notification Count}                N
 *    Name:             {Full Name}                         S
 *    Ts-C:             {Creation Timestamp}                N
 *    Ts-LU:            {Last Update Timestamp}             N
 *    Ts-LAU:           {Last Avatar Update Timestamp}      N
 *    Del:              {Deleted}                           BOOL                Optional
 *
 * USERNAME ITEM:
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {First Three Letters of Username}   S
 *    Range Key:        username|{Remainder}                S
 *  Other attributes:
 *    UserId:           {User Id}                           S
 *
 * USER-EMAIL ITEM:
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {Email}                             S
 *    Range Key:        email|{User Id}                     S
 */
@Repository
public class UserDAOImpl implements UserDAO {

  protected static final String USER_ITEM_PREFIX = "user";
  protected static final String USERNAME_ITEM_PREFIX = "username";
  protected static final String USER_EMAIL_ITEM_PREFIX = "email";

  protected static final String ATTRIBUTE_USERNAME = "Username";
  protected static final String ATTRIBUTE_EMAIL = "Email";
  protected static final String ATTRIBUTE_SESSION_ID = "Session";
  protected static final String ATTRIBUTE_NOTIFICATION_TOKEN = "NotTok";
  protected static final String ATTRIBUTE_NOTIFICATION_COUNT = "NotC";
  protected static final String ATTRIBUTE_NAME = "Name";
  protected static final String ATTRIBUTE_CREATION_TS = "Ts-C";
  protected static final String ATTRIBUTE_LAST_UPDATE_TS = "Ts-LU";
  protected static final String ATTRIBUTE_LAST_AVATAR_UPDATE_TS = "Ts-LAU";
  protected static final String ATTRIBUTE_DELETED = "Del";

  protected static final String ATTRIBUTE_USER_ID = "UserId";

  private final AmazonDynamoDB amazonDynamoDB;
  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

  @Autowired
  UserDAOImpl(DynamoDBClient dynamoDBClient) {
    this.amazonDynamoDB = dynamoDBClient.getAmazonDynamoDB();
    this.dynamoDB = dynamoDBClient.getDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public User putItem(User user) throws ResourceAlreadyExistsException {
    logger.debug(String.format("Putting user items in DB for user %s", user.getId()));

    var putUserItemAction = UserDAOTransactionAction.generatePutUserItemAction(user);
    var putUsernameItemAction = UserDAOTransactionAction.generatePutUsernameItemAction(user.getId(),
        user.getUsername());
    var putUserEmailItemAction = UserDAOTransactionAction.generatePutUserEmailItemAction(user);

    var transactionRequest = new TransactWriteItemsRequest()
        .withTransactItems(
            List.of(putUserItemAction, putUsernameItemAction, putUserEmailItemAction));

    try {
      amazonDynamoDB.transactWriteItems(transactionRequest);
      logger.debug(String.format("Successfully put user items in DB for user %s", user.getId()));

      return user;
    } catch (TransactionCanceledException e) {
      if (e.getCancellationReasons().get(0).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("User item already exists. User id %s", user.getId());
        logger.warn(msg);
        throw new ResourceAlreadyExistsException(msg, ResourceType.DB_USER_ITEM);
      } else if (e.getCancellationReasons().get(1).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("Username item already exists. Username %s", user.getUsername());
        logger.warn(msg);
        throw new ResourceAlreadyExistsException(msg, ResourceType.DB_USERNAME_ITEM);
      } else if (e.getCancellationReasons().get(2).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("User email item already exists. Email %s",
            user.getEmailAddress().getValue());
        logger.warn(msg);
        throw new ResourceAlreadyExistsException(msg, ResourceType.DB_USER_EMAIL_ITEM);
      } else {
        throw e;
      }
    } catch (Exception e) {
      logger.error(String.format("Error putting user items in DB for user %s", user.getId()));
      throw e;
    }
  }

  @Override
  public String updateUsername(String userId, String oldUsername, String newUsername,
      Instant requestTs) throws ResourceAlreadyExistsException {
    logger.debug(String.format("Updating username for user with id %s", userId));

    var deleteUsernameItemAction = UserDAOTransactionAction
        .generateDeleteUsernameItemAction(oldUsername);
    var putUsernameItemAction = UserDAOTransactionAction
        .generatePutUsernameItemAction(userId, newUsername);
    var updateUserItemAction = UserDAOTransactionAction
        .generateUpdateUserItemAction(userId, newUsername, requestTs);

    var transactionRequest = new TransactWriteItemsRequest()
        .withTransactItems(
            List.of(deleteUsernameItemAction, putUsernameItemAction, updateUserItemAction));

    try {
      amazonDynamoDB.transactWriteItems(transactionRequest);
      logger
          .debug(String.format("Successfully updated username for user with id %s", userId));

      return newUsername;
    } catch (TransactionCanceledException e) {
      if (e.getCancellationReasons().get(0).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("Username item not found. Username %s", oldUsername);
        logger.error(msg);
        throw new InternalError(msg);
      } else if (e.getCancellationReasons().get(1).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("Username item already exists. Username %s", newUsername);
        logger.warn(msg);
        throw new ResourceAlreadyExistsException(msg, ResourceType.DB_USERNAME_ITEM);
      } else if (e.getCancellationReasons().get(2).getCode()
          .equals(DynamoDAO.CONDITIONAL_CHECK_FAILED_CODE)) {
        var msg = String.format("No user with id %s exists", userId);
        logger.error(msg);
        throw new InternalError(msg);
      } else {
        throw e;
      }
    } catch (Exception e) {
      logger.error(String.format("Error updating username for user with id %s", userId));
      throw e;
    }
  }

  @Override
  public User updateUser(User user, Instant requestTs) throws ResourceNotFoundException {
    logger.debug(String.format("Updating user with id %s", user.getId()));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(N(ATTRIBUTE_LAST_UPDATE_TS).set(requestTs.getMillis()))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    if (user.getSessionId() != null) {
      xspec.addUpdate(S(ATTRIBUTE_SESSION_ID).set(user.getSessionId()));
    }
    if (user.getNotificationToken() != null) {
      xspec.addUpdate(S(ATTRIBUTE_NOTIFICATION_TOKEN).set(user.getNotificationToken()));
    }
    if (user.getName() != null) {
      xspec.addUpdate(S(ATTRIBUTE_NAME).set(user.getName()));
    }
    if (user.getLastAvatarUpdateTs() != null) {
      xspec.addUpdate(
          N(ATTRIBUTE_LAST_AVATAR_UPDATE_TS).set(user.getLastAvatarUpdateTs().getMillis()));
    }

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, user.getId(),
        DynamoDBClient.PRIMARY_KEY_R, USER_ITEM_PREFIX)
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var outcome = table.updateItem(updateItemSpec);
      logger.debug(String.format("Successfully updated user with id %s", user.getId()));
      return constructUserFromDbItem(outcome.getItem());
    } catch (ConditionalCheckFailedException e) {
      var msg = String
          .format("Attempted to update user with id %s, but the item does not exist", user.getId());
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(String.format("Error updating user with id %s", user.getId()));
      throw e;
    }
  }

  @Override
  public User getUser(String userId) {
    var item = getUserItem(userId);

    return item != null ? constructUserFromDbItem(item) : null;
  }

  private Item getUserItem(String userId) {
    try {
      return table.getItem(
          new GetItemSpec()
              .withPrimaryKey(
                  DynamoDBClient.PRIMARY_KEY_H, userId,
                  DynamoDBClient.PRIMARY_KEY_R, USER_ITEM_PREFIX));
    } catch (Exception e) {
      logger.error(String.format("Error reading user item in DB for user %s", userId));
      throw e;
    }
  }

  @Override
  public List<User> batchGetUser(List<String> userIds) {
    if (userIds.isEmpty()) {
      return List.of();
    }

    logger.debug(String.format("Starting batchGet operation for users with ids %s", userIds));

    var users = new ArrayList<User>();

    for (var partition : Lists.partition(userIds, DynamoDBClient.MAXIMUM_BATCH_GET_SIZE)) {
      var tableKeysAndAttributes = new TableKeysAndAttributes(DynamoDBClient.DB_TABLE_NAME);
      for (var userId : partition) {
        tableKeysAndAttributes.addHashAndRangePrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, userId,
            DynamoDBClient.PRIMARY_KEY_R, USER_ITEM_PREFIX);
      }

      try {
        var outcome = dynamoDB.batchGetItem(tableKeysAndAttributes);
        for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
          users.add(constructUserFromDbItem(item));
        }

        while (!outcome.getUnprocessedKeys().isEmpty()) {
          outcome = dynamoDB.batchGetItemUnprocessed(outcome.getUnprocessedKeys());
          for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
            users.add(constructUserFromDbItem(item));
          }
        }
      } catch (Exception e) {
        logger.error(String
            .format("Error executing batchWriteItem operation for users with ids: %s", userIds));
        throw e;
      }
    }

    return users;
  }

  @Override
  public String getUserId(String username) {
    logger.debug(String.format("Starting getUserId from username {%s} operation", username));

    var spec = new GetItemSpec()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, username.substring(0, 3),
            DynamoDBClient.PRIMARY_KEY_R,
            DynamoDAO.addPrefix(USERNAME_ITEM_PREFIX, username.substring(3)));

    try {
      var outcome = table.getItem(spec);

      return outcome != null ? outcome.getString(ATTRIBUTE_USER_ID) : null;
    } catch (Exception e) {
      logger.error(String.format("Error reading username item for username %s", username));
      throw e;
    }
  }

  @Override
  public String getUserId(EmailAddress emailAddress) {
    logger.debug(String
        .format("Starting getUserId from email {%s} operation", emailAddress.getValue()));

    var userIds = new ArrayList<String>();
    try {
      var querySpec = new QuerySpec().withKeyConditionExpression("#h = :h and begins_with(#p, :p)")
          .withNameMap(
              new NameMap()
                  .with("#h", DynamoDBClient.PRIMARY_KEY_H)
                  .with("#p", DynamoDBClient.PRIMARY_KEY_R))
          .withValueMap(
              new ValueMap()
                  .withString(":h", emailAddress.getValue())
                  .withString(":p", USER_EMAIL_ITEM_PREFIX));

      var dbItems = table.query(querySpec);
      dbItems.forEach(dbItem -> userIds.add(
          DynamoDAO.removePrefix(
              USER_EMAIL_ITEM_PREFIX,
              dbItem.getString(DynamoDBClient.PRIMARY_KEY_R))));
    } catch (Exception e) {
      logger.error(String
          .format("Error querying user item by email in DB for email: %s",
              emailAddress.getValue()));
      throw e;
    }

    if (userIds.size() > 1) {
      throw new InternalError(String
          .format("Query returned more than 1 user email item with email %s",
              emailAddress.getValue()));
    }

    return userIds.size() != 0 ? userIds.get(0) : null;
  }

  @Override
  public List<String> queryUserIdsByUsername(String beginsWith, int maxResultSize) {
    logger.debug(String
        .format("Starting query user ids by username that begins with %s operation", beginsWith));

    if (beginsWith.length() < 3) {
      throw new InternalError("Username query length must be at least 3 characters long");
    }

    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#h = :h and begins_with(#b, :b)")
          .withNameMap(
              new NameMap()
                  .with("#h", DynamoDBClient.PRIMARY_KEY_H)
                  .with("#b", DynamoDBClient.PRIMARY_KEY_R))
          .withValueMap(
              new ValueMap()
                  .withString(":h", beginsWith.substring(0, 3))
                  .withString(":b",
                      DynamoDAO.addPrefix(USERNAME_ITEM_PREFIX, beginsWith.substring(3))))
          .withMaxResultSize(maxResultSize);

      var dbItems = table.query(querySpec);

      var userIds = new ArrayList<String>();
      for (var dbItem : dbItems) {
        userIds.add(dbItem.getString(ATTRIBUTE_USER_ID));
      }
      return userIds;
    } catch (Exception e) {
      logger.error(String.format("Error querying username items that begin with %s", beginsWith));
      throw e;
    }
  }

  @Override
  public void updateNotificationCount(String userId, int notificationCount)
      throws ResourceNotFoundException {
    logger.debug(String.format("Updating notification count for user with id %s", userId));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(N(ATTRIBUTE_NOTIFICATION_COUNT).set(notificationCount))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, userId,
        DynamoDBClient.PRIMARY_KEY_R, USER_ITEM_PREFIX)
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      table.updateItem(updateItemSpec);
      logger.debug(
          String.format("Successfully updated notification count for user with id %s", userId));
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("Attempted to update notification count for user with id %s, "
          + "but the item does not exist", userId);
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(String.format("Error updating notification count for user with id %s", userId));
      throw e;
    }
  }

  @Override
  public List<User> batchGetUserWithNotificationCountIncrement(List<String> userIds) {
    var notificationTokens = new ArrayList<User>();

    for (var userId : userIds) {
      logger.debug(String.format("Incrementing notification count and retrieving notification "
          + "token for user with id %s", userId));

      var xspec = new ExpressionSpecBuilder()
          .addUpdate(N(ATTRIBUTE_NOTIFICATION_COUNT).add(1))
          .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
              .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

      var updateExpression = xspec.buildForUpdate();
      var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
          DynamoDBClient.PRIMARY_KEY_H, userId,
          DynamoDBClient.PRIMARY_KEY_R, USER_ITEM_PREFIX)
          .withUpdateExpression(updateExpression.getUpdateExpression())
          .withValueMap(updateExpression.getValueMap())
          .withNameMap(updateExpression.getNameMap())
          .withConditionExpression(updateExpression.getConditionExpression())
          .withReturnValues(ReturnValue.ALL_NEW);

      try {
        var outcome = table.updateItem(updateItemSpec);
        logger.debug(String
            .format("Successfully incremented notification count for user with id %s", userId));
        notificationTokens.add(constructUserFromDbItem(outcome.getItem()));
      } catch (ConditionalCheckFailedException e) {
        var msg = String.format("Attempted to increment notification count for user with id %s, "
            + "but the user item does not exist", userId);
        logger.error(msg);
      } catch (Exception e) {
        logger.error(String.format("Error incrementing notification count and retrieving "
            + "notification token for user with id %s", userId));
        throw e;
      }
    }

    return notificationTokens;
  }

  @Override
  public void removeNotificationToken(String userId) {
    logger.debug(String.format("Removing notification token for user with id %s", userId));

    var key = new PrimaryKey(DynamoDBClient.PRIMARY_KEY_H, userId, DynamoDBClient.PRIMARY_KEY_R,
        USER_ITEM_PREFIX);
    var update = new AttributeUpdate(ATTRIBUTE_NOTIFICATION_TOKEN).delete();

    table.updateItem(key, update);
  }

  private User constructUserFromDbItem(Item item) {
    var builder = new User.Builder(item.getString(DynamoDBClient.PRIMARY_KEY_H))
        .withNotificationCount(item.hasAttribute(ATTRIBUTE_NOTIFICATION_COUNT) ? item
            .getInt(ATTRIBUTE_NOTIFICATION_COUNT) : 0)
        .withSessionId(item.getString(ATTRIBUTE_SESSION_ID))
        .withUsername(item.getString(ATTRIBUTE_USERNAME))
        .withName(item.getString(ATTRIBUTE_NAME))
        .withEmailAddress(new EmailAddress(item.getString(ATTRIBUTE_EMAIL)))
        .withCreationTs(millisToInstant(item.getLong(ATTRIBUTE_CREATION_TS)))
        .withLastUpdateTs(millisToInstant(item.getLong(ATTRIBUTE_LAST_UPDATE_TS)))
        .withLastAvatarUpdateTs(millisToInstant(item.getLong(ATTRIBUTE_LAST_AVATAR_UPDATE_TS)))
        .setDeleted(item.isPresent(ATTRIBUTE_DELETED) && item.getBoolean(ATTRIBUTE_DELETED));

    if (item.isPresent(ATTRIBUTE_NOTIFICATION_TOKEN)) {
      builder.withNotificationToken(item.getString(ATTRIBUTE_NOTIFICATION_TOKEN));
    }

    return builder.build();
  }
}
