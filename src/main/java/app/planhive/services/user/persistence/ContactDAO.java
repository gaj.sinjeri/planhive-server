package app.planhive.services.user.persistence;

import app.planhive.exception.InvalidOperationException;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.persistence.model.ContactUpdate;

import org.joda.time.Instant;

import java.util.List;

public interface ContactDAO {

  void updateContacts(List<ContactUpdate> updates) throws InvalidOperationException;

  void refreshLastUpdateTs(String userId, String contactId, Instant requestTs);

  List<Contact> queryByUserId(String userId);

  List<Contact> queryByUserIdWithTsFilter(String contactId, Instant fromTs);

  List<Contact> batchGetItem(String userId, List<String> contactIds);

}
