package app.planhive.services.user.persistence.model;

public enum ContactUpdateComparisonOperator {
  EQUALS,
  NOT_EQUALS
}
