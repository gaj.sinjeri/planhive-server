package app.planhive.services.user.service;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.EmailAddress;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

  User createUser(String userId, String username, String name, EmailAddress emailAddress,
      MultipartFile avatar, Instant requestTs)
      throws InvalidImageException, ResourceAlreadyExistsException;

  User updateUserDetails(User user, String name, String username, Instant lastAvatarUpdateTs,
      MultipartFile avatar, Instant requestTs)
      throws InvalidImageException, ResourceAlreadyExistsException, ResourceNotFoundException;

  User updateSessionId(String userId, String sessionId, Instant requestTs)
      throws ResourceNotFoundException;

  String getUserId(String username);

  String getUserId(EmailAddress emailAddress);

  User getUser(String userId);

  List<User> getUsers(List<String> userIds);

  byte[] getAvatar(String userId) throws ResourceNotFoundException;

  byte[] getAvatarThumbnail(String userId) throws ResourceNotFoundException;

  byte[] getAvatarMicro(String userId) throws ResourceNotFoundException;

  List<User> queryUsersByUsername(String beginsWith);

}
