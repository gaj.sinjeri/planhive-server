package app.planhive.services.user.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.notification.NotificationClient;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.threading.TaskRunner;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class NotificationServiceImpl implements NotificationService {

  private static final String RESOURCE_TYPE = "typ";

  // TODO: using UserDAO directly to avoid a circular dependency. Ideally, the notification service
  //  would have its own DAO layer
  private final UserDAO userDAO;
  private final FeedService feedService;
  private final NotificationClient notificationClient;
  private final TaskRunner taskRunner;

  @Autowired
  NotificationServiceImpl(UserDAO userDAO, FeedService feedService,
      NotificationClient notificationClient, TaskRunner taskRunner) {
    this.userDAO = userDAO;
    this.feedService = feedService;
    this.notificationClient = notificationClient;
    this.taskRunner = taskRunner;
  }

  @Override
  public void updateNotificationToken(User user, String notificationToken, Instant requestTs)
      throws ResourceNotFoundException {
    if (notificationToken != null) {
      var userUpdate = new User.Builder(user.getId())
          .withNotificationToken(notificationToken)
          .build();
      userDAO.updateUser(userUpdate, requestTs);
    } else {
      userDAO.removeNotificationToken(user.getId());
    }
  }

  @Override
  public void updateNotificationCount(String userId, int notificationCount)
      throws ResourceNotFoundException {
    userDAO.updateNotificationCount(userId, notificationCount);
  }

  /**
   * This method starts a new async task and returns immediately
   */
  @Override
  public void sendNotificationByUserIds(Notification notification, List<String> userIds,
      boolean withCounterIncrement) {
    taskRunner.run(() -> {
      var registrationTokens = retrieveRegistrationTokens(userIds, withCounterIncrement);
      sendNotifications(notification, registrationTokens);
    });
  }

  /**
   * This method starts a new async task and returns immediately
   */
  @Override
  public void sendNotificationByNodeId(Notification notification, NodeId nodeId,
      List<String> excludedUserIds, boolean withCounterIncrement) {
    taskRunner.run(() -> {
      var feeds = feedService.getFeedsByResourceId(nodeId.toFeedItemKey());
      var userIds = feeds.stream()
          .filter(feed -> !feed.getMute())
          .map(Feed::getUserId)
          .filter(userId -> !excludedUserIds.contains(userId))
          .collect(Collectors.toUnmodifiableList());

      var registrationTokens = retrieveRegistrationTokens(userIds, withCounterIncrement);

      sendNotifications(notification, registrationTokens);
    });
  }

  private void sendNotifications(Notification notification,
      Map<String, Integer> registrationTokensWithCount) {
    notificationClient
        .sendNotifications(registrationTokensWithCount, notification.getTitle(),
            notification.getBody(),
            constructData(notification), notification.getCategory());
  }

  private Map<String, Integer> retrieveRegistrationTokens(List<String> userIds,
      boolean withCounterIncrement) {
    List<User> users;

    if (withCounterIncrement) {
      users = userDAO.batchGetUserWithNotificationCountIncrement(userIds);
    } else {
      users = userDAO.batchGetUser(userIds);
    }

    return users.stream()
        .filter(user -> user.getNotificationToken() != null)
        .filter(distinctByKey(User::getNotificationToken))
        .collect(Collectors.toMap(User::getNotificationToken, User::getNotificationCount));
  }

  private Map<String, String> constructData(Notification notification) {
    var data = new HashMap<String, String>();
    if (notification.getResourceType() != null) {
      data.put(RESOURCE_TYPE, notification.getResourceType());
    }
    if (notification.getAdditionalData() != null) {
      data.putAll(notification.getAdditionalData());
    }
    return data;
  }

  private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = ConcurrentHashMap.newKeySet();
    return t -> seen.add(keyExtractor.apply(t));
  }
}
