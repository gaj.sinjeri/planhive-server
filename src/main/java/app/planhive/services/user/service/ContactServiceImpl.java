package app.planhive.services.user.service;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactState;
import app.planhive.services.user.model.ContactWithUserView;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.UserPrivateView;
import app.planhive.services.user.notification.UserNotificationSender;
import app.planhive.services.user.persistence.ContactDAO;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.services.user.persistence.model.ContactUpdate;
import app.planhive.services.user.persistence.model.ContactUpdateComparisonOperator;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

  private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

  private final UserDAO userDAO;
  private final ContactDAO contactDAO;
  private final UserNotificationSender userNotificationSender;

  @Autowired
  ContactServiceImpl(UserDAO userDAO, ContactDAO contactDAO,
      UserNotificationSender userNotificationSender) {
    this.userDAO = userDAO;
    this.contactDAO = contactDAO;
    this.userNotificationSender = userNotificationSender;
  }

  @Override
  public List<Contact> batchGetContact(String userId, List<String> contactIds) {
    return contactDAO.batchGetItem(userId, contactIds);
  }

  @Override
  public List<ContactWithUserView> getContactUpdates(User user, Instant lastUpdateTs) {
    var contacts = contactDAO.queryByUserIdWithTsFilter(user.getId(), lastUpdateTs);
    if (contacts.isEmpty()) {
      return List.of();
    }

    var contactsWithUserViews = new ArrayList<ContactWithUserView>();
    for (var contact : contacts) {
      var contactUserItem = userDAO.getUser(contact.getContactId());
      contactsWithUserViews
          .add(new ContactWithUserView(contact, UserPrivateView.fromUser(contactUserItem)));
    }

    return contactsWithUserViews;
  }

  @Retryable(
      value = {Exception.class},
      maxAttempts = 4, backoff = @Backoff(2000))
  @Override
  public void refreshContacts(String userId, Instant requestTs) {
    var contacts = contactDAO.queryByUserId(userId);

    for (var contact : contacts) {
      contactDAO.refreshLastUpdateTs(contact.getContactId(), contact.getUserId(), requestTs);
    }
  }

  @Override
  public Contact createContactRequest(User requester, String inviteeId, Instant requestTs)
      throws ResourceNotFoundException, InvalidOperationException {
    logger.debug(String.format("Creating contact request for user id %s, "
        + "invitee id %s", requester.getId(), inviteeId));

    var invitee = userDAO.getUser(inviteeId);
    if (invitee == null) {
      throw new ResourceNotFoundException(
          String.format("User with id %s does not exist", inviteeId));
    }

    var requesterContact = new Contact(ContactState.REQUESTED, requester.getId(), inviteeId,
        requestTs);
    var inviteeContact = new Contact(ContactState.INVITED, inviteeId, requester.getId(), requestTs);

    contactDAO.updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.NOT_EQUALS),
        new ContactUpdate(inviteeContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.NOT_EQUALS)
    ));

    userNotificationSender.sendContactRequestNotification(requester, inviteeId);

    return requesterContact;
  }

  @Override
  public Contact respondToContactRequest(String requesterId, User invitee, boolean accepted,
      Instant requestTs) throws InvalidOperationException {
    var contactState = accepted ? ContactState.CONNECTED : ContactState.REJECTED;

    var requesterContact = new Contact(contactState, requesterId, invitee.getId(),
        requestTs);
    var inviteeContact = new Contact(contactState, invitee.getId(), requesterId,
        requestTs);

    contactDAO.updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.REQUESTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.INVITED,
            ContactUpdateComparisonOperator.EQUALS)
    ));

    if (accepted) {
      userNotificationSender.sendContactRequestAcceptedNotification(invitee, requesterId);
    }

    return inviteeContact;
  }

  @Override
  public Contact cancelContactRequest(User requester, String inviteeId, Instant requestTs)
      throws InvalidOperationException {
    var requesterContact = new Contact(ContactState.REJECTED, requester.getId(), inviteeId,
        requestTs);
    var inviteeContact = new Contact(ContactState.REJECTED, inviteeId, requester.getId(),
        requestTs);

    contactDAO.updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.REQUESTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.INVITED,
            ContactUpdateComparisonOperator.EQUALS)
    ));

    return requesterContact;
  }

  @Override
  public Contact deleteContact(User user, String contactId, Instant requestTs)
      throws InvalidOperationException {
    var requesterContact = new Contact(ContactState.DELETED, user.getId(), contactId,
        requestTs);
    var inviteeContact = new Contact(ContactState.DELETED, contactId, user.getId(),
        requestTs);

    contactDAO.updateContacts(List.of(
        new ContactUpdate(requesterContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.EQUALS),
        new ContactUpdate(inviteeContact, ContactState.CONNECTED,
            ContactUpdateComparisonOperator.EQUALS)
    ));

    return requesterContact;
  }
}
