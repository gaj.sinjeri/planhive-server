package app.planhive.services.user.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface NotificationService {

  void updateNotificationToken(User user, String notificationToken, Instant requestTs)
      throws ResourceNotFoundException;

  void updateNotificationCount(String userId, int notificationCount)
      throws ResourceNotFoundException;

  void sendNotificationByUserIds(Notification notification, List<String> userIds,
      boolean withCounterIncrement);

  void sendNotificationByNodeId(Notification notification, NodeId nodeId,
      List<String> excludedUserIds, boolean withCounterIncrement);

}
