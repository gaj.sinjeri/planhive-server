package app.planhive.services.user.service;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.user.model.Contact;
import app.planhive.services.user.model.ContactWithUserView;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface ContactService {

  List<Contact> batchGetContact(String userId, List<String> contactIds);

  List<ContactWithUserView> getContactUpdates(User user, Instant lastUpdateTs);

  void refreshContacts(String userId, Instant requestTs);

  Contact createContactRequest(User requester, String inviteeId, Instant requestTs)
      throws ResourceNotFoundException, InvalidOperationException;

  Contact respondToContactRequest(String requesterId, User invitee, boolean accepted,
      Instant requestTs) throws InvalidOperationException;

  Contact cancelContactRequest(User requester, String inviteeId, Instant requestTs)
      throws InvalidOperationException;

  Contact deleteContact(User user, String contactId, Instant requestTs)
      throws InvalidOperationException;

}
