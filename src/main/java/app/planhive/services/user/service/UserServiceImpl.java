package app.planhive.services.user.service;

import static app.planhive.services.user.model.User.NAME_PATTERN;
import static app.planhive.services.user.model.User.USERNAME_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;

import app.planhive.common.Image;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.EmailAddress;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.user.model.User;
import app.planhive.services.user.persistence.UserDAO;
import app.planhive.threading.TaskRunner;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.imageio.ImageIO;

public class UserServiceImpl implements UserService {

  private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  private static final String AVATAR_ORIGINAL_URI = "users/%s/avatar.jpeg";
  private static final String AVATAR_THUMBNAIL_URI = "users/%s/t-avatar.jpeg";
  private static final String AVATAR_MICRO_URI = "users/%s/m-avatar.jpeg";
  private static final int USERNAME_QUERY_MAX_RESULT_SIZE = 10;

  private final UserDAO userDAO;
  private final ContactService contactService;
  private final CloudStore cloudStore;
  private final SecureGenerator secureGenerator;
  private final TaskRunner taskRunner;
  private final int maxAvatarResolution;
  private final int maxAvatarThumbnailResolution;
  private final int maxAvatarMicroResolution;

  public UserServiceImpl(UserDAO userDAO, ContactService contactService, CloudStore cloudStore,
      SecureGenerator secureGenerator, TaskRunner taskRunner, int maxAvatarResolution,
      int maxAvatarThumbnailResolution, int maxAvatarMicroResolution) {
    this.userDAO = userDAO;
    this.contactService = contactService;
    this.cloudStore = cloudStore;
    this.secureGenerator = secureGenerator;
    this.taskRunner = taskRunner;
    this.maxAvatarResolution = maxAvatarResolution;
    this.maxAvatarThumbnailResolution = maxAvatarThumbnailResolution;
    this.maxAvatarMicroResolution = maxAvatarMicroResolution;
  }

  @Override
  public User createUser(String userId, String username, String name, EmailAddress emailAddress,
      MultipartFile avatar, Instant requestTs)
      throws InvalidImageException, ResourceAlreadyExistsException {
    if (avatar != null) {
      storeAvatar(avatar, userId);
    }

    var sessionId = secureGenerator.generate256BitKey();
    var user = new User.Builder(userId)
        .withSessionId(sessionId)
        .withNotificationCount(0)
        .withUsername(username)
        .withName(name)
        .withEmailAddress(emailAddress)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .withLastAvatarUpdateTs(avatar != null ? requestTs : new Instant(0))
        .build();

    return userDAO.putItem(user);
  }

  private void storeAvatar(MultipartFile avatar, String userId) throws InvalidImageException {
    if (!Objects.equals(avatar.getContentType(), "image/jpeg")) {
      throw new InvalidImageException("Image must be in jpeg format");
    }

    BufferedImage original;
    BufferedImage thumbnail;
    BufferedImage micro;

    try {
      original = ImageIO.read(avatar.getInputStream());
      Image.verifyResolution(original, maxAvatarResolution);

      thumbnail = Image.resize(original, maxAvatarThumbnailResolution);
      micro = Image.resize(original, maxAvatarMicroResolution);
    } catch (IOException e) {
      var msg = "Error handling image. This could be either due to not being able to access "
          + "the input stream of the original image or create thumbnail";
      logger.error(msg);
      throw new InternalError(msg, e);
    }

    var originalFileName = String.format(AVATAR_ORIGINAL_URI, userId);
    var thumbnailFileName = String.format(AVATAR_THUMBNAIL_URI, userId);
    var microFileName = String.format(AVATAR_MICRO_URI, userId);
    cloudStore.storeImage(originalFileName, original);
    cloudStore.storeImage(thumbnailFileName, thumbnail);
    cloudStore.storeImage(microFileName, micro);
  }

  @Override
  public User updateUserDetails(User user, String name, String username, Instant lastAvatarUpdateTs,
      MultipartFile avatar, Instant requestTs)
      throws InvalidImageException, ResourceAlreadyExistsException, ResourceNotFoundException {
    checkArgument(username == null || USERNAME_PATTERN.matcher(username).matches(),
        String.format("Username %s does not match pattern", username));
    checkArgument(name == null || NAME_PATTERN.matcher(name).matches(),
        String.format("Name %s does not match pattern", name));

    var updatedUserBuilder = new User.Builder(user.getId());

    if (avatar != null) {
      storeAvatar(avatar, user.getId());
    }

    if (name != null || avatar != null || user.getLastAvatarUpdateTs() != lastAvatarUpdateTs) {
      var userUpdate = new User.Builder(user.getId())
          .withName(name)
          .withLastAvatarUpdateTs(avatar != null ? requestTs : lastAvatarUpdateTs)
          .build();
      var updatedUser = userDAO.updateUser(userUpdate, requestTs);

      updatedUserBuilder
          .withName(updatedUser.getName())
          .withUsername(updatedUser.getUsername())
          .withLastUpdateTs(updatedUser.getLastUpdateTs())
          .withLastAvatarUpdateTs(updatedUser.getLastAvatarUpdateTs());
    }

    if (username != null) {
      var newUsername = userDAO
          .updateUsername(user.getId(), user.getUsername(), username, requestTs);
      updatedUserBuilder.withUsername(newUsername);
      updatedUserBuilder.withLastUpdateTs(requestTs);
    }

    taskRunner.run(() -> contactService.refreshContacts(user.getId(), requestTs));

    return updatedUserBuilder.build();
  }

  @Override
  public User updateSessionId(String userId, String sessionId, Instant requestTs)
      throws ResourceNotFoundException {
    var userUpdate = new User.Builder(userId)
        .withSessionId(sessionId)
        .build();
    return userDAO.updateUser(userUpdate, requestTs);
  }

  @Override
  public String getUserId(String username) {
    return userDAO.getUserId(username.toLowerCase());
  }

  @Override
  public String getUserId(EmailAddress emailAddress) {
    return userDAO.getUserId(emailAddress);
  }

  @Override
  public User getUser(String userId) {
    return userDAO.getUser(userId);
  }

  @Override
  public List<User> getUsers(List<String> userIds) {
    return userDAO.batchGetUser(userIds);
  }

  @Override
  public byte[] getAvatar(String userId) throws ResourceNotFoundException {
    var filePath = String.format(AVATAR_ORIGINAL_URI, userId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public byte[] getAvatarThumbnail(String userId) throws ResourceNotFoundException {
    var filePath = String.format(AVATAR_THUMBNAIL_URI, userId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public byte[] getAvatarMicro(String userId) throws ResourceNotFoundException {
    var filePath = String.format(AVATAR_MICRO_URI, userId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public List<User> queryUsersByUsername(String beginsWith) {
    var userIds = userDAO
        .queryUserIdsByUsername(beginsWith.toLowerCase(), USERNAME_QUERY_MAX_RESULT_SIZE);

    return userDAO.batchGetUser(userIds);
  }
}
