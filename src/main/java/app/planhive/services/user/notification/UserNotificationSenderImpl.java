package app.planhive.services.user.notification;

import app.planhive.notification.NotificationCategory;
import app.planhive.services.user.model.Notification.Builder;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import java.util.List;
import java.util.Map;

public class UserNotificationSenderImpl implements UserNotificationSender {

  private final NotificationService notificationService;

  public UserNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendContactRequestNotification(User user, String contactId) {
    var notification = new Builder("New contact request",
        String.format("%s sent you a contact request.", user.getName()),
        NotificationCategory.CONTACTS)
        .withResourceType(NotificationKey.USER.getValue())
        .withAdditionalData(Map.of(NotificationKey.USER_ID.getValue(), user.getId()))
        .build();
    notificationService.sendNotificationByUserIds(notification, List.of(contactId), true);
  }

  @Override
  public void sendContactRequestAcceptedNotification(User invitee, String requesterId) {
    var notification = new Builder("New contact",
        String.format("%s accepted your contact request!", invitee.getName()),
        NotificationCategory.CONTACTS)
        .withResourceType(NotificationKey.USER.getValue())
        .withAdditionalData(Map.of(NotificationKey.USER_ID.getValue(), invitee.getId()))
        .build();
    notificationService.sendNotificationByUserIds(notification, List.of(requesterId), true);
  }
}
