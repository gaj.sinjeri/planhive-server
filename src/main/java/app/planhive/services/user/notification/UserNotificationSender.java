package app.planhive.services.user.notification;

import app.planhive.services.user.model.User;

import java.util.List;

public interface UserNotificationSender {

  void sendContactRequestNotification(User user, String contactId);

  void sendContactRequestAcceptedNotification(User invitee, String requesterId);

}
