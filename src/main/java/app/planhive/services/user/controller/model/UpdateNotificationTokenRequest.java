package app.planhive.services.user.controller.model;

import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateNotificationTokenRequest {

  private final String notificationToken;

  @JsonCreator
  public UpdateNotificationTokenRequest(
      @JsonProperty("notificationToken") String notificationToken) {
    checkArgument(notificationToken == null || !notificationToken.isEmpty());

    this.notificationToken = notificationToken;
  }

  public String getNotificationToken() {
    return notificationToken;
  }

  @Override
  public String toString() {
    return "UpdateNotificationTokenRequest{" +
        "notificationToken='" + notificationToken + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateNotificationTokenRequest that = (UpdateNotificationTokenRequest) o;
    return Objects.equals(notificationToken, that.notificationToken);
  }

  @Override
  public int hashCode() {
    return Objects.hash(notificationToken);
  }
}
