package app.planhive.services.user.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class GetPublicUserDetailsRequest {

  private final List<String> userIds;

  @JsonCreator
  public GetPublicUserDetailsRequest(
      @JsonProperty(value = "userIds", required = true) List<String> userIds) {
    this.userIds = userIds;
  }

  public List<String> getUserIds() {
    return userIds;
  }

  @Override
  public String toString() {
    return "GetPublicUserDetailsRequest{" +
        "userIds=" + userIds +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetPublicUserDetailsRequest that = (GetPublicUserDetailsRequest) o;
    return Objects.equals(userIds, that.userIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userIds);
  }
}
