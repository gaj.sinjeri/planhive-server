package app.planhive.services.user.controller.model;

import java.util.Objects;

public class UsernameAvailabilityResponse {

  private final boolean available;

  public UsernameAvailabilityResponse(boolean available) {
    this.available = available;
  }

  public boolean isAvailable() {
    return available;
  }

  @Override
  public String toString() {
    return "UsernameAvailabilityResponse{" +
        "available=" + available +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UsernameAvailabilityResponse that = (UsernameAvailabilityResponse) o;
    return available == that.available;
  }

  @Override
  public int hashCode() {
    return Objects.hash(available);
  }
}
