package app.planhive.services.user.controller;

import app.planhive.model.RestResponse;
import app.planhive.services.user.controller.model.UsernameAvailabilityResponse;
import app.planhive.services.user.model.UserPublicView;
import app.planhive.services.user.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
public class UsernameController {

  private static final Logger logger = LoggerFactory.getLogger(UsernameController.class);

  private final UserService userService;

  @Autowired
  UsernameController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping(path = "usernames/{username}/availability")
  public ResponseEntity<RestResponse> checkUsernameAvailability(
      @PathVariable("username") String username) {
    logger.debug(String.format("Received request to check if username %s is available", username));

    var userId = userService.getUserId(username);

    return new ResponseEntity<>(
        RestResponse.successResponse(new UsernameAvailabilityResponse(userId == null)),
        HttpStatus.OK);
  }

  @GetMapping(path = "usernames/{usernameBeginning}/autocomplete")
  public ResponseEntity<RestResponse> getUsernameAutocompleteSuggestion(
      @PathVariable("usernameBeginning") String usernameBeginning) {
    logger.debug(String.format("Received request to get username autocomplete suggestions from %s",
        usernameBeginning));

    var userPublicViews = userService.queryUsersByUsername(usernameBeginning)
        .stream()
        .map(UserPublicView::fromUser)
        .collect(Collectors.toUnmodifiableList());

    return new ResponseEntity<>(RestResponse.successResponse(userPublicViews), HttpStatus.OK);
  }
}
