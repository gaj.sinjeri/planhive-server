package app.planhive.services.user.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateNotificationCountRequest {

  private final int notificationCount;

  @JsonCreator
  public UpdateNotificationCountRequest(
      @JsonProperty(value = "notificationCount", required = true) int notificationCount) {
    this.notificationCount = notificationCount;
  }

  public int getNotificationCount() {
    return notificationCount;
  }

  @Override
  public String toString() {
    return "UpdateNotificationCountRequest{" +
        "notificationCount=" + notificationCount +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateNotificationCountRequest that = (UpdateNotificationCountRequest) o;
    return notificationCount == that.notificationCount;
  }

  @Override
  public int hashCode() {
    return Objects.hash(notificationCount);
  }
}
