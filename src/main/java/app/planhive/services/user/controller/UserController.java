package app.planhive.services.user.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.ResourceType;
import app.planhive.model.ErrorCode;
import app.planhive.model.RestResponse;
import app.planhive.services.user.controller.model.GetContactUpdatesRequest;
import app.planhive.services.user.controller.model.GetPublicUserDetailsRequest;
import app.planhive.services.user.controller.model.RespondToContactInvitationRequest;
import app.planhive.services.user.controller.model.UpdateNotificationCountRequest;
import app.planhive.services.user.controller.model.UpdateNotificationTokenRequest;
import app.planhive.services.user.controller.model.UpdateUserRequest;
import app.planhive.services.user.model.User;
import app.planhive.services.user.model.UserPublicView;
import app.planhive.services.user.service.ContactService;
import app.planhive.services.user.service.NotificationService;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.stream.Collectors;

@RestController
@RequestMapping("authenticated")
public class UserController {

  private static final Logger logger = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;
  private final ContactService contactService;
  private final NotificationService notificationService;

  @Autowired
  UserController(UserService userService, ContactService contactService,
      NotificationService notificationService) {
    this.userService = userService;
    this.contactService = contactService;
    this.notificationService = notificationService;
  }

  @PatchMapping(path = "user")
  public ResponseEntity<RestResponse> updateUserDetails(@RequestAttribute(USER_ATTRIBUTE) User user,
      @RequestParam("request") UpdateUserRequest request,
      @RequestParam(name = "avatar", required = false) MultipartFile avatar) {
    logger.debug(
        String.format("Received request to update details for user with id %s", user.getId()));

    try {
      var updatedUser = userService
          .updateUserDetails(user, request.getName(), request.getUsername(),
              new Instant(request.getLastAvatarUpdateTs()), avatar, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(updatedUser), HttpStatus.OK);
    } catch (InvalidImageException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (ResourceAlreadyExistsException e) {
      if (e.getResourceType() == ResourceType.DB_USERNAME_ITEM) {
        logger.warn(e.getMessage());
        return new ResponseEntity<>(RestResponse.errorResponse(ErrorCode.USERNAME_TAKEN),
            HttpStatus.FORBIDDEN);
      } else {
        logger.error(e.getMessage());
        return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
  }

  @GetMapping(path = "user", produces = "application/json")
  public ResponseEntity<RestResponse> getUserDetails(@RequestAttribute(USER_ATTRIBUTE) User user) {
    logger.debug(String.format("Received request to get user details for user %s", user.getId()));

    return new ResponseEntity<>(RestResponse.successResponse(user), HttpStatus.OK);
  }

  @GetMapping(path = "user/contact-updates", consumes = "application/json")
  public ResponseEntity<RestResponse> getContactUpdates(
      @RequestAttribute(USER_ATTRIBUTE) User user, @RequestBody GetContactUpdatesRequest request) {
    logger.debug(
        String.format("Received request to get contact updates for user %s", user.getId()));

    var lastUpdateTs = new Instant(request.getLastUpdateTs());
    var contactsWithUserViews = contactService.getContactUpdates(user, lastUpdateTs);

    return new ResponseEntity<>(RestResponse.successResponse(contactsWithUserViews), HttpStatus.OK);
  }

  @PostMapping(path = "user/contact-requests/{contactId}")
  public ResponseEntity<RestResponse> createContactRequest(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("contactId") String contactId) {
    logger.debug(String.format("Received request to create contact request for user id %s, "
        + "contact id %s", user.getId(), contactId));

    try {
      var contact = contactService.createContactRequest(user, contactId, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(contact), HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (InvalidOperationException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "user/contact-requests/{requesterId}")
  public ResponseEntity<RestResponse> respondToContactRequest(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("requesterId") String requesterId,
      @RequestBody RespondToContactInvitationRequest request) {
    logger.debug(String.format("Received request to respond to contact request for "
        + "requester id %s, invitee id %s", requesterId, user.getId()));

    try {
      var contact = contactService
          .respondToContactRequest(requesterId, user, request.isAccepted(), Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(contact), HttpStatus.OK);
    } catch (InvalidOperationException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "user/contact-request-cancellations/{contactId}")
  public ResponseEntity<RestResponse> cancelContactRequest(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("contactId") String contactId) {
    logger.debug(String.format("Received request to cancel contact request for "
        + "user id %s, contact id %s", user.getId(), contactId));

    try {
      var contact = contactService.cancelContactRequest(user, contactId, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(contact), HttpStatus.OK);
    } catch (InvalidOperationException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "user/contacts/{contactId}")
  public ResponseEntity<RestResponse> deleteContact(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("contactId") String contactId) {
    logger.debug(String.format("Received request to delete contact for user id %s, "
        + "contact id %s", user.getId(), contactId));

    try {
      var contact = contactService.deleteContact(user, contactId, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(contact), HttpStatus.OK);
    } catch (InvalidOperationException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "users", consumes = "application/json")
  public ResponseEntity<RestResponse> batchGetPublicUserDetails(
      @RequestBody GetPublicUserDetailsRequest request) {
    logger.debug(String
        .format("Received request to get public user details for users %s", request.getUserIds()));

    var userPublicViews = userService.getUsers(request.getUserIds())
        .stream()
        .map(UserPublicView::fromUser)
        .collect(Collectors.toUnmodifiableList());

    return new ResponseEntity<>(RestResponse.successResponse(userPublicViews), HttpStatus.OK);
  }

  @GetMapping(path = "users/{userId}/avatar", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getAvatar(@PathVariable("userId") String userId) {
    logger.debug(String.format("Received request to get avatar for user %s", userId));

    try {
      var image = userService.getAvatar(userId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(path = "users/{userId}/avatar-thumbnail", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getAvatarThumbnail(@PathVariable("userId") String userId) {
    logger.debug(String.format("Received request to get avatar thumbnail for user %s", userId));

    try {
      var image = userService.getAvatarThumbnail(userId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(path = "users/{userId}/avatar-micro", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getAvatarMicro(@PathVariable("userId") String userId) {
    logger.debug(String.format("Received request to get avatar micro for user %s", userId));

    try {
      var image = userService.getAvatarMicro(userId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping(path = "user/notification-token", consumes = "application/json")
  public ResponseEntity<RestResponse> updateNotificationToken(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @RequestBody UpdateNotificationTokenRequest request) {
    logger.debug(String
        .format("Received request to update notification token for user %s. Token: %s",
            user.getId(), request.getNotificationToken()));

    try {
      notificationService
          .updateNotificationToken(user, request.getNotificationToken(), Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping(path = "user/notification-count", consumes = "application/json")
  public ResponseEntity<RestResponse> updateNotificationCount(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @RequestBody UpdateNotificationCountRequest request) {
    logger.debug(String
        .format("Received request to update notification count for user %s. Count: %s",
            user.getId(), request.getNotificationCount()));

    try {
      notificationService.updateNotificationCount(user.getId(), request.getNotificationCount());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(UpdateUserRequest.class, new UpdateUserRequest.Editor());
  }
}
