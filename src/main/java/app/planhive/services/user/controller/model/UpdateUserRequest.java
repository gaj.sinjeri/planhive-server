package app.planhive.services.user.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class UpdateUserRequest {

  private final String username;
  private final String name;
  private final Long lastAvatarUpdateTs;

  @JsonCreator
  public UpdateUserRequest(@JsonProperty("username") String username,
      @JsonProperty("name") String name,
      @JsonProperty(value = "lastAvatarUpdateTs", required = true) Long lastAvatarUpdateTs) {

    this.username = username;
    this.name = name;
    this.lastAvatarUpdateTs = lastAvatarUpdateTs;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, UpdateUserRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public String getUsername() {
    return username;
  }

  public String getName() {
    return name;
  }

  public Long getLastAvatarUpdateTs() {
    return lastAvatarUpdateTs;
  }

  @Override
  public String toString() {
    return "UpdateUserRequest{" +
        "username='" + username + '\'' +
        ", name='" + name + '\'' +
        ", lastAvatarUpdateTs=" + lastAvatarUpdateTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateUserRequest that = (UpdateUserRequest) o;
    return Objects.equals(username, that.username) &&
        Objects.equals(name, that.name) &&
        Objects.equals(lastAvatarUpdateTs, that.lastAvatarUpdateTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, name, lastAvatarUpdateTs);
  }
}
