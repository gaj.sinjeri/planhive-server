package app.planhive.services.user.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class RespondToContactInvitationRequest {

  private final boolean accepted;

  @JsonCreator
  public RespondToContactInvitationRequest(
      @JsonProperty(value = "accepted", required = true) boolean accepted) {

    this.accepted = accepted;
  }

  public boolean isAccepted() {
    return accepted;
  }

  @Override
  public String toString() {
    return "RespondToContactInvitationRequest{" +
        "accepted=" + accepted +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RespondToContactInvitationRequest that = (RespondToContactInvitationRequest) o;
    return accepted == that.accepted;
  }

  @Override
  public int hashCode() {
    return Objects.hash(accepted);
  }
}
