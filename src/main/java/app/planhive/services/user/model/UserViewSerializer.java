package app.planhive.services.user.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class UserViewSerializer {

  public static class UserPrivateViewSerializer extends JsonSerializer<UserPrivateView> {

    @Override
    public void serialize(UserPrivateView userView, JsonGenerator generator,
        SerializerProvider provider)
        throws IOException {

      generator.writeStartObject();

      generator.writeStringField("id", userView.getUser().getId());
      generator.writeStringField("username", userView.getUser().getUsername());
      generator.writeStringField("name", userView.getUser().getName());
      generator.writeNumberField("lastUpdateTs", userView.getUser().getLastUpdateTs().getMillis());
      generator.writeNumberField("lastAvatarUpdateTs",
          userView.getUser().getLastAvatarUpdateTs().getMillis());
      generator.writeBooleanField("deleted", userView.getUser().isDeleted());

      generator.writeEndObject();
    }
  }

  public static class UserPublicViewSerializer extends JsonSerializer<UserPublicView> {

    @Override
    public void serialize(UserPublicView userView, JsonGenerator generator,
        SerializerProvider provider)
        throws IOException {

      generator.writeStartObject();
      generator.writeStringField("id", userView.getUser().getId());
      generator.writeStringField("username", userView.getUser().getUsername());
      generator.writeStringField("name", userView.getUser().getName());
      generator.writeNumberField("lastUpdateTs", userView.getUser().getLastUpdateTs().getMillis());
      generator.writeNumberField("lastAvatarUpdateTs",
          userView.getUser().getLastAvatarUpdateTs().getMillis());
      generator.writeBooleanField("deleted", userView.getUser().isDeleted());
      generator.writeEndObject();
    }
  }
}
