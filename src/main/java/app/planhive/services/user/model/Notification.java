package app.planhive.services.user.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.notification.NotificationCategory;
import java.util.Map;
import java.util.Objects;

public class Notification {

  private final String title;
  private final String body;
  private final String resourceType;
  private final Map<String, String> additionalData;
  private final NotificationCategory category;

  private Notification(String title, String body, String resourceType,
      Map<String, String> additionalData, NotificationCategory category) {
    checkNotNull(title);
    checkNotNull(body);
    checkNotNull(category);

    this.title = title;
    this.body = body;
    this.resourceType = resourceType;
    this.additionalData = additionalData;
    this.category = category;
  }

  public static class Builder {

    private final String title;
    private final String body;
    private String resourceType;
    private Map<String, String> additionalData;
    private final NotificationCategory category;

    public Builder(String title, String body, NotificationCategory category) {
      this.title = title;
      this.body = body;
      this.category = category;
    }

    public Builder withResourceType(String resourceType) {
      this.resourceType = resourceType;
      return this;
    }

    public Builder withAdditionalData(Map<String, String> additionalData) {
      this.additionalData = additionalData;
      return this;
    }

    public Notification build() {
      return new Notification(title, body, resourceType, additionalData, category);
    }
  }

  public String getTitle() {
    return title;
  }

  public String getBody() {
    return body;
  }

  public String getResourceType() {
    return resourceType;
  }

  public Map<String, String> getAdditionalData() {
    return additionalData;
  }

  public NotificationCategory getCategory() {
    return category;
  }

  @Override
  public String toString() {
    return "Notification{" +
        "title='" + title + '\'' +
        ", body='" + body + '\'' +
        ", resourceType='" + resourceType + '\'' +
        ", additionalData=" + additionalData +
        ", category=" + category +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Notification that = (Notification) o;
    return Objects.equals(title, that.title) &&
        Objects.equals(body, that.body) &&
        Objects.equals(resourceType, that.resourceType) &&
        Objects.equals(additionalData, that.additionalData) &&
        Objects.equals(category, that.category);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, body, resourceType, additionalData, category);
  }
}
