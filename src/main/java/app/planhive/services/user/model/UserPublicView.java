package app.planhive.services.user.model;

import app.planhive.services.user.model.UserViewSerializer.UserPublicViewSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Objects;

@JsonSerialize(using = UserPublicViewSerializer.class)
public class UserPublicView implements UserView {

  private final User user;

  private UserPublicView(User user) {
    this.user = user;
  }

  public static UserPublicView fromUser(User user) {
    var userView = new User.Builder(user.getId())
        .withUsername(user.getUsername())
        .withName(user.getName())
        .withLastUpdateTs(user.getLastUpdateTs())
        .withLastAvatarUpdateTs(user.getLastAvatarUpdateTs())
        .setDeleted(user.isDeleted())
        .build();
    return new UserPublicView(userView);
  }

  public User getUser() {
    return user;
  }

  @Override
  public String toString() {
    return "UserPublicView{" +
        "id='" + user.getId() + '\'' +
        ", username='" + user.getUsername() + '\'' +
        ", name='" + user.getName() + '\'' +
        ", lastUpdateTs=" + user.getLastUpdateTs() +
        ", lastAvatarUpdateTs=" + user.getLastAvatarUpdateTs() +
        ", deleted=" + user.isDeleted() +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserPublicView that = (UserPublicView) o;
    return Objects.equals(user, that.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user);
  }
}
