package app.planhive.services.user.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class UserSerializer extends JsonSerializer<User> {

  @Override
  public void serialize(User user, JsonGenerator generator, SerializerProvider provider)
      throws IOException {

    generator.writeStartObject();

    generator.writeStringField("id", user.getId());
    generator.writeNumberField("lastUpdateTs", user.getLastUpdateTs().getMillis());
    generator.writeBooleanField("deleted", user.isDeleted());

    if (user.getUsername() != null) {
      generator.writeStringField("username", user.getUsername());
    }
    if (user.getName() != null) {
      generator.writeStringField("name", user.getName());
    }
    if (user.getCreationTs() != null) {
      generator.writeNumberField("creationTs", user.getCreationTs().getMillis());
    }
    if (user.getLastAvatarUpdateTs() != null) {
      generator.writeNumberField("lastAvatarUpdateTs", user.getLastAvatarUpdateTs().getMillis());
    }
    if (user.getEmailAddress() != null) {
      generator.writeStringField("email", user.getEmailAddress().getValue());
    }

    generator.writeEndObject();
  }
}
