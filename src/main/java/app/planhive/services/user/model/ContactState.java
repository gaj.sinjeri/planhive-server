package app.planhive.services.user.model;

public enum ContactState {
  REQUESTED,
  INVITED,
  CONNECTED,
  REJECTED,
  DELETED
}
