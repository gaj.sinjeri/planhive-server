package app.planhive.services.user.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ContactSerializer extends JsonSerializer<Contact> {

  @Override
  public void serialize(Contact contact, JsonGenerator generator, SerializerProvider provider)
      throws IOException {

    generator.writeStartObject();
    generator.writeStringField("contactState", contact.getContactState().name());
    generator.writeStringField("userId", contact.getUserId());
    generator.writeStringField("contactId", contact.getContactId());
    generator.writeNumberField("lastUpdateTs", contact.getLastUpdateTs().getMillis());
    generator.writeEndObject();
  }
}
