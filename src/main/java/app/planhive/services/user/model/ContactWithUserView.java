package app.planhive.services.user.model;

import java.util.Objects;

public class ContactWithUserView {

  private final Contact contact;
  private final UserView userView;

  public ContactWithUserView(Contact contact, UserView userView) {
    this.contact = contact;
    this.userView = userView;
  }

  public Contact getContact() {
    return contact;
  }

  public UserView getUserView() {
    return userView;
  }

  @Override
  public String toString() {
    return "ContactWithUserView{" +
        "contact=" + contact +
        ", userView=" + userView +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContactWithUserView that = (ContactWithUserView) o;
    return Objects.equals(contact, that.contact) &&
        Objects.equals(userView, that.userView);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contact, userView);
  }
}
