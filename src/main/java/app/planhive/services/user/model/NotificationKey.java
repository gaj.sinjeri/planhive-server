package app.planhive.services.user.model;

public enum NotificationKey {
  USER("user"),
  USER_ID("userId"),
  NODE("node"),
  NODE_IDS("nodeIds");

  private final String value;

  NotificationKey(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
