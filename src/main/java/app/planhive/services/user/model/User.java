package app.planhive.services.user.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.model.EmailAddress;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.Instant;

import java.util.Objects;
import java.util.regex.Pattern;

@JsonSerialize(using = UserSerializer.class)
public class User {

  public static final Pattern USERNAME_PATTERN = Pattern
      .compile("(?=^.{3,25}$)^[a-z0-9]([._]?[a-z0-9])*$");
  public static final Pattern NAME_PATTERN = Pattern
      .compile("(?=^.{3,25}$)^\\p{L}([ '-]?\\p{L})*$");

  private final String id;
  private final String sessionId;
  private final String notificationToken;
  private final int notificationCount;
  private final String username;
  private final String name;
  private final EmailAddress emailAddress;
  private final Instant creationTs;
  private final Instant lastUpdateTs;
  private final Instant lastAvatarUpdateTs;
  private final boolean deleted;

  private User(String id, String sessionId, String notificationToken, int notificationCount,
      String username, String name, EmailAddress emailAddress, Instant creationTs,
      Instant lastUpdateTs, Instant lastAvatarUpdateTs, boolean deleted) {
    checkArgument(username == null || USERNAME_PATTERN.matcher(username).matches(),
        String.format("Username %s does not match pattern", username));
    checkArgument(name == null || NAME_PATTERN.matcher(name).matches(),
        String.format("Name %s does not match pattern", name));

    this.id = id;
    this.sessionId = sessionId;
    this.notificationToken = notificationToken;
    this.notificationCount = notificationCount;
    this.username = username;
    this.name = name;
    this.emailAddress = emailAddress;
    this.creationTs = creationTs;
    this.lastUpdateTs = lastUpdateTs;
    this.lastAvatarUpdateTs = lastAvatarUpdateTs;
    this.deleted = deleted;
  }

  public static class Builder {

    private final String id;
    private String sessionId;
    private String notificationToken;
    private int notificationCount;
    private String username;
    private String name;
    private EmailAddress emailAddress;
    private Instant creationTs;
    private Instant lastUpdateTs;
    private Instant lastAvatarUpdateTs;
    private boolean deleted;

    public Builder(String id) {
      checkNotNull(id);
      this.id = id;
    }

    public Builder withSessionId(String sessionId) {
      this.sessionId = sessionId;
      return this;
    }

    public Builder withNotificationToken(String notificationToken) {
      this.notificationToken = notificationToken;
      return this;
    }

    public Builder withNotificationCount(int notificationCount) {
      this.notificationCount = notificationCount;
      return this;
    }

    public Builder withUsername(String username) {
      this.username = username;
      return this;
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withEmailAddress(EmailAddress emailAddress) {
      this.emailAddress = emailAddress;
      return this;
    }

    public Builder withCreationTs(Instant creationTs) {
      this.creationTs = creationTs;
      return this;
    }

    public Builder withLastUpdateTs(Instant lastUpdateTs) {
      this.lastUpdateTs = lastUpdateTs;
      return this;
    }

    public Builder withLastAvatarUpdateTs(Instant lastAvatarUpdateTs) {
      this.lastAvatarUpdateTs = lastAvatarUpdateTs;
      return this;
    }

    public Builder setDeleted(boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public User build() {
      return new User(id, sessionId, notificationToken, notificationCount, username, name,
          emailAddress, creationTs, lastUpdateTs, lastAvatarUpdateTs, deleted);
    }
  }

  public String getId() {
    return id;
  }

  public String getSessionId() {
    return sessionId;
  }

  public String getNotificationToken() {
    return notificationToken;
  }

  public int getNotificationCount() {
    return notificationCount;
  }

  public String getUsername() {
    return username;
  }

  public String getName() {
    return name;
  }

  public EmailAddress getEmailAddress() {
    return emailAddress;
  }

  public Instant getCreationTs() {
    return creationTs;
  }

  public Instant getLastUpdateTs() {
    return lastUpdateTs;
  }

  public Instant getLastAvatarUpdateTs() {
    return lastAvatarUpdateTs;
  }

  public boolean isDeleted() {
    return deleted;
  }

  @Override
  public String toString() {
    return "User{" +
        "id='" + id + '\'' +
        ", sessionId='" + sessionId + '\'' +
        ", notificationToken='" + notificationToken + '\'' +
        ", notificationCount=" + notificationCount +
        ", username='" + username + '\'' +
        ", name='" + name + '\'' +
        ", emailAddress=" + emailAddress +
        ", creationTs=" + creationTs +
        ", lastUpdateTs=" + lastUpdateTs +
        ", lastAvatarUpdateTs=" + lastAvatarUpdateTs +
        ", deleted=" + deleted +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return notificationCount == user.notificationCount &&
        deleted == user.deleted &&
        Objects.equals(id, user.id) &&
        Objects.equals(sessionId, user.sessionId) &&
        Objects.equals(notificationToken, user.notificationToken) &&
        Objects.equals(username, user.username) &&
        Objects.equals(name, user.name) &&
        Objects.equals(emailAddress, user.emailAddress) &&
        Objects.equals(creationTs, user.creationTs) &&
        Objects.equals(lastUpdateTs, user.lastUpdateTs) &&
        Objects.equals(lastAvatarUpdateTs, user.lastAvatarUpdateTs);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(id, sessionId, notificationToken, notificationCount, username, name, emailAddress,
            creationTs, lastUpdateTs, lastAvatarUpdateTs, deleted);
  }
}
