package app.planhive.services.user.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.Instant;

import java.util.Objects;

@JsonSerialize(using = ContactSerializer.class)
public class Contact {

  private final ContactState contactState;
  private final String userId;
  private final String contactId;
  private final Instant lastUpdateTs;

  public Contact(ContactState contactState, String userId, String contactId,
      Instant lastUpdateTs) {
    checkNotNull(contactState);
    checkNotNull(userId);
    checkNotNull(contactId);
    checkNotNull(lastUpdateTs);

    this.contactState = contactState;
    this.userId = userId;
    this.contactId = contactId;
    this.lastUpdateTs = lastUpdateTs;
  }

  public ContactState getContactState() {
    return contactState;
  }

  public String getUserId() {
    return userId;
  }

  public String getContactId() {
    return contactId;
  }

  public Instant getLastUpdateTs() {
    return lastUpdateTs;
  }

  @Override
  public String toString() {
    return "Contact{" +
        "contactState=" + contactState +
        ", userId='" + userId + '\'' +
        ", contactId='" + contactId + '\'' +
        ", lastUpdateTs=" + lastUpdateTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contact contact = (Contact) o;
    return contactState == contact.contactState &&
        Objects.equals(userId, contact.userId) &&
        Objects.equals(contactId, contact.contactId) &&
        Objects.equals(lastUpdateTs, contact.lastUpdateTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contactState, userId, contactId, lastUpdateTs);
  }
}
