package app.planhive.services.feed.persistence;

import app.planhive.services.feed.model.Feed;

import org.joda.time.Instant;

import java.util.List;

public interface FeedDAO {

  void batchPutItem(List<Feed> feeds);

  void refreshLastUpdateTs(List<Feed> feeds);

  Feed getFeed(String resourceId, String userId);

  List<Feed> queryByResourceId(String resourceId);

  List<Feed> queryByUserIdAndResourceType(String userId, String resourceType, Instant lastUpdateTs,
      int maxResultSize);

  List<Feed> queryByUserIdAndResourceTypeHistorical(String userId, String resourceType,
      Instant lastUpdateTs);

  void updateBeginTs(String resourceId, String userId, Instant beginTs, Instant requestTs);

  void batchHardDeleteFeed(String userId, List<String> resourceIds);

}
