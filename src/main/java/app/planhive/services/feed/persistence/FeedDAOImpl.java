package app.planhive.services.feed.persistence;

import static app.planhive.common.TimeConverter.millisToInstant;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.N;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;

import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.feed.model.Feed;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.collect.Lists;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * FeedDAOImpl provides methods for manipulating Feed items in the database.
 *
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {Resource Id}                       S
 *    Range Key:        feed|{User Id}                      S
 *  GSI1:
 *    Hash Key:         feed|{User Id}|{Resource Type}      S
 *    Range Key:        {Last Update Timestamp}             N
 *  Other attributes:
 *    Begin             {Feed Begin Timestamp}              N
 *    Mute              {Mute}                              B
 *    Ts-C              {Creation Timestamp}                N
 */
@Repository
public class FeedDAOImpl extends DynamoDAO implements FeedDAO {

  private static final String ID_PREFIX = "feed";

  private static final String ATTRIBUTE_FEED_BEGIN_TS = "Begin";
  private static final String ATTRIBUTE_MUTE = "Mute";
  private static final String ATTRIBUTE_CREATION_TS = "Ts-C";

  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(FeedDAOImpl.class);

  @Autowired
  FeedDAOImpl(DynamoDBClient dynamoDBClient) {
    super(ID_PREFIX);

    this.dynamoDB = dynamoDBClient.getDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public void batchPutItem(List<Feed> feeds) {
    for (var partition : Lists.partition(feeds, DynamoDBClient.MAXIMUM_BATCH_PUT_SIZE)) {
      var dbItems = partition.stream()
          .map(feed -> {
                var item = new Item()
                    .withPrimaryKey(
                        DynamoDBClient.PRIMARY_KEY_H, feed.getResourceId(),
                        DynamoDBClient.PRIMARY_KEY_R, addPrefix(feed.getUserId()))
                    .withString(DynamoDBClient.GSI1_H,
                        addPrefix(toGSI1HK(feed.getUserId(), feed.getResourceType())))
                    .withLong(DynamoDBClient.GSI1_R, feed.getLastUpdateTs().getMillis())
                    .withLong(ATTRIBUTE_CREATION_TS, feed.getCreationTs().getMillis());
                if (feed.getBeginTs() != null) {
                  item.withLong(ATTRIBUTE_FEED_BEGIN_TS, feed.getBeginTs().getMillis());
                }
                if (feed.getMute() != null) {
                  item.withBoolean(ATTRIBUTE_MUTE, feed.getMute());
                }
                return item;
              }
          ).collect(Collectors.toUnmodifiableList());
      var itemsToWrite = new TableWriteItems(DynamoDBClient.DB_TABLE_NAME)
          .withItemsToPut(dbItems);

      try {
        logger.debug("Starting batchPut operation for feed items");
        var outcome = dynamoDB.batchWriteItem(itemsToWrite);

        // TODO(#59): Replace loop with better solution
        while (!outcome.getUnprocessedItems().isEmpty()) {
          logger.debug("BatchPutting unprocessed feed items");
          outcome = dynamoDB.batchWriteItemUnprocessed(outcome.getUnprocessedItems());
        }
      } catch (Exception e) {
        logger.error("Error executing batchPut operation for feed items");
        throw e;
      }
    }
  }

  @Override
  public void refreshLastUpdateTs(List<Feed> feeds) {
    for (var feed : feeds) {
      logger.debug(String
          .format("Updating feed in DB for user %s with resource id %s", feed.getUserId(),
              feed.getResourceId()));

      var xspec = new ExpressionSpecBuilder()
          .addUpdate(N(DynamoDBClient.GSI1_R).set(feed.getLastUpdateTs().getMillis()))
          .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
              .and(S(DynamoDBClient.PRIMARY_KEY_R).exists())
              .and(N(DynamoDBClient.GSI1_R).lt(feed.getLastUpdateTs().getMillis())));

      try {
        table.updateItem(
            DynamoDBClient.PRIMARY_KEY_H, feed.getResourceId(),
            DynamoDBClient.PRIMARY_KEY_R, addPrefix(feed.getUserId()),
            xspec.buildForUpdate());
        logger.debug(String
            .format("Successfully updated feed in DB for user %s with resource id %s",
                feed.getUserId(), feed.getResourceId()));
      } catch (ConditionalCheckFailedException e) {
        logger.warn(String.format(
            "Could not update feed for user %s with resource id %s because either the item does "
                + "not exist or an item with a newer timestamp already exists", feed.getUserId(),
            feed.getResourceId()));
      } catch (Exception e) {
        logger.error(
            String.format("Error updating feed in DB for user %s with resource id %s",
                feed.getUserId(), feed.getResourceId()));
        throw e;
      }
    }
  }

  @Override
  public Feed getFeed(String resourceId, String userId) {
    var spec = new GetItemSpec()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, resourceId,
            DynamoDBClient.PRIMARY_KEY_R, addPrefix(userId));

    var item = table.getItem(spec);
    if (item == null) {
      return null;
    }

    return constructFeedFromDbItem(item);
  }

  @Override
  public List<Feed> queryByResourceId(String resourceId) {
    logger
        .debug(String.format("Starting queryByResourceId operation for resource %s", resourceId));

    var items = new ArrayList<Feed>();
    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#hk = :id and begins_with(#p, :p)")
          .withNameMap(
              new NameMap()
                  .with("#hk", DynamoDBClient.PRIMARY_KEY_H)
                  .with("#p", DynamoDBClient.PRIMARY_KEY_R))
          .withValueMap(
              new ValueMap()
                  .withString(":id", resourceId)
                  .withString(":p", String.format("%s|", ID_PREFIX)));

      var dbItems = table.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructFeedFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(
          String
              .format("Error executing queryByResourceId operation for resource %s", resourceId));
      throw e;
    }

    return items;
  }

  @Override
  public List<Feed> queryByUserIdAndResourceType(String userId, String resourceType,
      Instant lastUpdateTs, int maxResultSize) {
    logger.debug(String.format("Starting queryByUserIdAndResourceType operation for user %s and "
        + "resource type %s", userId, resourceType));

    var index = table.getIndex(DynamoDBClient.GSI1_INDEX);
    var items = new ArrayList<Feed>();

    try {
      var querySpec = new QuerySpec().withKeyConditionExpression("#hk = :id and #rk > :ts")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.GSI1_H)
              .with("#rk", DynamoDBClient.GSI1_R))
          .withValueMap(new ValueMap()
              .withString(":id", addPrefix(toGSI1HK(userId, resourceType)))
              .withLong(":ts", lastUpdateTs.getMillis()))
          .withMaxResultSize(maxResultSize);

      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructFeedFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format("Error executing queryByUserIdAndResourceType operation for "
          + "user %s and resource type %s", userId, resourceType));
      throw e;
    }

    return items;
  }

  @Override
  public List<Feed> queryByUserIdAndResourceTypeHistorical(String userId, String resourceType,
      Instant lastUpdateTs) {
    logger.debug(String.format("Starting queryByUserIdAndResourceTypeHistorical operation for "
        + "user %s and resource type %s", userId, resourceType));

    var index = table.getIndex(DynamoDBClient.GSI1_INDEX);
    var items = new ArrayList<Feed>();

    try {
      var querySpec = new QuerySpec().withKeyConditionExpression("#hk = :id and #rk < :ts")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.GSI1_H)
              .with("#rk", DynamoDBClient.GSI1_R))
          .withValueMap(new ValueMap()
              .withString(":id", addPrefix(toGSI1HK(userId, resourceType)))
              .withLong(":ts", lastUpdateTs.getMillis()));

      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructFeedFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format("Error executing queryByUserIdAndResourceTypeHistorical operation "
          + "for user %s and resource type %s", userId, resourceType));
      throw e;
    }

    return items;
  }

  @Override
  public void updateBeginTs(String resourceId, String userId, Instant beginTs, Instant requestTs) {
    logger.debug(
        String.format("Updating beginTs for resource id %s, user id %s", resourceId, userId));

    var xspec = new ExpressionSpecBuilder()
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()))
        .addUpdate(N(ATTRIBUTE_FEED_BEGIN_TS).set(beginTs.getMillis()))
        .addUpdate(N(DynamoDBClient.GSI1_R).set(requestTs.getMillis()));

    try {
      table.updateItem(
          DynamoDBClient.PRIMARY_KEY_H, resourceId,
          DynamoDBClient.PRIMARY_KEY_R, addPrefix(userId),
          xspec.buildForUpdate());
      logger.debug(String
          .format("Successfully updated beginTs for resource id %s, user id %s", resourceId,
              userId));
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("Could not update beginTs for resource id %s, user id %s because "
          + "the feed item does not exist", resourceId, userId);
      throw new InternalError(msg);
    } catch (Exception e) {
      logger.error(String
          .format("Error updating beginTs for resource id %s, user id %s", resourceId, userId));
      throw e;
    }
  }

  @Override
  public void batchHardDeleteFeed(String userId, List<String> resourceIds) {
    for (var partition : Lists.partition(resourceIds, DynamoDBClient.MAXIMUM_BATCH_PUT_SIZE)) {
      var itemsToWrite = new TableWriteItems(DynamoDBClient.DB_TABLE_NAME);

      partition.forEach(resourceId -> itemsToWrite.addPrimaryKeyToDelete(
          new PrimaryKey(
              DynamoDBClient.PRIMARY_KEY_H, resourceId,
              DynamoDBClient.PRIMARY_KEY_R, addPrefix(userId))));

      try {
        logger.debug("Starting batchDelete operation for feed items");
        var outcome = dynamoDB.batchWriteItem(itemsToWrite);

        // TODO(#59): Replace loop with better solution
        while (!outcome.getUnprocessedItems().isEmpty()) {
          logger.debug("BatchDeleting unprocessed feed items");
          outcome = dynamoDB.batchWriteItemUnprocessed(outcome.getUnprocessedItems());
        }
      } catch (Exception e) {
        logger.error(String
            .format("Error executing batchDelete operation for feed items. Resource ids: %s",
                partition));
        throw e;
      }
    }
  }

  private String toGSI1HK(String userId, String resourceType) {
    return String.format("%s|%s", userId, resourceType);
  }

  private String extractResourceTypeFromGSI1HK(String hk) {
    return hk.split("\\|", 2)[1];
  }

  private Feed constructFeedFromDbItem(Item item) {
    var builder = new Feed.Builder(item.getString(DynamoDBClient.PRIMARY_KEY_H),
        removePrefix(item.getString(DynamoDBClient.PRIMARY_KEY_R)))
        .withResourceType(
            extractResourceTypeFromGSI1HK(removePrefix(item.getString(DynamoDBClient.GSI1_H))))
        .setMute(item.isPresent(ATTRIBUTE_MUTE) && item.getBoolean(ATTRIBUTE_MUTE))
        .withCreationTs(millisToInstant(item.getLong(ATTRIBUTE_CREATION_TS)))
        .withLastUpdateTs(millisToInstant(item.getLong(DynamoDBClient.GSI1_R)));

    if (item.isPresent(ATTRIBUTE_FEED_BEGIN_TS)) {
      builder.withBeginTs(millisToInstant(item.getLong(ATTRIBUTE_FEED_BEGIN_TS)));
    }

    return builder.build();
  }
}
