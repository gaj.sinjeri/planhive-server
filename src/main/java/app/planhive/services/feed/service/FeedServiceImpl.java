package app.planhive.services.feed.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.persistence.FeedDAO;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeedServiceImpl implements FeedService {

  private final FeedDAO feedDAO;

  @Autowired
  FeedServiceImpl(FeedDAO feedDAO) {
    this.feedDAO = feedDAO;
  }

  @Override
  public void createFeeds(String resourceId, List<String> userIds, String resourceType,
      Instant requestTs) {
    var feeds = userIds.stream().map(
        userId -> new Feed.Builder(resourceId, userId)
            .withResourceType(resourceType)
            .withBeginTs(new Instant(0))
            .withCreationTs(requestTs)
            .withLastUpdateTs(requestTs)
            .build())
        .collect(Collectors.toUnmodifiableList());

    feedDAO.batchPutItem(feeds);
  }

  @Override
  public void createFeeds(String resourceId, List<String> userIds, String resourceType,
      Instant beginTs, Instant requestTs) {
    var feeds = userIds.stream().map(
        userId -> new Feed.Builder(resourceId, userId)
            .withResourceType(resourceType)
            .withBeginTs(beginTs)
            .withCreationTs(requestTs)
            .withLastUpdateTs(requestTs)
            .build())
        .collect(Collectors.toUnmodifiableList());

    feedDAO.batchPutItem(feeds);
  }

  @Override
  public void refreshFeedsForResource(String resourceId, Instant requestTs) {
    var feeds = feedDAO.queryByResourceId(resourceId);
    var updatedFeeds = feeds.stream()
        .map(feed -> new Feed.Builder(feed.getResourceId(), feed.getUserId())
            .withLastUpdateTs(requestTs)
            .build())
        .collect(Collectors.toUnmodifiableList());

    feedDAO.refreshLastUpdateTs(updatedFeeds);
  }

  @Override
  public void batchRefreshFeedsForResource(List<String> resourceIds, Instant requestTs) {
    for (var resourceId : resourceIds) {
      var feeds = feedDAO.queryByResourceId(resourceId);
      var updatedFeeds = feeds.stream()
          .map(feed -> new Feed.Builder(feed.getResourceId(), feed.getUserId())
              .withLastUpdateTs(requestTs)
              .build())
          .collect(Collectors.toUnmodifiableList());

      feedDAO.refreshLastUpdateTs(updatedFeeds);
    }
  }

  @Override
  public Feed getFeedOrThrow(String resourceId, String userId) throws ResourceNotFoundException {
    var feed = feedDAO.getFeed(resourceId, userId);

    if (feed == null) {
      throw new ResourceNotFoundException(
          String.format("No feed with resource id %s, user id %s exists", resourceId, userId));
    }

    return feed;
  }

  @Override
  public List<Feed> getFeedsForUser(String userId, String resourceType, Instant lastUpdateTs,
      int sizeLimit) {
    return feedDAO.queryByUserIdAndResourceType(userId, resourceType, lastUpdateTs, sizeLimit);
  }

  @Override
  public List<Feed> getHistoricalFeedsForUser(String userId, String resourceType,
      Instant lastUpdateTs) {
    return feedDAO.queryByUserIdAndResourceTypeHistorical(userId, resourceType, lastUpdateTs);
  }

  @Override
  public List<Feed> getFeedsByResourceId(String resourceId) {
    return feedDAO.queryByResourceId(resourceId);
  }

  @Override
  public void updateBeginTs(String resourceId, String userId, Instant beginTs, Instant requestTs) {
    feedDAO.updateBeginTs(resourceId, userId, beginTs, requestTs);
  }

  @Override
  public void batchHardDeleteFeed(String userId, List<String> resourceIds) {
    feedDAO.batchHardDeleteFeed(userId, resourceIds);
  }
}
