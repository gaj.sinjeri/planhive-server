package app.planhive.services.feed.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.model.Feed;

import org.joda.time.Instant;

import java.util.List;

public interface FeedService {

  void createFeeds(String resourceId, List<String> userIds, String resourceType, Instant requestTs);

  void createFeeds(String resourceId, List<String> userIds, String resourceType, Instant beginTs,
      Instant requestTs);

  void refreshFeedsForResource(String resourceId, Instant requestTs);

  void batchRefreshFeedsForResource(List<String> resourceIds, Instant requestTs);

  Feed getFeedOrThrow(String resourceId, String userId) throws ResourceNotFoundException;

  List<Feed> getFeedsForUser(String userId, String resourceType, Instant lastUpdateTs,
      int itemLimit);

  List<Feed> getHistoricalFeedsForUser(String userId, String resourceType, Instant lastUpdateTs);

  List<Feed> getFeedsByResourceId(String resourceId);

  void updateBeginTs(String resourceId, String userId, Instant beginTs, Instant requestTs);

  void batchHardDeleteFeed(String userId, List<String> resourceIds);

}
