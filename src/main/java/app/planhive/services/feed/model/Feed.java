package app.planhive.services.feed.model;

import static com.google.common.base.Preconditions.checkNotNull;

import org.joda.time.Instant;

import java.util.Objects;

public class Feed {

  private final String resourceId;
  private final String userId;
  private final String resourceType;
  private final Instant beginTs;
  private final Boolean mute;
  private final Instant creationTs;
  private final Instant lastUpdateTs;

  private Feed(String resourceId, String userId, String resourceType, Instant beginTs, Boolean mute,
      Instant creationTs, Instant lastUpdateTs) {
    this.resourceId = resourceId;
    this.userId = userId;
    this.resourceType = resourceType;
    this.beginTs = beginTs;
    this.mute = mute;
    this.creationTs = creationTs;
    this.lastUpdateTs = lastUpdateTs;
  }

  public static class Builder {

    private final String resourceId;
    private final String userId;
    private String resourceType;
    private Instant beginTs;
    private Boolean mute;
    private Instant creationTs;
    private Instant lastUpdateTs;

    public Builder(String resourceId, String userId) {
      checkNotNull(resourceId);
      checkNotNull(userId);

      this.resourceId = resourceId;
      this.userId = userId;
    }

    public Builder withResourceType(String resourceType) {
      this.resourceType = resourceType;
      return this;
    }

    public Builder withBeginTs(Instant beginTs) {
      this.beginTs = beginTs;
      return this;
    }

    public Builder setMute(Boolean mute) {
      this.mute = mute;
      return this;
    }

    public Builder withCreationTs(Instant creationTs) {
      this.creationTs = creationTs;
      return this;
    }

    public Builder withLastUpdateTs(Instant lastUpdateTs) {
      this.lastUpdateTs = lastUpdateTs;
      return this;
    }

    public Feed build() {
      return new Feed(resourceId, userId, resourceType, beginTs, mute, creationTs, lastUpdateTs);
    }
  }

  public String getResourceId() {
    return resourceId;
  }

  public String getUserId() {
    return userId;
  }

  public String getResourceType() {
    return resourceType;
  }

  public Instant getBeginTs() {
    return beginTs;
  }

  public Boolean getMute() {
    return mute;
  }

  public Instant getCreationTs() {
    return creationTs;
  }

  public Instant getLastUpdateTs() {
    return lastUpdateTs;
  }

  @Override
  public String toString() {
    return "Feed{" +
        "resourceId='" + resourceId + '\'' +
        ", userId='" + userId + '\'' +
        ", resourceType='" + resourceType + '\'' +
        ", beginTs=" + beginTs +
        ", mute=" + mute +
        ", creationTs=" + creationTs +
        ", lastUpdateTs=" + lastUpdateTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Feed feed = (Feed) o;
    return Objects.equals(resourceId, feed.resourceId) &&
        Objects.equals(userId, feed.userId) &&
        Objects.equals(resourceType, feed.resourceType) &&
        Objects.equals(beginTs, feed.beginTs) &&
        Objects.equals(mute, feed.mute) &&
        Objects.equals(creationTs, feed.creationTs) &&
        Objects.equals(lastUpdateTs, feed.lastUpdateTs);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(resourceId, userId, resourceType, beginTs, mute, creationTs, lastUpdateTs);
  }
}
