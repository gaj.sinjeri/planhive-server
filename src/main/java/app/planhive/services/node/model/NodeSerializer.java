package app.planhive.services.node.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class NodeSerializer {

  public static class ClientFacingNodeSerializer extends JsonSerializer<Node> {

    @Override
    public void serialize(Node node, JsonGenerator generator, SerializerProvider provider)
        throws IOException {

      generator.writeStartObject();

      generator.writeStringField("id", node.getNodeId().getId());
      generator.writeStringField("parentId", node.getNodeId().getParentId());
      generator.writeStringField("type", node.getNodeId().getType().name());
      generator.writeStringField("creatorId", node.getCreatorId());
      generator.writeNumberField("lastUpdateTs", node.getLastUpdateTs().getMillis());

      if (node.getPrivateNode() != null) {
        generator.writeBooleanField("privateNode", node.getPrivateNode());
      }
      if (node.getPermissionGroupId() != null) {
        generator.writeStringField("permissionGroupId", node.getPermissionGroupId());
      }
      if (node.getCreationTs() != null) {
        generator.writeNumberField("creationTs", node.getCreationTs().getMillis());
      }
      if (node.getData() != null) {
        generator.writeStringField("data", node.getData());
      }
      if (node.getDeleted() != null) {
        generator.writeBooleanField("deleted", node.getDeleted());
      }

      generator.writeEndObject();
    }
  }
}
