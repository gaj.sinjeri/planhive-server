package app.planhive.services.node.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public abstract class NodeData {

  private static final TypeReference<Map<String, Object>> DATA_TYPE_REF = new TypeReference<>() {
  };

  protected static final ObjectMapper mapper = new ObjectMapper();

  public static <T extends NodeData> T fromJson(String source, Class<T> type) {
    try {
      return mapper.readValue(source, type);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public String toJson() {
    try {
      return mapper.writeValueAsString(this);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
