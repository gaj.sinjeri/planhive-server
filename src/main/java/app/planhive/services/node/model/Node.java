package app.planhive.services.node.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeSerializer.ClientFacingNodeSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.Instant;

import java.util.Objects;

@JsonSerialize(using = ClientFacingNodeSerializer.class)
public class Node {

  private final NodeId nodeId;
  private final String creatorId;
  private final Boolean privateNode;
  private final String permissionGroupId;
  private final Instant creationTs;
  private final Instant lastUpdateTs;
  private final String data;
  private final Boolean deleted;

  private Node(NodeId nodeId, String creatorId, Boolean privateNode,
      String permissionGroupId, Instant creationTs, Instant lastUpdateTs, String data,
      Boolean deleted) {
    this.nodeId = nodeId;
    this.creatorId = creatorId;
    this.privateNode = privateNode;
    this.permissionGroupId = permissionGroupId;
    this.creationTs = creationTs;
    this.lastUpdateTs = lastUpdateTs;
    this.data = data;
    this.deleted = deleted;
  }

  public static class Builder {

    private final NodeId nodeId;
    private String creatorId;
    private Boolean privateNode;
    private String permissionGroupId;
    private Instant creationTs;
    private Instant lastUpdateTs;
    private String data;
    private Boolean deleted;

    public Builder(NodeId nodeId) {
      checkNotNull(nodeId);

      this.nodeId = nodeId;
    }

    public Builder withCreatorId(String creatorId) {
      this.creatorId = creatorId;
      return this;
    }

    public Builder setPrivateNode(Boolean privateNode) {
      this.privateNode = privateNode;
      return this;
    }

    public Builder withPermissionGroupId(String permissionGroupId) {
      this.permissionGroupId = permissionGroupId;
      return this;
    }

    public Builder withCreationTs(Instant creationTs) {
      this.creationTs = creationTs;
      return this;
    }

    public Builder withLastUpdateTs(Instant lastUpdateTs) {
      this.lastUpdateTs = lastUpdateTs;
      return this;
    }

    public Builder withData(String data) {
      this.data = data;
      return this;
    }

    public Builder setDeleted(Boolean deleted) {
      this.deleted = deleted;
      return this;
    }

    public Node build() {
      return new Node(nodeId, creatorId, privateNode, permissionGroupId, creationTs,
          lastUpdateTs, data, deleted);
    }
  }

  public NodeId getNodeId() {
    return nodeId;
  }

  public String getCreatorId() {
    return creatorId;
  }

  public Boolean getPrivateNode() {
    return privateNode;
  }

  public String getPermissionGroupId() {
    return permissionGroupId;
  }

  public Instant getCreationTs() {
    return creationTs;
  }

  public Instant getLastUpdateTs() {
    return lastUpdateTs;
  }

  public String getData() {
    return data;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  @Override
  public String toString() {
    return "Node{" +
        "nodeId=" + nodeId +
        ", creatorId='" + creatorId + '\'' +
        ", privateNode=" + privateNode +
        ", permissionGroupId='" + permissionGroupId + '\'' +
        ", creationTs=" + creationTs +
        ", lastUpdateTs=" + lastUpdateTs +
        ", data='" + data + '\'' +
        ", deleted=" + deleted +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Node node = (Node) o;
    return Objects.equals(nodeId, node.nodeId) &&
        Objects.equals(creatorId, node.creatorId) &&
        Objects.equals(privateNode, node.privateNode) &&
        Objects.equals(permissionGroupId, node.permissionGroupId) &&
        Objects.equals(creationTs, node.creationTs) &&
        Objects.equals(lastUpdateTs, node.lastUpdateTs) &&
        Objects.equals(data, node.data) &&
        Objects.equals(deleted, node.deleted);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(nodeId, creatorId, privateNode, permissionGroupId, creationTs, lastUpdateTs,
            data, deleted);
  }
}
