package app.planhive.services.node.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.Objects;

@JsonInclude(Include.NON_EMPTY)
public class NodeId {

  private final String id;
  private final String parentId;
  private final NodeType type;

  public NodeId(String id, NodeType type) {
    this(id, null, type);
  }

  public NodeId(String id, String parentId, NodeType type) {
    checkNotNull(id);
    checkNotNull(type);
    checkArgument(!id.equals(parentId), "Node id cannot be the same as parent id");

    this.id = id;
    this.parentId = parentId;
    this.type = type;
  }

  public String toFeedItemKey() {
    if (parentId != null) {
      return String.format("%s|%s|%s", this.getType().name(), this.getParentId(), this.getId());
    } else {
      return String.format("%s|%s", this.getType().name(), this.getId());
    }
  }

  public static NodeId fromFeedItemKey(String s){
    var components = s.split("\\|", 3);

    var nodeType = NodeType.valueOf(components[0]);
    if (components.length == 2) {
      var id = components[1];

      return new NodeId(id, nodeType);
    } else {
      var id = components[2];
      var parentId = components[1];

      return new NodeId(id, parentId, nodeType);
    }
  }

  public String getId() {
    return id;
  }

  public String getParentId() {
    return parentId;
  }

  public NodeType getType() {
    return type;
  }

  @Override
  public String toString() {
    return "NodeId{" +
        "id='" + id + '\'' +
        ", parentId='" + parentId + '\'' +
        ", type=" + type +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NodeId nodeId = (NodeId) o;
    return Objects.equals(id, nodeId.id) &&
        Objects.equals(parentId, nodeId.parentId) &&
        type == nodeId.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, parentId, type);
  }
}
