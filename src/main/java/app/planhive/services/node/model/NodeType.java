package app.planhive.services.node.model;

import java.util.Set;

public enum NodeType {
  EVENT,
  PARTICIPATION,
  PARTICIPANT,
  TIMELINE,
  ACTIVITY,
  ACTIVITY_REACTION,
  CHAT_DIRECT,
  CHAT_GROUP,
  THREAD,
  MESSAGE,
  PHOTOS,
  PHOTO;

  private static final Set<NodeType> INTERNAL_NODE_TYPES = Set
      .of(NodeType.EVENT, NodeType.PARTICIPATION, NodeType.TIMELINE, NodeType.CHAT_DIRECT,
          NodeType.CHAT_GROUP, NodeType.THREAD, NodeType.PHOTOS);

  public static boolean isInternalNode(NodeType nodeType) {
    return INTERNAL_NODE_TYPES.contains(nodeType);
  }
}
