package app.planhive.services.node.service;

import static app.planhive.services.node.model.NodeType.isInternalNode;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.model.Feed;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.persistence.NodeDAO;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NodeServiceImpl implements NodeService {

  private static final int FEED_QUERY_SIZE_LIMIT = 100;

  private final NodeDAO nodeDAO;
  private final FeedService feedService;

  private static final Logger logger = LoggerFactory.getLogger(NodeServiceImpl.class);

  @Autowired
  NodeServiceImpl(NodeDAO nodeDAO, FeedService feedService) {
    this.nodeDAO = nodeDAO;
    this.feedService = feedService;
  }

  @Override
  public void insertNode(Node node) throws ResourceAlreadyExistsException {
    checkNotNull(node);

    nodeDAO.putItem(node);
  }

  @Override
  public void batchInsertNode(List<Node> nodes) {
    nodeDAO.batchPutItem(nodes);
  }

  @Override
  public Node insertNodeWithDataOnly(NodeId nodeId, String creatorId, String data,
      Instant requestTs) {
    return nodeDAO.insertNodeWithDataOnly(nodeId, creatorId, data, requestTs);
  }

  @Override
  public Node updateNodeData(NodeId nodeId, String data, Instant requestTs) {
    return nodeDAO.updateNodeData(nodeId, data, requestTs);
  }

  @Override
  public Node refreshLastUpdateTs(NodeId nodeId, Instant requestTs)
      throws ResourceNotFoundException {
    return nodeDAO.refreshLastUpdateTs(nodeId, requestTs);
  }

  @Override
  public Node getNodeOrThrow(NodeId nodeId)
      throws ResourceNotFoundException, ResourceDeletedException {
    var node = nodeDAO.readItem(nodeId);
    if (node == null) {
      throw new ResourceNotFoundException(String
          .format("No %s node with id %s, parent id %s exists", nodeId.getType().name(),
              nodeId.getId(), nodeId.getParentId()));
    } else if (node.getDeleted()) {
      throw new ResourceDeletedException(String
          .format("%s node with id %s, parent id %s has been deleted", nodeId.getType().name(),
              nodeId.getId(), nodeId.getParentId()), node);
    }
    return node;
  }

  @Override
  public List<Node> getChildNodes(String parentId) {
    return nodeDAO.queryByParentNodeId(parentId);
  }

  @Override
  public List<Node> getChildNodesWithTsRange(User user, NodeId parentNodeId, Instant toTs)
      throws ResourceNotFoundException {
    if (!isInternalNode(parentNodeId.getType())) {
      throw new InternalError(
          String.format("This operation does not support nodes of type %s",
              parentNodeId.getType().name()));
    }

    var feed = feedService.getFeedOrThrow(parentNodeId.toFeedItemKey(), user.getId());

    if (feed.getBeginTs().isAfter(toTs)) {
      return List.of();
    }

    return nodeDAO.queryByParentNodeIdWithTsRange(parentNodeId.getId(), feed.getBeginTs(), toTs);
  }

  @Override
  public List<Node> getUpdates(User user, NodeType nodeType, Instant lastUpdateTs) {
    if (!isInternalNode(nodeType)) {
      throw new InternalError(
          String.format("This operation does not support nodes of type %s", nodeType.name()));
    }

    var feeds = feedService
        .getFeedsForUser(user.getId(), nodeType.name(), lastUpdateTs, FEED_QUERY_SIZE_LIMIT);

    var nodeIds = new ArrayList<NodeId>();
    var feedMap = new HashMap<NodeId, Feed>();

    feeds.forEach(feed -> {
      if (!feed.getBeginTs().isAfter(feed.getLastUpdateTs())) {
        var nodeId = NodeId.fromFeedItemKey(feed.getResourceId());
        nodeIds.add(nodeId);
        feedMap.put(nodeId, feed);
      }
    });

    var parentNodes = nodeDAO.batchGetItem(nodeIds);

    var childNodes = new ArrayList<Node>();
    for (var node : parentNodes) {
      var beginTs = feedMap.get(node.getNodeId()).getBeginTs();
      var fromTs =
          beginTs == null ? lastUpdateTs : (beginTs.isAfter(lastUpdateTs) ? beginTs : lastUpdateTs);

      childNodes.addAll(nodeDAO.queryByParentNodeIdWithTsFilter(node.getNodeId().getId(), fromTs));
    }

    return Stream.concat(parentNodes.stream(), childNodes.stream())
        .collect(Collectors.toUnmodifiableList());
  }

  @Override
  public List<Node> getHistoricalData(User user, NodeType nodeType, Instant lastUpdateTs) {
    if (!isInternalNode(nodeType)) {
      throw new InternalError(
          String.format("This operation does not support nodes of type %s", nodeType.name()));
    }

    var feeds = feedService.getHistoricalFeedsForUser(user.getId(), nodeType.name(), lastUpdateTs);

    var nodeIds = new ArrayList<NodeId>();

    feeds.forEach(feed -> {
      if (!feed.getBeginTs().isAfter(feed.getLastUpdateTs())) {
        var nodeId = NodeId.fromFeedItemKey(feed.getResourceId());
        nodeIds.add(nodeId);
      }
    });

    return nodeDAO.batchGetItem(nodeIds);
  }

  @Override
  public List<Node> queryNode(String parentNodeId, NodeType type, String beginsWith) {
    return nodeDAO.queryNode(parentNodeId, type, beginsWith);
  }

  @Override
  public Node softDeleteNode(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException {
    return nodeDAO.softDeleteNode(nodeId, requestTs);
  }

  @Override
  public List<Node> batchSoftDeleteNode(List<NodeId> nodeIds, Instant requestTs) {
    var nodes = new ArrayList<Node>();

    nodeIds.forEach(nodeId -> {
      try {
        nodes.add(nodeDAO.softDeleteNode(nodeId, requestTs));
      } catch (ResourceNotFoundException e) {
        logger.error(String.format("Could not delete node with id %s", nodeId), e);
      }
    });

    return nodes;
  }


  @Async
  @Retryable(
      value = {Exception.class},
      maxAttempts = 4, backoff = @Backoff(2000))
  @Override
  public void softDeleteChildNodesAsync(String parentNodeId, Instant requestTs) {
    var queryTs = new Instant(0);

    while (true) {
      var childNodes = nodeDAO.queryByParentNodeIdWithTsFilter(parentNodeId, queryTs);
      if (childNodes.isEmpty()) {
        break;
      }

      childNodes.forEach(childNode -> {
        try {
          nodeDAO.softDeleteNode(childNode.getNodeId(), requestTs);
        } catch (ResourceNotFoundException e) {
          logger.error(String.format("Could not delete node with id %s", childNode.getNodeId()), e);
          throw new InternalError(e);
        }
      });

      queryTs = childNodes.get(childNodes.size() - 1).getLastUpdateTs().plus(1);
    }
  }
}
