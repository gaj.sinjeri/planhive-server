package app.planhive.services.node.service;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface NodeService {

  void insertNode(Node node) throws ResourceAlreadyExistsException;

  void batchInsertNode(List<Node> nodes);

  Node insertNodeWithDataOnly(NodeId nodeId, String creatorId, String data,
      Instant requestTs);

  Node updateNodeData(NodeId nodeId, String data, Instant requestTs);

  Node refreshLastUpdateTs(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException;

  Node getNodeOrThrow(NodeId nodeId) throws ResourceNotFoundException, ResourceDeletedException;

  List<Node> getChildNodes(String parentId);

  List<Node> getChildNodesWithTsRange(User user, NodeId parentNodeId, Instant toTs)
      throws ResourceNotFoundException;

  List<Node> getUpdates(User user, NodeType nodeType, Instant lastUpdateTs);

  List<Node> getHistoricalData(User user, NodeType nodeType, Instant lastUpdateTs);

  List<Node> queryNode(String parentNodeId, NodeType type, String beginsWith);

  Node softDeleteNode(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException;

  List<Node> batchSoftDeleteNode(List<NodeId> nodeIds, Instant requestTs);

  void softDeleteChildNodesAsync(String parentNodeId, Instant requestTs);

}
