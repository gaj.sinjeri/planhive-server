package app.planhive.services.node.common;

public class LeafNode {

  public static String generateLeafNodeId(String parentNodeId, String userId) {
    return String.format("%s|%s", parentNodeId, userId);
  }
}
