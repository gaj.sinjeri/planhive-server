package app.planhive.services.node.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class GetChildNodesRequest {

  private final long toTs;

  @JsonCreator
  public GetChildNodesRequest(@JsonProperty(value = "toTs", required = true) long toTs) {
    this.toTs = toTs;
  }

  public long getToTs() {
    return toTs;
  }

  @Override
  public String toString() {
    return "GetChildNodesRequest{" +
        "toTs=" + toTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetChildNodesRequest that = (GetChildNodesRequest) o;
    return toTs == that.toTs;
  }

  @Override
  public int hashCode() {
    return Objects.hash(toTs);
  }
}
