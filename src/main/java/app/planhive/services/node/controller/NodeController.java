package app.planhive.services.node.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.controller.model.GetChildNodesRequest;
import app.planhive.services.node.controller.model.GetHistoricalDataRequest;
import app.planhive.services.node.controller.model.GetUpdatesRequest;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authenticated/nodes")
public class NodeController {

  private static final Logger logger = LoggerFactory.getLogger(NodeController.class);

  private final NodeService nodeService;

  @Autowired
  NodeController(NodeService nodeService) {
    this.nodeService = nodeService;
  }

  @GetMapping(path = {"{parentId}/{nodeType}/{id}", "{nodeType}/{id}"})
  public ResponseEntity<RestResponse> getNode(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable(value = "parentId", required = false) String parentId,
      @PathVariable("nodeType") NodeType nodeType, @PathVariable("id") String id) {
    logger.debug(String
        .format("Received request to get %s node with id %s for user %s", nodeType.name(), id,
            user.getId()));

    NodeId nodeId;
    if (parentId != null) {
      nodeId = new NodeId(id, parentId, nodeType);
    } else {
      nodeId = new NodeId(id, nodeType);
    }

    try {
      var node = nodeService.getNodeOrThrow(nodeId);
      return new ResponseEntity<>(RestResponse.successResponse(node), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(path = "{nodeType}", consumes = "application/json")
  public ResponseEntity<RestResponse> getUpdates(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("nodeType") NodeType nodeType, @RequestBody GetUpdatesRequest request) {
    logger.debug(String
        .format("Received request to get %s node updates for user %s", nodeType.name(),
            user.getId()));

    var nodes = nodeService.getUpdates(user, nodeType, new Instant(request.getLastUpdateTs()));

    return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
  }

  @GetMapping(path = "{nodeType}/historical-data", consumes = "application/json")
  public ResponseEntity<RestResponse> getHistoricalData(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("nodeType") NodeType nodeType, @RequestBody GetHistoricalDataRequest request) {
    logger.debug(String.format("Received request to get historical data for nodes of type %s "
        + "for user %s", nodeType.name(), user.getId()));

    var nodes = nodeService
        .getHistoricalData(user, nodeType, new Instant(request.getLastUpdateTs()));

    return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
  }

  @GetMapping(path = {"{parentId}/{nodeType}/{nodeId}/children",
      "{nodeType}/{nodeId}/children"}, consumes = "application/json")
  public ResponseEntity<RestResponse> getChildNodesHistorical(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable(value = "parentId", required = false) String parentId,
      @PathVariable("nodeType") NodeType nodeType,
      @PathVariable("nodeId") String id, @RequestBody GetChildNodesRequest request) {
    logger.debug(String.format("Received request to get child nodes of parent node "
        + "{type: %s, id: %s} for user %s", nodeType.name(), id, user.getId()));

    NodeId nodeId;
    if (parentId != null) {
      nodeId = new NodeId(id, parentId, nodeType);
    } else {
      nodeId = new NodeId(id, nodeType);
    }

    try {
      var nodes = nodeService
          .getChildNodesWithTsRange(user, nodeId, new Instant(request.getToTs()));
      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (ResourceNotFoundException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }
}
