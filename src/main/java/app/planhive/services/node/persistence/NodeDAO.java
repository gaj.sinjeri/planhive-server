package app.planhive.services.node.persistence;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;

import org.joda.time.Instant;

import java.util.List;

public interface NodeDAO {

  void putItem(Node node) throws ResourceAlreadyExistsException;

  void batchPutItem(List<Node> nodes);

  Node insertNodeWithDataOnly(NodeId nodeId, String creatorId, String data, Instant requestTs);

  Node updateNodeData(NodeId nodeId, String data, Instant requestTs);

  Node refreshLastUpdateTs(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException;

  Node readItem(NodeId nodeId);

  List<Node> batchGetItem(List<NodeId> nodeIds);

  List<Node> queryNode(String parentNodeId, NodeType type, String beginsWith);

  List<Node> queryByParentNodeId(String parentNodeId);

  List<Node> queryByParentNodeIdWithTsFilter(String parentNodeId, Instant fromTs);

  List<Node> queryByParentNodeIdWithTsRange(String parentNodeId, Instant fromTs, Instant toTs);

  Node softDeleteNode(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException;

}
