package app.planhive.services.node.persistence;

import static app.planhive.common.TimeConverter.millisToInstant;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.BOOL;
import static com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder.S;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.persistence.DynamoDAO;
import app.planhive.persistence.DynamoDBClient;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.xspec.ExpressionSpecBuilder;
import com.google.common.collect.Lists;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/*
 * NodeDAOImpl provides methods for manipulating Node items in the database.
 *
 * Attributes:
 *  Primary Key:
 *    Hash Key:         {Parent Node Id}                    S
 *    Range Key:        node|{Node Type}|{Node Id}          S
 *  LSI1:
 *    Hash Key:         {Primary Key Hash Key}              S
 *    Range Key:        node|{Last Update Timestamp}        S
 *  Other attributes:
 *    Creator:          {Creator Id}                        S
 *    Priv:             {Private Node}                      B                   Optional
 *    Perm:             {Permission Group Id}               S                   Optional
 *    Ts-C:             {Creation Timestamp}                N                   Optional
 *    Data:             {Node Data}                         S                   Optional
 *    Del:              {Deleted}                           B                   Optional
 */
@Repository
public class NodeDAOImpl extends DynamoDAO implements NodeDAO {

  private static final String ID_PREFIX = "node";

  private static final String ATTRIBUTE_CREATOR_ID = "Creator";
  private static final String ATTRIBUTE_PRIVATE_NODE = "Priv";
  private static final String ATTRIBUTE_PERMISSION_GROUP_ID = "Perm";
  private static final String ATTRIBUTE_CREATION_TS = "Ts-C";
  private static final String ATTRIBUTE_DATA = "Data";
  private static final String ATTRIBUTE_DELETED = "Del";

  private final DynamoDB dynamoDB;
  private final Table table;

  private static final Logger logger = LoggerFactory.getLogger(NodeDAOImpl.class);

  @Autowired
  NodeDAOImpl(DynamoDBClient dynamoDBClient) {
    super(ID_PREFIX);

    this.dynamoDB = dynamoDBClient.getDynamoDB();
    this.table = dynamoDBClient.getTable();
  }

  @Override
  public void putItem(Node node) throws ResourceAlreadyExistsException {
    logger.debug(String
        .format("Putting %s node in DB with id %s", node.getNodeId().getType().name(),
            node.getNodeId().getId()));

    var tableItem = new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(node.getNodeId()),
            DynamoDBClient.PRIMARY_KEY_R,
            toPrimaryRK(node.getNodeId().getType(), node.getNodeId().getId()))
        .withString(DynamoDBClient.LSI1_R,
            addPrefix(Long.toString(node.getLastUpdateTs().getMillis(), 36)));

    if (node.getCreatorId() != null) {
      tableItem = tableItem.withString(ATTRIBUTE_CREATOR_ID, node.getCreatorId());
    }
    if (node.getPrivateNode() != null) {
      tableItem = tableItem.withBoolean(ATTRIBUTE_PRIVATE_NODE, node.getPrivateNode());
    }
    if (node.getPermissionGroupId() != null) {
      tableItem = tableItem.withString(ATTRIBUTE_PERMISSION_GROUP_ID, node.getPermissionGroupId());
    }
    if (node.getCreationTs() != null) {
      tableItem = tableItem.withLong(ATTRIBUTE_CREATION_TS, node.getCreationTs().getMillis());
    }
    if (node.getData() != null) {
      tableItem = tableItem.withString(ATTRIBUTE_DATA, node.getData());
    }

    try {
      var putItemSpec = new PutItemSpec()
          .withItem(tableItem)
          .withConditionExpression(
              String.format("attribute_not_exists(%s) and attribute_not_exists(%s)",
                  DynamoDBClient.PRIMARY_KEY_H, DynamoDBClient.PRIMARY_KEY_R));
      table.putItem(putItemSpec);
      logger.debug(String
          .format("Successfully put %s node in DB with id %s", node.getNodeId().getType().name(),
              node.getNodeId().getId()));
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format("%s node with id %s already exists in DB", node.getNodeId().getType(),
          node.getNodeId().getId());
      logger.warn(msg);
      throw new ResourceAlreadyExistsException(msg);
    } catch (Exception e) {
      logger.error(String
          .format("Error putting %s node in DB with id %s", node.getNodeId().getType().name(),
              node.getNodeId().getId()));
      throw e;
    }
  }

  @Override
  public void batchPutItem(List<Node> nodes) {
    for (var partition : Lists.partition(nodes, DynamoDBClient.MAXIMUM_BATCH_PUT_SIZE)) {
      var dbItems = partition.stream()
          .map(node -> {
                var item = new Item()
                    .withPrimaryKey(
                        DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(node.getNodeId()),
                        DynamoDBClient.PRIMARY_KEY_R,
                        toPrimaryRK(node.getNodeId().getType(), node.getNodeId().getId()))
                    .withString(DynamoDBClient.LSI1_R,
                        addPrefix(Long.toString(node.getLastUpdateTs().getMillis(), 36)));

                if (node.getCreatorId() != null) {
                  item = item.withString(ATTRIBUTE_CREATOR_ID, node.getCreatorId());
                }
                if (node.getPrivateNode() != null) {
                  item = item.withBoolean(ATTRIBUTE_PRIVATE_NODE, node.getPrivateNode());
                }
                if (node.getPermissionGroupId() != null) {
                  item = item.withString(ATTRIBUTE_PERMISSION_GROUP_ID, node.getPermissionGroupId());
                }
                if (node.getCreationTs() != null) {
                  item = item.withLong(ATTRIBUTE_CREATION_TS, node.getCreationTs().getMillis());
                }
                if (node.getData() != null) {
                  item = item.withString(ATTRIBUTE_DATA, node.getData());
                }
                return item;
              }
          ).collect(Collectors.toUnmodifiableList());
      var itemsToWrite = new TableWriteItems(DynamoDBClient.DB_TABLE_NAME)
          .withItemsToPut(dbItems);

      try {
        logger.debug("Starting batchPut operation for node items");
        var outcome = dynamoDB.batchWriteItem(itemsToWrite);

        // TODO(#59): Replace loop with better solution
        while (!outcome.getUnprocessedItems().isEmpty()) {
          logger.debug("BatchPutting unprocessed feed items");
          outcome = dynamoDB.batchWriteItemUnprocessed(outcome.getUnprocessedItems());
        }
      } catch (Exception e) {
        logger.error("Error executing batchPut operation for feed items");
        throw e;
      }
    }
  }

  @Override
  public Node insertNodeWithDataOnly(NodeId nodeId, String creatorId, String data,
      Instant requestTs) {
    logger.debug(String
        .format("Inserting %s node data in DB with id %s", nodeId.getType().name(),
            nodeId.getId()));

    var tableItem = new Item()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
            DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()))
        .withString(ATTRIBUTE_CREATOR_ID, creatorId)
        .withString(DynamoDBClient.LSI1_R, addPrefix(Long.toString(requestTs.getMillis(), 36)))
        .withString(ATTRIBUTE_DATA, data);

    try {
      table.putItem(tableItem);
      logger.debug(String.format("Successfully inserted %s node data in DB with id %s",
          nodeId.getType().name(), nodeId.getId()));
      return constructNodeFromDbItem(tableItem);
    } catch (Exception e) {
      logger.error(String
          .format("Error inserting %s node data in DB with id %s", nodeId.getType().name(),
              nodeId.getId()));
      throw e;
    }
  }

  @Override
  public Node updateNodeData(NodeId nodeId, String data, Instant requestTs) {
    logger.debug(String
        .format("Updating data for %s node with id %s", nodeId.getType().name(), nodeId.getId()));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(
            S(DynamoDBClient.LSI1_R).set(addPrefix(Long.toString(requestTs.getMillis(), 36))))
        .addUpdate(S(ATTRIBUTE_DATA).set(data))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists())
            .and(S(DynamoDBClient.LSI1_R).lt(addPrefix(Long.toString(requestTs.getMillis(), 36)))));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
        DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()))
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var result = table.updateItem(updateItemSpec);
      logger.debug(String.format("Successfully updated data for %s node with id %s",
          nodeId.getType().name(), nodeId.getId()));
      return constructNodeFromDbItem(result.getItem());
    } catch (ConditionalCheckFailedException e) {
      logger.warn(String.format("Could not update data for %s node with id %s because an item "
          + "with a newer timestamp already exists", nodeId.getType().name(), nodeId.getId()));
      throw e;
    } catch (Exception e) {
      logger.error(String
          .format("Error updating data for %s node with id %s", nodeId.getType().name(),
              nodeId.getId()));
      throw e;
    }
  }

  @Override
  public Node refreshLastUpdateTs(NodeId nodeId, Instant requestTs)
      throws ResourceNotFoundException {
    logger.debug(String
        .format("Refreshing lastUpdateTs for %s node with id %s", nodeId.getType().name(),
            nodeId.getId()));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(
            S(DynamoDBClient.LSI1_R).set(addPrefix(Long.toString(requestTs.getMillis(), 36))))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
        DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()))
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var result = table.updateItem(updateItemSpec);
      logger.debug(String.format("Successfully refreshing lastUpdateTs for %s node with id %s",
          nodeId.getType().name(), nodeId.getId()));
      return constructNodeFromDbItem(result.getItem());
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format(
          "Could not refresh lastUpdateTs for %s node with id %s because either the item does "
              + "not exist or an item with a newer timestamp already exists",
          nodeId.getType().name(), nodeId.getId());
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(
          String.format("Error refreshing lastUpdateTs for %s node with id %s",
              nodeId.getType().name(), nodeId.getId()));
      throw e;
    }
  }

  @Override
  public Node readItem(NodeId nodeId) {
    var spec = new GetItemSpec()
        .withPrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
            DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()));

    var item = table.getItem(spec);
    if (item == null) {
      return null;
    }

    return constructNodeFromDbItem(item);
  }

  @Override
  public List<Node> batchGetItem(List<NodeId> nodeIds) {
    logger.debug("Starting batchGet operation for node items");

    if (nodeIds.isEmpty()) {
      return List.of();
    }

    var nodes = new ArrayList<Node>();

    for (var partition : Lists.partition(nodeIds, DynamoDBClient.MAXIMUM_BATCH_GET_SIZE)) {
      var tableKeysAndAttributes = new TableKeysAndAttributes(DynamoDBClient.DB_TABLE_NAME);
      for (var nodeId : partition) {
        tableKeysAndAttributes.addHashAndRangePrimaryKey(
            DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
            DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()));
      }

      try {
        var outcome = dynamoDB.batchGetItem(tableKeysAndAttributes);
        for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
          nodes.add(constructNodeFromDbItem(item));
        }

        while (!outcome.getUnprocessedKeys().isEmpty()) {
          outcome = dynamoDB.batchGetItemUnprocessed(outcome.getUnprocessedKeys());
          for (var item : outcome.getTableItems().get(DynamoDBClient.DB_TABLE_NAME)) {
            nodes.add(constructNodeFromDbItem(item));
          }
        }
      } catch (Exception e) {
        logger.error(
            String.format("Error executing batchGet operation for nodes %s", nodeIds));
        throw e;
      }
    }

    return nodes;
  }

  @Override
  public List<Node> queryNode(String parentNodeId, NodeType type, String beginsWith) {
    logger.debug(String.format("Starting queryNode operation for %s nodes "
        + "with parent id %s, whose ids begin with %s", type.name(), parentNodeId, beginsWith));

    var items = new ArrayList<Node>();
    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#hk = :id and begins_with(#rk, :st)")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.PRIMARY_KEY_H)
              .with("#rk", DynamoDBClient.PRIMARY_KEY_R))
          .withValueMap(new ValueMap()
              .withString(":id", parentNodeId)
              .withString(":st", String.format("%s|%s|%s", ID_PREFIX, type.name(), beginsWith)));

      var dbItems = table.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructNodeFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format("Error executing queryByParentNodeId operation for %s nodes with "
          + "parent id %s, whose ids begin with %s", type.name(), parentNodeId, beginsWith));
      throw e;
    }
    return items;
  }

  @Override
  public List<Node> queryByParentNodeId(String parentNodeId) {
    logger.debug(String.format("Starting queryByParentNodeId operation for nodes with parent id %s",
        parentNodeId));

    var items = new ArrayList<Node>();
    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#hk = :id and begins_with(#p, :p)")
          .withNameMap(
              new NameMap()
                  .with("#hk", DynamoDBClient.PRIMARY_KEY_H)
                  .with("#p", DynamoDBClient.PRIMARY_KEY_R))
          .withValueMap(
              new ValueMap()
                  .withString(":id", parentNodeId)
                  .withString(":p", String.format("%s|", ID_PREFIX)));

      var dbItems = table.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructNodeFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String
          .format("Error executing queryByParentNodeId operation for nodes with parent id %s",
              parentNodeId));
      throw e;
    }

    return items.stream()
        .filter(item -> !item.getNodeId().getId().equals(parentNodeId))
        .collect(Collectors.toUnmodifiableList());
  }

  @Override
  public List<Node> queryByParentNodeIdWithTsFilter(String parentNodeId, Instant fromTs) {
    logger.debug(String.format("Starting queryByParentNodeIdWithTsFilter operation for nodes "
        + "with parent id %s", parentNodeId));

    var index = table.getIndex(DynamoDBClient.LSI1_INDEX);
    var items = new ArrayList<Node>();

    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#hk = :id AND #rk >= :ts")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.PRIMARY_KEY_H)
              .with("#rk", DynamoDBClient.LSI1_R))
          .withValueMap(new ValueMap()
              .withString(":id", parentNodeId)
              .withString(":ts", addPrefix(Long.toString(fromTs.getMillis(), 36))));

      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructNodeFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format(
          "Error executing queryByParentNodeIdWithTsFilter operation for nodes with parent id %s",
          parentNodeId));
      throw e;
    }

    return items.stream()
        .filter(item -> !item.getNodeId().getId().equals(parentNodeId))
        .collect(Collectors.toUnmodifiableList());
  }

  @Override
  public List<Node> queryByParentNodeIdWithTsRange(String parentNodeId, Instant fromTs,
      Instant toTs) {
    logger.debug(String.format("Starting queryByParentNodeIdWithTsRange operation for "
        + "nodes with parent id %s", parentNodeId));

    var index = table.getIndex(DynamoDBClient.LSI1_INDEX);
    var items = new ArrayList<Node>();

    try {
      var querySpec = new QuerySpec()
          .withKeyConditionExpression("#hk = :id AND #rk between :fromTs and :toTs")
          .withNameMap(new NameMap()
              .with("#hk", DynamoDBClient.PRIMARY_KEY_H)
              .with("#rk", DynamoDBClient.LSI1_R))
          .withValueMap(new ValueMap()
              .withString(":id", parentNodeId)
              .withString(":fromTs", addPrefix(Long.toString(fromTs.getMillis() - 1, 36)))
              .withString(":toTs", addPrefix(Long.toString(toTs.getMillis() + 1, 36))));

      var dbItems = index.query(querySpec);
      dbItems.forEach(dbItem -> items.add(constructNodeFromDbItem(dbItem)));
    } catch (Exception e) {
      logger.error(String.format(
          "Error executing queryByParentNodeIdWithTsRange operation for nodes with parent id %s",
          parentNodeId));
      throw e;
    }

    return items.stream()
        .filter(item -> !item.getNodeId().getId().equals(parentNodeId))
        .collect(Collectors.toUnmodifiableList());
  }

  @Override
  public Node softDeleteNode(NodeId nodeId, Instant requestTs) throws ResourceNotFoundException {
    logger.debug(String.format("Marking %s node with id %s as deleted", nodeId.getType().name(),
        nodeId.getId()));

    var xspec = new ExpressionSpecBuilder()
        .addUpdate(BOOL(ATTRIBUTE_DELETED).set(true))
        .addUpdate(
            S(DynamoDBClient.LSI1_R).set(addPrefix(Long.toString(requestTs.getMillis(), 36))))
        .withCondition(S(DynamoDBClient.PRIMARY_KEY_H).exists()
            .and(S(DynamoDBClient.PRIMARY_KEY_R).exists()));

    var updateExpression = xspec.buildForUpdate();
    var updateItemSpec = new UpdateItemSpec().withPrimaryKey(
        DynamoDBClient.PRIMARY_KEY_H, toPrimaryHK(nodeId),
        DynamoDBClient.PRIMARY_KEY_R, toPrimaryRK(nodeId.getType(), nodeId.getId()))
        .withUpdateExpression(updateExpression.getUpdateExpression())
        .withValueMap(updateExpression.getValueMap())
        .withNameMap(updateExpression.getNameMap())
        .withConditionExpression(updateExpression.getConditionExpression())
        .withReturnValues(ReturnValue.ALL_NEW);

    try {
      var outcome = table.updateItem(updateItemSpec);
      logger.debug(String.format("Successfully marked %s node with id %s as deleted",
          nodeId.getType().name(), nodeId.getId()));
      return constructNodeFromDbItem(outcome.getItem());
    } catch (ConditionalCheckFailedException e) {
      var msg = String.format(
          "Attempted to set %s node with id %s as deleted but the node does not exist",
          nodeId.getType().name(), nodeId.getId());
      logger.warn(msg);
      throw new ResourceNotFoundException(msg);
    } catch (Exception e) {
      logger.error(String
          .format("Error marking %s node with id %s as deleted", nodeId.getType().name(),
              nodeId.getId()));
      throw e;
    }
  }

  private String toPrimaryHK(NodeId nodeId) {
    return nodeId.getParentId() != null ? nodeId.getParentId() : nodeId.getId();
  }

  private String toPrimaryRK(NodeType type, String nodeId) {
    return addPrefix(String.format("%s|%s", type.name(), nodeId));
  }

  private String extractNodeIdFromPrimaryRK(String rk) {
    return rk.split("\\|", 3)[2];
  }

  private String extractNodeTypeFromPrimaryRK(String rk) {
    return rk.split("\\|", 3)[1];
  }

  private Node constructNodeFromDbItem(Item item) {
    var id = extractNodeIdFromPrimaryRK(item.getString(DynamoDBClient.PRIMARY_KEY_R));
    var parentId = item.getString(DynamoDBClient.PRIMARY_KEY_H);
    var nodeType = NodeType
        .valueOf(extractNodeTypeFromPrimaryRK(item.getString(DynamoDBClient.PRIMARY_KEY_R)));
    var nodeId = new NodeId(id, !parentId.equals(id) ? parentId : null, nodeType);

    var builder = new Node.Builder(nodeId)
        .withCreatorId(item.getString(ATTRIBUTE_CREATOR_ID))
        .setPrivateNode(
            item.isPresent(ATTRIBUTE_PRIVATE_NODE) && item.getBoolean(ATTRIBUTE_PRIVATE_NODE))
        .withLastUpdateTs(
            millisToInstant(
                Long.parseLong(removePrefix(item.getString(DynamoDBClient.LSI1_R)), 36)))
        .setDeleted(
            item.isPresent(ATTRIBUTE_DELETED) && item.getBoolean(ATTRIBUTE_DELETED)
        );

    if (item.isPresent(ATTRIBUTE_PERMISSION_GROUP_ID)) {
      builder.withPermissionGroupId(item.getString(ATTRIBUTE_PERMISSION_GROUP_ID));
    }
    if (item.isPresent(ATTRIBUTE_CREATION_TS)) {
      builder.withCreationTs(millisToInstant(item.getLong(ATTRIBUTE_CREATION_TS)));
    }
    if (item.isPresent(ATTRIBUTE_DATA)) {
      builder.withData(item.getString(ATTRIBUTE_DATA));
    }

    return builder.build();
  }
}
