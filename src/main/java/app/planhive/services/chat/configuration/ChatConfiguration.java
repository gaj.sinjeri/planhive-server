package app.planhive.services.chat.configuration;

import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.chat.notification.GroupChatNotificationSender;
import app.planhive.services.chat.service.GroupChatService;
import app.planhive.services.chat.service.GroupChatServiceImpl;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.service.UserService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChatConfiguration {

  @Value("${group.chat.cover.photo.max.resolution}")
  private int maxGroupChatCoverPhotoResolution;

  @Value("${group.chat.cover.photo.thumbnail.resolution}")
  private int groupChatCoverPhotoThumbnailResolution;

  @Bean
  public GroupChatService groupChatService(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService,
      GroupChatNotificationSender groupChatNotificationSender, ThreadService threadService,
      UserService userService, GroupService groupService, CloudStore cloudStore) {
    return new GroupChatServiceImpl(secureGenerator, nodeService, feedService, permissionService,
        groupChatNotificationSender, threadService, userService, groupService, cloudStore,
        maxGroupChatCoverPhotoResolution, groupChatCoverPhotoThumbnailResolution);
  }
}
