package app.planhive.services.chat.service;

import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DirectChatServiceImpl implements DirectChatService {

  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final ThreadService threadService;

  @Autowired
  DirectChatServiceImpl(NodeService nodeService, FeedService feedService,
      PermissionService permissionService, ThreadService threadService) {
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.threadService = threadService;
  }

  @Override
  public List<Node> getDirectChat(User sender, String recipientId, Instant requestTs)
      throws ResourceDeletedException {
    var chatId = constructDeterministicId(sender.getId(), recipientId);
    var chatNodeId = new NodeId(chatId, NodeType.CHAT_DIRECT);

    try {
      var chatNode = nodeService.getNodeOrThrow(chatNodeId);
      var threadNodes = nodeService.getChildNodes(chatId);

      return Stream.concat(Stream.of(chatNode), threadNodes.stream())
          .collect(Collectors.toUnmodifiableList());
    } catch (ResourceNotFoundException e) {
      var usersWithAccessControl = Map.of(
          sender.getId(), AccessControl.withRights(AccessRight.READ),
          recipientId, AccessControl.withRights(AccessRight.READ));
      var permissionGroupId = permissionService
          .createPermissionGroup(usersWithAccessControl, requestTs);

      var chatNode = new Node.Builder(chatNodeId)
          .withCreatorId(sender.getId())
          .withPermissionGroupId(permissionGroupId)
          .withCreationTs(requestTs)
          .withLastUpdateTs(requestTs)
          .build();
      insertNodeAndCreateFeed(chatNode, sender.getId(), List.of(recipientId), requestTs);

      var threadNode = threadService
          .createThread(chatNodeId, permissionGroupId, false, sender.getId(), List.of(recipientId),
              requestTs);

      return List.of(chatNode, threadNode);
    }
  }

  private String constructDeterministicId(String userId1, String userId2) {
    var sorted = Stream.of(userId1, userId2)
        .sorted()
        .collect(Collectors.toUnmodifiableList());
    return String.format("%s~%s", sorted.get(0), sorted.get(1));
  }

  private void insertNodeAndCreateFeed(Node chatNode, String creatorId, List<String> userIds,
      Instant requestTs) {
    try {
      nodeService.insertNode(chatNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService
        .createFeeds(chatNode.getNodeId().toFeedItemKey(),
            Stream.concat(userIds.stream(), Stream.of(creatorId))
                .collect(Collectors.toUnmodifiableList()),
            chatNode.getNodeId().getType().name(), requestTs);
  }
}
