package app.planhive.services.chat.service;

import app.planhive.common.Image;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.chat.model.ChatData;
import app.planhive.services.chat.notification.GroupChatNotificationSender;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

public class GroupChatServiceImpl implements GroupChatService {

  private static final Logger logger = LoggerFactory.getLogger(GroupChatServiceImpl.class);

  private static final String COVER_PHOTO_ORIGINAL_URI = "chats/%s/cover.jpeg";
  private static final String COVER_PHOTO_THUMBNAIL_URI = "chats/%s/t-cover.jpeg";

  private final SecureGenerator secureGenerator;
  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final GroupChatNotificationSender groupChatNotificationSender;
  private final ThreadService threadService;
  private final UserService userService;
  private final GroupService groupService;
  private final CloudStore cloudStore;
  private final int maxCoverPhotoResolution;
  private final int coverPhotoThumbnailResolution;

  public GroupChatServiceImpl(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService,
      GroupChatNotificationSender groupChatNotificationSender, ThreadService threadService,
      UserService userService, GroupService groupService, CloudStore cloudStore,
      int maxCoverPhotoResolution, int coverPhotoThumbnailResolution) {
    this.secureGenerator = secureGenerator;
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.groupChatNotificationSender = groupChatNotificationSender;
    this.threadService = threadService;
    this.userService = userService;
    this.groupService = groupService;
    this.cloudStore = cloudStore;
    this.maxCoverPhotoResolution = maxCoverPhotoResolution;
    this.coverPhotoThumbnailResolution = coverPhotoThumbnailResolution;
  }

  @Override
  public List<Node> createGroupChat(User creator, List<String> userIds, String chatName,
      MultipartFile coverPhoto, Instant requestTs) throws InvalidImageException {
    var chatId = secureGenerator.generateUUID();

    if (coverPhoto != null) {
      storeCoverPhoto(coverPhoto, chatId);
    }

    // TODO(#205): Retrieve users from DB and only use ids of users that exist

    var usersWithAccessControl = userIds.stream().collect(
        Collectors.toMap(Function.identity(), x -> AccessControl.withRights(AccessRight.READ)));
    usersWithAccessControl.put(creator.getId(),
        AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE));
    var permissionGroupId = permissionService
        .createPermissionGroup(usersWithAccessControl, requestTs);

    var chatData = new ChatData(chatName, coverPhoto != null ? requestTs.getMillis() : 0);
    var chatNodeId = new NodeId(chatId, NodeType.CHAT_GROUP);
    var chatNode = new Node.Builder(chatNodeId)
        .withCreatorId(creator.getId())
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .withData(chatData.toJson())
        .build();
    insertNodeAndCreateFeed(chatNode, creator.getId(), userIds, requestTs);

    var threadNode = threadService
        .createThread(chatNodeId, permissionGroupId, false, creator.getId(), userIds, requestTs);

    groupChatNotificationSender
        .sendNewChatNotification(creator.getName(), chatNodeId, chatName, threadNode.getNodeId(),
            userIds);

    return List.of(chatNode, threadNode);
  }

  private void storeCoverPhoto(MultipartFile coverPhoto, String chatId)
      throws InvalidImageException {
    if (!Objects.equals(coverPhoto.getContentType(), "image/jpeg")) {
      throw new InvalidImageException("Image must be in jpeg format");
    }

    BufferedImage original;
    BufferedImage thumbnail;

    try (var imageInputStream = coverPhoto.getInputStream()) {
      original = ImageIO.read(imageInputStream);
      Image.verifyResolution(original, maxCoverPhotoResolution);
      Image.verifyAspectRatio(original, 2);

      thumbnail = Image.resize(original, coverPhotoThumbnailResolution);
    } catch (IOException e) {
      var msg = "Error handling image. This could be either due to not being able to access "
          + "the input stream of the original image or create thumbnail";
      logger.error(msg);
      throw new InternalError(msg, e);
    }

    var originalFileName = String.format(COVER_PHOTO_ORIGINAL_URI, chatId);
    var thumbnailFileName = String.format(COVER_PHOTO_THUMBNAIL_URI, chatId);
    cloudStore.storeImage(originalFileName, original);
    cloudStore.storeImage(thumbnailFileName, thumbnail);
  }

  private void insertNodeAndCreateFeed(Node chatNode, String creatorId, List<String> userIds,
      Instant requestTs) {
    try {
      nodeService.insertNode(chatNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService
        .createFeeds(chatNode.getNodeId().toFeedItemKey(),
            Stream.concat(userIds.stream(), Stream.of(creatorId))
                .collect(Collectors.toUnmodifiableList()),
            chatNode.getNodeId().getType().name(), requestTs);
  }

  @Override
  public byte[] getCoverPhoto(User user, String chatId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var chatNode = nodeService.getNodeOrThrow(new NodeId(chatId, NodeType.CHAT_GROUP));

    permissionService
        .verifyAccessRight(user.getId(), chatNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String.format(COVER_PHOTO_ORIGINAL_URI, chatId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public byte[] getCoverPhotoThumbnail(User user, String chatId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var chatNode = nodeService.getNodeOrThrow(new NodeId(chatId, NodeType.CHAT_GROUP));

    permissionService
        .verifyAccessRight(user.getId(), chatNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String.format(COVER_PHOTO_THUMBNAIL_URI, chatId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public Node updateChatData(User user, NodeId chatNodeId, String name, Long coverTs,
      MultipartFile coverPhoto, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException {
    var chatNode = nodeService.getNodeOrThrow(chatNodeId);

    permissionService.verifyAccessRight(user.getId(), chatNode.getPermissionGroupId(),
        AccessRight.WRITE);

    if (coverPhoto != null) {
      storeCoverPhoto(coverPhoto, chatNodeId.getId());
    }

    var chatData = new ChatData(name, coverPhoto != null ? requestTs.getMillis() : coverTs);
    var chatNodeUpdated = nodeService.updateNodeData(chatNodeId, chatData.toJson(), requestTs);

    feedService.refreshFeedsForResource(chatNodeId.toFeedItemKey(), requestTs);

    return chatNodeUpdated;
  }

  @Override
  public void inviteToChat(NodeId chatNodeId, User user, List<String> userIds, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var chatNode = nodeService.getNodeOrThrow(chatNodeId);

    permissionService
        .verifyAccessRight(user.getId(), chatNode.getPermissionGroupId(), AccessRight.WRITE);

    var newMembers = groupService.addUsersToGroup(chatNode, userIds, requestTs, requestTs);

    var chatData = NodeData.fromJson(chatNode.getData(), ChatData.class);
    groupChatNotificationSender
        .sendInvitedToChatNotification(user.getName(), chatNodeId, chatData.getName(),
            newMembers);
    groupChatNotificationSender
        .sendNewMemberNotification(chatNodeId, chatData.getName(), newMembers,
            List.of(user.getId()));
  }

  @Override
  public void setMemberAdminStatus(NodeId chatNodeId, User user, String memberId, boolean setAdmin,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var chatNode = nodeService.getNodeOrThrow(chatNodeId);

    groupService.setMemberAdminStatus(chatNode, user, memberId, setAdmin, requestTs);

    var chatData = NodeData.fromJson(chatNode.getData(), ChatData.class);

    if (setAdmin) {
      groupChatNotificationSender
          .sendNewAdminNotification(chatNodeId, chatData.getName(), memberId);
    }
  }

  @Override
  public void removeUserFromChat(NodeId chatNodeId, User requester, String targetUserId,
      Instant requestTs)
      throws UnauthorisedException, ResourceDeletedException, ResourceNotFoundException {
    var chatNode = nodeService.getNodeOrThrow(chatNodeId);
    var targetUser = userService.getUser(targetUserId);

    var newAdminUserId = groupService
        .removeUserFromGroup(chatNode, requester, targetUser, requestTs);

    newAdminUserId.ifPresent(id -> {
      var chatData = NodeData.fromJson(chatNode.getData(), ChatData.class);
      groupChatNotificationSender
          .sendNewAdminNotification(chatNodeId, chatData.getName(), id);
    });

    // TODO(#172): this should be recorded somewhere. E.g. if a user leaves a chat,
    //  we should display it on the client.
  }

  @Override
  public List<Node> deleteChat(NodeId chatNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var chatNode = nodeService.getNodeOrThrow(chatNodeId);

    var deletedNodes = groupService.deleteGroup(chatNode, user, requestTs);

    var chatData = NodeData.fromJson(chatNode.getData(), ChatData.class);
    groupChatNotificationSender
        .sendGroupChatDeletedNotification(chatNodeId, chatData.getName(), user.getId());

    return deletedNodes;
  }
}
