package app.planhive.services.chat.service;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;

public interface DirectChatService {

  List<Node> getDirectChat(User sender, String recipientId, Instant requestTs)
      throws ResourceDeletedException;

}
