package app.planhive.services.chat.service;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface GroupChatService {

  List<Node> createGroupChat(User creator, List<String> userIds, String chatName,
      MultipartFile coverPhoto, Instant requestTs) throws InvalidImageException;

  byte[] getCoverPhoto(User user, String chatId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  byte[] getCoverPhotoThumbnail(User user, String chatId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateChatData(User user, NodeId chatNodeId, String name, Long coverTs,
      MultipartFile coverPhoto, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException;

  void inviteToChat(NodeId chatNodeId, User user, List<String> userIds, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void setMemberAdminStatus(NodeId chatNodeId, User user, String memberId, boolean setAdmin,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void removeUserFromChat(NodeId chatNodeId, User requester, String targetUserId, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  List<Node> deleteChat(NodeId chatNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  // TODO: add handler to mute chat
}
