package app.planhive.services.chat.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class UpdateChatDataRequest {

  private final String name;
  private final Long coverTs;

  @JsonCreator
  public UpdateChatDataRequest(@JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "coverTs", required = true) Long coverTs) {
    this.name = name;
    this.coverTs = coverTs;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, UpdateChatDataRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public String getName() {
    return name;
  }

  public Long getCoverTs() {
    return coverTs;
  }

  @Override
  public String toString() {
    return "UpdateChatDataRequest{" +
        "name='" + name + '\'' +
        ", coverTs=" + coverTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateChatDataRequest that = (UpdateChatDataRequest) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(coverTs, that.coverTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, coverTs);
  }
}
