package app.planhive.services.chat.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.chat.controller.model.CreateGroupChatRequest;
import app.planhive.services.chat.controller.model.GroupChatAdminStatusRequest;
import app.planhive.services.chat.controller.model.InviteToChatRequest;
import app.planhive.services.chat.controller.model.UpdateChatDataRequest;
import app.planhive.services.chat.service.GroupChatService;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("authenticated")
public class GroupChatController {

  private static final Logger logger = LoggerFactory.getLogger(GroupChatController.class);

  private final GroupChatService groupChatService;

  @Autowired
  GroupChatController(GroupChatService groupChatService) {
    this.groupChatService = groupChatService;
  }

  @PostMapping(path = "chats")
  public ResponseEntity<RestResponse> createGroupChat(@RequestAttribute(USER_ATTRIBUTE) User user,
      @RequestParam("request") CreateGroupChatRequest request,
      @RequestParam(name = "coverPhoto", required = false) MultipartFile coverPhoto) {
    logger.debug(String
        .format("Received request to create group chat %s for user %s", request.getChatName(),
            user.getId()));

    try {
      var nodes = groupChatService
          .createGroupChat(user, request.getUserIds(), request.getChatName(), coverPhoto,
              Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (InvalidImageException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(path = "chats/{chatId}/cover-photo", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getCoverPhoto(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId) {
    logger.debug(
        String.format("Received request to get cover photo for group chat %s", chatId));

    try {
      var image = groupChatService.getCoverPhoto(user, chatId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "chats/{chatId}/cover-photo-thumbnail", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getCoverPhotoThumbnail(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId) {
    logger.debug(
        String.format("Received request to get cover photo thumbnail for group chat %s", chatId));

    try {
      var image = groupChatService.getCoverPhotoThumbnail(user, chatId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "chats/{chatId}")
  public ResponseEntity<RestResponse> updateChatData(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId, @RequestParam("request") UpdateChatDataRequest request,
      @RequestParam(name = "coverPhoto", required = false) MultipartFile coverPhoto) {
    logger.debug(String
        .format("Received request from user %s to update chat data for group chat %s", user.getId(),
            chatId));

    try {
      var chatNode = groupChatService
          .updateChatData(user, new NodeId(chatId, NodeType.CHAT_GROUP), request.getName(),
              request.getCoverTs(), coverPhoto, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(chatNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    } catch (InvalidImageException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping(path = "chats/{chatId}/members", consumes = "application/json")
  public ResponseEntity<RestResponse> inviteToChat(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId, @RequestBody InviteToChatRequest request) {
    logger.debug(String
        .format("Received request from user %s to invite users to  groupchat %s", user.getId(),
            chatId));

    try {
      groupChatService
          .inviteToChat(new NodeId(chatId, NodeType.CHAT_GROUP), user, request.getUserIds(),
              Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PutMapping(path = "chats/{chatId}/members/{memberId}/admin-state")
  public ResponseEntity<RestResponse> setMemberAdminStatus(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("chatId") String chatId,
      @PathVariable("memberId") String memberId, @RequestBody GroupChatAdminStatusRequest request) {
    logger.debug(String
        .format("Received request to update admin status for member %s of group chat %s", memberId,
            chatId));

    try {
      groupChatService.setMemberAdminStatus(new NodeId(chatId, NodeType.CHAT_GROUP), user, memberId,
          request.isSetAdmin(), Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "chats/{chatId}/members/{memberId}")
  public ResponseEntity<RestResponse> removeFromChat(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId, @PathVariable("memberId") String memberId) {
    logger.debug(String
        .format("Received request from user %s to remove user %s from group chat %s", user.getId(),
            memberId, chatId));

    try {
      groupChatService.removeUserFromChat(new NodeId(chatId, NodeType.CHAT_GROUP), user, memberId,
          Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "chats/{chatId}")
  public ResponseEntity<RestResponse> deleteGroupChat(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("chatId") String chatId) {
    logger.debug(
        String
            .format("Received request from user %s to delete group chat %s", user.getId(), chatId));

    try {
      var deletedNodes = groupChatService
          .deleteChat(new NodeId(chatId, NodeType.CHAT_GROUP), user, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(deletedNodes), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(CreateGroupChatRequest.class, new CreateGroupChatRequest.Editor());
    binder.registerCustomEditor(UpdateChatDataRequest.class, new UpdateChatDataRequest.Editor());
  }
}
