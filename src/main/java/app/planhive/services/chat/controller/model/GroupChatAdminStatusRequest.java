package app.planhive.services.chat.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class GroupChatAdminStatusRequest {

  private final boolean setAdmin;

  @JsonCreator
  public GroupChatAdminStatusRequest(
      @JsonProperty(value = "setAdmin", required = true) boolean setAdmin) {
    this.setAdmin = setAdmin;
  }

  public boolean isSetAdmin() {
    return setAdmin;
  }

  @Override
  public String toString() {
    return "GroupChatAdminStatusRequest{" +
        "setAdmin=" + setAdmin +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupChatAdminStatusRequest that = (GroupChatAdminStatusRequest) o;
    return setAdmin == that.setAdmin;
  }

  @Override
  public int hashCode() {
    return Objects.hash(setAdmin);
  }
}
