package app.planhive.services.chat.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class InviteToChatRequest {

  private final List<String> userIds;

  @JsonCreator
  public InviteToChatRequest(
      @JsonProperty(value = "userIds", required = true) List<String> userIds) {
    this.userIds = userIds;
  }

  public List<String> getUserIds() {
    return userIds;
  }

  @Override
  public String
  toString() {
    return "InviteToChatRequest{" +
        "userIds=" + userIds +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InviteToChatRequest that = (InviteToChatRequest) o;
    return Objects.equals(userIds, that.userIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userIds);
  }
}
