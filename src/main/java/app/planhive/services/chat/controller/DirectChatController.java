package app.planhive.services.chat.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.ResourceDeletedException;
import app.planhive.model.RestResponse;
import app.planhive.services.chat.service.DirectChatService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authenticated")
public class DirectChatController {

  private static final Logger logger = LoggerFactory.getLogger(DirectChatController.class);

  private final DirectChatService directChatService;

  @Autowired
  DirectChatController(DirectChatService directChatService) {
    this.directChatService = directChatService;
  }

  @GetMapping(path = "direct-chats/{contactId}")
  public ResponseEntity<RestResponse> getDirectChat(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("contactId") String contactId) {
    logger.debug(String
        .format("Received request to get direct chat for user %s, contact %s", user.getId(),
            contactId));

    try {
      var nodes = directChatService.getDirectChat(user, contactId, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (ResourceDeletedException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
