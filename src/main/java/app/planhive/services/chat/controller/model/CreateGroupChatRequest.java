package app.planhive.services.chat.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.List;
import java.util.Objects;

public class CreateGroupChatRequest {

  private final List<String> userIds;
  private final String chatName;

  @JsonCreator
  public CreateGroupChatRequest(
      @JsonProperty(value = "userIds", required = true) List<String> userIds,
      @JsonProperty(value = "chatName", required = true) String chatName) {

    this.userIds = userIds;
    this.chatName = chatName;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, CreateGroupChatRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public List<String> getUserIds() {
    return userIds;
  }

  public String getChatName() {
    return chatName;
  }

  @Override
  public String toString() {
    return "CreateGroupChatRequest{" +
        "userIds=" + userIds +
        ", chatName='" + chatName + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateGroupChatRequest that = (CreateGroupChatRequest) o;
    return Objects.equals(userIds, that.userIds) &&
        Objects.equals(chatName, that.chatName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userIds, chatName);
  }
}
