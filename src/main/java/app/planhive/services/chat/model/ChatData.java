package app.planhive.services.chat.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.regex.Pattern;

@JsonInclude(Include.NON_EMPTY)
public class ChatData extends NodeData {

  private static final Pattern NAME_PATTERN = Pattern.compile("^.{1,25}$");

  private final String name;
  private final Long coverTs; // Last Update Timestamp for Cover Photo

  @JsonCreator
  public ChatData(@JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "coverTs", required = true) Long coverTs) {
    checkArgument(name != null && NAME_PATTERN.matcher(name).matches(),
        "Group chat name does not match pattern");
    checkNotNull(coverTs);

    this.name = name.trim();
    this.coverTs = coverTs;
  }

  public String getName() {
    return name;
  }

  public Long getCoverTs() {
    return coverTs;
  }

  @Override
  public String toString() {
    return "ChatData{" +
        "name='" + name + '\'' +
        ", coverTs=" + coverTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChatData chatData = (ChatData) o;
    return Objects.equals(name, chatData.name) &&
        Objects.equals(coverTs, chatData.coverTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, coverTs);
  }
}
