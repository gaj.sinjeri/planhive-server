package app.planhive.services.chat.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GroupChatNotificationSenderImpl extends ObjectSerializer implements
    GroupChatNotificationSender {

  private final NotificationService notificationService;

  @Autowired
  GroupChatNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendNewMemberNotification(NodeId chatNodeId, String chatName, List<User> newMembers,
      List<String> excludedUserIds) {
    if (newMembers.isEmpty()) {
      return;
    }

    var newMemberIds = newMembers.stream()
        .map(User::getId)
        .collect(Collectors.toUnmodifiableList());

    String body;
    if (newMembers.size() == 1) {
      body = String.format("%s joined the group chat.", newMembers.get(0).getName());
    } else {
      body = String
          .format("%s and %s other %s joined the group chat.", newMembers.get(0).getName(),
              newMembers.size() - 1,
              newMembers.size() <= 2 ? "person" : "people");
    }

    var allExcludedUserIds = new ArrayList<>(newMemberIds);
    allExcludedUserIds.addAll(excludedUserIds);

    var notification = new Notification.Builder(chatName, body, NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(chatNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, chatNodeId, allExcludedUserIds, true);
  }

  @Override
  public void sendNewChatNotification(String creatorName, NodeId chatNodeId, String chatName,
      NodeId threadNodeId, List<String> userIds) {
    var notification = new Notification.Builder(
        creatorName, String.format("Created a new chat: %s", chatName),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(chatNodeId, threadNodeId))))
        .build();
    notificationService.sendNotificationByUserIds(notification, userIds, true);
  }

  @Override
  public void sendInvitedToChatNotification(String inviterName, NodeId chatNodeId, String chatName,
      List<User> users) {
    var userIds = users.stream()
        .map(User::getId)
        .collect(Collectors.toUnmodifiableList());

    var notification = new Notification.Builder(
        "New group chat", String.format("%s added you to %s.", inviterName, chatName),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(chatNodeId))))
        .build();
    notificationService.sendNotificationByUserIds(notification, userIds, true);
  }

  @Override
  public void sendNewAdminNotification(NodeId chatNodeId, String chatName, String userId) {
    var notification = new Notification.Builder(chatName, "You are now an admin.",
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(chatNodeId))))
        .build();
    notificationService.sendNotificationByUserIds(notification, List.of(userId), true);
  }

  @Override
  public void sendGroupChatDeletedNotification(NodeId chatNodeId, String chatName, String userId) {
    var notification = new Notification.Builder(chatName, "Group chat has been deleted.",
        NotificationCategory.GROUP_CHATS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(chatNodeId))))
        .build();
    notificationService.sendNotificationByNodeId(notification, chatNodeId, List.of(userId), true);
  }
}
