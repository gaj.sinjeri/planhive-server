package app.planhive.services.chat.notification;

import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

import java.util.List;

public interface GroupChatNotificationSender {

  void sendNewMemberNotification(NodeId chatNodeId, String chatName, List<User> newMembers,
      List<String> excludedUserIds);

  void sendNewChatNotification(String creatorName, NodeId chatNodeId, String chatName,
      NodeId threadNodeId, List<String> userIds);

  void sendInvitedToChatNotification(String inviterName, NodeId chatNodeId, String chatName,
      List<User> users);

  void sendNewAdminNotification(NodeId chatNodeId, String chatName, String userId);

  void sendGroupChatDeletedNotification(NodeId chatNodeId, String chatName, String userId);

}
