package app.planhive.services.photo.notification;

import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

public interface PhotoNotificationSender {

  void sendNewPhotosNotification(User user, NodeId eventNodeId, String eventName,
      NodeId photosNodeId);

}
