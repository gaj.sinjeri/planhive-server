package app.planhive.services.photo.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.Notification.Builder;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PhotoNotificationSenderImpl extends ObjectSerializer implements
    PhotoNotificationSender {

  private final NotificationService notificationService;

  @Autowired
  PhotoNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendNewPhotosNotification(User user, NodeId eventNodeId, String eventName,
      NodeId photosNodeId) {
    var notification = new Builder(eventName,
        String.format("%s has uploaded new photos to the event album!", user.getName()),
        NotificationCategory.ALBUMS)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(),
                serializeObject(List.of(eventNodeId, photosNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, photosNodeId, List.of(user.getId()), true);
  }
}
