package app.planhive.services.photo.configuration;

import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.notification.PhotoNotificationSender;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.photo.service.PhotoServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PhotoConfiguration {

  @Value("${photo.max.resolution}")
  private int maxPhotoResolution;

  @Value("${photo.thumbnail.resolution}")
  private int photoThumbnailResolution;

  @Bean
  public PhotoService photoService(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService, CloudStore cloudStore,
      PhotoNotificationSender photoNotificationSender) {
    return new PhotoServiceImpl(secureGenerator, nodeService, feedService, permissionService,
        cloudStore, photoNotificationSender, maxPhotoResolution, photoThumbnailResolution);
  }
}
