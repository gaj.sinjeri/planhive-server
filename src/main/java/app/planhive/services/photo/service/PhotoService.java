package app.planhive.services.photo.service;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PhotoService {

  Node addPhotosWidget(String parentId, String permissionGroupId, String creatorId,
      List<String> userIds, Instant requestTs);

  List<Node> addPhoto(NodeId photosNodeId, User user, MultipartFile photo, Instant requestTs)
      throws InvalidImageException, ResourceNotFoundException, ResourceDeletedException,
      UnauthorisedException;

  void sendNewPhotosNotification(User user, NodeId photosNodeId)
      throws ResourceNotFoundException, ResourceDeletedException;

  byte[] retrievePhoto(NodeId photosNodeId, NodeId photoNodeId, User user)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  byte[] retrieveThumbnail(NodeId photosNodeId, NodeId photoNodeId, User user)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void deletePhoto(NodeId photosNodeId, NodeId photoNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

}
