package app.planhive.services.photo.service;

import static app.planhive.common.Image.resize;
import static app.planhive.common.Image.verifyResolution;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.notification.PhotoNotificationSender;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

public class PhotoServiceImpl implements PhotoService {

  private static final Logger logger = LoggerFactory.getLogger(PhotoServiceImpl.class);

  private static final String PHOTO_ORIGINAL_URI = "events/%s/photos/%s/%s.jpeg";
  private static final String PHOTO_THUMBNAIL_URI = "events/%s/photos/%s/t-%s.jpeg";

  private final SecureGenerator secureGenerator;
  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final CloudStore cloudStore;
  private final PhotoNotificationSender photoNotificationSender;
  private final int maxPhotoResolution;
  private final int photoThumbnailResolution;

  public PhotoServiceImpl(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService, CloudStore cloudStore,
      PhotoNotificationSender photoNotificationSender, int maxPhotoResolution,
      int photoThumbnailResolution) {
    this.secureGenerator = secureGenerator;
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.cloudStore = cloudStore;
    this.photoNotificationSender = photoNotificationSender;
    this.maxPhotoResolution = maxPhotoResolution;
    this.photoThumbnailResolution = photoThumbnailResolution;
  }

  @Override
  public Node addPhotosWidget(String parentId, String permissionGroupId, String creatorId,
      List<String> userIds, Instant requestTs) {

    var widgetId = secureGenerator.generateUUID();
    var nodeId = new NodeId(widgetId, parentId, NodeType.PHOTOS);
    var node = new Node.Builder(nodeId)
        .withCreatorId(creatorId)
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .build();

    try {
      nodeService.insertNode(node);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService.createFeeds(node.getNodeId().toFeedItemKey(),
        Stream.concat(userIds.stream(), Stream.of(creatorId))
            .collect(Collectors.toUnmodifiableList()),
        node.getNodeId().getType().name(), requestTs);

    return node;
  }

  @Override
  public List<Node> addPhoto(NodeId photosNodeId, User user, MultipartFile photo, Instant requestTs)
      throws InvalidImageException, ResourceNotFoundException, ResourceDeletedException,
      UnauthorisedException {
    if (!Objects.equals(photo.getContentType(), "image/jpeg")) {
      throw new InvalidImageException("Image must be in jpeg format");
    }

    var photosNode = nodeService.getNodeOrThrow(photosNodeId);

    // TODO: This should ideally check for WRITE rights,
    //  but non-admin members only have READ right at the moment
    permissionService
        .verifyAccessRight(user.getId(), photosNode.getPermissionGroupId(), AccessRight.READ);

    var photoId = secureGenerator.generateUUID();

    storePhoto(photo, photosNodeId.getParentId(), photosNodeId.getId(), photoId);

    var photoNodeId = new NodeId(photoId, photosNodeId.getId(), NodeType.PHOTO);
    var photoNode = new Node.Builder(photoNodeId)
        .withCreatorId(user.getId())
        .withPermissionGroupId(photosNode.getPermissionGroupId())
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .build();

    try {
      nodeService.insertNode(photoNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    var updatedPhotosNode = nodeService.refreshLastUpdateTs(photosNodeId, requestTs);
    feedService.refreshFeedsForResource(photosNodeId.toFeedItemKey(), requestTs);

    return List.of(updatedPhotosNode, photoNode);
  }

  private void storePhoto(MultipartFile photo, String groupId, String photosNodeId, String photoId)
      throws InvalidImageException {
    BufferedImage original;
    BufferedImage thumbnail;
    try (var photoInputStream = photo.getInputStream()) {
      original = ImageIO.read(photoInputStream);

      if (original == null) {
        throw new InvalidImageException("The provided image wasn't saved in a supported format");
      }

      verifyResolution(original, maxPhotoResolution);
      thumbnail = resize(original, photoThumbnailResolution);

      var originalFileName = String.format(PHOTO_ORIGINAL_URI, groupId, photosNodeId, photoId);
      var thumbnailFileName = String.format(PHOTO_THUMBNAIL_URI, groupId, photosNodeId, photoId);
      cloudStore.storeImage(originalFileName, original);
      cloudStore.storeImage(thumbnailFileName, thumbnail);
    } catch (IOException e) {
      var msg = "Error handling image. This could be either due to not being able to access "
          + "the input stream of the original image or create thumbnail";
      logger.error(msg);
      throw new InternalError(msg, e);
    }
  }

  @Override
  public void sendNewPhotosNotification(User user, NodeId photosNodeId)
      throws ResourceNotFoundException, ResourceDeletedException {
    var eventNode = nodeService
        .getNodeOrThrow(new NodeId(photosNodeId.getParentId(), NodeType.EVENT));
    var eventNodeData = NodeData.fromJson(eventNode.getData(), EventData.class);

    photoNotificationSender
        .sendNewPhotosNotification(user, eventNode.getNodeId(), eventNodeData.getName(),
            photosNodeId);
  }

  @Override
  public byte[] retrievePhoto(NodeId photosNodeId, NodeId photoNodeId, User user)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var photoNode = nodeService.getNodeOrThrow(photoNodeId);

    permissionService
        .verifyAccessRight(user.getId(), photoNode.getPermissionGroupId(), AccessRight.READ);

    return cloudStore.getFile(String
        .format(PHOTO_ORIGINAL_URI, photosNodeId.getParentId(), photosNodeId.getId(),
            photoNodeId.getId()));
  }

  @Override
  public byte[] retrieveThumbnail(NodeId photosNodeId, NodeId photoNodeId, User user)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var photoNode = nodeService.getNodeOrThrow(photoNodeId);

    permissionService
        .verifyAccessRight(user.getId(), photoNode.getPermissionGroupId(), AccessRight.READ);

    return cloudStore.getFile(String
        .format(PHOTO_THUMBNAIL_URI, photosNodeId.getParentId(), photosNodeId.getId(),
            photoNodeId.getId()));
  }

  @Override
  public void deletePhoto(NodeId photosNodeId, NodeId photoNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    nodeService.getNodeOrThrow(photosNodeId);
    var photoNode = nodeService.getNodeOrThrow(photoNodeId);

    if (!photoNode.getCreatorId().equals(user.getId())) {
      permissionService
          .verifyAccessRight(user.getId(), photoNode.getPermissionGroupId(), AccessRight.DELETE);
    }

    nodeService.softDeleteNode(photoNodeId, requestTs);
    feedService.refreshFeedsForResource(photosNodeId.toFeedItemKey(), requestTs);
  }
}
