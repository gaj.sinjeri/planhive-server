package app.planhive.services.photo.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("authenticated")
public class PhotoController {

  private static final Logger logger = LoggerFactory.getLogger(PhotoController.class);

  private final PhotoService photoService;

  @Autowired
  PhotoController(PhotoService photoService) {
    this.photoService = photoService;
  }

  @PostMapping(path = "events/{eventId}/photos/{photosWidgetId}/files")
  public ResponseEntity<RestResponse> addPhoto(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @PathVariable("photosWidgetId") String photosWidgetId,
      @RequestParam("photo") MultipartFile photo) {
    logger
        .debug(String.format("Received request to add photo to photos widget %s", photosWidgetId));

    try {
      var photosNodeId = new NodeId(photosWidgetId, eventId, NodeType.PHOTOS);
      var nodes = photoService.addPhoto(photosNodeId, user, photo, Instant.now());

      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (InvalidImageException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PostMapping(path = "events/{eventId}/photos/{photosWidgetId}/notification")
  public ResponseEntity<RestResponse> sendPhotoUploadNotification(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @PathVariable("photosWidgetId") String photosWidgetId) {
    logger.debug(String.format("Received request to send notification for photo uploads "
        + "to photos widget %s", photosWidgetId));

    try {
      var photosNodeId = new NodeId(photosWidgetId, eventId, NodeType.PHOTOS);
      photoService.sendNewPhotosNotification(user, photosNodeId);

      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(path = "events/{eventId}/photos/{photosWidgetId}/files/{photoId}/original",
      produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getPhoto(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @PathVariable("photosWidgetId") String photosWidgetId,
      @PathVariable("photoId") String photoId) {
    logger.debug(String.format("Received request to get photo %s", photoId));

    try {
      var photosNodeId = new NodeId(photosWidgetId, eventId, NodeType.PHOTOS);
      var photoNodeId = new NodeId(photoId, photosWidgetId, NodeType.PHOTO);
      var photo = photoService.retrievePhoto(photosNodeId, photoNodeId, user);

      return new ResponseEntity<>(photo, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "events/{eventId}/photos/{photosWidgetId}/files/{photoId}/thumbnail",
      produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getThumbnail(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @PathVariable("photosWidgetId") String photosWidgetId,
      @PathVariable("photoId") String photoId) {
    logger.debug(String.format("Received request to get photo %s", photoId));

    try {
      var photosNodeId = new NodeId(photosWidgetId, eventId, NodeType.PHOTOS);
      var photoNodeId = new NodeId(photoId, photosWidgetId, NodeType.PHOTO);
      var photo = photoService.retrieveThumbnail(photosNodeId, photoNodeId, user);

      return new ResponseEntity<>(photo, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "events/{eventId}/photos/{photosWidgetId}/files/{photoId}")
  public ResponseEntity<RestResponse> deletePhoto(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @PathVariable("photosWidgetId") String photosWidgetId,
      @PathVariable("photoId") String photoId) {
    logger
        .debug(String.format("Received request to delete photo %s", photoId));

    try {
      var photosNodeId = new NodeId(photosWidgetId, eventId, NodeType.PHOTOS);
      var photoNodeId = new NodeId(photoId, photosWidgetId, NodeType.PHOTO);

      photoService.deletePhoto(photosNodeId, photoNodeId, user, Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }
}
