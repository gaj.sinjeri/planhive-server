package app.planhive.services.timeline.model;

import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;

import java.util.Objects;

@JsonInclude(Include.NON_EMPTY)
public class Location {

  private final String title;
  private final String address;
  private final String latitude;
  private final String longitude;

  @JsonCreator
  public Location(@JsonProperty(value = "title") String title,
      @JsonProperty(value = "address") String address,
      @JsonProperty(value = "latitude") String latitude,
      @JsonProperty(value = "longitude") String longitude) {
    checkArgument(!Strings.isNullOrEmpty(latitude), "Latitude cannot be null or empty");
    checkArgument(!Strings.isNullOrEmpty(longitude), "Longitude cannot be null or empty");

    this.title = title;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getTitle() {
    return title;
  }

  public String getAddress() {
    return address;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  @Override
  public String toString() {
    return "Location{" +
        "title='" + title + '\'' +
        ", address='" + address + '\'' +
        ", latitude='" + latitude + '\'' +
        ", longitude='" + longitude + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Location location = (Location) o;
    return Objects.equals(title, location.title) &&
        Objects.equals(address, location.address) &&
        Objects.equals(latitude, location.latitude) &&
        Objects.equals(longitude, location.longitude);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, address, latitude, longitude);
  }
}
