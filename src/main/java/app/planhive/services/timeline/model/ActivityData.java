package app.planhive.services.timeline.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.regex.Pattern;

@JsonInclude(Include.NON_EMPTY)
public class ActivityData extends NodeData {

  private static final Pattern TITLE_PATTERN = Pattern.compile("^.{1,25}$");
  private static final Pattern DESCRIPTION_PATTERN = Pattern.compile("^.{0,512}$");
  private static final Pattern NOTE_PATTERN = Pattern.compile("^.{0,128}$");

  private final ActivityState activityState;
  private final String title;
  private final String description;
  private final String note;
  private final ActivityTime time;
  private final Location location;

  @JsonCreator
  private ActivityData(@JsonProperty("activityState") ActivityState activityState,
      @JsonProperty("title") String title, @JsonProperty("description") String description,
      @JsonProperty("note") String note, @JsonProperty("time") ActivityTime time,
      @JsonProperty("location") Location location) {
    checkNotNull(activityState, "Activity State cannot be null");
    checkArgument(title != null && TITLE_PATTERN.matcher(title).matches());
    checkArgument(description == null || DESCRIPTION_PATTERN.matcher(description).matches());
    checkArgument(note == null || NOTE_PATTERN.matcher(note).matches());
    checkNotNull(time, "Time cannot be null");

    this.activityState = activityState;
    this.title = title;
    this.description = description;
    this.note = note;
    this.time = time;
    this.location = location;
  }

  public static class Builder {

    private ActivityState activityState;
    private String title;
    private String description;
    private String note;
    private ActivityTime time;
    private Location location;

    public Builder withActivityState(ActivityState activityState) {
      this.activityState = activityState;
      return this;
    }

    public Builder withTitle(String title) {
      this.title = title;
      return this;
    }

    public Builder withDescription(String description) {
      this.description = description;
      return this;
    }

    public Builder withNote(String note) {
      this.note = note;
      return this;
    }

    public Builder withTime(ActivityTime time) {
      this.time = time;
      return this;
    }

    public Builder withLocation(Location location) {
      this.location = location;
      return this;
    }

    public ActivityData build() {
      return new ActivityData(activityState, title, description, note, time, location);
    }
  }

  public ActivityState getActivityState() {
    return activityState;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getNote() {
    return note;
  }

  public ActivityTime getTime() {
    return time;
  }

  public Location getLocation() {
    return location;
  }

  @Override
  public String toString() {
    return "ActivityData{" +
        "activityState=" + activityState +
        ", title='" + title + '\'' +
        ", description='" + description + '\'' +
        ", note='" + note + '\'' +
        ", time=" + time +
        ", location=" + location +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActivityData that = (ActivityData) o;
    return activityState == that.activityState &&
        Objects.equals(title, that.title) &&
        Objects.equals(description, that.description) &&
        Objects.equals(note, that.note) &&
        Objects.equals(time, that.time) &&
        Objects.equals(location, that.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activityState, title, description, note, time, location);
  }
}
