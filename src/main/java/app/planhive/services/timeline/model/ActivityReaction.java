package app.planhive.services.timeline.model;

public enum ActivityReaction {
  VOTE,
  REJECT
}
