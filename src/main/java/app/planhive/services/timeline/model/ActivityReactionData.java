package app.planhive.services.timeline.model;

import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class ActivityReactionData extends NodeData {

  public final ActivityReaction activityReaction;

  @JsonCreator
  public ActivityReactionData(
      @JsonProperty(value = "activityReaction", required = true) ActivityReaction activityReaction) {
    checkNotNull(activityReaction);

    this.activityReaction = activityReaction;
  }

  public ActivityReaction getActivityReaction() {
    return activityReaction;
  }

  @Override
  public String toString() {
    return "ActivityReactionData{" +
        "activityReaction=" + activityReaction +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActivityReactionData that = (ActivityReactionData) o;
    return activityReaction == that.activityReaction;
  }

  @Override
  public int hashCode() {
    return Objects.hash(activityReaction);
  }
}
