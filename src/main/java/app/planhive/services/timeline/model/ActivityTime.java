package app.planhive.services.timeline.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonInclude(Include.NON_EMPTY)
public class ActivityTime {

  private final Long startTs;
  private final Long endTs;

  @JsonCreator
  public ActivityTime(@JsonProperty(value = "startTs", required = true) Long startTs,
      @JsonProperty(value = "endTs", required = true) Long endTs) {
    checkNotNull(startTs);
    checkNotNull(endTs);

    if (endTs != 0) {
      checkArgument(startTs < endTs, "Start timestamp must be before end timestamp");
    }

    this.startTs = startTs;
    this.endTs = endTs;
  }

  public Long getStartTs() {
    return startTs;
  }

  public Long getEndTs() {
    return endTs;
  }

  @Override
  public String toString() {
    return "ActivityTime{" +
        "startTs=" + startTs +
        ", endTs=" + endTs +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActivityTime that = (ActivityTime) o;
    return Objects.equals(startTs, that.startTs) &&
        Objects.equals(endTs, that.endTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(startTs, endTs);
  }
}
