package app.planhive.services.timeline.model;

public enum ActivityState {
  SUGGESTION,
  APPROVED,
  CANCELED
}
