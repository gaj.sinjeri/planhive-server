package app.planhive.services.timeline.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.exception.UnauthorisedException;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.event.model.EventData;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TimelineNotificationSenderImpl extends ObjectSerializer implements
    TimelineNotificationSender {

  private final NotificationService notificationService;
  private final PermissionService permissionService;

  @Autowired
  TimelineNotificationSenderImpl(NotificationService notificationService,
      PermissionService permissionService) {
    this.notificationService = notificationService;
    this.permissionService = permissionService;
  }

  @Override
  public void sendNewActivityNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, ActivityState activityState) {
    var notificationBody = String.format("%s %s a new activity.", sender.getName(),
        activityState.equals(ActivityState.APPROVED) ? "added" : "suggested");

    var notification = new Notification.Builder(eventName, notificationBody,
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNodeId, timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, timelineNodeId, List.of(sender.getId()), true);
  }

  @Override
  public void sendActivityCanceledNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, String activityTitle) {
    var notification = new Notification.Builder(eventName,
        String.format("%s has been cancelled.", activityTitle), NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNodeId, timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, timelineNodeId, List.of(sender.getId()), true);
  }

  @Override
  public void sendActivityApprovedNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, String activityTitle) {
    var notification = new Notification.Builder(eventName,
        String.format("%s has been approved.", activityTitle), NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNodeId, timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, timelineNodeId, List.of(sender.getId()), true);
  }

  @Override
  public void sendActivityDataChangedNotification(String userId, NodeId eventNodeId,
      String eventName, NodeId timelineNodeId, NodeId activityNodeId, String activityTitle,
      boolean isSuggestion) {
    var bodyBuilder = new StringBuilder();
    bodyBuilder.append(isSuggestion ? "A suggestion" : "An activity");
    if (activityTitle != null) {
      bodyBuilder.append(" (");
      bodyBuilder.append(activityTitle);
      bodyBuilder.append(")");
    }
    bodyBuilder.append(" has been updated!");

    var notification = new Notification.Builder(eventName, bodyBuilder.toString(),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNodeId, timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, timelineNodeId, List.of(userId), true);
  }

  @Override
  public void sendActivityReactionNotification(User user, Node eventNode, NodeId timelineNodeId,
      NodeId activityNodeId, String activityTitle, ActivityReaction activityReaction) {
    if (user.getId().equals(eventNode.getCreatorId())) {
      return;
    }

    try {
      permissionService
          .verifyAccessRight(eventNode.getCreatorId(), eventNode.getPermissionGroupId(),
              AccessRight.WRITE);
    } catch (UnauthorisedException e) {
      // If creator is no longer part of the event, the notification is not sent.
      return;
    }

    var eventName = NodeData.fromJson(eventNode.getData(), EventData.class).getName();

    var bodyBuilder = new StringBuilder();
    bodyBuilder.append(user.getName());
    bodyBuilder.append(activityReaction == ActivityReaction.VOTE ? " liked"
        : activityReaction == ActivityReaction.REJECT ? " rejected" : " reacted to");
    bodyBuilder.append(" a suggestion");
    if (activityTitle != null) {
      bodyBuilder.append(" (");
      bodyBuilder.append(activityTitle);
      bodyBuilder.append(")");
    }
    bodyBuilder.append("!");

    var notification = new Notification.Builder(eventName, bodyBuilder.toString(),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNode.getNodeId(), timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByUserIds(notification, List.of(eventNode.getCreatorId()), false);
  }

  @Override
  public void sendActivityDeletedNotification(User sender, Node eventNode, NodeId timelineNodeId,
      NodeId activityNodeId, String activityTitle) {
    var eventName = NodeData.fromJson(eventNode.getData(), EventData.class).getName();

    var bodyBuilder = new StringBuilder();
    bodyBuilder.append("An activity has been deleted");
    if (activityTitle != null) {
      bodyBuilder.append(" (");
      bodyBuilder.append(activityTitle);
      bodyBuilder.append(")");
    }
    bodyBuilder.append(".");

    var notification = new Notification.Builder(eventName, bodyBuilder.toString(),
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(Map.of(NotificationKey.NODE_IDS.getValue(),
            serializeObject(List.of(eventNode.getNodeId(), timelineNodeId, activityNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, timelineNodeId, List.of(sender.getId()), true);
  }
}
