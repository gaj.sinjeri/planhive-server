package app.planhive.services.timeline.notification;

import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.user.model.User;

public interface TimelineNotificationSender {

  void sendNewActivityNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, ActivityState activityState);

  void sendActivityCanceledNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, String activityTitle);

  void sendActivityApprovedNotification(User sender, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, String activityTitle);

  void sendActivityDataChangedNotification(String userId, NodeId eventNodeId, String eventName,
      NodeId timelineNodeId, NodeId activityNodeId, String activityName, boolean isSuggestion);

  void sendActivityReactionNotification(User user, Node eventNode, NodeId timelineNodeId,
      NodeId activityNodeId, String activityTitle, ActivityReaction activityReaction);

  void sendActivityDeletedNotification(User sender, Node eventNode, NodeId timelineNodeId,
      NodeId activityNodeId, String activityTitle);

}
