package app.planhive.services.timeline.service;

import static app.planhive.services.node.common.LeafNode.generateLeafNodeId;
import static com.google.common.base.Preconditions.checkArgument;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.model.EventData;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityReactionData;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.timeline.notification.TimelineNotificationSender;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TimelineServiceImpl implements TimelineService {

  private final SecureGenerator secureGenerator;
  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final TimelineNotificationSender timelineNotificationSender;

  @Autowired
  TimelineServiceImpl(SecureGenerator secureGenerator, NodeService nodeService,
      FeedService feedService, PermissionService permissionService,
      TimelineNotificationSender timelineNotificationSender) {
    this.secureGenerator = secureGenerator;
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.timelineNotificationSender = timelineNotificationSender;
  }

  @Override
  public List<Node> addTimelineWidget(String parentId, String permissionGroupId, String creatorId,
      List<String> userIds, Optional<ActivityData> activityData, Instant requestTs) {
    var timelineId = secureGenerator.generateUUID();
    var timelineNodeId = new NodeId(timelineId, parentId, NodeType.TIMELINE);
    var timelineNode = new Node.Builder(timelineNodeId)
        .withCreatorId(creatorId)
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .build();

    Node activityNode = null;
    if (activityData.isPresent()) {
      var activityNodeId = new NodeId(secureGenerator.generateUUID(), timelineId,
          NodeType.ACTIVITY);
      activityNode = new Node.Builder(activityNodeId)
          .withCreatorId(creatorId)
          .withPermissionGroupId(permissionGroupId)
          .withCreationTs(requestTs)
          .withLastUpdateTs(requestTs)
          .withData(activityData.get().toJson())
          .build();
    }

    var nodes = new ArrayList<Node>();
    nodes.add(timelineNode);
    if (activityNode != null){
      nodes.add(activityNode);
    }

    nodeService.batchInsertNode(nodes);

    feedService.createFeeds(timelineNodeId.toFeedItemKey(),
        Stream.concat(userIds.stream(), Stream.of(creatorId))
            .collect(Collectors.toUnmodifiableList()), timelineNodeId.getType().name(), requestTs);

    return nodes;
  }

  @Override
  public Node addActivity(User user, NodeId timelineNodeId, ActivityData activityData,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNodeId = new NodeId(timelineNodeId.getParentId(), NodeType.EVENT);
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var timelineNode = nodeService.getNodeOrThrow(timelineNodeId);

    AccessRight accessRight;
    if (activityData.getActivityState() == ActivityState.APPROVED) {
      accessRight = AccessRight.WRITE;
    } else if (activityData.getActivityState() == ActivityState.SUGGESTION) {
      accessRight = AccessRight.READ;
    } else {
      throw new UnsupportedOperationException(String
          .format("Activity state {%s} not supported for this operation",
              activityData.getActivityState()));
    }

    permissionService
        .verifyAccessRight(user.getId(), timelineNode.getPermissionGroupId(), accessRight);

    var activityNodeId = new NodeId(secureGenerator.generateUUID(), timelineNodeId.getId(),
        NodeType.ACTIVITY);
    var activityNode = new Node.Builder(activityNodeId)
        .withCreatorId(user.getId())
        .withPermissionGroupId(timelineNode.getPermissionGroupId())
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .withData(activityData.toJson())
        .build();

    try {
      nodeService.insertNode(activityNode);
      feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

      var eventNodeData = NodeData.fromJson(eventNode.getData(), EventData.class);
      timelineNotificationSender
          .sendNewActivityNotification(user, eventNodeId, eventNodeData.getName(), timelineNodeId,
              activityNodeId, activityData.getActivityState());

      return activityNode;
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }
  }

  @Override
  public Node updateActivityState(User user, NodeId timelineNodeId, NodeId activityNodeId,
      ActivityState activityState, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var activityNode = nodeService.getNodeOrThrow(activityNodeId);
    var activityData = NodeData.fromJson(activityNode.getData(), ActivityData.class);

    if (activityState == ActivityState.APPROVED) {
      checkArgument(activityData.getActivityState() == ActivityState.SUGGESTION,
          String.format("Cannot approve activity unless state is %s", ActivityState.SUGGESTION));
    } else if (activityState == ActivityState.CANCELED) {
      checkArgument(activityData.getActivityState() == ActivityState.APPROVED,
          String.format("Cannot cancel activity unless state is %s", ActivityState.APPROVED));
    } else {
      throw new UnsupportedOperationException(
          String.format("Unsupported activity state {%s}", activityState));
    }

    permissionService.verifyAccessRight(user.getId(), activityNode.getPermissionGroupId(),
        AccessRight.WRITE);

    var newActivityData = new ActivityData.Builder()
        .withActivityState(activityState)
        .withTitle(activityData.getTitle())
        .withDescription(activityData.getDescription())
        .withNote(activityData.getNote())
        .withTime(activityData.getTime())
        .withLocation(activityData.getLocation())
        .build();
    var activityNodeUpdated = nodeService
        .updateNodeData(activityNodeId, newActivityData.toJson(), requestTs);

    var eventNode = nodeService
        .getNodeOrThrow(new NodeId(timelineNodeId.getParentId(), NodeType.EVENT));
    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    if (activityState == ActivityState.APPROVED) {
      timelineNotificationSender
          .sendActivityApprovedNotification(user, eventNode.getNodeId(), eventData.getName(),
              timelineNodeId, activityNodeId, newActivityData.getTitle());
    } else {
      timelineNotificationSender
          .sendActivityCanceledNotification(user, eventNode.getNodeId(), eventData.getName(),
              timelineNodeId, activityNodeId, newActivityData.getTitle());
    }

    feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

    return activityNodeUpdated;
  }

  @Override
  public Node updateActivityData(User user, NodeId timelineNodeId, NodeId activityNodeId,
      String title, String description, String note, ActivityTime time, Location location,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    nodeService.getNodeOrThrow(timelineNodeId);
    var activityNode = nodeService.getNodeOrThrow(activityNodeId);
    var activityData = NodeData.fromJson(activityNode.getData(), ActivityData.class);

    if (activityData.getActivityState() == ActivityState.SUGGESTION && !user.getId()
        .equals(activityNode.getCreatorId())) {
      throw new UnauthorisedException(String
          .format("Cannot update data for activity %s since user is not creator",
              activityNodeId.getId()));
    } else if (activityData.getActivityState() == ActivityState.APPROVED) {
      permissionService.verifyAccessRight(user.getId(), activityNode.getPermissionGroupId(),
          AccessRight.WRITE);
    } else if (activityData.getActivityState() == ActivityState.CANCELED) {
      throw new UnsupportedOperationException(String
          .format("Activity state {%s} not supported for this operation",
              activityData.getActivityState()));
    }

    var newActivityData = new ActivityData.Builder()
        .withActivityState(activityData.getActivityState())
        .withTitle(title)
        .withDescription(description)
        .withNote(note)
        .withTime(time)
        .withLocation(location)
        .build();
    var activityNodeUpdated = nodeService
        .updateNodeData(activityNodeId, newActivityData.toJson(), requestTs);

    feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

    var eventNode = nodeService
        .getNodeOrThrow(new NodeId(timelineNodeId.getParentId(), NodeType.EVENT));
    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    timelineNotificationSender
        .sendActivityDataChangedNotification(user.getId(), eventNode.getNodeId(),
            eventData.getName(), timelineNodeId, activityNodeId, title,
            activityData.getActivityState() == ActivityState.SUGGESTION);

    return activityNodeUpdated;
  }

  @Override
  public Node updateReaction(User user, NodeId timelineNodeId, NodeId activityNodeId,
      ActivityReaction activityReaction, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, InvalidOperationException,
      UnauthorisedException {
    nodeService.getNodeOrThrow(timelineNodeId);
    var activityNode = nodeService.getNodeOrThrow(activityNodeId);

    if (activityNode.getCreatorId().equals(user.getId())) {
      throw new InvalidOperationException(String
          .format("User cannot react to their own activity. Activity id %s, user id %s",
              activityNodeId.getId(), user.getId()));
    }

    permissionService
        .verifyAccessRight(user.getId(), activityNode.getPermissionGroupId(), AccessRight.READ);

    var activityReactionId = new NodeId(generateLeafNodeId(activityNodeId.getId(), user.getId()),
        timelineNodeId.getId(), NodeType.ACTIVITY_REACTION);
    var activityReactionData = new ActivityReactionData(activityReaction);

    var activityReactionNode = nodeService
        .insertNodeWithDataOnly(activityReactionId, user.getId(), activityReactionData.toJson(),
            requestTs);

    feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

    var eventNode = nodeService
        .getNodeOrThrow(new NodeId(timelineNodeId.getParentId(), NodeType.EVENT));
    var activityData = NodeData.fromJson(activityNode.getData(), ActivityData.class);

    timelineNotificationSender
        .sendActivityReactionNotification(user, eventNode, timelineNodeId, activityNodeId,
            activityData.getTitle(), activityReaction);

    return activityReactionNode;
  }

  @Override
  public Node deleteReaction(User user, NodeId timelineNodeId, NodeId activityNodeId,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    nodeService.getNodeOrThrow(timelineNodeId);
    var activityNode = nodeService.getNodeOrThrow(activityNodeId);

    permissionService
        .verifyAccessRight(user.getId(), activityNode.getPermissionGroupId(), AccessRight.READ);

    var activityReactionId = new NodeId(generateLeafNodeId(activityNodeId.getId(), user.getId()),
        timelineNodeId.getId(), NodeType.ACTIVITY_REACTION);
    var activityReactionNode = nodeService.softDeleteNode(activityReactionId, requestTs);

    feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

    return activityReactionNode;
  }

  @Override
  public Node deleteActivity(User user, NodeId timelineNodeId, NodeId activityNodeId,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    nodeService.getNodeOrThrow(timelineNodeId);
    var activityNode = nodeService.getNodeOrThrow(activityNodeId);
    var activityData = NodeData.fromJson(activityNode.getData(), ActivityData.class);

    var isSuggestion = activityData.getActivityState().equals(ActivityState.SUGGESTION);
    var isCreator = activityNode.getCreatorId().equals(user.getId());
    if (!isSuggestion || !isCreator) {
      permissionService.verifyAccessRight(user.getId(), activityNode.getPermissionGroupId(),
          AccessRight.DELETE);
    }

    var activityNodeUpdated = nodeService.softDeleteNode(activityNodeId, requestTs);

    var childNodes = nodeService
        .queryNode(timelineNodeId.getId(), NodeType.ACTIVITY_REACTION, activityNodeId.getId());
    nodeService
        .batchSoftDeleteNode(
            childNodes.stream().map(Node::getNodeId).collect(Collectors.toUnmodifiableList()),
            requestTs);

    feedService.refreshFeedsForResource(timelineNodeId.toFeedItemKey(), requestTs);

    var eventNode = nodeService
        .getNodeOrThrow(new NodeId(timelineNodeId.getParentId(), NodeType.EVENT));

    timelineNotificationSender
        .sendActivityDeletedNotification(user, eventNode, timelineNodeId, activityNodeId,
            activityData.getTitle());

    return activityNodeUpdated;
  }
}
