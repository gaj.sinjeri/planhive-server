package app.planhive.services.timeline.service;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityReaction;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;
import java.util.Optional;

public interface TimelineService {

  List<Node> addTimelineWidget(String parentId, String permissionGroupId, String creatorId,
      List<String> userIds, Optional<ActivityData> activityData, Instant requestTs);

  Node addActivity(User user, NodeId timelineNodeId, ActivityData activityData, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateActivityState(User user, NodeId timelineNodeId, NodeId activityNodeId,
      ActivityState activityState, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateActivityData(User user, NodeId timelineNodeId, NodeId activityNodeId,
      String title, String description, String note, ActivityTime time, Location location,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateReaction(User user, NodeId timelineNodeId, NodeId activityNodeId,
      ActivityReaction activityReaction, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, InvalidOperationException,
      UnauthorisedException;

  Node deleteReaction(User user, NodeId timelineNodeId, NodeId activityNodeId, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node deleteActivity(User user, NodeId timelineNodeId, NodeId activityNodeId, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;
}
