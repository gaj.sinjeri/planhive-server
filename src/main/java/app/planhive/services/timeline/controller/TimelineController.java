package app.planhive.services.timeline.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.timeline.controller.model.AddActivityRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityDataRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityReactionRequest;
import app.planhive.services.timeline.controller.model.UpdateActivityStateRequest;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.timeline.service.TimelineService;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authenticated")
public class TimelineController {

  private static final Logger logger = LoggerFactory.getLogger(TimelineController.class);

  private final TimelineService timelineService;

  @Autowired
  TimelineController(TimelineService timelineService) {
    this.timelineService = timelineService;
  }

  @PostMapping(path = "events/{eventId}/timelines/{timelineId}/activities",
      consumes = "application/json")
  public ResponseEntity<RestResponse> addActivity(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @PathVariable("timelineId") String timelineId,
      @RequestBody AddActivityRequest request) {
    logger.debug(String
        .format("Received request to add activity to timeline %s, event %s", timelineId, eventId));

    var timelineNodeId = new NodeId(timelineId, eventId, NodeType.TIMELINE);

    var activityTime = new ActivityTime(request.getStartTs(), request.getEndTs());

    Location location = null;
    if (request.getLatitude() != null && request.getLongitude() != null) {
      location = new Location(request.getLocationTitle(), request.getAddress(),
          request.getLatitude(), request.getLongitude());
    }

    var activityData = new ActivityData.Builder()
        .withActivityState(request.getActivityState())
        .withTitle(request.getActivityTitle())
        .withDescription(request.getDescription())
        .withNote(request.getNote())
        .withTime(activityTime)
        .withLocation(location)
        .build();

    try {
      var timelineNode = timelineService
          .addActivity(user, timelineNodeId, activityData, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(timelineNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PutMapping(path = "events/{eventId}/timelines/{timelineId}/activities/{activityId}/state",
      consumes = "application/json")
  public ResponseEntity<RestResponse> updateActivityState(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @PathVariable("timelineId") String timelineId,
      @PathVariable("activityId") String activityId,
      @RequestBody UpdateActivityStateRequest request) {
    logger.debug(String
        .format("Received request to update state for activity %s, user %s", activityId,
            user.getId()));

    var timelineNodeId = new NodeId(timelineId, eventId, NodeType.TIMELINE);
    var activityNodeId = new NodeId(activityId, timelineId, NodeType.ACTIVITY);

    try {
      var activityNode = timelineService
          .updateActivityState(user, timelineNodeId, activityNodeId, request.getActivityState(),
              Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(activityNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "events/{eventId}/timelines/{timelineId}/activities/{activityId}/data",
      consumes = "application/json")
  public ResponseEntity<RestResponse> updateActivityData(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("eventId") String eventId,
      @PathVariable("timelineId") String timelineId, @PathVariable("activityId") String activityId,
      @RequestBody UpdateActivityDataRequest request) {
    logger.debug(String
        .format("Received request to update data for activity %s, user %s", activityId,
            user.getId()));

    var timelineNodeId = new NodeId(timelineId, eventId, NodeType.TIMELINE);
    var activityNodeId = new NodeId(activityId, timelineId, NodeType.ACTIVITY);

    var activityTime = new ActivityTime(request.getStartTs(), request.getEndTs());

    Location activityLocation = null;
    if (request.getLatitude() != null && request.getLongitude() != null) {
      activityLocation = new Location(request.getLocationTitle(), request.getAddress(),
          request.getLatitude(), request.getLongitude());
    }

    try {
      var activityNode = timelineService
          .updateActivityData(user, timelineNodeId, activityNodeId, request.getActivityTitle(),
              request.getDescription(), request.getNote(), activityTime, activityLocation,
              Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(activityNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PutMapping(path = "events/{eventId}/timelines/{timelineId}/activities/{activityId}/reaction",
      consumes = "application/json")
  public ResponseEntity<RestResponse> updateActivityReaction(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("eventId") String eventId,
      @PathVariable("timelineId") String timelineId, @PathVariable("activityId") String activityId,
      @RequestBody UpdateActivityReactionRequest request) {
    logger.debug(String
        .format("Received request to update reaction for activity %s, user %s", activityId,
            user.getId()));

    var timelineNodeId = new NodeId(timelineId, eventId, NodeType.TIMELINE);
    var activityNodeId = new NodeId(activityId, timelineId, NodeType.ACTIVITY);

    Node activityReactionNode;
    try {
      if (!request.isReset() && request.getActivityReaction() != null) {
        activityReactionNode = timelineService
            .updateReaction(user, timelineNodeId, activityNodeId, request.getActivityReaction(),
                Instant.now());
      } else if (request.isReset()) {
        activityReactionNode = timelineService
            .deleteReaction(user, timelineNodeId, activityNodeId, Instant.now());
      } else {
        return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
      }
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (InvalidOperationException | UnauthorisedException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }

    return new ResponseEntity<>(RestResponse.successResponse(activityReactionNode), HttpStatus.OK);
  }

  @DeleteMapping(path = "events/{eventId}/timelines/{timelineId}/activities/{activityId}")
  public ResponseEntity<RestResponse> deleteActivity(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @PathVariable("timelineId") String timelineId,
      @PathVariable("activityId") String activityId) {
    logger
        .debug(String.format("Received request to delete activity %s from user %s",
            activityId, user.getId()));

    var timelineNodeId = new NodeId(timelineId, eventId, NodeType.TIMELINE);
    var activityNodeId = new NodeId(activityId, timelineId, NodeType.ACTIVITY);

    try {
      var activityNode = timelineService
          .deleteActivity(user, timelineNodeId, activityNodeId, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(activityNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }
}
