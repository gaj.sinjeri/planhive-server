package app.planhive.services.timeline.controller.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateActivityDataRequest {

  private final String activityTitle;
  private final String description;
  private final String note;
  private final Long startTs;
  private final Long endTs;
  private final String locationTitle;
  private final String address;
  private final String latitude;
  private final String longitude;

  @JsonCreator
  public UpdateActivityDataRequest(
      @JsonProperty(value = "activityTitle", required = true) String activityTitle,
      @JsonProperty(value = "description") String description,
      @JsonProperty(value = "note") String note,
      @JsonProperty(value = "startTs", required = true) Long startTs,
      @JsonProperty(value = "endTs", required = true) Long endTs,
      @JsonProperty(value = "locationTitle") String locationTitle,
      @JsonProperty(value = "address") String address,
      @JsonProperty(value = "latitude") String latitude,
      @JsonProperty(value = "longitude") String longitude) {
    if (startTs != null || endTs != null) {
      checkNotNull(startTs);
      checkNotNull(endTs);
      checkArgument(endTs == 0L || endTs > startTs, "Invalid start and/or end timestamp");
    }

    this.activityTitle = activityTitle;
    this.description = description;
    this.note = note;
    this.startTs = startTs;
    this.endTs = endTs;
    this.locationTitle = locationTitle;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getActivityTitle() {
    return activityTitle;
  }

  public String getDescription() {
    return description;
  }

  public String getNote() {
    return note;
  }

  public Long getStartTs() {
    return startTs;
  }

  public Long getEndTs() {
    return endTs;
  }

  public String getLocationTitle() {
    return locationTitle;
  }

  public String getAddress() {
    return address;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  @Override
  public String toString() {
    return "UpdateActivityDataRequest{" +
        "activityTitle='" + activityTitle + '\'' +
        ", description='" + description + '\'' +
        ", note='" + note + '\'' +
        ", startTs=" + startTs +
        ", endTs=" + endTs +
        ", locationTitle='" + locationTitle + '\'' +
        ", address='" + address + '\'' +
        ", latitude='" + latitude + '\'' +
        ", longitude='" + longitude + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateActivityDataRequest that = (UpdateActivityDataRequest) o;
    return Objects.equals(activityTitle, that.activityTitle) &&
        Objects.equals(description, that.description) &&
        Objects.equals(note, that.note) &&
        Objects.equals(startTs, that.startTs) &&
        Objects.equals(endTs, that.endTs) &&
        Objects.equals(locationTitle, that.locationTitle) &&
        Objects.equals(address, that.address) &&
        Objects.equals(latitude, that.latitude) &&
        Objects.equals(longitude, that.longitude);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(activityTitle, description, note, startTs, endTs, locationTitle, address, latitude,
            longitude);
  }
}
