package app.planhive.services.timeline.controller.model;

import app.planhive.services.timeline.model.ActivityReaction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateActivityReactionRequest {

  private final boolean reset;
  private final ActivityReaction activityReaction;

  @JsonCreator
  public UpdateActivityReactionRequest(
      @JsonProperty(value = "reset", required = true) boolean reset,
      @JsonProperty(value = "activityReaction") ActivityReaction activityReaction) {

    this.reset = reset;
    this.activityReaction = activityReaction;
  }

  public boolean isReset() {
    return reset;
  }

  public ActivityReaction getActivityReaction() {
    return activityReaction;
  }

  @Override
  public String toString() {
    return "UpdateActivityReactionRequest{" +
        "reset=" + reset +
        ", activityReaction=" + activityReaction +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateActivityReactionRequest that = (UpdateActivityReactionRequest) o;
    return reset == that.reset &&
        activityReaction == that.activityReaction;
  }

  @Override
  public int hashCode() {
    return Objects.hash(reset, activityReaction);
  }
}
