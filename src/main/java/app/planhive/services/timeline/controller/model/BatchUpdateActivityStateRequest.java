package app.planhive.services.timeline.controller.model;

import app.planhive.services.timeline.model.ActivityState;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Objects;

public class BatchUpdateActivityStateRequest {

  private final Map<String, ActivityState> activityStateUpdates;

  @JsonCreator
  public BatchUpdateActivityStateRequest(
      @JsonProperty(value = "activityStateUpdates", required = true)
          Map<String, ActivityState> activityStateUpdates) {

    this.activityStateUpdates = activityStateUpdates;
  }

  public Map<String, ActivityState> getActivityStateUpdates() {
    return activityStateUpdates;
  }

  @Override
  public String toString() {
    return "BatchUpdateActivityStateRequest{" +
        "activityStateUpdates=" + activityStateUpdates +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BatchUpdateActivityStateRequest that = (BatchUpdateActivityStateRequest) o;
    return Objects.equals(activityStateUpdates, that.activityStateUpdates);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activityStateUpdates);
  }
}
