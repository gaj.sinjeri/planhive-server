package app.planhive.services.timeline.controller.model;

import app.planhive.services.timeline.model.ActivityState;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateActivityStateRequest {

  private final ActivityState activityState;

  @JsonCreator
  public UpdateActivityStateRequest(
      @JsonProperty(value = "activityState", required = true) ActivityState activityState) {
    this.activityState = activityState;
  }

  public ActivityState getActivityState() {
    return activityState;
  }

  @Override
  public String toString() {
    return "UpdateActivityStateRequest{" +
        "activityState=" + activityState +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateActivityStateRequest that = (UpdateActivityStateRequest) o;
    return activityState == that.activityState;
  }

  @Override
  public int hashCode() {
    return Objects.hash(activityState);
  }
}
