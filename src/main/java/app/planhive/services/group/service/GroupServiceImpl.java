package app.planhive.services.group.service;

import static com.google.common.base.Preconditions.checkArgument;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.model.Permission;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GroupServiceImpl implements GroupService {

  private static final AccessControl ADMIN_ACC_CTRL = AccessControl
      .withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE);

  private final NodeService nodeService;
  private final FeedService feedService;
  private final PermissionService permissionService;
  private final UserService userService;

  @Autowired
  GroupServiceImpl(NodeService nodeService, FeedService feedService,
      PermissionService permissionService, UserService userService) {
    this.nodeService = nodeService;
    this.feedService = feedService;
    this.permissionService = permissionService;
    this.userService = userService;
  }

  @Override
  public List<User> addUsersToGroup(Node groupNode, List<String> userIds, Instant accessFromTs,
      Instant requestTs) {
    var existingUserIds = permissionService
        .getPermissions(groupNode.getPermissionGroupId(), requestTs)
        .stream()
        .map(Permission::getUserId)
        .collect(Collectors.toUnmodifiableSet());

    var newUserIds = new HashSet<>(userIds);
    newUserIds.removeAll(existingUserIds);

    var retrievedUsers = userService.getUsers(new ArrayList<>(newUserIds));
    var retrievedUserIds = retrievedUsers.stream()
        .map(User::getId)
        .collect(Collectors.toUnmodifiableList());
    if (retrievedUserIds.isEmpty()) {
      return List.of();
    }

    var childNodes = nodeService.getChildNodes(groupNode.getNodeId().getId()).stream()
        .filter(node -> !node.getPrivateNode())
        .collect(Collectors.toUnmodifiableList());
    var permissionGroupIds = childNodes.stream()
        .map(Node::getPermissionGroupId)
        .distinct()
        .collect(Collectors.toList());
    if (!permissionGroupIds.contains(groupNode.getPermissionGroupId())) {
      permissionGroupIds.add(groupNode.getPermissionGroupId());
    }

    for (var permissionGroupId : permissionGroupIds) {
      var usersWithAccessControl = retrievedUserIds.stream().collect(
          Collectors.toMap(Function.identity(), x -> AccessControl.withRights(AccessRight.READ)));

      permissionService
          .addUsersToPermissionGroup(usersWithAccessControl, permissionGroupId, requestTs);
    }

    feedService.createFeeds(groupNode.getNodeId().toFeedItemKey(), retrievedUserIds,
        groupNode.getNodeId().getType().name(), requestTs);
    childNodes.forEach(childNode -> feedService
        .createFeeds(childNode.getNodeId().toFeedItemKey(), retrievedUserIds,
            childNode.getNodeId().getType().name(), accessFromTs, requestTs));

    return retrievedUsers;
  }

  @Override
  public void setMemberAdminStatus(Node groupNode, User user, String memberId, boolean setAdmin,
      Instant requestTs) throws ResourceNotFoundException, UnauthorisedException {
    checkArgument(!user.getId().equals(memberId), "User cannot change their own admin status");

    if (!user.getId().equals(groupNode.getCreatorId())) {
      throw new UnauthorisedException("Only the group creator can change admins");
    }

    var accessControl = setAdmin
        ? AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE)
        : AccessControl.withRights(AccessRight.READ);

    permissionService
        .updateAccessControl(memberId, groupNode.getPermissionGroupId(), accessControl, requestTs);
  }

  @Override
  public Optional<String> removeUserFromGroup(Node groupNode, User requester, User targetUser,
      Instant requestTs) throws ResourceNotFoundException, UnauthorisedException {
    var permissions = permissionService
        .getPermissions(requester, groupNode.getPermissionGroupId(), new Instant(0));

    verifyGroupRemovalPermissions(groupNode, requester, targetUser, permissions);

    permissions = permissions
        .stream()
        .filter(permission -> !permission.getUserId().equals(targetUser.getId()))
        .collect(Collectors.toUnmodifiableList());

    var admins = permissions.stream()
        .filter(permission -> permission.getAccessControl().equals(ADMIN_ACC_CTRL))
        .collect(Collectors.toUnmodifiableList());

    String newAdminUserId = null;
    if (admins.isEmpty()) {
      var permission = permissions.stream()
          .min(Comparator.comparing(Permission::getCreationTs));
      if (permission.isPresent()) {
        permissionService.updateAccessControl(permission.get().getUserId(),
            permission.get().getPermissionGroupId(), ADMIN_ACC_CTRL, requestTs);
        newAdminUserId = permission.get().getUserId();
      }
    }

    var childNodes = nodeService.getChildNodes(groupNode.getNodeId().getId());

    var permissionGroupIds = childNodes.stream()
        .map(Node::getPermissionGroupId)
        .collect(Collectors.toSet());
    permissionGroupIds.add(groupNode.getPermissionGroupId());

    permissionService
        .batchSoftDeletePermissions(targetUser.getId(), new ArrayList<>(permissionGroupIds),
            requestTs);

    var resourceIds = childNodes.stream()
        .map(node -> node.getNodeId().toFeedItemKey())
        .collect(Collectors.toList());
    resourceIds.add(groupNode.getNodeId().toFeedItemKey());

    feedService.batchHardDeleteFeed(targetUser.getId(), resourceIds);

    return Optional.ofNullable(newAdminUserId);
  }

  private void verifyGroupRemovalPermissions(Node groupNode, User requester, User targetUser,
      List<Permission> permissions) throws UnauthorisedException {
    var requesterIsTarget = requester.equals(targetUser);
    var requesterIsCreator = groupNode.getCreatorId().equals(requester.getId());
    var requesterIsAdmin = false;

    var targetIsCreator = groupNode.getCreatorId().equals(targetUser.getId());
    var targetIsAdmin = false;

    if (requesterIsCreator || requesterIsTarget) {
      return;
    }

    for (var permission : permissions) {
      if (permission.getUserId().equals(requester.getId()) && permission.getAccessControl()
          .hasRight(AccessRight.WRITE)) {
        requesterIsAdmin = true;
      } else if (permission.getUserId().equals(targetUser.getId()) && permission.getAccessControl()
          .hasRight(AccessRight.WRITE)) {
        targetIsAdmin = true;
      }
    }

    if (targetIsCreator) {
      throw new UnauthorisedException("Non-creator cannot remove creator from group");
    } else if (!requesterIsAdmin) {
      throw new UnauthorisedException("A non-admin cannot remove members from group");
    } else if (targetIsAdmin) {
      throw new UnauthorisedException("An admin cannot remove another admin from group");
    }
  }

  @Override
  public List<Node> deleteGroup(Node groupNode, User user, Instant requestTs)
      throws UnauthorisedException {
    if (!groupNode.getCreatorId().equals(user.getId())) {
      throw new UnauthorisedException("Only the group creator can delete the group");
    }

    var childNodes = nodeService.getChildNodes(groupNode.getNodeId().getId());
    var allNodeIds = Stream
        .concat(childNodes.stream().map(Node::getNodeId), Stream.of(groupNode.getNodeId()))
        .collect(Collectors.toUnmodifiableList());

    var deletedNodes = nodeService.batchSoftDeleteNode(allNodeIds, requestTs);
    feedService.batchRefreshFeedsForResource(
        allNodeIds.stream().map(NodeId::toFeedItemKey).collect(Collectors.toUnmodifiableList()),
        requestTs);

    var permissionGroupIds = childNodes.stream()
        .map(Node::getPermissionGroupId)
        .collect(Collectors.toSet());
    permissionGroupIds.add(groupNode.getPermissionGroupId());

    permissionService
        .batchSoftDeletePermissions(user.getId(), new ArrayList<>(permissionGroupIds), requestTs);

    childNodes.forEach(childNode -> nodeService
        .softDeleteChildNodesAsync(childNode.getNodeId().getId(), requestTs));

    return deletedNodes;
  }
}
