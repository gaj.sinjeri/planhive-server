package app.planhive.services.group.service;

import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.node.model.Node;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;

import java.util.List;
import java.util.Optional;

public interface GroupService {

  List<User> addUsersToGroup(Node groupNode, List<String> userIds, Instant accessFromTs,
      Instant requestTs);

  void setMemberAdminStatus(Node groupNode, User user, String memberId, boolean setAdmin,
      Instant requestTs) throws ResourceNotFoundException, UnauthorisedException;

  Optional<String> removeUserFromGroup(Node groupNode, User requester, User targetUser,
      Instant requestTs) throws ResourceNotFoundException, UnauthorisedException;

  List<Node> deleteGroup(Node groupNode, User user, Instant requestTs) throws UnauthorisedException;

}
