package app.planhive.services.event.configuration;

import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.services.event.notification.EventNotificationSender;
import app.planhive.services.event.service.EventService;
import app.planhive.services.event.service.EventServiceImpl;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.participation.service.ParticipationService;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.timeline.service.TimelineService;
import app.planhive.services.user.service.UserService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventConfiguration {

  @Value("${event.cover.photo.max.resolution}")
  private int maxEventCoverPhotoResolution;

  @Value("${event.cover.photo.thumbnail.resolution}")
  private int eventCoverPhotoThumbnailResolution;

  @Bean
  public EventService eventService(SecureGenerator secureGenerator,
      PermissionService permissionService, NodeService nodeService, CloudStore cloudStore,
      ParticipationService participationService, TimelineService timelineService,
      ThreadService threadService, PhotoService photoService, FeedService feedService,
      UserService userService, EventNotificationSender eventNotificationSender,
      GroupService groupService) {
    return new EventServiceImpl(secureGenerator, permissionService, nodeService, cloudStore,
        participationService, timelineService, threadService, photoService, feedService,
        userService, eventNotificationSender, groupService, maxEventCoverPhotoResolution,
        eventCoverPhotoThumbnailResolution);
  }
}
