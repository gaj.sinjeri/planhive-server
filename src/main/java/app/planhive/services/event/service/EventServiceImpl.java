package app.planhive.services.event.service;

import app.planhive.common.Image;
import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceAlreadyExistsException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.persistence.CloudStore;
import app.planhive.security.SecureGenerator;
import app.planhive.security.exception.InvalidCiphertextException;
import app.planhive.services.event.model.EventData;
import app.planhive.services.event.model.EventState;
import app.planhive.services.event.notification.EventNotificationSender;
import app.planhive.services.feed.service.FeedService;
import app.planhive.services.group.service.GroupService;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.node.service.NodeService;
import app.planhive.services.participation.service.ParticipationService;
import app.planhive.services.permission.model.AccessControl;
import app.planhive.services.permission.model.AccessRight;
import app.planhive.services.permission.service.PermissionService;
import app.planhive.services.photo.service.PhotoService;
import app.planhive.services.thread.service.ThreadService;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.service.TimelineService;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.UserService;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.imageio.ImageIO;

public class EventServiceImpl implements EventService {

  private static final String COVER_PHOTO_ORIGINAL_URI = "events/%s/cover.jpeg";
  private static final String COVER_PHOTO_THUMBNAIL_URI = "events/%s/t-cover.jpeg";

  private final SecureGenerator secureGenerator;
  private final PermissionService permissionService;
  private final NodeService nodeService;
  private final CloudStore cloudStore;
  private final ParticipationService participationService;
  private final TimelineService timelineService;
  private final ThreadService threadService;
  private final PhotoService photoService;
  private final FeedService feedService;
  private final UserService userService;
  private final EventNotificationSender eventNotificationSender;
  private final GroupService groupService;
  private final int maxCoverPhotoResolution;
  private final int coverPhotoThumbnailResolution;

  private static final Logger logger = LoggerFactory.getLogger(EventServiceImpl.class);

  public EventServiceImpl(SecureGenerator secureGenerator, PermissionService permissionService,
      NodeService nodeService, CloudStore cloudStore, ParticipationService participationService,
      TimelineService timelineService, ThreadService threadService, PhotoService photoService,
      FeedService feedService, UserService userService,
      EventNotificationSender eventNotificationSender, GroupService groupService,
      int maxCoverPhotoResolution, int coverPhotoThumbnailResolution) {
    this.secureGenerator = secureGenerator;
    this.permissionService = permissionService;
    this.nodeService = nodeService;
    this.cloudStore = cloudStore;
    this.participationService = participationService;
    this.timelineService = timelineService;
    this.threadService = threadService;
    this.photoService = photoService;
    this.feedService = feedService;
    this.userService = userService;
    this.eventNotificationSender = eventNotificationSender;
    this.groupService = groupService;
    this.maxCoverPhotoResolution = maxCoverPhotoResolution;
    this.coverPhotoThumbnailResolution = coverPhotoThumbnailResolution;
  }

  @Override
  public List<Node> createEvent(User creator, String name, String description,
      MultipartFile image, Optional<ActivityData> activityData, Instant requestTs)
      throws InvalidImageException {
    var eventId = secureGenerator.generateUUID();

    if (image != null) {
      storeCoverPhoto(image, eventId);
    }

    // TODO(#205): Retrieve users from DB and only use ids of users that exist

    var permissionGroupId = permissionService.createPermissionGroup(Map.of(creator.getId(),
        AccessControl.withRights(AccessRight.READ, AccessRight.WRITE, AccessRight.DELETE)),
        requestTs);

    var eventData = new EventData(EventState.ACTIVE, name, description,
        image != null ? requestTs.getMillis() : 0, null);
    var eventNodeId = new NodeId(eventId, NodeType.EVENT);
    var eventNode = new Node.Builder(eventNodeId)
        .withCreatorId(creator.getId())
        .withPermissionGroupId(permissionGroupId)
        .withCreationTs(requestTs)
        .withLastUpdateTs(requestTs)
        .withData(eventData.toJson())
        .build();

    try {
      nodeService.insertNode(eventNode);
    } catch (ResourceAlreadyExistsException e) {
      throw new InternalError(e);
    }

    feedService.createFeeds(eventNode.getNodeId().toFeedItemKey(), List.of(creator.getId()),
        eventNode.getNodeId().getType().name(), requestTs);

    var participationNodes = participationService
        .addParticipationWidget(eventId, permissionGroupId, creator.getId(), List.of(), requestTs);
    var timelineNodes = timelineService
        .addTimelineWidget(eventId, permissionGroupId, creator.getId(), List.of(), activityData,
            requestTs);
    var threadNode = threadService
        .createThread(eventNodeId, permissionGroupId, false, creator.getId(), List.of(), requestTs);
    var photosNode = photoService
        .addPhotosWidget(eventId, permissionGroupId, creator.getId(), List.of(), requestTs);

    var createdNodes = new ArrayList<Node>();
    createdNodes.add(eventNode);
    createdNodes.addAll(participationNodes);
    createdNodes.addAll(timelineNodes);
    createdNodes.add(threadNode);
    createdNodes.add(photosNode);

    return createdNodes;
  }

  private void storeCoverPhoto(MultipartFile image, String eventId) throws InvalidImageException {
    if (!Objects.equals(image.getContentType(), "image/jpeg")) {
      throw new InvalidImageException("Image must be in jpeg format");
    }

    BufferedImage original;
    BufferedImage thumbnail;

    try (var imageInputStream = image.getInputStream()) {
      original = ImageIO.read(imageInputStream);
      Image.verifyResolution(original, maxCoverPhotoResolution);
      Image.verifyAspectRatio(original, 2);

      thumbnail = Image.resize(original, coverPhotoThumbnailResolution);
    } catch (IOException e) {
      var msg = "Error handling image. This could be either due to not being able to access "
          + "the input stream of the original image or create thumbnail";
      logger.error(msg);
      throw new InternalError(msg, e);
    }

    var originalFileName = String.format(COVER_PHOTO_ORIGINAL_URI, eventId);
    var thumbnailFileName = String.format(COVER_PHOTO_THUMBNAIL_URI, eventId);
    cloudStore.storeImage(originalFileName, original);
    cloudStore.storeImage(thumbnailFileName, thumbnail);
  }

  @Override
  public byte[] getCoverPhoto(User user, String eventId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(new NodeId(eventId, NodeType.EVENT));

    permissionService
        .verifyAccessRight(user.getId(), eventNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String.format(COVER_PHOTO_ORIGINAL_URI, eventId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public byte[] getCoverPhotoThumbnail(User user, String eventId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(new NodeId(eventId, NodeType.EVENT));

    permissionService
        .verifyAccessRight(user.getId(), eventNode.getPermissionGroupId(), AccessRight.READ);

    var filePath = String.format(COVER_PHOTO_THUMBNAIL_URI, eventId);
    return cloudStore.getFile(filePath);
  }

  @Override
  public Node updateEventState(User user, NodeId eventNodeId, EventState eventState,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    permissionService
        .verifyAccessRight(user.getId(), eventNode.getPermissionGroupId(), AccessRight.WRITE);

    var eventDataUpdated = new EventData(eventState, eventData.getName(),
        eventData.getDescription(), eventData.getCoverTs(), eventData.getEncryptedEventId());
    var eventNodeUpdated = nodeService
        .updateNodeData(eventNodeId, eventDataUpdated.toJson(), requestTs);

    feedService.refreshFeedsForResource(eventNodeId.toFeedItemKey(), requestTs);

    if (eventState == EventState.CANCELED) {
      eventNotificationSender
          .sendEventCanceledNotification(eventNodeId, eventData.getName(), user.getId());
    }

    return eventNodeUpdated;
  }

  @Override
  public Node updateEventData(User user, NodeId eventNodeId, String name, String description,
      Long coverTs, boolean invitationLinkEnabled, MultipartFile coverImage, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException {
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    permissionService.verifyAccessRight(user.getId(), eventNode.getPermissionGroupId(),
        AccessRight.WRITE);

    if (coverImage != null) {
      storeCoverPhoto(coverImage, eventNodeId.getId());
    }

    var encryptedEventId =
        !invitationLinkEnabled ? null : (eventData.getEncryptedEventId() != null ? eventData
            .getEncryptedEventId() : secureGenerator.encryptBase64String(eventNodeId.getId()));
    var eventDataUpdated = new EventData(eventData.getEventState(), name, description,
        coverImage != null ? requestTs.getMillis() : coverTs, encryptedEventId);
    var eventNodeUpdated = nodeService
        .updateNodeData(eventNodeId, eventDataUpdated.toJson(), requestTs);

    feedService.refreshFeedsForResource(eventNodeId.toFeedItemKey(), requestTs);

    if (!Objects.equals(eventData.getName(), name)
        || !Objects.equals(eventData.getDescription(), description)
        || !Objects.equals(eventData.getCoverTs(), coverTs)) {
      eventNotificationSender
          .sendEventDataChangedNotification(user.getId(), eventNodeId, eventData, eventDataUpdated);
    }

    return eventNodeUpdated;
  }

  @Override
  public Node acceptEventInvitation(String encryptedEventId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidOperationException {
    String eventId;
    try {
      eventId = secureGenerator.decryptBase64String(encryptedEventId);
    } catch (InvalidCiphertextException e) {
      throw new ResourceNotFoundException("The encrypted event id is invalid");
    }

    var eventNodeId = new NodeId(eventId, NodeType.EVENT);
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);

    if (eventData.getEncryptedEventId() == null) {
      throw new UnauthorisedException(
          String.format("Cannot join event with id %s because it does "
              + "not have a public link enabled", eventId));
    }

    var newMembers = groupService
        .addUsersToGroup(eventNode, List.of(user.getId()), new Instant(0), requestTs);

    if (newMembers.isEmpty()) {
      throw new InvalidOperationException(
          String.format("User %s is already a member of event %s", user.getId(), eventId));
    }

    eventNotificationSender
        .sendNewMemberNotification(eventNodeId, eventData.getName(), newMembers, List.of());

    return eventNode;
  }

  @Override
  public void inviteToEvent(String eventId, User user, List<String> contactIds, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNodeId = new NodeId(eventId, NodeType.EVENT);
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);

    permissionService
        .verifyAccessRight(user.getId(), eventNode.getPermissionGroupId(), AccessRight.WRITE);

    var newMembers = groupService
        .addUsersToGroup(eventNode, contactIds, new Instant(0), requestTs);

    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);
    eventNotificationSender
        .sendInvitedToEventNotification(user.getName(), eventNodeId, eventData.getName(),
            newMembers);
    eventNotificationSender.sendNewMemberNotification(eventNodeId, eventData.getName(), newMembers,
        List.of(user.getId()));
  }

  @Override
  public void setMemberAdminStatus(NodeId eventNodeId, User user, String memberId, boolean setAdmin,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);

    groupService.setMemberAdminStatus(eventNode, user, memberId, setAdmin, requestTs);

    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);
    if (setAdmin) {
      eventNotificationSender
          .sendNewAdminNotification(eventNodeId, eventData.getName(), memberId);
    }
  }

  @Override
  public void removeUserFromEvent(NodeId eventNodeId, User requester, String targetUserId,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);
    var targetUser = userService.getUser(targetUserId);

    var newAdminUserId = groupService
        .removeUserFromGroup(eventNode, requester, targetUser, requestTs);

    newAdminUserId.ifPresent(id -> {
      var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);
      eventNotificationSender
          .sendNewAdminNotification(eventNodeId, eventData.getName(), id);
    });

    // TODO(#172): this should be recorded somewhere. E.g. if a user leaves an event,
    //  we should display it on the client.
  }

  @Override
  public List<Node> deleteEvent(NodeId eventNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException {
    var eventNode = nodeService.getNodeOrThrow(eventNodeId);

    var deletedNodes = groupService.deleteGroup(eventNode, user, requestTs);

    var eventData = NodeData.fromJson(eventNode.getData(), EventData.class);
    eventNotificationSender
        .sendEventDeletedNotification(eventNodeId, eventData.getName(), user.getId());

    return deletedNodes;
  }
}
