package app.planhive.services.event.service;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.services.event.model.EventState;
import app.planhive.services.node.model.Node;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface EventService {

  List<Node> createEvent(User creator, String name, String description, MultipartFile image,
      Optional<ActivityData> activityData, Instant requestTs) throws InvalidImageException;

  byte[] getCoverPhoto(User user, String eventId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  byte[] getCoverPhotoThumbnail(User user, String eventId)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateEventState(User user, NodeId eventNodeId, EventState eventState, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  Node updateEventData(User user, NodeId eventNodeId, String name, String description,
      Long coverTs, boolean invitationLinkEnabled, MultipartFile coverImage, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidImageException;

  Node acceptEventInvitation(String encryptedEventId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException,
      InvalidOperationException;

  void inviteToEvent(String eventId, User user, List<String> contactIds, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void setMemberAdminStatus(NodeId eventNodeId, User user, String memberId, boolean setAdmin,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  void removeUserFromEvent(NodeId eventNodeId, User requester, String targetUserId,
      Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

  List<Node> deleteEvent(NodeId eventNodeId, User user, Instant requestTs)
      throws ResourceNotFoundException, ResourceDeletedException, UnauthorisedException;

}
