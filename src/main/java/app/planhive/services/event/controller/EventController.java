package app.planhive.services.event.controller;

import static app.planhive.filter.RequestFilter.USER_ATTRIBUTE;

import app.planhive.exception.InvalidImageException;
import app.planhive.exception.InvalidOperationException;
import app.planhive.exception.ResourceDeletedException;
import app.planhive.exception.ResourceNotFoundException;
import app.planhive.exception.UnauthorisedException;
import app.planhive.model.RestResponse;
import app.planhive.services.event.controller.model.CreateEventRequest;
import app.planhive.services.event.controller.model.EventAdminStatusRequest;
import app.planhive.services.event.controller.model.InviteToEventRequest;
import app.planhive.services.event.controller.model.UpdateEventDataRequest;
import app.planhive.services.event.controller.model.UpdateEventStateRequest;
import app.planhive.services.event.service.EventService;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.node.model.NodeType;
import app.planhive.services.timeline.model.ActivityData;
import app.planhive.services.timeline.model.ActivityState;
import app.planhive.services.timeline.model.ActivityTime;
import app.planhive.services.timeline.model.Location;
import app.planhive.services.user.model.User;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@RestController
@RequestMapping("authenticated/events")
public class EventController {

  private static final Logger logger = LoggerFactory.getLogger(EventController.class);

  private final EventService eventService;

  @Autowired
  EventController(EventService eventService) {
    this.eventService = eventService;
  }

  @PostMapping(path = "")
  public ResponseEntity<RestResponse> createEvent(@RequestAttribute(USER_ATTRIBUTE) User user,
      @RequestParam("request") CreateEventRequest request,
      @RequestParam(name = "coverPhoto", required = false) MultipartFile coverPhoto) {
    logger
        .debug(String.format("Received request to create event with name %s", request.getName()));

    ActivityData activityData = null;

    if (request.getStartTs() != 0) {
      Location location = null;
      if (request.getLatitude() != null && request.getLongitude() != null) {
        location = new Location(request.getLocationTitle(), request.getAddress(),
            request.getLatitude(), request.getLongitude());
      }

      activityData = new ActivityData.Builder()
          .withTitle("Main Activity")
          .withActivityState(ActivityState.APPROVED)
          .withTime(new ActivityTime(request.getStartTs(), request.getEndTs()))
          .withLocation(location)
          .build();
    }

    try {
      var nodes = eventService
          .createEvent(user, request.getName(), request.getDescription(), coverPhoto,
              Optional.ofNullable(activityData), Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(nodes), HttpStatus.OK);
    } catch (InvalidImageException e) {
      logger.error(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(path = "{eventId}/cover-photo", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getCoverPhoto(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId) {
    logger.debug(
        String.format("Received request to get cover photo for event %s", eventId));

    try {
      var image = eventService.getCoverPhoto(user, eventId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @GetMapping(path = "{eventId}/cover-photo-thumbnail", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<byte[]> getCoverPhotoThumbnail(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId) {
    logger.debug(
        String.format("Received request to get cover photo for event %s", eventId));

    try {
      var image = eventService.getCoverPhotoThumbnail(user, eventId);
      return new ResponseEntity<>(image, HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(new byte[0], HttpStatus.FORBIDDEN);
    }
  }

  @PutMapping(path = "{eventId}/state", consumes = "application/json")
  public ResponseEntity<RestResponse> updateEventState(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @RequestBody UpdateEventStateRequest request) {
    logger.debug(String
        .format("Received request from user %s to update state of event %s", user.getId(),
            eventId));

    try {
      var eventNode = eventService
          .updateEventState(user, new NodeId(eventId, NodeType.EVENT), request.getEventState(),
              Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(eventNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PatchMapping(path = "{eventId}")
  public ResponseEntity<RestResponse> updateEventData(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId,
      @RequestParam("request") UpdateEventDataRequest request,
      @RequestParam(name = "coverImage", required = false) MultipartFile coverPhoto) {
    logger.debug(String
        .format("Received request from user %s to update data for event %s", user.getId(),
            eventId));

    try {
      var eventNode = eventService
          .updateEventData(user, new NodeId(eventId, NodeType.EVENT), request.getName(),
              request.getDescription(), request.getCoverTs(), request.isInvitationLinkEnabled(),
              coverPhoto, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(eventNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    } catch (InvalidImageException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping(path = "encrypted/{encryptedEventId}/members")
  public ResponseEntity<RestResponse> acceptEventInvitation(
      @RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("encryptedEventId") String encryptedEventId) {
    logger.debug(
        String.format("Received request to accept event invitation for user %s", user.getId()));

    try {
      var eventNode = eventService.acceptEventInvitation(encryptedEventId, user, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(eventNode), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    } catch (InvalidOperationException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping(path = "{eventId}/members", consumes = "application/json")
  public ResponseEntity<RestResponse> inviteToEvent(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @RequestBody InviteToEventRequest request) {
    logger.debug(String
        .format("Received request to invite users to event %s", eventId));

    try {
      eventService.inviteToEvent(eventId, user, request.getContactIds(), Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @PutMapping(path = "{eventId}/members/{memberId}/admin-state")
  public ResponseEntity<RestResponse> setMemberAdminStatus(
      @RequestAttribute(USER_ATTRIBUTE) User user, @PathVariable("eventId") String eventId,
      @PathVariable("memberId") String memberId, @RequestBody EventAdminStatusRequest request) {
    logger.debug(String
        .format("Received request to update admin status for member %s of event %s", memberId,
            eventId));

    try {
      eventService.setMemberAdminStatus(new NodeId(eventId, NodeType.EVENT), user, memberId,
          request.isSetAdmin(), Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "{eventId}/users/{userId}")
  public ResponseEntity<RestResponse> removeFromEvent(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId, @PathVariable("userId") String userId) {
    logger.debug(
        String.format("Received request from user %s to remove user %s from event %s", user.getId(),
            userId, eventId));

    try {
      eventService
          .removeUserFromEvent(new NodeId(eventId, NodeType.EVENT), user, userId, Instant.now());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(path = "{eventId}")
  public ResponseEntity<RestResponse> deleteEvent(@RequestAttribute(USER_ATTRIBUTE) User user,
      @PathVariable("eventId") String eventId) {
    logger.debug(
        String.format("Received request from user %s to delete event %s", user.getId(), eventId));

    try {
      var deletedNodes = eventService
          .deleteEvent(new NodeId(eventId, NodeType.EVENT), user, Instant.now());
      return new ResponseEntity<>(RestResponse.successResponse(deletedNodes), HttpStatus.OK);
    } catch (ResourceNotFoundException | ResourceDeletedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.NOT_FOUND);
    } catch (UnauthorisedException e) {
      logger.warn(e.getMessage());
      return new ResponseEntity<>(RestResponse.emptyResponse(), HttpStatus.FORBIDDEN);
    }
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(CreateEventRequest.class, new CreateEventRequest.Editor());
    binder.registerCustomEditor(UpdateEventDataRequest.class, new UpdateEventDataRequest.Editor());
  }
}
