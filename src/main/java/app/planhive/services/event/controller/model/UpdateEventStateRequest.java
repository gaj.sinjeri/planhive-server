package app.planhive.services.event.controller.model;

import app.planhive.services.event.model.EventState;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UpdateEventStateRequest {

  private final EventState eventState;

  @JsonCreator
  public UpdateEventStateRequest(
      @JsonProperty(value = "eventState", required = true) EventState eventState) {
    this.eventState = eventState;
  }

  public EventState getEventState() {
    return eventState;
  }

  @Override
  public String toString() {
    return "UpdateEventStateRequest{" +
        "eventState=" + eventState +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateEventStateRequest that = (UpdateEventStateRequest) o;
    return eventState == that.eventState;
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventState);
  }
}
