package app.planhive.services.event.controller.model;

import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class CreateEventRequest {

  private final String name;
  private final String description;
  private final Long startTs;
  private final Long endTs;
  private final String locationTitle;
  private final String address;
  private final String latitude;
  private final String longitude;

  @JsonCreator
  public CreateEventRequest(@JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "description") String description,
      @JsonProperty(value = "startTs", required = true) Long startTs,
      @JsonProperty(value = "endTs", required = true) Long endTs,
      @JsonProperty(value = "locationTitle") String locationTitle,
      @JsonProperty(value = "address") String address,
      @JsonProperty(value = "latitude") String latitude,
      @JsonProperty(value = "longitude") String longitude) {

    var containsTime = startTs != 0;

    checkArgument(
        containsTime || (endTs == 0 && locationTitle == null && address == null && latitude == null
            && longitude == null));
    checkArgument(!containsTime || (endTs == 0L || endTs > startTs),
        "Invalid start and/or end timestamp");

    this.name = name;
    this.description = description;
    this.startTs = startTs;
    this.endTs = endTs;
    this.locationTitle = locationTitle;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, CreateEventRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }

    }
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Long getStartTs() {
    return startTs;
  }

  public Long getEndTs() {
    return endTs;
  }

  public String getLocationTitle() {
    return locationTitle;
  }

  public String getAddress() {
    return address;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  @Override
  public String toString() {
    return "CreateEventRequest{" +
        "name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", startTs=" + startTs +
        ", endTs=" + endTs +
        ", locationTitle='" + locationTitle + '\'' +
        ", address='" + address + '\'' +
        ", latitude='" + latitude + '\'' +
        ", longitude='" + longitude + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateEventRequest that = (CreateEventRequest) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(description, that.description) &&
        Objects.equals(startTs, that.startTs) &&
        Objects.equals(endTs, that.endTs) &&
        Objects.equals(locationTitle, that.locationTitle) &&
        Objects.equals(address, that.address) &&
        Objects.equals(latitude, that.latitude) &&
        Objects.equals(longitude, that.longitude);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(name, description, startTs, endTs, locationTitle, address, latitude, longitude);
  }
}
