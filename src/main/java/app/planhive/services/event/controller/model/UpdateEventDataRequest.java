package app.planhive.services.event.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

public class UpdateEventDataRequest {

  private final String name;
  private final String description;
  private final Long coverTs;
  private final boolean invitationLinkEnabled;

  @JsonCreator
  public UpdateEventDataRequest(@JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "description", required = true) String description,
      @JsonProperty(value = "coverTs", required = true) Long coverTs,
      @JsonProperty(value = "invitationLinkEnabled", required = true) boolean invitationLinkEnabled) {
    this.name = name;
    this.description = description;
    this.coverTs = coverTs;
    this.invitationLinkEnabled = invitationLinkEnabled;
  }

  public static class Editor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
      try {
        setValue(new ObjectMapper().readValue(text, UpdateEventDataRequest.class));
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Long getCoverTs() {
    return coverTs;
  }

  public boolean isInvitationLinkEnabled() {
    return invitationLinkEnabled;
  }

  @Override
  public String toString() {
    return "UpdateEventDataRequest{" +
        "name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", coverTs=" + coverTs +
        ", invitationLinkEnabled=" + invitationLinkEnabled +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateEventDataRequest that = (UpdateEventDataRequest) o;
    return invitationLinkEnabled == that.invitationLinkEnabled &&
        Objects.equals(name, that.name) &&
        Objects.equals(description, that.description) &&
        Objects.equals(coverTs, that.coverTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, coverTs, invitationLinkEnabled);
  }
}
