package app.planhive.services.event.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class InviteToEventRequest {

  private final List<String> contactIds;

  @JsonCreator
  public InviteToEventRequest(
      @JsonProperty(value = "contactIds", required = true) List<String> contactIds) {
    this.contactIds = contactIds;
  }

  public List<String> getContactIds() {
    return contactIds;
  }

  @Override
  public String toString() {
    return "InviteToEventRequest{" +
        "contactIds=" + contactIds +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InviteToEventRequest that = (InviteToEventRequest) o;
    return Objects.equals(contactIds, that.contactIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contactIds);
  }
}
