package app.planhive.services.event.model;

public enum EventState {
  ACTIVE,
  CANCELED
}
