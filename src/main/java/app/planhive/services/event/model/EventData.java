package app.planhive.services.event.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import app.planhive.services.node.model.NodeData;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;

import java.util.Objects;
import java.util.regex.Pattern;

@JsonInclude(Include.NON_EMPTY)
public class EventData extends NodeData {

  private static final Pattern NAME_PATTERN = Pattern.compile("^.{1,25}$");
  private static final Pattern DESCRIPTION_PATTERN = Pattern.compile("^.{0,512}$");

  private final EventState eventState;
  private final String name;
  private final String description;
  private final Long coverTs; // Last Update Timestamp for Cover Photo
  private final String encryptedEventId;

  @JsonCreator
  public EventData(@JsonProperty(value = "eventState", required = true) EventState eventState,
      @JsonProperty(value = "name", required = true) String name,
      @JsonProperty(value = "description") String description,
      @JsonProperty(value = "coverTs", required = true) Long coverTs,
      @JsonProperty(value = "encryptedEventId") String encryptedEventId) {
    checkNotNull(eventState);
    checkArgument(name != null && NAME_PATTERN.matcher(name).matches());
    checkArgument(description == null || DESCRIPTION_PATTERN.matcher(description).matches());
    checkNotNull(coverTs);

    this.eventState = eventState;
    this.name = name.trim();
    this.description = !Strings.isNullOrEmpty(description) ? description.trim() : null;
    this.coverTs = coverTs;
    this.encryptedEventId = encryptedEventId;
  }

  public EventState getEventState() {
    return eventState;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Long getCoverTs() {
    return coverTs;
  }

  public String getEncryptedEventId() {
    return encryptedEventId;
  }

  @Override
  public String toString() {
    return "EventData{" +
        "eventState=" + eventState +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", coverTs=" + coverTs +
        ", encryptedEventId='" + encryptedEventId + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventData eventData = (EventData) o;
    return eventState == eventData.eventState &&
        Objects.equals(name, eventData.name) &&
        Objects.equals(description, eventData.description) &&
        Objects.equals(coverTs, eventData.coverTs) &&
        Objects.equals(encryptedEventId, eventData.encryptedEventId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventState, name, description, coverTs, encryptedEventId);
  }
}
