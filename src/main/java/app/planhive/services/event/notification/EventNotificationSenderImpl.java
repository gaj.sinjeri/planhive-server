package app.planhive.services.event.notification;

import app.planhive.common.ObjectSerializer;
import app.planhive.notification.NotificationCategory;
import app.planhive.services.event.model.EventData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.Notification;
import app.planhive.services.user.model.NotificationKey;
import app.planhive.services.user.model.User;
import app.planhive.services.user.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EventNotificationSenderImpl extends ObjectSerializer implements
    EventNotificationSender {

  private final NotificationService notificationService;

  @Autowired
  EventNotificationSenderImpl(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Override
  public void sendNewMemberNotification(NodeId eventNodeId, String eventName,
      List<User> newMembers, List<String> excludedUserIds) {
    if (newMembers.isEmpty()) {
      return;
    }

    var newMemberIds = newMembers.stream()
        .map(User::getId)
        .collect(Collectors.toUnmodifiableList());

    String body;
    if (newMembers.size() == 1) {
      body = String.format("%s joined the event.", newMembers.get(0).getName());
    } else {
      body = String
          .format("%s and %s other %s joined the event.", newMembers.get(0).getName(),
              newMembers.size() - 1,
              newMembers.size() <= 2 ? "person" : "people");
    }

    var allExcludedUserIds = new ArrayList<>(newMemberIds);
    allExcludedUserIds.addAll(excludedUserIds);

    var notification = new Notification.Builder(eventName, body, NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService
        .sendNotificationByNodeId(notification, eventNodeId, allExcludedUserIds, true);
  }

  @Override
  public void sendInvitedToEventNotification(String inviterName, NodeId eventNodeId,
      String eventName, List<User> users) {
    var userIds = users.stream()
        .map(User::getId)
        .collect(Collectors.toUnmodifiableList());

    var notification = new Notification.Builder("New event",
        String.format("%s added you to %s.", inviterName, eventName),
        NotificationCategory.INVITES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService.sendNotificationByUserIds(notification, userIds, true);
  }

  @Override
  public void sendEventDataChangedNotification(String userId, NodeId eventNodeId,
      EventData oldEventData, EventData newEventData) {
    if (Objects.equals(oldEventData.getName(), newEventData.getName())
        && Objects.equals(oldEventData.getDescription(), newEventData.getDescription())) {
      return;
    }

    var title = Objects.equals(oldEventData.getName(), newEventData.getName())
        ? oldEventData.getName()
        : String.format("%s (prev. %s)", newEventData.getName(), oldEventData.getName());

    String body;

    if (!Objects.equals(oldEventData.getName(), newEventData.getName())) {
      if (!Objects.equals(oldEventData.getDescription(), newEventData.getDescription())) {
        body = "Event name and description updated.";
      } else {
        body = "Event name updated.";
      }
    } else {
      body = "Event description updated.";
    }

    var notification = new Notification.Builder(title, body, NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService.sendNotificationByNodeId(notification, eventNodeId, List.of(userId), true);
  }

  @Override
  public void sendNewAdminNotification(NodeId eventNodeId, String eventName, String userId) {
    var notification = new Notification.Builder(eventName, "You are now an admin.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService.sendNotificationByUserIds(notification, List.of(userId), true);
  }

  @Override
  public void sendEventCanceledNotification(NodeId eventNodeId, String eventName, String userId) {
    var notification = new Notification.Builder(eventName, "Event has been canceled.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService.sendNotificationByNodeId(notification, eventNodeId, List.of(userId), true);
  }

  @Override
  public void sendEventDeletedNotification(NodeId eventNodeId, String eventName, String userId) {
    var notification = new Notification.Builder(eventName, "Event has been deleted.",
        NotificationCategory.EVENT_UPDATES)
        .withResourceType(NotificationKey.NODE.getValue())
        .withAdditionalData(
            Map.of(NotificationKey.NODE_IDS.getValue(), serializeObject(List.of(eventNodeId))))
        .build();
    notificationService.sendNotificationByNodeId(notification, eventNodeId, List.of(userId), true);
  }
}
