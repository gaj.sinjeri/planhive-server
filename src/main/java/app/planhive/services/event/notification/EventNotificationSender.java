package app.planhive.services.event.notification;

import app.planhive.services.event.model.EventData;
import app.planhive.services.node.model.NodeId;
import app.planhive.services.user.model.User;

import java.util.List;

public interface EventNotificationSender {

  void sendNewMemberNotification(NodeId eventNodeId, String eventName, List<User> newMembers,
      List<String> excludedUserIds);

  void sendInvitedToEventNotification(String inviterName, NodeId eventNodeId, String eventName,
      List<User> users);

  void sendEventDataChangedNotification(String userId, NodeId eventNodeId, EventData oldEventData,
      EventData newEventData);

  void sendNewAdminNotification(NodeId eventNodeId, String eventName, String userId);

  void sendEventCanceledNotification(NodeId eventNodeId, String eventName, String userId);

  void sendEventDeletedNotification(NodeId eventNodeId, String eventName, String userId);

}
