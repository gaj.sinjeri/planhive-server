# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- batchCancelActivity endpoint added
- Canceled state added to activity data
- updateEventData endpoint added
- Send notification when user is made admin
- Only admins can invite people to groups
- updateEventState endpoint added
- Notification Count attribute to User item

### Changed
- addActivity and approveActivity methods now return errors if activity state != approved or suggestion
- makeMemberAdmin EP is now setMemberAdminStatus and takes in a boolean
- Merge update-user-details endpoints into one
- addKnownNumbers now returns list of contacts+users
- Update user now refreshes contacts
- get-contact-updates refactored to return contact + user
- LSI1R has been changed from number to string

### Removed
- batchCancelActivity endpoint removed
